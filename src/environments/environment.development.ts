export const environment = {
  API_URL: 'http://localhost:3179/api/',
  AUTH_URL: 'http://localhost:3179/api/auth/',

  //Login
  login: 'login',
  refreshToken: 'update-token',

  //Gateway
  applications: 'master/application',
  applicationId: 'master/application/',
  userApp: 'master/user-app',
  userAppId: 'master/user-app/',
  image: 'master/image/',
  multiImage: 'master/image/multi',
  pdfMulti: 'master/pdf/multi',
  getImage: 'image/',

  //master
  getAllMachine: 'master/mst_machine',
  getAllArea: 'master/mst_area',
  getMachineById: 'master/mst_machine/',
  insertMachine: 'master/mst_machine',
  updateMachine: 'master/mst_machine/',

  //funloc
  getFunloc: 'master/mst_function_location',
  getFunlocById: 'master/mst_function_location/',
  insertFunloc: 'master/mst_function_location',
  updateFunloc: 'master/mst_function_location/',

  //master plan
  getAllMasterPlan: 'master/tr_activity_group',
  getActivityGroupById: 'master/tr_activity_group/',
  getActivityByGroupId: 'master/tr_activity_by_group/',
  getActivityById: 'master/tr_activity/',
  getActivityMaterialByActivityId: 'master/tr_activity_material_by_activity/',
  getAllFillingMechanical: 'master/tr_activity_group/allfillingmechanical',
  insertActivityGroup: 'master/tr_activity_group',
  updateActivityGroup: 'master/tr_activity_group/',
  insertActivity: 'master/tr_activity',
  updateActivity: 'master/tr_activity/',
  insertMaterial: 'master/tr_activity_material',
  updateMaterial: 'master/tr_activity_material/',
  deleteActivityGroup: 'master/tr_activity_group',
  deleteActivity: 'master/tr_activity',
  deleteMaterial: 'master/tr_activity_material',

  getTabelViewMasterPlan: 'master/tr_activity_group/tabel_view',
  getProgressMasterPlan: 'master/tr_activity_group/progress',
  getFillMechaCorective: 'master/tr_activity_group/fillMechaCorec',
  getFillMechaOverHaul: 'master/tr_activity_group/fillMechaOver',
  getFillElecPreventive: 'master/tr_activity_group/fillElecPrev',
  getFillElecCorective: 'master/tr_activity_group/fillElecCorec',
  getFillElecOverHaul: 'master/tr_activity_group/fillElecOver',
  getPreMechaPreventive: 'master/tr_activity_group/PreMechaPrev',
  getPreMechaCorective: 'master/tr_activity_group/PreMechaCorec',
  getPreMechaOverHaul: 'master/tr_activity_group/PreMechaOver',
  getPreElecPreventive: 'master/tr_activity_group/PreElecPrev',
  getPreElecCorective: 'master/tr_activity_group/PreElecCorec',
  getPreElecOverHaul: 'master/tr_activity_group/PreElecOver',
  getPacMechaPreventive: 'master/tr_activity_group/PacMechaPrev',
  getPacMechaCorective: 'master/tr_activity_group/PacMechaCorec',
  getPacMechaOverHaul: 'master/tr_activity_group/PacMechaOver',
  getPacElecPreventive: 'master/tr_activity_group/PacElecPrev',
  getPacElecCorective: 'master/tr_activity_group/PacElecCorec',
  getPacElecOverHaul: 'master/tr_activity_group/PacElecOver',

  //report master
  getAllReportMaster: 'master/tr_report_master',
  getPlanByMasterId: 'master/tr_report_plan_by_master/:id_report_master',
  getReportByLineId: 'master/tr_report_master/by_line/',
  getReportByAreaLine: 'master/tr_report_master/by_area_line',
  getReportMasterById: 'master/tr_report_master/',
  insertReportMaster: 'master/tr_report_master',
  updateReportMaster: 'master/tr_report_master/',

  //report master plan
  getAllReportMasterPlan: 'master/tr_report_master_plan',
  getReportMasterPlanByReportMasterId: 'master/tr_report_master_plan_by_report_master/',
  getReportMasterPlanById: 'master/tr_report_master_plan/',
  insertReportMasterPlan: 'master/tr_report_master_plan',
  updateReportMasterPlan: 'master/tr_report_master_plan/',
  updatePlanByReportId: 'master/tr_report_master_plan/',

  //weekly care
  getAllWeeklyCare: 'master/tr_weekly_care',
  getAllWeekly: 'master/tr_weekly_care/weekly',
  getFindingsFailureMachine: 'master/tr_weekly_care/findings',
  getWeeklyCareById: 'master/tr_weekly_care/',
  insertWeeklyCare: 'master/tr_weekly_care',
  updateWeeklyCare: 'master/tr_weekly_care/',
  
  //schedule weekly
  getAllSheduleWeekly: 'master/tr_schedule_weekly',
  getTabelViewSheduleWeekly: 'master/tr_schedule_weekly/tabel_view',
  getProgressScheduleWeekly: 'master/tr_schedule_weekly/progress',
  getAllType: 'master/tr_schedule_weekly/Alltype',
  getSheduleWeeklyById: 'master/tr_schedule_weekly/',
  getNotificationWeekly: 'master/tr_schedule_weekly/notification',
  insertSheduleWeekly: 'master/tr_schedule_weekly',
  updateSheduleWeekly: 'master/tr_schedule_weekly/',

  //autonomous maintenance
  getAllAutonomous: 'master/tr_autonomous_maintenance',
  getProgressAutonomous: 'master/tr_autonomous_maintenance/progress',
  getNotifAutonomous: 'master/tr_autonomous_maintenance/notif',
  getAllAutonomousMaintenance: 'master/tr_autonomous_maintenance/all',
  getAutonomousById: 'master/tr_autonomous_maintenance/',
  getAutonomousByPictureId: 'master/tr_autonomous_maintenance/',
  insertAutonomous: 'master/tr_autonomous_maintenance',
  updateAutonomous: 'master/tr_autonomous_maintenance/',

  //pictture
  getAllPicture: 'master/tr_autonomous_picture',
  getPictureById: 'master/tr_autonomous_picture/',
  insertPicture: 'master/tr_autonomous_picture',
  updatePicture: 'master/tr_autonomous_picture/',

  //task improvement
  getAllTaskImprovement: 'master/tr_task_improvement',
  getTaskImprovementById: 'master/tr_task_improvement/',
  getStatus: 'master/tr_task_improvement/status',
  getPriority: 'master/tr_task_improvement/priority',
  insertTaskImprovement: 'master/tr_task_improvement',
  updateTaskImprovement: 'master/tr_task_improvement/',

  //subtask improvement
  getAllSubTaskImprovement: 'master/tr_subtask_improvement',
  getSubTaskImprovementById: 'master/tr_subtask_improvement/',
  getStatusSub: 'master/tr_subtask_improvement/status',
  getAllData: 'master/tr_subtask_improvement/all_data',
  getSubTaskByTaskId: 'master/tr_subtask_improvement/subtask/',
  insertSubTaskImprovement: 'master/tr_subtask_improvement',
  updateSubTaskImprovement: 'master/tr_subtask_improvement/',
  updateSubTaskByTaskId: 'master/tr_subtask_improvement/by_task/',

  //schedule maintenance
  getAllScheduleMaintenance: 'master/tr_schedule_maintenance',
  getScheduleMaintenanceById: 'master/tr_schedule_maintenance/',
  getProgressMaintenance: 'master/tr_schedule_maintenance/progress',
  cobaNotif: 'master/tr_schedule_maintenance/cobaNotif',
  getNewData: 'master/tr_schedule_maintenance/newData',
  getMechaDaily: 'master/tr_schedule_maintenance/mecha/daily',
  getMechaUnplanned: 'master/tr_schedule_maintenance/mecha/unplanned',
  getMechaWeekly: 'master/tr_schedule_maintenance/mecha/weekly',
  getMechaPending: 'master/tr_schedule_maintenance/mecha/pending',
  getElecDaily: 'master/tr_schedule_maintenance/elec/daily',
  getElecUnplanned: 'master/tr_schedule_maintenance/elec/unplanned',
  getElecWeekly: 'master/tr_schedule_maintenance/elec/weekly',
  getElecPending: 'master/tr_schedule_maintenance/elec/pending',
  insertScheduleMaintenance: 'master/tr_schedule_maintenance',
  updateScheduleMaintenance: 'master/tr_schedule_maintenance/',

  //master user
  getAllMasterUser: 'master/mst_user',
  getAllTabelViewUser: 'master/mst_user/tabel_view',
  getNamePic: 'master/view_name_mst_user',
  getAllPic: 'master/pic_all_table',
  getPicMasterPlan: 'master/pic_master_plan',
  getPicAutonomous: 'master/pic_autonomous_maintenance',
  getPicScheduleWeekly: 'master/pic_schedule_weekly_care',
  getPicFindingWekklyCare: 'master/pic_finding_weekly_care',
  getPicScheduleMaintenance: 'master/pic_schedule_maintenance',
  getRole: 'master/mst_user/role',
  getMasterUserById: 'master/mst_user/',
  insertMasterUser: 'master/mst_user',
  updateMasterUser: 'master/mst_user/',

  //quote
  getQuote: 'quote/all',
  getQuoteById: 'quote/',
  insertQuote: 'quote',
  updateQuote: 'quote/',

  //image
  getImageLanding: 'image/landing',
  getImageLandingById: 'image/landing/',
  insertImageLanding: 'image/landing',
  updateImageLanding: 'image/landing/',

  //budgeting
  getAllBudgeting: 'master/tr_budgeting',
  getAllMonth: 'master/mst_month',
  getAllFunctionLocation: 'master/mst_function_location',
  getBudgetingById: 'master/tr_budgeting/',
  getRepairBudgeting: 'master/tr_budgeting/repair',
  getMaintenanceBudgeting: 'master/tr_budgeting/maintenance',
  getOverHaulBudgeting: 'master/tr_budgeting/overhaul',
  insertBudgeting: 'master/tr_budgeting',
  updateBudgeting: 'master/tr_budgeting/',

  //subbudgeting
  getAllSubBudgeting: 'master/tr_sub_budgeting',
  getSubBudgetingByBudgetingId: 'master/tr_sub_budgeting_by_budgeting/',
  getSubBudgetingById: 'master/tr_sub_budgeting/',
  insertSubBudgeting: 'master/tr_sub_budgeting',
  updateSubBudgeting: 'master/tr_sub_budgeting/',

  //budgeting details
  getBudgetingDetailsById: 'master/tr_budgeting_details/',
  getBudgetingDetailsBySubBudgeting: 'master/tr_budgeting_details_by_subbudgeting/',
  getAllBudgetingDetails: 'master/tr_budgeting_details',
  getDetails: 'master/tr_budgeting_details/details',
  insertBudgetingDetails: 'master/tr_budgeting_details',
  updateBudgetingDetails: 'master/tr_budgeting_details/',

  //Approval
  getPdf: 'master/pdf/download',
  getApproval: 'master/tr_approval',
  getApprovalById: 'master/tr_approval/',
  insertApproval: 'master/tr_approval',
  updateApproval: 'master/tr_approval/',

  //Approval signed
  getAllApprovalSigned: 'master/tr_approval_signed',
  getApprovalSigned: 'master/tr_approval_signed/signed',
  getApprovalSignedById: 'master/tr_approval_signed/',
  insertApprovalSigned: 'master/tr_approval_signed',
  updateApprovalSigned: 'master/tr_approval_signed/',

  //task spv
  getTaskSpv: 'master/tr_task_spv',
  getTaskSpvByFilter: 'master/tr_task_spv/by_filter',
  getDeadlineTaskSpv: 'master/tr_task_spv/deadline_task',
  getAchievementsSpv: 'master/tr_task_spv/achievements_spv',
  getTaskSpvById: 'master/tr_task_spv/',
  insertTaskSpv: 'master/tr_task_spv',
  updateTaskSpv: 'master/tr_task_spv/',

  //task spv detail
  getTaskSpvDetail: 'master/tr_task_spv_detail',
  getTaskEmployee: 'master/tr_task_spv_detail/task_employee',
  getTaskSpvDetailById: 'master/tr_task_spv_detail/',
  insertTaskSpvDetail: 'master/tr_task_spv_detail',
  updateTaskSpvDetail: 'master/tr_task_spv_detail/',

  //ppic
  getPpic: 'master/tr_ppic',
  getPpicById: 'master/tr_ppic/',
  insertPpic: 'master/tr_ppic',
  updatePpic: 'master/tr_ppic/',

  //pro
  getPro: 'master/tr_pro',
  getProByFilter: 'master/tr_pro/by_filter',
  getProById: 'master/tr_pro/',
  insertPro: 'master/tr_pro',
  updatePro: 'master/tr_pro/',

  //schedule production
  getScheduleProduction: 'master/tr_schedule_production',
  getScheduleProductionByFilter: 'master/tr_schedule_production/by_filter',
  getScheduleProductionById: 'master/tr_schedule_production/',
  insertScheduleProduction: 'master/tr_schedule_production',
  updateScheduleProduction: 'master/tr_schedule_production/',

  //schedule production detail
  getScheduleProductionDetail: 'master/tr_schedule_production_detail',
  getScheduleProductionDetailByScheduleId: 'master/tr_schedule_production_detail_by_schedule/',
  getScheduleProductionDetailById: 'master/tr_schedule_production_detail/',
  insertScheduleProductionDetail: 'master/tr_schedule_production_detail',
  updateScheduleProductionDetail: 'master/tr_schedule_production_detail/',

  //acces line
  getAccesLine: 'master/tr_acces_line',
  getHistoryAcces: 'master/tr_acces_line/history',
  getAccesLineById: 'master/tr_acces_line/',
  insertAccesLine: 'master/tr_acces_line',
  updateAccesLine: 'master/tr_acces_line/',
};
