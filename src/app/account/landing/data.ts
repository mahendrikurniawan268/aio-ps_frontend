const Teams = [
    {
        image: 'assets/images/pak_dian.png',
        name: 'Dian Alphiansyah',
        position: 'Head Of Production Canpet',
    },
    {
        image: 'assets/images/pak_eka.png',
        name: 'Eka Surahmat',
        position: 'Supervisor',
    },
    {
        image: 'assets/images/pak_cecep.png',
        name: 'Cecep Suhendar',
        position: 'Supervisor',
    },
    {
        image: 'assets/images/pak_alfan.png',
        name: 'Alfan Aditama',
        position: 'Supervisor',
    },
    {
        image: 'assets/images/users/avatar-4.jpg',
        name: 'Adhy Septa W',
        position: 'Supervisor',
    },
    {
        image: 'assets/images/users/avatar-7.jpg',
        name: 'Ari Budiman',
        position: 'Planner',
    },
    {
        image: 'assets/images/mahen.png',
        name: 'Mahendri Kurniawan',
        position: 'Web Developer',
    },
    {
        image: 'assets/images/ari.png',
        name: 'Ari Firmansyah',
        position: 'Web Developer',
    },
];


export { Teams };
