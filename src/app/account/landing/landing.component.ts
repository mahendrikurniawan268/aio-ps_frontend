import { Component, OnInit, HostListener } from '@angular/core';
import { TeamModel } from './team.model';
import { Teams } from './data';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  @ViewChild('homeSection') homeSection!: ElementRef;
  @ViewChild('contactSection') contactSection!: ElementRef;
  @ViewChild('informationSection') informationSection!: ElementRef;
  @ViewChild('tutorialSection') tutorialSection!: ElementRef;
  @ViewChild('teamSection') teamSection!: ElementRef;
  @ViewChild('quoteSection') quoteSection!: ElementRef;
  @ViewChild('reviewSection') reviewSection!: ElementRef;

  currentSection = 'home';
  showNavigationArrows: any;
  showNavigationIndicators: any;
  isNavbarHidden: boolean = false;
  prevScrollPos = window.pageYOffset;
  Teams!: TeamModel[];
  dataQuote:any;
  dataImage:any;
  interval: any;
  currentIndex: number | string = 0;

  constructor(private RestApiService:RestApiService, private modalService:NgbModal, private router:Router) { }

  ngOnInit(): void {
    this._fetchData();
    this.getQuote();
    this.getImageLanding();
    this.interval = setInterval(() => {
      this.getQuote();
    }, 60000);
  }
  private _fetchData() {
    this.Teams = Teams;
  }

  /**
   * Open modal
   * @param quote modal content
   */
  openQuote() {
    this.router.navigate(['/auth/quote/add-data']);
  }

  /**
   * Window scroll method
   */
  // tslint:disable-next-line: typedef
  getQuote(){
    this.RestApiService.getAllQuote().subscribe(
      (res:any) => {
        const randomIndex = Math.floor(Math.random() * res.data.length);
        this.dataQuote = res.data[randomIndex];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getImageLanding(){
    this.RestApiService.getAllImageLanding().subscribe(
      (res:any) => {
        this.dataImage = res.data;
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getImgFileBefore(file: any) {
    return environment.API_URL + '/image/' + file
  }

  scrollToHome() {
    this.homeSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToInformation() {
    this.informationSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToTutorial() {
    this.tutorialSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToReview() {
    this.reviewSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToQuote() {
    this.quoteSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToTeam() {
    this.teamSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  scrollToContact() {
    this.contactSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }


  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  windowScroll() {
    const navbar = document.getElementById('navbar');
    if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
      navbar?.classList.add('is-sticky');
    }
    else {
      navbar?.classList.remove('is-sticky');
    }

    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "block"
    } else {
      (document.getElementById("back-to-top") as HTMLElement).style.display = "none"
    }
  }

  /**
  * Section changed method
  * @param sectionId specify the current sectionID
  */
  onSectionChange(sectionId: string) {
    this.currentSection = sectionId;
  }

  /**
* Toggle navbar
*/
  toggleMenu() {
    document.getElementById('navbarSupportedContent')?.classList.toggle('show');
  }

  topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {
    const navbar = document.getElementById('navbar');
    const currentScrollPos = window.pageYOffset;
    if (this.prevScrollPos > currentScrollPos) {
      this.isNavbarHidden = false;
    } else {
      this.isNavbarHidden = true;
    }

    this.prevScrollPos = currentScrollPos;
  }



}
