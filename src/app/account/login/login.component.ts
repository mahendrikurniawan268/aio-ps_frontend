import { Component} from '@angular/core';
import {  Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UserData } from './user.model';
import { AuthenticationService } from '../../core/services/auth.service';
import Swal from 'sweetalert2';
// import { AuthfakeauthenticationService } from '../../core/services/authfake.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/**
 * Login Component
 */
export class LoginComponent {
  loginData: UserData = {
  nik: '',
  password: '',
};

isNIKEmpty: boolean = false;
isPasswordEmpty: boolean = false;
year: number = new Date().getFullYear();
isLoginFailed: boolean = false;
isRemembered: boolean = false;
loginError: string = '';

constructor(private AuthenticationService : AuthenticationService, private router: Router) {}

login() {
  this.isLoginFailed = false;
  this.loginError = '';
  this.validateForm();
  if (!this.isNIKEmpty && !this.isPasswordEmpty) {
    this.AuthenticationService
      .login(this.loginData.nik, this.loginData.password)
      .subscribe({
        next: (res: any) => {
          if (!res.error) {
            if (!this.isRemembered) {
              this.AuthenticationService.setToken(res.token);
              this.AuthenticationService.setUserData(res.userData);
            } else {
              this.AuthenticationService.setAuthData(
                res.token,
                res.refreshToken,
                res.userData
              );
            }
            this.router.navigate(['menu/top-menu']);
          } else {
            this.isLoginFailed = true;
          }
        },
        error: (err: any) => {
          console.error(err);
          this.isLoginFailed = true;
          Swal.fire({
            icon: 'error',
            title: 'Login Error',
            text: 'An error occurred while logging in. Please try again.',
          });
        },
      });
  }
}

onRememberMeChecked() {
  if (!this.isRemembered) {
    this.isRemembered = true;
  } else {
    this.isRemembered = false;
  }
}

validateForm() {
  this.isNIKEmpty = this.loginData.nik === null;
  this.isPasswordEmpty = this.loginData.password.trim() === '';
}
}