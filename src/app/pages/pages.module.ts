import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NgbToastModule, NgbProgressbarModule, NgbPagination
} from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CountToModule } from 'angular-count-to';
import { NgApexchartsModule } from 'ng-apexcharts';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { SimplebarAngularModule } from 'simplebar-angular';
import {
  NgbCarouselModule, NgbTooltipModule, NgbCollapseModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Swiper Slider
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { DatePipe } from '@angular/common';
import { LightboxModule } from 'ngx-lightbox';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

// Load Icons
import { defineElement } from 'lord-icon-element';
import lottie from 'lottie-web';
import { MatRippleModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Pages Routing
import { PagesRoutingModule } from "./pages-routing.module";
import { DashboardComponent } from './dashboards/dashboard/dashboard.component';
import { ToastsContainer } from './dashboards/dashboard/toasts-container.component';
import { DashboardsModule } from "./dashboards/dashboards.module";
import { HomeGatewayComponent } from './home-gateway/home-gateway.component';
import { AppsModalComponent } from './home-gateway/apps-modal/apps-modal.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { FilMechaPreventiveComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-preventive.component';
import { FilMechaCorectiveComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-corective.component';
import { FilMechaOverHaulComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-haul.component';
import { FilMechaPrevPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-popup/filling-mecha-prev-popup.component';
import { FillingMechaPrevAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-add-data/filling-mecha-prev-add-data.component';
import { FilElecPreventiveComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-preventive.component';
import { FilElecCorectiveComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-corective.component';
import { FilElecOverHaulComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-haul.component';
import { PreMechaPreventiveComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/pre-mecha-preventive.component';
import { PreMechaCorectiveComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/pre-mecha-corective.component';
import { PreMechaOverHaulComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/pre-mecha-over-haul.component';
import { PreElecPreventiveComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/pre-elec-preventive.component';
import { PreElecCorectiveComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/pre-elec-corective.component';
import { PreElecOverHaulComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/pre-elec-over-haul.component';
import { PacMechaPreventiveComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/pac-mecha-preventive.component';
import { PacMechaCorectiveComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/pac-mecha-corective.component';
import { PacMechaOverHaulComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/pac-mecha-over-haul.component';
import { PacElecPreventiveComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/pac-elec-preventive.component';
import { PacElecCorectiveComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/pac-elec-corective.component';
import { PacElecOverHaulComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/pac-elec-over-haul.component';
import { ImprovementComponent } from './maintenance/can/continous-improvement/improvement/improvement.component';
import { ScheduleMechaMonthlyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-monthly/schedule-mecha-monthly.component';
import { ScheduleMechaWeeklyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-weekly/schedule-mecha-weekly.component';
import { ScheduleMechaDailyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-daily.component';
import { ScheduleElecMonthlyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-monthly/schedule-elec-monthly.component';
import { ScheduleElecWeeklyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-weekly/schedule-elec-weekly.component';
import { ScheduleElecDailyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-daily.component';
import { ContinousImprovementAddDataComponent } from './maintenance/can/continous-improvement/improvement/continous-improvement-add-data/continous-improvement-add-data.component';
import { PopupDescriptionMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-material/popup-description-material.component';
import { FillingMechaPrevEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-edit-data/filling-mecha-prev-edit-data.component';
import { FillingMechaCorAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-add-data/filling-mecha-cor-add-data.component';
import { FillingMechaCorEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-edit-data/filling-mecha-cor-edit-data.component';
import { FillingMechaCorPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-popup/filling-mecha-cor-popup.component';
import { FillingMechaCorMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-material/filling-mecha-cor-material.component';
import { FillingMechaOverAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-add-data/filling-mecha-over-add-data.component';
import { FillingMechaOverEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-edit-data/filling-mecha-over-edit-data.component';
import { FillingMechaOverMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-material/filling-mecha-over-material.component';
import { FillingMechaOverPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-popup/filling-mecha-over-popup.component';
import { FillingElecPrevAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-add-data/filling-elec-prev-add-data.component';
import { FillingElecPrevEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-edit-data/filling-elec-prev-edit-data.component';
import { FillingElecPrevMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-material/filling-elec-prev-material.component';
import { FillingElecPrevPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-popup/filling-elec-prev-popup.component';
import { FillingElecCorAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-add-data/filling-elec-cor-add-data.component';
import { FillingElecCorEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-edit-data/filling-elec-cor-edit-data.component';
import { FillingElecCorMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-material/filling-elec-cor-material.component';
import { FillingElecCorPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-popup/filling-elec-cor-popup.component';
import { FillingElecOverAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-add-data/filling-elec-over-add-data.component';
import { FillingElecOverEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-edit-data/filling-elec-over-edit-data.component';
import { FillingElecOverMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-material/filling-elec-over-material.component';
import { FillingElecOverPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-popup/filling-elec-over-popup.component';
import { PackingElecCorAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-add-data/packing-elec-cor-add-data.component';
import { PackingElecCorEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-edit-data/packing-elec-cor-edit-data.component';
import { PackingElecCorMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-material/packing-elec-cor-material.component';
import { PackingElecCorPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-popup/packing-elec-cor-popup.component';
import { PackingElecOverAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-add-data/packing-elec-over-add-data.component';
import { PackingElecOverEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-edit-data/packing-elec-over-edit-data.component';
import { PackingElecOverMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-material/packing-elec-over-material.component';
import { PackingElecOverPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-popup/packing-elec-over-popup.component';
import { PackingElecPrevAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-add-data/packing-elec-prev-add-data.component';
import { PackingElecPrevEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-edit-data/packing-elec-prev-edit-data.component';
import { PackingElecPrevMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-material/packing-elec-prev-material.component';
import { PackingElecPrevPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-popup/packing-elec-prev-popup.component';
import { PackingMechaCorAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-add-data/packing-mecha-cor-add-data.component';
import { PackingMechaCorEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-edit-data/packing-mecha-cor-edit-data.component';
import { PackingMechaCorMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-material/packing-mecha-cor-material.component';
import { PackingMechaCorPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-popup/packing-mecha-cor-popup.component';
import { PackingMechaOverAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-add-data/packing-mecha-over-add-data.component';
import { PackingMechaOverEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-edit-data/packing-mecha-over-edit-data.component';
import { PackingMechaOverMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-material/packing-mecha-over-material.component';
import { PackingMechaOverPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-popup/packing-mecha-over-popup.component';
import { PackingMechaPrevAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-add-data/packing-mecha-prev-add-data.component';
import { PackingMechaPrevEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-edit-data/packing-mecha-prev-edit-data.component';
import { PackingMechaPrevMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-material/packing-mecha-prev-material.component';
import { PackingMechaPrevPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-popup/packing-mecha-prev-popup.component';
import { PreparationElecCorAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-add-data/preparation-elec-cor-add-data.component';
import { PreparationElecCorEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-edit-data/preparation-elec-cor-edit-data.component';
import { PreparationElecCorMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-material/preparation-elec-cor-material.component';
import { PreparationElecCorPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-popup/preparation-elec-cor-popup.component';
import { PreparationElecOverAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-add-data/preparation-elec-over-add-data.component';
import { PreparationElecOverEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-edit-data/preparation-elec-over-edit-data.component';
import { PreparationElecOverMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-material/preparation-elec-over-material.component';
import { PreparationElecOverPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-popup/preparation-elec-over-popup.component';
import { PreparationElecPrevAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-add-data/preparation-elec-prev-add-data.component';
import { PreparationElecPrevEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-edit-data/preparation-elec-prev-edit-data.component';
import { PreparationElecPrevMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-material/preparation-elec-prev-material.component';
import { PreparationElecPrevPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-popup/preparation-elec-prev-popup.component';
import { PreparationMechaCorAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-add-data/preparation-mecha-cor-add-data.component';
import { PreparationMechaCorEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-edit-data/preparation-mecha-cor-edit-data.component';
import { PreparationMechaCorMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-material/preparation-mecha-cor-material.component';
import { PreparationMechaCorPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-popup/preparation-mecha-cor-popup.component';
import { PreparationMechaOverAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-add-data/preparation-mecha-over-add-data.component';
import { PreparationMechaOverEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-edit-data/preparation-mecha-over-edit-data.component';
import { PreparationMechaOverMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-material/preparation-mecha-over-material.component';
import { PreparationMechaOverPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-popup/preparation-mecha-over-popup.component';
import { PreparationMechaPrevAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-add-data/preparation-mecha-prev-add-data.component';
import { PreparationMechaPrevEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-edit-data/preparation-mecha-prev-edit-data.component';
import { PreparationMechaPrevMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-material/preparation-mecha-prev-material.component';
import { PreparationMechaPrevPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-popup/preparation-mecha-prev-popup.component';
import { ScheduleWeeklyCareComponent } from './maintenance/can/continous-improvement/weekly-care/schedule/schedule-weekly-care/schedule-weekly-care.component';
import { SheduleMasterPlanComponent } from './maintenance/can/master-plan/shedule-master-plan/shedule-master-plan.component';
import { ReportMasterPlanComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan.component';
import { ReportMasterPlanAddDataComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan-add-data/report-master-plan-add-data.component';
import { ReportMasterPlanViewComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan-view/report-master-plan-view.component';
import { ContinousImprovementEditDataComponent } from './maintenance/can/continous-improvement/improvement/continous-improvement-edit-data/continous-improvement-edit-data.component';
import { ImprovementTimelineComponent } from './maintenance/can/continous-improvement/improvement/improvement-timeline/improvement-timeline.component';
import { ScheduleMechaAddDataComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-add-data/schedule-mecha-add-data.component';
import { ScheduleMechaPopupComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-popup/schedule-mecha-popup.component';
import { ScheduleMechaUnplannedComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-unplanned/schedule-mecha-unplanned.component';
import { ScheduleMechaWeeklyPopupComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-weekly/schedule-mecha-weekly-popup/schedule-mecha-weekly-popup.component';
import { DashboardCanComponent } from './maintenance/can/dashboard/dashboard-can/dashboard-can.component';
import { ScheduleElecAddDataComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-add-data/schedule-elec-add-data.component';
import { ScheduleElecPopupComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-popup/schedule-elec-popup.component';
import { ScheduleElecUnplannedComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-unplanned/schedule-elec-unplanned.component';
import { ScheduleElecWeeklyPopupComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-weekly/schedule-elec-weekly-popup/schedule-elec-weekly-popup.component';
import { ProfileComponent } from './user/profile/profile/profile.component';
import { AddUserComponent } from './user/add-user/add-user/add-user.component';
import { AddDataUserComponent } from './user/add-user/add-data-user/add-data-user.component';
import { ListReportComponent } from './maintenance/can/master-plan/list-report/list-report.component';
import { ReportScheduleMaintenanceComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance.component';
import { ListReportScheduleMaintenanceComponent } from './maintenance/can/schedule-maintenance/list-report-schedule-maintenance/list-report-schedule-maintenance.component';
import { ReportScheduleMaintenanceAddDataComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance-add-data/report-schedule-maintenance-add-data.component';
import { ReportScheduleMaintenanceViewComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance-view/report-schedule-maintenance-view.component';
import { ListReportEditComponent } from './maintenance/can/master-plan/list-report/list-report-edit/list-report-edit.component';
import { ListReportScheduleMaintenanceEditComponent } from './maintenance/can/schedule-maintenance/list-report-schedule-maintenance/list-report-schedule-maintenance-edit/list-report-schedule-maintenance-edit.component';
import { SharedModule } from '../shared/shared.module';
import { BudgetingMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance.component';
import { BudgetingRepairMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-maintenance.component';
import { BudgetingMaintenanceMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-maintenance.component';
import { BudgetingOverhaulMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-maintenance.component';
import { BudgetingRepairAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-add-data/budgeting-repair-add-data.component';
import { BudgetingRepairEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-edit-data/budgeting-repair-edit-data.component';
import { QuoteComponent } from './master/quote/quote.component';
import { PictureComponent } from './master/picture/picture.component';
import { BudgetingOverhaulAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-add-data/budgeting-overhaul-add-data.component';
import { BudgetingOverhaulEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-edit-data/budgeting-overhaul-edit-data.component';
import { BudgetingMaintenanceAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-add-data/budgeting-maintenance-add-data.component';
import { BudgetingMaintenanceEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-edit-data/budgeting-maintenance-edit-data.component';
import { UploadApprovalComponent } from './admin/upload-approval/upload-approval.component';
import { SignedApprovalComponent } from './admin/signed-approval/signed-approval.component';
import { ReportWeeklyCareComponent } from './maintenance/can/continous-improvement/weekly-care/report-weekly-care/report-weekly-care.component';
import { FunctionLocationComponent } from './master/function-location/function-location.component';
import { MachineComponent } from './master/machine/machine.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ToastsContainer,
    HomeGatewayComponent,
    AppsModalComponent,
    FilMechaPreventiveComponent,
    FilMechaCorectiveComponent,
    FilMechaOverHaulComponent,
    FilMechaPrevPopupComponent,
    FillingMechaPrevAddDataComponent,
    FilElecPreventiveComponent,
    FilElecCorectiveComponent,
    FilElecOverHaulComponent,
    PreMechaPreventiveComponent,
    PreMechaCorectiveComponent,
    PreMechaOverHaulComponent,
    PreElecPreventiveComponent,
    PreElecCorectiveComponent,
    PreElecOverHaulComponent,
    PacMechaPreventiveComponent,
    PacMechaCorectiveComponent,
    PacMechaOverHaulComponent,
    PacElecPreventiveComponent,
    PacElecCorectiveComponent,
    PacElecOverHaulComponent,
    ImprovementComponent,
    ScheduleMechaMonthlyComponent,
    ScheduleMechaWeeklyComponent,
    ScheduleMechaDailyComponent,
    ScheduleElecMonthlyComponent,
    ScheduleElecWeeklyComponent,
    ScheduleElecDailyComponent,
    ContinousImprovementAddDataComponent,
    PopupDescriptionMaterialComponent,
    FillingMechaPrevEditDataComponent,
    FillingMechaCorAddDataComponent,
    FillingMechaCorEditDataComponent,
    FillingMechaCorPopupComponent,
    FillingMechaCorMaterialComponent,
    FillingMechaOverAddDataComponent,
    FillingMechaOverEditDataComponent,
    FillingMechaOverMaterialComponent,
    FillingMechaOverPopupComponent,
    FillingElecPrevAddDataComponent,
    FillingElecPrevEditDataComponent,
    FillingElecPrevMaterialComponent,
    FillingElecPrevPopupComponent,
    FillingElecCorAddDataComponent,
    FillingElecCorEditDataComponent,
    FillingElecCorMaterialComponent,
    FillingElecCorPopupComponent,
    FillingElecOverAddDataComponent,
    FillingElecOverEditDataComponent,
    FillingElecOverMaterialComponent,
    FillingElecOverPopupComponent,
    PackingElecCorAddDataComponent,
    PackingElecCorEditDataComponent,
    PackingElecCorMaterialComponent,
    PackingElecCorPopupComponent,
    PackingElecOverAddDataComponent,
    PackingElecOverEditDataComponent,
    PackingElecOverMaterialComponent,
    PackingElecOverPopupComponent,
    PackingElecPrevAddDataComponent,
    PackingElecPrevEditDataComponent,
    PackingElecPrevMaterialComponent,
    PackingElecPrevPopupComponent,
    PackingMechaCorAddDataComponent,
    PackingMechaCorEditDataComponent,
    PackingMechaCorMaterialComponent,
    PackingMechaCorPopupComponent,
    PackingMechaOverAddDataComponent,
    PackingMechaOverEditDataComponent,
    PackingMechaOverMaterialComponent,
    PackingMechaOverPopupComponent,
    PackingMechaPrevAddDataComponent,
    PackingMechaPrevEditDataComponent,
    PackingMechaPrevMaterialComponent,
    PackingMechaPrevPopupComponent,
    PreparationElecCorAddDataComponent,
    PreparationElecCorEditDataComponent,
    PreparationElecCorMaterialComponent,
    PreparationElecCorPopupComponent,
    PreparationElecOverAddDataComponent,
    PreparationElecOverEditDataComponent,
    PreparationElecOverMaterialComponent,
    PreparationElecOverPopupComponent,
    PreparationElecPrevAddDataComponent,
    PreparationElecPrevEditDataComponent,
    PreparationElecPrevMaterialComponent,
    PreparationElecPrevPopupComponent,
    PreparationMechaCorAddDataComponent,
    PreparationMechaCorEditDataComponent,
    PreparationMechaCorMaterialComponent,
    PreparationMechaCorPopupComponent,
    PreparationMechaOverAddDataComponent,
    PreparationMechaOverEditDataComponent,
    PreparationMechaOverMaterialComponent,
    PreparationMechaOverPopupComponent,
    PreparationMechaPrevAddDataComponent,
    PreparationMechaPrevEditDataComponent,
    PreparationMechaPrevMaterialComponent,
    PreparationMechaPrevPopupComponent,
    ScheduleWeeklyCareComponent,
    SheduleMasterPlanComponent,
    ReportMasterPlanComponent,
    ReportMasterPlanAddDataComponent,
    ReportMasterPlanViewComponent,
    ContinousImprovementEditDataComponent,
    ImprovementTimelineComponent,
    ScheduleMechaAddDataComponent,
    ScheduleMechaPopupComponent,
    ScheduleMechaUnplannedComponent,
    ScheduleMechaWeeklyPopupComponent,
    DashboardCanComponent,
    ScheduleElecAddDataComponent,
    ScheduleElecPopupComponent,
    ScheduleElecUnplannedComponent,
    ScheduleElecWeeklyPopupComponent,
    ProfileComponent,
    AddUserComponent,
    AddDataUserComponent,
    ListReportComponent,
    ReportScheduleMaintenanceComponent,
    ListReportScheduleMaintenanceComponent,
    ReportScheduleMaintenanceAddDataComponent,
    ReportScheduleMaintenanceViewComponent,
    ListReportEditComponent,
    ListReportScheduleMaintenanceEditComponent,
    BudgetingMaintenanceComponent,
    BudgetingRepairMaintenanceComponent,
    BudgetingMaintenanceMaintenanceComponent,
    BudgetingOverhaulMaintenanceComponent,
    BudgetingRepairAddDataComponent,
    BudgetingRepairEditDataComponent,
    QuoteComponent,
    PictureComponent,
    BudgetingOverhaulAddDataComponent,
    BudgetingOverhaulEditDataComponent,
    BudgetingMaintenanceAddDataComponent,
    BudgetingMaintenanceEditDataComponent,
    UploadApprovalComponent,
    SignedApprovalComponent,
    ReportWeeklyCareComponent,
    FunctionLocationComponent,
    MachineComponent
   
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    FormsModule,
    NgbToastModule,
    FlatpickrModule.forRoot(),
    CountToModule,
    NgApexchartsModule,
    LeafletModule,
    NgbDropdownModule,
    SimplebarAngularModule,
    PagesRoutingModule,
    NgxUsefulSwiperModule,
    LightboxModule,
    DashboardsModule,
    NgbCarouselModule,
    NgbTooltipModule,
    NgbCollapseModule,
    NgSelectModule,
    ScrollToModule,
    DropzoneModule,
    MdbModalModule,
    MatRippleModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FullCalendarModule,
    NgbPagination,
    NgbModule,
    SharedModule,
    LightboxModule
  ],
  providers: [
  DatePipe,
],
})
export class PagesModule {
  constructor() {
    defineElement(lottie.loadAnimation);
  }
}
