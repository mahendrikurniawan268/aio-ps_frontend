import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { MasterStock } from '../../kartustock/master/master.model';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';
import { Structure } from './structure.model';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';

interface Transaksi {
  id_stock: number;
  pic: string;
  date: string;
  no_sppb: string;
  type: 'INPUT' | 'OUTPUT';
  id_stock_detail: number;
  kode_barang: string;
  nama_barang: string;
  qty: number;
}

interface JumlahBarang {
  [key: string]: number;
}

interface Jumlah {
  INPUT: { [key: string]: number };
  OUTPUT: { [key: string]: number };
}

interface Jumlah {
  INPUT: JumlahBarang;
  OUTPUT: JumlahBarang;
}

interface Activity {
  appear: string;
  start_date: string;
  end_date: string;
  actual_start: string;
  actual_end: string;
  type:string;
}

@Component({
  selector: 'app-dashboard-prod',
  templateUrl: './dashboard-prod.component.html',
  styleUrls: ['./dashboard-prod.component.scss']
})
export class DashboardProdComponent implements OnInit {
  breadCrumbItems!: Array<{}>;
  masterInfo: Transaksi[] = [];
  masterStock: any[] = [];
  basicChart: any = {};
  UserData: any;
  stackedBarChartfilling: any;
  stackedBarChartpreparasi: any;
  stackedBarChartpacking: any;
  filterDataUser: any;
  totalUser: any;
  Data: any;
  type: any;
  totalMachine: any;
  totalAbnormal: any;
  totalSchedule: any;
  OverviewChart: any;
  multipleRadialbarChart: any;
  TeamMembers: any;
  status7: any = {};
  status8: any = {};
  status9: any = {};
  basicBarChart: any;
  simplePieChart: any = {};
  gmpPieChart: any = {};
  fillingPieChart: any = {};
  preparasiPieChart: any = {};
  injectionPieChart: any = {};
  blowPieChart: any = {};
  packingPieChart: any = {};
  produksiAreaChart: any = {};
  @ViewChild('scrollRef') scrollRef:any;

  //Abnormal
  dataAllAbnormal: any [] = [] ;
  dataAllAbnormalByLine: any [] = [] ;
  dataAbnormalPre : any[] = [] ;
  dataAbnormalFill : any [] = [] ;
  dataAbnormalPack : any [] = [] ;
  dataAbnormalPrePet : any[] = [] ;
  dataAbnormalInjecPet : any[] = [] ;
  dataAbnormalBlowPet: any[] = [] ;
  dataAbnormalFillPet : any [] = [] ;
  dataAbnormalPackPet : any [] = [] ;

  //Activity daily
  dataActivityDaily : any[] = [] ;
  dataActivityDailyByLine : any [] = [];

  //Gmp
  dataGmp : any[] = [] ;
  dataGmpByLine : any [] = [];

  //efficiency
  dataEfficiency : any[] = [] ;
  dataEfficiencyByLine : any
  dataEfficiencyById:any;
  totalFinishGood: number = 0;
  totalProductionMinutes: number = 0;
  lineEfficiency: string ='';

  //activity weekly
  dataWeekly: any [] = [];
  dataWeeklyByLine: any [] = [];
  totalOnProgressWeekly: number = 0;
  totalClosedWeekly: number = 0;
  totalRevisiWeekly: number = 0;

  //Structure
  dataStructureCan: any;
  dataStructurePet: any;
  newStructure : Structure = {
    id_line: null,
    img_structure: ''
  }
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl:any;
  idStructure: any;

  //kartu stoc
  page: number = 1;
  pageSize : number = 10;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  newMasterStock : MasterStock = {
    id_line: null,
    nama_barang: '',
    id_status: null
  }

  //schedule
  dataAllSchedule: any [] = [];
  dataAllScheduleByLine: any;

  filterdata: any;
  totalRecords: any;
  dataMasterStock: any[] = [];
  dataMasterStockByLine: any;
  private statusUpdated: boolean = false;

  totalActivity: any;
  totalActivitytoday: any;
  totalonprogress: number = 0;
  totalpending: number = 0;
  totalclosed: number = 0;
  totalplanning: number = 0;
  totalonprogressDaily: number = 0;
  totalpendingDaily: number = 0;
  totalclosedDaily: number = 0;
  totalplanningDaily: number = 0;
  totalonprogressMonthly: number = 0;
  totalpendingMonthly: number = 0;
  totalclosedMonthly: number = 0;
  totalplanningMonthly: number = 0;
  totalDaily: number = 0;
  totalWeekly: number = 0;
  totalMonthly: number = 0;
  AbnormalJoin: any;
  exporting: boolean = false;
  fillingDataLight: any;
  finalFinish: any;
  averageResult: number = 0;
  groupedData: any[] = [];
  totalDailyProses: any;
  idLine:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;

  constructor(
    private apiService : ApiService, private route : ActivatedRoute, private lightbox : Lightbox, private RestApiService : RestApiService,
    private modalService : NgbModal, private authService : AuthenticationService
    ) {}

  async ngOnInit(): Promise<void> {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    await this.getAllHistory();
    await this.getAllWeeklyReport();
    await this.getAllGmp();
    this.getAllUser();
    this.getAllMachine();
    this.getAllAbnormal();
    this.getAllSchedule();
    this.getJoinReportDaily();
    await this.getAllActivity()
    this.getMaster();
    this.getImageStructureCan();
    this.getImageStructurePet();
    this.getChartAbnormal()
    this._status7('["--vz-success", "--vz-info", "--vz-primary", "--vz-danger"]');
    this._status8('["--vz-info", "--vz-primary", "--vz-warning", "--vz-danger"]');
    this._status9('["--vz-success", "--vz-primary", "--vz-warning", "--vz-danger"]');
    this._multipleRadialbarChart('["--vz-success", "--vz-primary", "--vz-warning", "--vz-danger"]');
    this._fillingPieChart(["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]);
    this._preparasiPieChart(["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]);
    this._injectionPieChart(["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]);
    this._blowPieChart(["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]);
    this._packingPieChart(["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]);
    this._gmpPieChart(["--vz-warning", "--vz-danger", "--vz-info"]);
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  insertStructure(dataStructure: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataStructure.img_structure = res.uploadedFiles[0];
          
          this.newStructure.id_line = this.idLine
          this.apiService.insertStructure(dataStructure).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.getImageStructureCan();
                this.getImageStructurePet();
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.apiService.insertStructure(dataStructure).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.getImageStructureCan();
            this.getImageStructurePet();
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  

  onSubmit(){
    this.insertStructure(this.newStructure);
    Swal.fire({
      icon: 'success',
      title: 'Add Data Successfully',
      text: 'Add has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
  }

  updateStructure(id:any, dataStructure: any) {
    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });

        this.RestApiService.uploadMultipleImage(formData).subscribe({
            next: (res: any) => {
                this.imageUrl = res.uploadedFiles;
                const structureFileIndex = this.selectedFiles.findIndex(file => file.type === 'img-structure');

                if (structureFileIndex !== -1) {
                    dataStructure.img_structure = res.uploadedFiles[structureFileIndex];
                }

                this.apiService.updateStructure(this.idStructure, dataStructure).subscribe(
                    (res: any) => {
                        if (res.status == true) {
                            this.newStructure = dataStructure;
                            this.getImageStructureCan();
                            this.getImageStructurePet();
                        }
                    },
                    (error) => {
                        console.error(error)
                    }
                );
            },
            error: (err) => console.error(err),
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.apiService.updateStructure(this.idStructure, dataStructure).subscribe(
            (res: any) => {
                if (res.status == true) {
                    this.newStructure = dataStructure;
                    this.getImageStructureCan();
                    this.getImageStructurePet();
                }
            },
            (error) => {
                console.error(error)
            }
        );
      }
  }

  onUpdate() {
      console.log(this.newStructure)
      this.updateStructure(this.idStructure, this.newStructure);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    this.modalService.dismissAll();
  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(content: any) {
    this.modalService.open(content, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param editContent modal content
   */
  openEditModal(editContent: any, data:any , id:any) {
    this.newStructure = { ...data};
    this.idStructure = id;
    this.modalService.open(editContent, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateStructure(id, deleteData)
      }
    });
  }

  clearFormData(){
    this.newStructure = {
      id_line: null,
      img_structure: ''
    }
  }

  getImageStructureCan(){
    this.apiService.getAllStructureCan().subscribe(
      (res:any) => {
        this.dataStructureCan = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getImageStructurePet(){
    this.apiService.getAllStructurePet().subscribe(
      (res:any) => {
        this.dataStructurePet = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  getImgFileStructure(file: any) {
    return environment.API_URL + '/image/' + file
  }

  openLightboxStructure(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Structure',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getMaster() {
    this.apiService.getAllMasterStock().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataMasterStock = res.data;

          if (!this.statusUpdated) {
            this.updateStatusBasedOnJumlah();
            this.statusUpdated = true;
          }

          this.dataMasterStockByLine = this.dataMasterStock.filter(item => item.id_line == this.idLine &&  item.id_status === 1)
          this.filterdata = this.dataMasterStockByLine;
          this.totalRecords = this.filterdata.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
        }
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  updateStatusBasedOnJumlah() {
    this.dataMasterStock.forEach(item => {
      let originalStatus = item.id_status;
      
      if (item.jumlah === 0) {
        item.id_status = 2;
      } else if (item.jumlah > 0) {
        item.id_status = 1;
      }
      
      if (originalStatus !== item.id_status) {
        this.apiService.updateMasterStock(item.id_stock, item).subscribe(
          (res: any) => {
            if (res.status) {
              console.log(`Status updated for item id: ${item.id_stock}`);
            } else {
              console.error(`Failed to update status for item id: ${item.id_stock}`, res);
            }
          },
          (error) => {
            console.error(`Failed to update status for item id: ${item.id_stock}`, error);
          }
        );
      }
    });
  }

  updateMasterStock(id:any, dataMasterStock:any){
    this.apiService.updateMasterStock(id, dataMasterStock).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newMasterStock = dataMasterStock;
          this.getMaster();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  async getAllGmp(){
    return new Promise((resolve, reject) => {
      this.apiService.getAllGmp().subscribe(
        (res:any) => {
          this.dataGmp = res.data;
          this.dataGmpByLine = this.dataGmp.filter(item => item.id_line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        }
      );
    })
  }

   _gmpPieChart(gmpData: any[]){
    const countOpen = this.dataGmpByLine.filter(item => !item.follow_up && !item.img_after).length;
    const countClose= this.dataGmpByLine.filter(item => item.follow_up && item.img_after).length;
    this.gmpPieChart = {
      series: [countOpen, countClose],
        chart: {
          height: 279,
          type: "pie",
        },
        labels: ["Open", "Close"],
        legend: {
          position: "bottom",
        },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
      colors: this.getChartColorsArray('["--vz-success", "--vz-primary", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
    };
  }

  getShowingText(): string {
    const startIndex = (this.page - 1) * this.pageSize + 1;
    const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
    return `Showing ${startIndex} - ${endIndex}`;
    }
    
    onPageSizeChange() {
    this.startIndex = 1;
    this.endIndex = this.pageSize;
    }
    
    setPaginationData() {
    this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
    }
    
    getStatusLabelClass(id_status: number): string {
      switch (id_status) {
          case 1:
              return 'success'; // New
          case 2:
              return 'danger'; // On Progress
          case 3:
              return 'info'; // Done
          default:
              return 'secondary';
      }
    }
    
    getStatusLabel(id_status: number): string {
      switch (id_status) {
          case 1:
              return 'Ready Stock';
          case 2:
              return 'Stock Not Available';
          case 3:
              return 'Revisi';
          default:
              return 'Unknown';
      }
    }

getJoinReportDaily() {
  this.apiService.getJoinReportDaily().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.Data = res.data;
        this.totalDailyProses = this.Data.length;
        if (this.Data && this.Data.length > 0) {
        }
      }
    }
  });
}

private _fillingPieChart(abnormalData: any[]): void {
  const countOnProgress = this.dataAbnormalFill.filter(item => item.id_status === 1).length || this.dataAbnormalFillPet.filter(item => item.id_status === 1).length;
  const countDone= this.dataAbnormalFill.filter(item => item.id_status === 2).length || this.dataAbnormalFillPet.filter(item => item.id_status === 2).length;
  const countRevisi = this.dataAbnormalFill.filter(item => item.id_status === 3).length || this.dataAbnormalFillPet.filter(item => item.id_status === 3).length;
  this.fillingPieChart = {
    series: [countOnProgress, countDone, countRevisi],
      chart: {
        height: 279,
        type: "pie",
      },
      labels: ["On Progress", "Done", "Revisi"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
    colors: this.getChartColorsArray('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
  };
}

private _preparasiPieChart(preparasiData: any[]): void {
  const countOnProgress = this.dataAbnormalPre.filter(item => item.id_status === 1).length || this.dataAbnormalPrePet.filter(item => item.id_status === 1).length;
  const countDone= this.dataAbnormalPre.filter(item => item.id_status === 2).length || this.dataAbnormalPrePet.filter(item => item.id_status === 2).length;
  const countRevisi = this.dataAbnormalPre.filter(item => item.id_status === 3).length || this.dataAbnormalPrePet.filter(item => item.id_status === 3).length;
  this.preparasiPieChart = {
    series: [countOnProgress, countDone, countRevisi],
      chart: {
        height: 279,
        type: "pie",
      },
      labels: ["On Progress", "Done", "Revisi"],
      legend: {
        position: "bottom",
      },
    dataLabels: {
      dropShadow: {
        enabled: false,
      },
    },
    colors: this.getChartColorsArray('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
  };
}

private _injectionPieChart(injectionData: any[]): void {
  const countOnProgress = this.dataAbnormalInjecPet.filter(item => item.id_status === 1).length;
  const countDone= this.dataAbnormalInjecPet.filter(item => item.id_status === 2).length;
  const countRevisi = this.dataAbnormalInjecPet.filter(item => item.id_status === 3).length;
  this.injectionPieChart = {
    series: [countOnProgress, countDone, countRevisi],
      chart: {
        height: 279,
        type: "pie",
      },
      labels: ["On Progress", "Done", "Revisi"],
      legend: {
        position: "bottom",
      },
    dataLabels: {
      dropShadow: {
        enabled: false,
      },
    },
    colors: this.getChartColorsArray('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
  };
}

private _blowPieChart(blowData: any[]): void {
  const countOnProgress = this.dataAbnormalBlowPet.filter(item => item.id_status === 1).length;
  const countDone= this.dataAbnormalBlowPet.filter(item => item.id_status === 2).length;
  const countRevisi = this.dataAbnormalBlowPet.filter(item => item.id_status === 3).length;
  this.blowPieChart = {
    series: [countOnProgress, countDone, countRevisi],
      chart: {
        height: 279,
        type: "pie",
      },
      labels: ["On Progress", "Done", "Revisi"],
      legend: {
        position: "bottom",
      },
    dataLabels: {
      dropShadow: {
        enabled: false,
      },
    },
    colors: this.getChartColorsArray('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
  };
}

private _packingPieChart(packingData: any[]): void {
  const countOnProgress = this.dataAbnormalPack.filter(item => item.id_status === 1).length || this.dataAbnormalPackPet.filter(item => item.id_status === 1).length;
  const countDone= this.dataAbnormalPack.filter(item => item.id_status === 2).length || this.dataAbnormalPackPet.filter(item => item.id_status === 2).length;
  const countRevisi = this.dataAbnormalPack.filter(item => item.id_status === 3).length || this.dataAbnormalPackPet.filter(item => item.id_status === 3).length;
  this.packingPieChart = {
    series: [countOnProgress, countDone, countRevisi],
      chart: {
        height: 279,
        type: "pie",
      },
      labels: ["On Progress", "Done", "Revisi"],
      legend: {
        position: "bottom",
      },
    dataLabels: {
      dropShadow: {
        enabled: false,
      },
    },

    colors: this.getChartColorsArray('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]'),
  };
}

getChartAbnormal(): void {
  this.apiService.getAllabnormal().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.dataAllAbnormal = res.data
        this.dataAllAbnormalByLine = this.dataAllAbnormal.filter(item => item.id_line == this.idLine)
        this.dataAbnormalPre = this.dataAllAbnormalByLine.filter(item => item.id_area === 1)
        this.dataAbnormalFill = this.dataAllAbnormalByLine.filter(item => item.id_area === 2)
        this.dataAbnormalPack = this.dataAllAbnormalByLine.filter(item => item.id_area === 3)
        this.dataAbnormalPrePet = this.dataAllAbnormalByLine.filter(item => item.id_area === 4)
        this.dataAbnormalInjecPet = this.dataAllAbnormalByLine.filter(item => item.id_area === 5)
        this.dataAbnormalBlowPet = this.dataAllAbnormalByLine.filter(item => item.id_area === 6)
        this.dataAbnormalFillPet = this.dataAllAbnormalByLine.filter(item => item.id_area === 7)
        this.dataAbnormalPackPet = this.dataAllAbnormalByLine.filter(item => item.id_area === 8)
        this._fillingPieChart(this.dataAllAbnormalByLine);
        this._injectionPieChart(this.dataAllAbnormalByLine);
        this._blowPieChart(this.dataAllAbnormalByLine)
        this._preparasiPieChart(this.dataAllAbnormalByLine);
        this._packingPieChart(this.dataAllAbnormalByLine);
      }
    }
  });
}

    groupDataBySectionKode(data: any[]): Record<string, any[]> {
      const groupedData: Record<string, any[]> = {};
      data.forEach(item => {
        const sectionKode = item.machine_section_kode;
        if (groupedData[sectionKode]) {
          groupedData[sectionKode].push(item);
        } else {
          groupedData[sectionKode] = [item];
        }
      });
      const countPerSection: Record<string, number> = {};
      for (const sectionKode in groupedData) {
        if (groupedData.hasOwnProperty(sectionKode)) {
          countPerSection[sectionKode] = groupedData[sectionKode].length;
        }
      }
      return groupedData;
    }

  getAllUser() {
    this.apiService.getAllUsers().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.UserData = res.data.filter((item: { status: number; }) => item.status === 1);
          this.totalUser = this.UserData.length;
          
        }}
    });
  }

  getAllMachine() {
    this.apiService.getAllMachine().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.Data = res.data.filter((item: { status: string; }) => item.status === 'active');
          this.totalMachine = this.Data.length;
      }}
    });
  }

  getAllAbnormal() {
    this.apiService.getAllabnormal().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataAllAbnormal = res.data;
          this.dataAllAbnormalByLine = this.dataAllAbnormal.filter(item => item.id_line == this.idLine)
          this.totalAbnormal = this.dataAllAbnormalByLine.length;
      }}
    });
  }

  getAllSchedule() {
    this.apiService.getAllschedule().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataAllSchedule = res.data;
          this.dataAllScheduleByLine = this.dataAllSchedule.filter(item => item.id_line == this.idLine)
          this.totalSchedule = this.dataAllScheduleByLine.length;
      }}
    });
  }

  async getAllActivity() {
    return new Promise((resolve, reject) => {
      this.apiService.getAllActivity().subscribe(
        (res: any) => {
          this.dataActivityDaily = res.data;
          this.dataActivityDailyByLine = this.dataActivityDaily.filter(item => item.id_line == this.idLine)
          
          this.totalActivity = this.dataActivityDailyByLine ? this.dataActivityDailyByLine.length : 0;
  
          if (this.dataActivityDailyByLine && this.dataActivityDailyByLine.length > 0) {
  
            this.totalonprogressDaily = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'onprogress').length;
            this.totalplanningDaily = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'planning').length;
            this.totalpendingDaily = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'pending').length;
            this.totalclosedDaily = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'closed').length;
  
            this.totalDaily = this.totalonprogressDaily + this.totalpendingDaily + this.totalclosedDaily + this.totalplanningDaily;
  
            resolve(true);
          } else {
            console.warn('Data is empty or undefined.');
            resolve(false);
          }
        },
        (error: any) => {
          console.error(error);
          reject(error);
        }
      );
    });
  }

  calculateStatus(start_date: Date, end_date: Date, actual_start: string, actual_end: string): string {
    start_date.setHours(start_date.getHours() - start_date.getTimezoneOffset() / 60);
    end_date.setHours(end_date.getHours() - end_date.getTimezoneOffset() / 60);
    const actualStartDate = actual_start !== '0000-00-00' ? new Date(actual_start) : null;
    const actualEndDate = actual_end !== '0000-00-00' ? new Date(actual_end) : null;
    const currentDate = new Date();

    if (actualStartDate && actualEndDate) {
        return 'closed';
    } else if (currentDate.toISOString().split('T')[0] < start_date.toISOString().split('T')[0]) {
        return 'planning';
    } else if (currentDate.toISOString().split('T')[0] === start_date.toISOString().split('T')[0] &&
        currentDate.toISOString().split('T')[0] <= end_date.toISOString().split('T')[0]) {
        return 'onprogress';
    } else if (!actualStartDate && currentDate > end_date) {
        return 'pending';
    } else {
        return 'pending';
    }
}

_status7(colors: any) {
  colors = this.getChartColorsArray(colors);
  const totalonprogress = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'onprogress').length;
  const totalplanning = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'planning').length;
  const totalpending = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'pending').length;
  const totalclosed = this.dataActivityDailyByLine.filter(activity => this.calculateStatus(new Date(activity.start_date), new Date(activity.end_date), activity.actual_start, activity.actual_end) === 'closed').length;
  this.status7 = {
    series: [totalplanning, totalonprogress, totalclosed, totalpending],
    labels: ["Planning", "On Progress", "Closed", "Pending"],
    chart: {
      type: "donut",
      height: 230,      
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              height: 200
            }
          }
        }
      ]
    },
    plotOptions: {
      pie: {
        offsetX: 0,
        offsetY: 0,
        donut: {
          size: "90%",
          labels: {
            show: false,
          }
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: false,
    },
    stroke: {
      lineCap: "round",
      width: 0
    },
    colors: colors
  };
}

  private getChartColorsArray(colors: any): string[] {
    try {
      const colorArray = JSON.parse(colors);
  
      if (Array.isArray(colorArray)) {
        return colorArray.map((value: string) => {
          const newValue = value.replace(/\s/g, '');
  
          if (newValue && newValue.indexOf) {
            if (newValue.indexOf(',') === -1) {
              const color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
              return color ? color.replace(/\s/g, '') : newValue;
            } else {
              const val = newValue.split(',');
              if (val.length === 2) {
                const rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
                return `rgba(${rgbaColor},${val[1]})`;
              } else {
                return newValue;
              }
            }
          } else {
            console.error('Invalid color value:', newValue);
            return '';
          }
        });
      } else {
        console.error('Provided colors is not an array:', colorArray);
        return [];
      }
    } catch (error) {
      console.error('Error parsing JSON:', error);
      return [];
    }
  }

  async getAllWeeklyReport(){
    return new Promise((resolve, reject) => {
      this.apiService.getTabelViewWeekly().subscribe(
        (res:any) => {
          this.dataWeekly = res.data;
          this.dataWeeklyByLine = this.dataWeekly.filter(item => item.id_line == this.idLine)
          this.totalWeekly = this.dataWeeklyByLine.length
          this.totalOnProgressWeekly = this.dataWeeklyByLine.filter(item => item.id_status === 1).length
          this.totalClosedWeekly = this.dataWeeklyByLine.filter(item => item.id_status === 2).length
          this.totalRevisiWeekly = this.dataWeeklyByLine.filter(item => item.id_status === 3).length
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        }
      );
    })
  }

  _status8(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalOnProgress = this.totalOnProgressWeekly
    const totalClosed = this.totalClosedWeekly
    const totalRevisi = this.totalRevisiWeekly
    this.status8 = {
      series: [totalOnProgress, totalClosed, totalRevisi],
      labels: ["On Progress", "Closed", "Revisi"],
      chart: {
          type: "donut",
          height: 230,
          responsive: [
            {
              breakpoint: 480,
              options: {
                chart: {
                  height: 200
                }
              }
            }
          ]
      },
      plotOptions: {
          pie: {
              offsetX: 0,
              offsetY: 0,
              donut: {
                  size: "90%",
                  labels: {
                      show: false,
                  }
              },
          },
      },
      dataLabels: {
          enabled: false,
      },
      legend: {
          show: false,
      },
      stroke: {
          lineCap: "round",
          width: 0
      },
      colors: colors
    };
  }

  _status9(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalonprogress = this.totalonprogressMonthly
    const totalplanning = this.totalplanningMonthly
    const totalpending = this.totalpendingMonthly
    const totalclosed = this.totalclosedMonthly
    this.status9 = {
      series: [totalonprogress, totalplanning, totalpending, totalclosed],
      labels: ["onprogress", "planning", "pending", "closed"],
      chart: {
          type: "donut",
          height: 230,
          responsive: [
            {
              breakpoint: 480,
              options: {
                chart: {
                  height: 200
                }
              }
            }
          ]
      },
      plotOptions: {
          pie: {
              offsetX: 0,
              offsetY: 0,
              donut: {
                  size: "90%",
                  labels: {
                      show: false,
                  }
              },
          },
      },
      dataLabels: {
          enabled: false,
      },
      legend: {
          show: false,
      },
      stroke: {
          lineCap: "round",
          width: 0
      },
      colors: colors
    };
  }

  async getAllHistory() {
    return new Promise((resolve, reject) => {
      this.apiService.getAllHistory().subscribe(
        (res: any) => {
          this.dataEfficiency = res.data.filter((item: any) => item.status === 'active' && item.id_line == this.idLine);
          if (this.dataEfficiency.length > 0) {
            this.dataEfficiencyByLine = this.dataEfficiency[0];
            const newId = this.dataEfficiencyByLine.id_history;
            this.apiService.getByIdHistory(newId).subscribe(
              (res: any) => {
                this.dataEfficiencyById = res.data;
                if (res.data && res.data.length > 0) {
                  this.totalFinishGood = res.data[0].total_finish_good;
            
                  this.dataEfficiencyById.forEach((item: any) => {
                    const productionHours = this.calculateMinutes(item.production_hours);
                    this.totalProductionMinutes += productionHours;
                  });
            
                  this.lineEfficiency = ((this.totalFinishGood / (this.totalProductionMinutes * 530)) * 100).toFixed();
    
                } else {
                  console.log('Data Production Hours is not available.');
                }
                resolve(true);
              },
              (error: any) => {
                console.error("Error fetching history:", error);
                reject(error);
              }
            );
            resolve(true);
          } else {
            console.log("Data efficiency not found for the specified line.");
            resolve(false);
          }
        });
      });
    }

    calculateMinutes(productionHours: string): number {
      if (!productionHours) return 0;
      const hoursAndMinutes = productionHours.split(' ');
      const hours = parseInt(hoursAndMinutes[0], 10);
      const minutes = parseInt(hoursAndMinutes[2], 10);
      return hours * 60 + minutes;
    }

  _multipleRadialbarChart(colors:any) {
    colors = this.getChartColorsArray(colors);
    const lineEfficiency = this.lineEfficiency
    this.multipleRadialbarChart = {
      series: [lineEfficiency],
      chart: {
          height: 350,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              track: {
                strokeWidth: '50%', 
                margin: 20, 
              },
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "20px",
                  },
                  total: {
                      show: true,
                      label: "Line Efficiency",
                      formatter: function () {
                        return lineEfficiency.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Line Eficiency"],
      colors: colors,
    };
  }

  isToday(activityDate: string, today: string): boolean {
    return activityDate.startsWith(today);
  }

  formatDateInWIB(date: Date): string {
    const options = { timeZone: 'Asia/Jakarta' };
    const formatter = new Intl.DateTimeFormat('en-US', options);
    const formattedDate = formatter.format(date);
    return formattedDate;
  }

  hitungJumlahBarang(data: Transaksi[]): Jumlah {
    const jumlah: Jumlah = { INPUT: {}, OUTPUT: {} };
    data.forEach((item) => {
      const jenis = item.type;
      const namaBarang = item.nama_barang;
      if (!jumlah[jenis][namaBarang]) {
        jumlah[jenis][namaBarang] = 0;
      }
      jumlah[jenis][namaBarang] += item.qty;
    });
    return jumlah;
  }

  exportReportToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('dashboard-container') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'dashboard.png');
        this.exporting = false;
      });
    });
  }
  
  exportProductivityToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('productivity') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'productivity.png');
        this.exporting = false;
      });
    });
  }
  
  exportAbnormalitasToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('abnormalitas') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'abnormalitas.png');
        this.exporting = false;
      });
    });
  }

  exportStockToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('kartu-stock') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'stock.png');
        this.exporting = false;
      });
    });
  }
}