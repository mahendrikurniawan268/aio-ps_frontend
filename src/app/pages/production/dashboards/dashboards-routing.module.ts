import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component Pages
import { DashboardProdComponent } from './dashboard-prod/dashboard-prod.component';

const routes: Routes = [

  {
    path: "dashboard-prod",
    component: DashboardProdComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardsRoutingModule { }
