import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScheduleComponent } from './schedule/schedule.component';
import { TabelComponent } from './tabel/tabel.component';

const routes: Routes = [
  {
    path: "calendar",
    component: ScheduleComponent
  },
  {
    path: "tabel",
    component: TabelComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }
