import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-tabel',
  templateUrl: './tabel.component.html',
  styleUrls: ['./tabel.component.scss']
})
export class TabelComponent {
  breadCrumbItems!: Array<{}>;
  dataschedule: any []=[];
  dataScheduleByLine:any;
  isConnected: boolean = true;
  searchTerm: string = ''; 
  filterdata: any[] = [];
  isTableView: boolean = true;
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  selectedMonth: string = ''
  startDate: string = '';
  endDate: string = '';
  idLine:any;

   //contructor
  constructor(
    private apiService: ApiService,
    private datePipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // this.userData = this.AuthenticationService.getUserData();
    // this.isAdmin = this.AuthenticationService.isAdmin();
    this.getAllSchedule();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.breadCrumbItems = [
      { label: "Production" },
      { label: "Schedule" },
      { label: "Calendar", active: true },
    ];


    this.selectedMonth === '--- Month ---';
  }

  months: string[] = [
    '--- Month ---','January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December',
  ];
  
  onSelectedViewCheck(event: any) {
    if (event.target.id == 'btnCalendar') {
      this.isTableView = false
      this.router.navigate(["/production/schedule/calendar"], {
        queryParams: { tableCalendar: false, lineId: this.idLine },
      });
    } else if (event.target.id == 'btnList') {
      this.isTableView = true
      this.router.navigate(["/production/schedule/tabel"], {
        queryParams: { tableList: true, lineId: this.idLine },
      });
    }
  }

  getAllSchedule() {
    this.apiService.getAllschedule().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataschedule = res.data
          this.dataScheduleByLine = this.dataschedule.filter(item => item.id_line == this.idLine)
          this.filterdata = this.dataScheduleByLine;
          this.totalRecords = this.filterdata.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      },
    });
  }
  

formatTime(timeString: string): string {
  const timeParts = timeString.split(':');
  const date = new Date();
  date.setHours(Number(timeParts[0]));
  date.setMinutes(Number(timeParts[1]));
  date.setSeconds(Number(timeParts[2]));
  return this.datePipe.transform(date, 'HH:mm') || '';
}

filter() {
  this.filterdata = this.dataScheduleByLine.filter((item: { start_date: string;}) => {
    const itemDate = new Date(item.start_date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;
    
    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.setPaginationData();
}

onSearch(): void {
  if (!this.searchTerm) {
    this.filterdata = this.dataScheduleByLine;
  } else {
    const searchTermLower = this.searchTerm.toLowerCase();
    this.filterdata = this.dataScheduleByLine.filter((data: {
      event_name: string, 
      start_date: string, 
      start_time: string, 
      location: string, 
      description: string, 
      category: string
    }) => {
      const startDateMatch = this.dateIncludes(data.start_date, searchTermLower);
      const startTimeMatch = data.start_time.includes(searchTermLower);

      return (
        data.category.toLowerCase().includes(searchTermLower) ||
        data.event_name.toLowerCase().includes(searchTermLower) ||
        startDateMatch ||
        startTimeMatch ||
        data.location.toLowerCase().includes(searchTermLower) ||
        data.description.toLowerCase().includes(searchTermLower)
      );
    });
  }
}

dateIncludes(dateString: string, searchTerm: string): boolean {
  const date = new Date(dateString);
  if (!isNaN(date.getTime())) {
    // Jika tanggal valid, konversi ke format string yang mudah dicocokkan
    const formattedDate = `${date.getFullYear()}-${(date.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;

    return formattedDate.includes(searchTerm);
  }
  return false;
}

getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1;
  this.endIndex = this.pageSize;
}
setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}
}
