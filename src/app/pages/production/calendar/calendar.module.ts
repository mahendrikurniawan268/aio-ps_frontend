import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { ProductionModule } from '../production.module';
import { ScheduleComponent } from './schedule/schedule.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { TabelComponent } from './tabel/tabel.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { provideNgxMask } from 'ngx-mask';


@NgModule({
  declarations: [
    ScheduleComponent,
    TabelComponent
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    ProductionModule,
    FlatpickrModule.forRoot(),
    NgbModule,
  ],
  providers: [
    DatePipe,
    provideNgxMask(),
    { provide: LOCALE_ID, useValue: 'id-ID' },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarModule { }
