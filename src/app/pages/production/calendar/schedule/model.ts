
export interface ScheduleData {
  id_schedule: number;
  id_line: number;
  category: string;
  event_name: string;
  location: string;
  description:string;
  start_date: Date;
  end_date: Date;
  event_date: string;
  // tambahkan properti lain sesuai kebutuhan
}