import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { UntypedFormGroup, UntypedFormBuilder, Validators, AbstractControl, } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit{
  breadCrumbItems!: Array<{}>;
  machineadd!: UntypedFormGroup;
  machineedit!: UntypedFormGroup;
  
  MachineData: any;
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';
  filterdata: any[] = [];
  dataEdit: any[]=[];
  existingMachines: any[] = [];
  submitted = false;
  dataId: any;
  data:any;


  constructor(
    private apiService: ApiService,  
    private modalService: NgbModal,
    private formBuilder: UntypedFormBuilder){
  }

  ngOnInit(): void {
  this.getAllMachine()
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Machine', active: true }
    ];

    this.machineadd = this.formBuilder.group({
      id_machine: [null, [Validators.required]],
      kode: [null],
      section: [null],
    });

    // Validation
    this.machineedit = this.formBuilder.group({
      id_machine: [null],
      kode: [null],
      section: [null],
    });

  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(content: any) {
    this.submitted = false;
    this.modalService.open(content, { size: 'md', centered: true });
  }

    /**
  * Open modal
  * @param contentEdit modal content
  */
    editModal(contentEdit: any, data: any) {
      this.dataEdit = data
      this.modalService.open(contentEdit, { size: 'md', centered: true });
      this.machineedit.get('id_machine')?.setValue(data.id_machine)       
      this.machineedit.get('kode')?.setValue(data.kode)       
      this.machineedit.get('section')?.setValue(data.section) 
    }

  getAllMachine() {
    this.apiService.getAllMachine().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.MachineData = res.data.filter((item : any) => item.status === 'active')
          this.filterdata = this.MachineData;
          this.totalRecords = this.filterdata.length;
          this.setPaginationData();
          
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }

  onSubmit() {
    const id_machine_value = this.machineadd.value.id_machine;
    const existingMachineClient = this.existingMachines.find(machine => machine.id_machine === id_machine_value);
    if (existingMachineClient) {
      console.error('ID Machine sudah ada di sisi klien. Mohon pilih ID Machine yang berbeda.');
      return;
    }
    this.apiService.getAllMachine().subscribe({
      next: (res: any) => {
        if (res.status) {
          const existingMachineServer = res.data.find((machine: { id_machine: any; }) => machine.id_machine === id_machine_value);
          if (existingMachineServer) {
            console.error('ID Machine sudah ada di sisi server. Mohon pilih ID Machine yang berbeda.');
          } else {
            this.apiService.insertMachine(this.machineadd.value).subscribe({
              next: (res: any) => {
                if (res.status) {
                  this.modalService.dismissAll();
                  this.ngOnInit();
                }
              },
              error: (err: any) => {
                if (err.error && err.error.code === 'ER_DUP_ENTRY') {
                  console.error('ID Machine sudah ada di sisi server setelah sinkronisasi. Mohon pilih ID Machine yang berbeda.');
                } else {
                  console.error(err);
                }
              }
            });
          }
        } else {
          console.error(`${res.data.message}`);
        }
      },
      error: (err: any) => {
        console.error(err);
      }
    });
  }

  onUpdate() {
    const id = this.machineedit.get('id_machine')?.value;
    const data = {
      kode: this.machineedit.get('kode')?.value,
      section: this.machineedit.get('section')?.value,
    };
    this.apiService.updatemachine(id, data).subscribe({
      next: (res: any) => {
        this.modalService.dismissAll();
          this.ngOnInit()
      },
      error: (err) => {
        console.log('error', err);
      }
    });
  }

  getByIdMachine(id: any) {
    this.apiService.getByIdMachine(id).subscribe({
      next: (res: any) => {
        this.dataId = res;
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

  deleteMachine(id: any) {
    this.apiService.deleteMachine(id).subscribe({
      next: (res: any) => {
        if (res.status && res.data !== null) {
          this.ngOnInit()
        } else {
          console.error('Gagal menghapus data', res.data?.message || 'Tidak ada data yang dihapus');
        }
      },
      error: (err: any) => {
        console.error('Terjadi kesalahan saat menghapus data', err);
      },
    });
  }

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          status: 'nonactive',
        };
        this.apiService.updatemachine(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filterdata = this.MachineData;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filterdata = this.MachineData.filter((data: 
        { id_machine: string, kode: string, section:string }) => {
        return (
          data.id_machine.toString().toLowerCase().includes(searchTermLower) ||
          data.kode.toString().toLowerCase().includes(searchTermLower) ||
          data.section.toString().toLowerCase().includes(searchTermLower)
        );
      });
    }
  }

  getShowingText(): string {
    const startIndex = (this.page - 1) * this.pageSize + 1;
    const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
    return `Showing ${startIndex} - ${endIndex}`;
  }

  onPageSizeChange() {
    this.startIndex = 1;
    this.endIndex = this.pageSize;
  }

  setPaginationData() {
    this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
  }
}


