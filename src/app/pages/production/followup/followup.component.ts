import { Component } from '@angular/core';
import { UntypedFormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-followup',
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.scss']
})
export class FollowupComponent {
  breadCrumbItems!: Array<{}>;
  followupEdit!: UntypedFormGroup;

  //User Roll
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;

  needFollowUp: boolean = false;
  userData: any;
  username: any;
  isConnected: boolean = false;
  data: any;

  constructor(
    private formBuilder: FormBuilder, 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,) {
  }

  ngOnInit(): void {
    this.getByUser();
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/production/report/daily'},
      { label: 'daily', link: '/production/report/daily'},
      { label: 'activity', active: true }
    ];
  
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.apiService.getByIdFollowup(id).subscribe((response: any) => {
          this.data = response.data[0];
          this.followupEdit.patchValue({
            id_followup: this.data.id_followup,
            id_activity: this.data.id_activity,
            pic: this.data.pic,
            start_date: this.data.start_date,
            end_date: this.data.end_date,
            actual_start: this.data.actual_start == "0000-00-00"? null : this.data.actual_start,
            actual_end: this.data.actual_end == "0000-00-00"? null : this.data.actual_end,
            action_plan: this.data.action_plan,
            remark: this.data.remark,
            next_plan: this.data.next_plan,
          });
        }, error => {
          console.error('Error:', error);
        });
      }
    });

    this.username = this.userData.name;
    this.followupEdit = this.formBuilder.group({
      id_followup: [null],
      id_activity: [null],
      start_date: [null],
      end_date: [null],
      actual_start: [null],
      actual_end: [null],
      pic: [null],
      remark: [null],
      action_plan: [null],
      next_plan: [null],
    });
  }

  convertDatabaseDate(date: Date | null | undefined): string {
    if (!date) {
      return '';
    }
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return `${year}-${month}-${day}`;
  }
  
  onSubmit() {
    try {
      const id = this.followupEdit.get('id_followup')?.value;
      const setStart = this.followupEdit.get('start_date')?.value;
      const setEnd = this.followupEdit.get('end_date')?.value;
      const remark = this.followupEdit.get('remark')?.value;
      const action_plan = this.followupEdit.get('action_plan')?.value;
      const next_plan = this.followupEdit.get('next_plan')?.value;
      const actualStart = this.followupEdit.get('actual_start')?.value;
      const actualEnd = this.followupEdit.get('actual_end')?.value;
  
      const start_date = this.parseDate(setStart);
      const end_date = this.parseDate(setEnd);
      const actual_start = this.parseDate(actualStart);
      const actual_end = this.parseDate(actualEnd);
  
      this.validateDate(start_date, 'Tanggal mulai tidak valid');
      this.validateDate(end_date, 'Tanggal akhir tidak valid');
      this.validateDate(actual_start, 'Tanggal actual mulai tidak valid');
      this.validateDate(actual_end, 'Tanggal actual akhir tidak valid');
  
      if (start_date) {
        this.adjustTimeZone(start_date);
      }
  
      if (end_date) {
        this.adjustTimeZone(end_date);
      }
  
      if (actual_start) {
        this.adjustTimeZone(actual_start);
      }
  
      if (actual_end) {
        this.adjustTimeZone(actual_end);
      }
  
      const dataFollowup = {
        id_followup: id,
        action_plan: action_plan,
        next_plan: next_plan,
        remark: remark,
        start_date: this.convertDatabaseDate(start_date),
        end_date: this.convertDatabaseDate(end_date),
        actual_start: actual_start ? this.convertDatabaseDate(actual_start) : null,
        actual_end: actual_end ? this.convertDatabaseDate(actual_end) : null,
      };
      const updateFollowup$ = this.apiService.updateActivityFollow(id, dataFollowup);
      const dataActivity = {
        actual_start: actual_start ? this.convertDatabaseDate(actual_start) : null,
        actual_end: actual_end ? this.convertDatabaseDate(actual_end) : null,
      };
      const updateActivity$ = this.apiService.updateActivity(id, dataActivity);
      forkJoin([updateFollowup$, updateActivity$]).subscribe({
        next: ([followupRes, activityRes]) => {
          this.router.navigate(['production/report/weekly/activity']);
          this.ngOnInit();
        },
        error: (err) => {
          console.log('error', err);
        }
      });
    } catch (error) {
      console.log('Error:', error);
    }
  }

  private parseDate(dateString: string | null | undefined): Date | null {
    if (!dateString || dateString === '0000-00-00') {
      return null;
    }
    return new Date(dateString);
  }
  
  private validateDate(date: Date | null, errorMessage: string): void {
    if (date && isNaN(date.getTime())) {
      throw new Error(errorMessage);
    }
  }
  
  private adjustTimeZone(date: Date | null): void {
    if (date) {
      date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    }
  }

getByUser() {
  this.isConnected = true;
  this.apiService.getAllUsers().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.username = res.data.map((user: any) => user.name);

      } else {
        console.error(`${res.data.message}`);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      }
    },
    error: (err: any) => {
      console.error(err);
      setTimeout(() => {
        this.isConnected = false;
      }, 1000);
    },
  });
}
}

