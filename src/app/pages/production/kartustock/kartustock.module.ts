import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { KartustockRoutingModule } from './kartustock-routing.module';
import { MasterComponent } from './master/master.component';
import { InventoryComponent } from './inventory/inventory.component';
import { InventoryDetailComponent } from './inventory-detail/inventory-detail.component';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { HttpClientModule } from '@angular/common/http';
import { NgbTypeaheadModule, NgbPaginationModule, NgbNavModule, NgbAccordionModule, NgbCollapseModule, NgbDropdownModule, NgbModule, NgbProgressbarModule, NgbRatingModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ArchwizardModule } from 'angular-archwizard';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask';
import { UiSwitchModule } from 'ngx-ui-switch';
import { SimplebarAngularModule } from 'simplebar-angular';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import { CountToModule } from 'angular-count-to';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    MasterComponent,
    InventoryComponent,
    InventoryDetailComponent,
    InventoryAddComponent
  ],
  imports: [
    CommonModule,
    KartustockRoutingModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    NgbTypeaheadModule,
    HttpClientModule,
    FlatpickrModule,
    SimplebarAngularModule,
    NgbPaginationModule,
    DatePipe,
    NgbNavModule,
    NgSelectModule,
    UiSwitchModule,
    ColorPickerModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    NgxSliderModule,
    ArchwizardModule,
    AutocompleteLibModule,

    FormsModule,
    DropzoneModule,
    ScrollSpyModule.forRoot(),
    NgbDropdownModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    ReactiveFormsModule,
    SharedModule,
    DatePipe,
    FormsModule,
    NgbTooltipModule,
    NgbDropdownModule,
    NgbAccordionModule,
    NgbProgressbarModule,
    NgbNavModule,
    NgbPaginationModule,
    NgbCollapseModule,
    FeatherModule.pick(allIcons),
    FlatpickrModule.forRoot(),
    SimplebarAngularModule,
    CountToModule,
    NgApexchartsModule,
    LeafletModule,
    NgSelectModule,
    NgxUsefulSwiperModule,
    NgxMaskDirective, 
    NgbModule,
  ]
})
export class KartustockModule { }
