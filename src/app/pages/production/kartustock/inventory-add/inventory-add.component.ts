import { Component, OnInit } from '@angular/core';
import { Validators, FormArray, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-inventory-add',
  templateUrl: './inventory-add.component.html',
  styleUrls: ['./inventory-add.component.scss']
})
export class InventoryAddComponent implements OnInit {
  lastIdStockDetail: number = 1;
  breadCrumbItems!: Array<{}>;
  stockadd!: FormGroup;
  lastIdInventory: number = 1;

  masterData: any[] = [];
  lastInsertedId!: number ;

  constructor(
    public apiService: ApiService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.selectMaster();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Kartu Stock', active: true },
    ];
    this.stockadd = this.formBuilder.group({
      rows: this.formBuilder.array([this.createRow()]),
    });
  }
  get rowsFormArray() {
    return this.stockadd.get('rows') as FormArray;
  }

selectMaster() {
  this.apiService.getAllMasterStock().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.masterData = res.data;
        this.rowsFormArray.controls.forEach((control: AbstractControl) => {
          const row = control as FormGroup;
          if (row instanceof FormGroup) {
            row.get('nama_barang')?.valueChanges.subscribe((selectedNamaBarang) => {
              const selectedMaster = this.masterData.find(item => item.nama_barang === selectedNamaBarang);
              if (selectedMaster) {
                row.patchValue({
                  kode_barang: selectedMaster.kode_barang,
                });
              }
            });
          }
        });
      } else {
        console.error(`${res.data.message}`);
      }
    },
    error: (err: any) => {
      console.error(err);
    },
  });
}

  createRow(): FormGroup {
    return this.formBuilder.group({
      nama_barang: ['', Validators.required],
      kode_barang: [''],
      qty: [],
    });
  }

addRow() {
  const newRow = this.formBuilder.group({
    kode_barang: [''],
    nama_barang: ['', Validators.required],
    qty: [''],
  });

  this.rowsFormArray.push(newRow);
  newRow.get('nama_barang')?.valueChanges.subscribe((selectedNamaBarang) => {
    const selectedMaster = this.masterData.find(item => item.nama_barang === selectedNamaBarang);
    if (selectedMaster) {
      newRow.patchValue({
        kode_barang: selectedMaster.kode_barang,
      });
    }
  });
  this.lastIdStockDetail++;
}

onSubmit() {
    const parameterValue = this.route.snapshot.paramMap.get('id');
    if (!this.stockadd.valid) {
      console.error('Form tidak valid. Silakan periksa data yang dimasukkan.');
      return;
    }
  
    const formData = this.stockadd.value.rows.map((row: any) => ({
      ...row,
      id_stock: parameterValue,
    }));
    this.apiService.insertDataStock(formData).subscribe({
      next: (res: any) => {
        if (res.status) {
          this.modalService.dismissAll();
          this.ngOnInit();
          this.router.navigate(['production/kartustock/inventory']);
        } else {
          console.error('Gagal menyimpan data. Server mengembalikan kesalahan:', res.message);
        }
      },
      error: (err: any) => {
        if (err.status === 400) {
          console.error('Permintaan tidak valid. Periksa data yang Anda kirimkan.');
        } else if (err.status === 500) {
          console.error('Kesalahan server internal. Silakan coba lagi nanti.');
        } else {
          console.error('Kesalahan tidak terduga terjadi:', err);
        }
      },
    });
  }
}
