import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasterComponent } from './master/master.component';
import { InventoryComponent } from './inventory/inventory.component';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import { InventoryDetailComponent } from './inventory-detail/inventory-detail.component';

const routes: Routes = [
  {
    path: 'master',
    component: MasterComponent
  },
  {
    path: 'inventory/:id',
    component: InventoryComponent
  },
  {
    path: 'inventory/add/:id',
    component: InventoryAddComponent
  },
  {
    path: 'inventory/detail/:id',
    component: InventoryDetailComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KartustockRoutingModule { }
