import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-inventory-detail',
  templateUrl: './inventory-detail.component.html',
  styleUrls: ['./inventory-detail.component.scss']
})
export class InventoryDetailComponent implements OnInit{
  id: number | undefined;
  DataById: any;
  dataID: any;
  data: any;
  idParam: any;
  idLine: any;
  dataTransaction: any [] = [];
  filterData: any;

  breadCrumbItems!: Array<{}>;
  
  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private apiService: ApiService, 
    private http: HttpClient) {}

  ngOnInit(){
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getTransaction(this.idParam)
  }

  getTransaction(id:any) {
    this.apiService.getTabelViewTransaction(id).subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataTransaction = res.data;
          console.log(this.dataTransaction);
          
          this.filterData = this.dataTransaction;
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }

  goBack(id:any){
    this.router.navigate([`production/kartustock/inventory/${id}`], { queryParams: {lineId: this.idLine}})
  }

}
