import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { MasterStock } from './master.model';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {
  filterdata: any;
  totalRecords: any;
  dataMasterStock: any[] = [];
  dataMasterStockByLine: any;

  page: number = 1;
  pageSize : number = 10;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';
  dataEdit: any[]=[];
  activityId: any;
  idLine:any;
  idTask: any;

  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;

  newMasterStock : MasterStock = {
    id_line: null,
    nama_barang: '',
    id_status: null
  }

  private statusUpdated: boolean = false;

  constructor(
    public apiservice: ApiService,
    private apiService: ApiService,
    private modalService: NgbModal,
    private route : ActivatedRoute,
    private router : Router, 
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getMaster();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(content: any) {
    this.modalService.open(content, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param editContent modal content
   */
  openEditModal(editContent: any, task:any , id:any) {
    this.newMasterStock = { ...task };
    this.idTask = id;
    this.modalService.open(editContent, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  getMaster() {
    this.apiService.getAllMasterStock().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataMasterStock = res.data;

          if (!this.statusUpdated) {
            this.updateStatusBasedOnJumlah();
            this.statusUpdated = true;
          }

          this.dataMasterStockByLine = this.dataMasterStock.filter(item => item.id_line == this.idLine)
          this.filterdata = this.dataMasterStockByLine;
          this.totalRecords = this.filterdata.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
        }
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  updateStatusBasedOnJumlah() {
    this.dataMasterStock.forEach(item => {
      let originalStatus = item.id_status;
      
      if (item.jumlah === 0) {
        item.id_status = 2;
      } else if (item.jumlah > 0) {
        item.id_status = 1;
      }
    
      if (originalStatus !== item.id_status) {
        this.apiService.updateMasterStock(item.id_stock, item).subscribe(
          (res: any) => {
            if (res.status) {
              console.log(`Status updated for item id: ${item.id_stock}`);
            } else {
              console.error(`Failed to update status for item id: ${item.id_stock}`, res);
            }
          },
          (error) => {
            console.error(`Failed to update status for item id: ${item.id_stock}`, error);
          }
        );
      }
    });
  }

  insertMasterStock(dataMasterStock: any) {
    this.apiService.insertMasterStock(dataMasterStock).subscribe(
      (res: any) => {
        this.dataMasterStock = res.data;
        this.getMaster();
      })
  }

  onSubmit(){
    this.newMasterStock.id_status = 2;
    this.newMasterStock.id_line = this.idLine
    this.insertMasterStock(this.newMasterStock);
    Swal.fire({
      icon: 'success',
      title: 'Add Data Successfully',
      text: 'Add has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
  }

updateMasterStock(id:any, dataMasterStock:any){
  this.apiService.updateMasterStock(id, dataMasterStock).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.newMasterStock = dataMasterStock;
        this.getMaster();
      }
    },
    (error) => {
      console.error(error)
    }
  )
}
  
  onUpdate(){
  this.updateMasterStock(this.idTask, this.newMasterStock);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
  }

onDelete(id:any){
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to delete this data?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oke',
    cancelButtonText: 'Cancel',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const deleteData = { status: 0 };
      this.updateMasterStock(id, deleteData)
    }
  });
}

onSearch(): void {
    if (!this.searchTerm) {
      this.filterdata = this.dataMasterStockByLine;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filterdata = this.dataMasterStockByLine.filter((data: { nama_barang: string }) => {
        return (
          data.nama_barang.toString().toLowerCase().includes(searchTermLower)
        );
      });
    }
}

getShowingText(): string {
const startIndex = (this.page - 1) * this.pageSize + 1;
const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
this.startIndex = 1;
this.endIndex = this.pageSize;
}

setPaginationData() {
this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

getStatusLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'success'; // New
      case 2:
          return 'danger'; // On Progress
      case 3:
          return 'info'; // Done
      default:
          return 'secondary';
  }
}

getStatusLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'Ready Stock';
      case 2:
          return 'Stock Not Available';
      case 3:
          return 'Revisi';
      default:
          return 'Unknown';
  }
}

  clearFormData(){
    this.newMasterStock = {
      id_line: null,
      nama_barang: '',
      id_status: null,
    }
  }

  onTransaction(id:any){
    this.router.navigate([`production/kartustock/inventory/${id}`], { queryParams: {lineId: this.idLine}})
  }

}
