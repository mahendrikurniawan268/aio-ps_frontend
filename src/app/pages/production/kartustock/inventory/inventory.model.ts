export interface Transaction {
    id_pic : null | number;
    date : string;
    barang_masuk : null | number;
    barang_keluar : null | number; 
    type : string; 
    id_stock : null | number;
}