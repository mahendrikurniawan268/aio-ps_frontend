import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { Transaction } from './inventory.model';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { MasterStock } from '../master/master.model';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit{
  breadCrumbItems!: Array<{}>;

  inventaroryAdd!: UntypedFormGroup;
  totalRecords: any;
  dataTransaction: any [] = [];
  dataMasterById: any;
  masterAdd: any;
  idStock!: number;
  isConnected: boolean | undefined;
  idTransaction: any;

  //pages
  page: number = 1;
  pageSize : number = 10;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';
  filterdata: any[] = [];
  idParam: any;
  idLine: any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  
  isItemIn: boolean = true;

  newTransaction : Transaction = {
    id_pic : null,
    date : '',
    barang_masuk : null,
    barang_keluar : null,
    type: '',
    id_stock : null
  }

  newMasterStock : MasterStock = {
    id_line: null,
    nama_barang: '',
    id_status: null
  }

  constructor(
    public apiservice: ApiService,
    private router: Router,
    private apiService: ApiService,
    private modalService: NgbModal,
    private formBuilder: UntypedFormBuilder,
    private route : ActivatedRoute,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getTransaction(this.idParam)
    this.getMasterStockById(this.idParam)
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

    /**
   * Open modal
   * @param content modal content
   */
    openModal(content: any) {
      this.modalService.open(content, { size: 'md', centered: true });
    }

    /**
   * Open modal
   * @param editContent modal content
   */
  openEditModal(editContent: any, task:any , id:any) {
    this.newTransaction = { ...task };
    this.idTransaction = id;
    this.newTransaction.date = this.convertDate(this.newTransaction.date)
    this.modalService.open(editContent, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  getMasterStockById(id:any){
    this.apiService.getMasterStockById(id).subscribe(
      (res:any) => {
        this.dataMasterById = res.data[0];
      })
  }

  getTransaction(id:any) {
    this.apiService.getTabelViewTransaction(id).subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataTransaction = res.data
          this.filterdata = this.dataTransaction;
          this.totalRecords = this.filterdata.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }

  insertTransaction(dataMasterStock: any) {
    this.apiService.insertTransactionStock(dataMasterStock).subscribe(
      (res: any) => {
        this.dataTransaction = res.data;
        this.getTransaction(this.idParam);
      })
  }

  updateMasterStock(dataMasterStock:any, id:any){
    this.apiService.updateMasterStock(id, dataMasterStock).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newMasterStock = dataMasterStock;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  updateTransactionStock(id:any, dataTransactionStock:any){
    this.apiService.updateTransactionStock(id, dataTransactionStock).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTransaction = dataTransactionStock;
          this.getTransaction(this.idParam);
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate() {
    const updatedTransaction = {
      id_pic: this.newTransaction.id_pic,
      date: this.newTransaction.date,
      barang_masuk: this.newTransaction.barang_masuk!,
      barang_keluar: this.newTransaction.barang_keluar!,
      type: this.newTransaction.type,
      id_stock: this.newTransaction.id_stock
    };
  
    const previousTransaction = { 
      barang_masuk: this.dataTransaction.find(item => item.id_stock_detail === this.idTransaction)?.barang_masuk || 0,
      barang_keluar: this.dataTransaction.find(item => item.id_stock_detail === this.idTransaction)?.barang_keluar || 0
    };
  
    const differenceBarangMasuk = updatedTransaction.barang_masuk - previousTransaction.barang_masuk;
    const differenceBarangKeluar = updatedTransaction.barang_keluar - previousTransaction.barang_keluar;
    const updatedMasterStock = { jumlah: this.dataMasterById.jumlah + differenceBarangMasuk - differenceBarangKeluar};
    
    if(updatedMasterStock.jumlah < 0){
      Swal.fire({
        icon: 'error',
        title: 'Stock Not Enough',
        text: 'The stock is not enough.',
      });
    } else {
      this.updateMasterStock(updatedMasterStock, this.newTransaction.id_stock);
      this.updateTransactionStock(this.idTransaction, updatedTransaction);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    }
    this.modalService.dismissAll();
  }  
  
  onDelete(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const transactionToDelete = this.dataTransaction.find((transaction: any) => transaction.id_stock_detail === id);
        
        if (transactionToDelete.barang_masuk) {
          const updatedJumlah = this.dataMasterById.jumlah - transactionToDelete.barang_masuk;
          const updatedMasterStock: MasterStock = { ...this.dataMasterById, jumlah: updatedJumlah };
          this.updateMasterStock(updatedMasterStock, this.idParam);
        } else if (transactionToDelete.barang_keluar) {
          const updatedJumlah = this.dataMasterById.jumlah + transactionToDelete.barang_keluar;
          const updatedMasterStock: MasterStock = { ...this.dataMasterById, jumlah: updatedJumlah };
          this.updateMasterStock(updatedMasterStock, this.idParam);
        }
        
        const deleteData = { status: 0 };
        this.updateTransactionStock(id, deleteData);
      }
    });
  }    

  onSubmit() {
    this.newTransaction.id_pic = this.userData.id;
    this.newTransaction.id_stock = this.idParam;
  
    const countItemIn = this.newTransaction.barang_masuk || 0;
    const countItemOut = this.newTransaction.barang_keluar || 0;
    const updatedJumlah = this.dataMasterById.jumlah + countItemIn - countItemOut;

    if (updatedJumlah < 0) {
      Swal.fire({
        icon: 'error',
        title: 'Stock Not Enough',
        text: 'The stock is not enough.',
      });
    } else {
      const updatedMasterStock: MasterStock = { ...this.dataMasterById, jumlah: updatedJumlah };
      this.updateMasterStock(updatedMasterStock, this.idParam);
      this.insertTransaction(this.newTransaction);
      Swal.fire({
        icon: 'success',
        title: 'Add Data Successfully',
        text: 'Add has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    }
    this.modalService.dismissAll();
  }
  

  goBack(){
    this.router.navigate(['production/kartustock/master'], { queryParams: {lineId: this.idLine}});
  }

  viewReport(id:any){
    this.router.navigate([`production/kartustock/inventory/detail/${this.idParam}`], { queryParams: {lineId: this.idLine}});
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filterdata = this.dataTransaction;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filterdata = this.dataTransaction.filter((data: { name: string }) => {
        return (
          data.name.toString().toLowerCase().includes(searchTermLower)
        );
      });
    }
}

getShowingText(): string {
const startIndex = (this.page - 1) * this.pageSize + 1;
const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
this.startIndex = 1;
this.endIndex = this.pageSize;
}

setPaginationData() {
this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

onTypeChange(type: string) {
  if (type === 'in') {
    this.isItemIn = true;
  } else if (type === 'out') {
    this.isItemIn = false; 
  }
}

filter() {
  this.filterdata = this.dataTransaction.filter((item: { date: string; }) => {
    const itemDate = new Date(item.date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;

    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.setPaginationData();
}

clearFormData(){
  this.newTransaction = {
    id_pic: null,
    date: '',
    barang_masuk: null,
    barang_keluar: null,
    type: '',
    id_stock: null,
  }
}

convertDate(date: string): string {
  if (date) {
    const inputDate = new Date(date);
    inputDate.setDate(inputDate.getDate() + 1);
    const formattedDate = inputDate.toISOString().split('T')[0];
    return formattedDate;
  }
  return '';
}

}

