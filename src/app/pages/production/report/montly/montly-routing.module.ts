import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PendingJobComponent } from './pending-job/pending-job.component';
import { PlanningComponent } from './planning/planning.component';
import { OnprogressComponent } from './onprogress/onprogress.component';
import { GpmComponent } from './gpm/gpm.component';
import { GmpAddComponent } from './gpm/gmp-add/gmp-add.component';

const routes: Routes = [
  {
    path: 'planning',
    component: PlanningComponent,
  },

  {
    path: 'onprogress',
    component: OnprogressComponent,
  },

  {
    path: 'pendingjob',
    component: PendingJobComponent,
  },
  {
    path: 'resume', loadChildren: () => import('./resume/resume.module').then(m => m.ResumeModule)
  },
  {
    path: 'gmp',
    component: GpmComponent,
  },
  {
    path: 'gmp/add',
    component: GmpAddComponent,
  },
  {
    path: 'gmp/edit/:id',
    component: GmpAddComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MontlyRoutingModule { }
