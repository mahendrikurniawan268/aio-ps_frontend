import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MontlyRoutingModule } from './montly-routing.module';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask';
import { ProductionModule } from '../../production.module';
import { ReportModule } from '../report.module';
import { PendingJobComponent } from './pending-job/pending-job.component';
import { PlanningComponent } from './planning/planning.component';
import { OnprogressComponent } from './onprogress/onprogress.component';
import { GpmComponent } from './gpm/gpm.component';
import { GmpAddComponent } from './gpm/gmp-add/gmp-add.component';


@NgModule({
  declarations: [
    PendingJobComponent,
    PlanningComponent,
    OnprogressComponent,
    GpmComponent,
    GmpAddComponent,
  ],
  imports: [
    CommonModule,
    MontlyRoutingModule,
    ProductionModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    ReportModule,
  ]
})
export class MontlyModule { }
