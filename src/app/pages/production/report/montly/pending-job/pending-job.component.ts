import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pending-job',
  templateUrl: './pending-job.component.html',
  styleUrls: ['./pending-job.component.scss']
})
export class PendingJobComponent implements OnInit{
  breadCrumbItems!: Array<{}>;
  submitted = false;

  //pages
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';

  //roll akses
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;

  //data
  filterdata: any[] = [];
  userData: any;
  MontlyPending: any;
  MontlyId: any;

  constructor(
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private router: Router,
    ) {
  }

  ngOnInit(): void {
    this.getDataBasedOnRole()
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/dashboard-prod' },
      { label: 'Monthly', link: '/dashboard-prod' },
      { label: 'Activity', link: '/dashboard-prod' },
      { label: 'Pending-Job', active: true},
    ];
  }

    formatDate(date: string): string {
      const formattedDate = new Date(date);
      const monthNames = [
        'Januari', 'Februari', 'Maret', 'April',
        'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
      ];

      const day = formattedDate.getDate();
      const monthIndex = formattedDate.getMonth();
      const year = formattedDate.getFullYear();
      return `${day} ${monthNames[monthIndex]} ${year}`;
    }

    calculateStatus(start_date: Date, end_date: Date, actual_start: string, actual_end: string): string {
      start_date.setHours(start_date.getHours() - start_date.getTimezoneOffset() / 60);
      end_date.setHours(end_date.getHours() - end_date.getTimezoneOffset() / 60);
      const actualStartDate = actual_start !== '0000-00-00' ? new Date(actual_start) : null;
      const actualEndDate = actual_end !== '0000-00-00' ? new Date(actual_end) : null;
      const currentDate = new Date();
      const duration = end_date.getTime() - start_date.getTime();
      const currentDuration = currentDate.getTime() - start_date.getTime();
  
      if (actualStartDate && actualEndDate) {
        return 'closed';
      } else if (currentDuration >= 0 && currentDuration <= duration) {
        return 'onprogress';
      } else if (currentDate.toISOString().split('T')[0] === start_date.toISOString().split('T')[0]) {
        return 'onprogress';
      } else if (currentDate.toISOString().split('T')[0] === end_date.toISOString().split('T')[0]) {
        return 'onprogress';
      } else if (currentDuration < 0) {
        return 'planning';
      } else if (currentDuration == duration + 2) {
        return 'onprogress';
      } else if (currentDuration > duration){
        return 'pending';
      } else {
        return 'pending';
      }
    }

    getDataBasedOnRole() {
      if (this.AuthenticationService.isAdmin() || this.AuthenticationService.isSpv()) {
        this.getAlldailyAdmin();
      } else if (this.AuthenticationService.isEmployee()) {
        this.getAlldailyKaryawan();
      } else{
        alert('error | not found')
      }
    }
    
    getAlldailyAdmin() {
      this.apiService.getTabelViewDaily().subscribe({
        next: (res: any) => {
          if (res.status) {
            if (Array.isArray(res.data)) {
              this.MontlyPending = res.data.filter((data: { status: string, type: string, appear:string }) => {
                const isActive = data.appear === 'active';
                return data.type === 'monthly' && isActive;
              });
              if (Array.isArray(this.MontlyPending)) {
                this.MontlyPending.forEach((data: any) => {
                  const actual_start = data.actual_start instanceof Date ? data.actual_start.toISOString() : data.actual_start;
                  const actual_end = data.actual_end instanceof Date ? data.actual_end.toISOString() : data.actual_end;
                  data.status = this.calculateStatus(new Date(data.start_date), new Date(data.end_date), actual_start, actual_end);
                });
                const filteredByStatus = this.MontlyPending.filter(data => data.status === 'pending');
                this.filterdata = filteredByStatus;
                this.totalRecords = this.filterdata.length;
                this.setPaginationData();
              } else {
                console.error('Data Montly tidak valid.');
              }
            } else {
              console.error('Data respon tidak valid.');
            }
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error('Kesalahan dalam permintaan:', err);
          setTimeout(() => {
          }, 1000);
        },
      });
    }

    getAlldailyKaryawan() {
      const karyawanData = this.AuthenticationService.getUserData();
      const karyawanName = karyawanData.name;
      this.apiService.getTabelViewDaily().subscribe({
        next: (res: any) => {
          if (res.status) {
            if (Array.isArray(res.data)) {

              this.MontlyPending = res.data.filter((data: { pic: string, status: string, type: string, appear: string }) => {
                const picArray = data.pic.split(',').map(pic => pic.trim());
                const isActive = data.appear === 'active'; 
                return picArray.some(pic => pic === karyawanName) && data.type === 'monthly' && isActive;
              });

              if (Array.isArray(this.MontlyPending)) {
                this.MontlyPending.forEach((data: any) => {
                  const actual_start = data.actual_start instanceof Date ? data.actual_start.toISOString() : data.actual_start;
                  const actual_end = data.actual_end instanceof Date ? data.actual_end.toISOString() : data.actual_end;

                  data.status = this.calculateStatus(new Date(data.start_date), new Date(data.end_date), actual_start, actual_end);
                });
                const filteredByStatus = this.MontlyPending.filter(data => data.status === 'pending');
                this.filterdata = filteredByStatus;
                this.totalRecords = this.filterdata.length;
                this.setPaginationData();
              } else {
                console.error('Data Daily Activity tidak valid.');
              }
            } else {
              console.error('Data respon tidak valid.');
            }
          } else {
            console.error(`${res.data.message}`);
          }
        },
        error: (err: any) => {
          console.error('Kesalahan dalam permintaan:', err);
        },
      });
    }

  getByIdFollow(id: any) {
    this.apiService.getByIdFollowup(id).subscribe({
      next: (res: any) => {
        this.MontlyId = res;
        this.router.navigate(['production/report/wekly/followup', id], { state: { data: this.MontlyId } });
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

AddActivity(){
  this.router.navigate(['/production/activity']);
}

confirm(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        appear: 'nonactive',
      };
      this.apiService.updateActivity(id, data).subscribe({
        next: (res: any) => {
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onSearch(): void {
  if (!this.searchTerm) {
    this.filterdata = this.MontlyPending;
  } else {
    const searchTermLower = this.searchTerm.toLowerCase();
    this.filterdata = this.MontlyPending.filter((data: 
      { pic: string, 
        task: string, 
        due_start: string, 
        remark: string, 
        next_plan: string 
      }) => {
      return (
        (data.pic && data.pic.toString().toLowerCase().includes(searchTermLower)) ||
        (data.task && data.task.toString().toLowerCase().includes(searchTermLower)) ||
        (data.due_start && this.isDateMatching(data.due_start, searchTermLower)) ||
        (data.remark && data.remark.toString().toLowerCase().includes(searchTermLower)) ||
        (data.next_plan && data.next_plan.toString().toLowerCase().includes(searchTermLower))
      );
    });
  }
}

isDateMatching(dateString: string, searchTerm: string): boolean {
  try {
    const date = new Date(dateString);
    if (!isNaN(date.getTime())) {
      return date.toISOString().toLowerCase().includes(searchTerm);
    }
  } catch (error) {
    console.error('Error parsing date:', error);
  }
  return false;
}

getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1;
  this.endIndex = this.pageSize;
}

setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

}


