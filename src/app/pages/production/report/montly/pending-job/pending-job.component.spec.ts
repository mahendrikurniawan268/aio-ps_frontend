import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingJobComponent } from './pending-job.component';

describe('PendingJobComponent', () => {
  let component: PendingJobComponent;
  let fixture: ComponentFixture<PendingJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingJobComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PendingJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
