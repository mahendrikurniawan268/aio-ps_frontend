import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { EventService } from 'src/app/core/services/event.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  // bread crumb items
  breadCrumbItems!: Array<{}>;
  isConnected: boolean | undefined;
  imageUrl = `${environment.API_URL}${environment.getImageResume}`;
  record_time: any;

  constructor(
    public apiservice: ApiService,
    private route: ActivatedRoute,
    private router: Router 
  ) {}

  id: number | undefined;
  data: any;

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/dashboard-prod' },
      { label: 'Monthly', link: '/dashboard-prod' },
      { label: 'Resume', link: '/production/report/monthly/resume' },
      { label: 'Print Report', active: true},
    ];

    this.route.params.subscribe(params => {
      const id = params['id'];
      this.apiservice.getByIdResume(id).subscribe((response: any) => {
        this.data = response.data[0]
        this.data.img_performance = this.data.img_performance.split(',');
        this.data.img_issue = this.data.img_issue.split(',');
        this.data.img_other = this.data.img_other.split(',');
        
      }, error => {
        console.error('Error:', error);
      });
    }); 
  }
  formatTime(time: string): string {
    const timeParts = time.split(':');
    const hours = timeParts[0];
    const minutes = timeParts[1];
    return `${hours}:${minutes}`;
  }

  calculateRecordTime() {
    const startTimeParts = this.data.start_time.split(':');
    const endTimeParts = this.data.end_time.split(':');

    const startHour = parseInt(startTimeParts[0], 10);
    const startMinute = parseInt(startTimeParts[1], 10);
    const endHour = parseInt(endTimeParts[0], 10);
    const endMinute = parseInt(endTimeParts[1], 10);

    const totalMinutesStart = startHour * 60 + startMinute;
    const totalMinutesEnd = endHour * 60 + endMinute;

    const differenceMinutes = totalMinutesEnd - totalMinutesStart;
    const hours = Math.floor(differenceMinutes / 60);
    const minutes = differenceMinutes % 60;

    this.record_time = `${hours} jam ${minutes} menit`;
  }
  
  back(){
    this.router.navigate(['/production/report/monthly/resume']);
  }

}



