import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResumeRoutingModule } from './resume-routing.module';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { DetailComponent } from './detail/detail.component';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask';
import { ProductionModule } from '../../../production.module';
import { ReportModule } from '../../report.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    ViewComponent,
    AddComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    ResumeRoutingModule,
    ProductionModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    ReportModule,
    NgbPaginationModule,
  ]
})
export class ResumeModule { }
