import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Lightbox } from 'ngx-lightbox';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent {
  searchTerm: string = ''; 
  breadCrumbItems!: Array<{}>;
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  idLine:any;
  dataYear:any;

  isConnected = false;
  filterResume: any[] = [];
  totalPages: number = 0;
  dataMonthly: any []=[];
  dataMonthlyByLine: any [] = [];
  DataById: any;
  
    //roll akses
    userData:any;
    isAdmin: boolean = false;
    isSpv: boolean = false;
    isEmployee:boolean = false;
    isPlanner: boolean = false;

  constructor(public apiservice:ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private lightbox: Lightbox
    ) {
  }

  ngOnInit(): void {
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.apiservice.getByIdResume(id).subscribe((res: any) => {
        });
      }
    });
    this.getAllReportMonthly();
  }

  onYearChange(event: any) {
    const selectedYear = event.target.value;
    this.filterResume = this.dataMonthlyByLine.filter((monthly: any) => monthly.year === selectedYear);
}

  addData(){
    this.router.navigate(['production/report/monthly/resume/add'], {queryParams: {lineId: this.idLine}});
  }

  editData(id:any){
    this.router.navigate([`production/report/monthly/resume/edit/${id}`], {queryParams: {lineId: this.idLine}});
  }

  getAllReportMonthly() {
    this.apiservice.getTabelViewMonthly().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.dataMonthly = res.data;
          this.dataMonthlyByLine = this.dataMonthly.filter(item => item.id_line == this.idLine);
          const uniqueYears = [...new Set(this.dataMonthlyByLine.map(item => item.year))];
          this.dataYear = uniqueYears;
          this.filterResume = this.dataMonthlyByLine;
          console.log(this.filterResume);
          this.totalRecords = this.filterResume.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      },
    });
}


  getByIdResume(id: any) {
    this.apiservice.getByIdResume(id).subscribe({
      next: (res: any) => {
        this.DataById = res;
        this.router.navigate(['production/report/monthly/resume/detail', id], { state: { data: this.DataById } });
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          status: 0,
        };
        this.apiservice.updateActivityMonthly(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onDone(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be complated this data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, Complated!',
    }).then((result) => {
      if (result.value) {
        const data = {
          id_status: 3,
        };
        this.apiservice.updateActivityMonthly(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filterResume = this.dataMonthlyByLine;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filterResume = this.dataMonthlyByLine.filter((data: { month:string }) => {
        return (
          data.month.toString().toLowerCase().includes(searchTermLower)
        );
      });
    }
}

  getShowingText(): string {
    const startIndex = (this.page - 1) * this.pageSize + 1;
    const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
    return `Showing ${startIndex} - ${endIndex}`;
  }

  onPageSizeChange() {
    this.startIndex = 1;
    this.endIndex = this.pageSize;
  }
  setPaginationData() {
    this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
  }

  openLightboxPerformance(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Performance',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxMaterial(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Material',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxRedArea(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Red Area',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxIssue(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Issue',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxNextPlan(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Next Plan',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxOther(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Other',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImgFilePerformance(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileMaterial(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileRedArea(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileIssue(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileNextPlan(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileOther(file: any) {
    return environment.API_URL + '/image/' + file
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];

    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        default:
            return 'secondary';
    }
  }
  
  getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        default:
            return 'Unknown';
    }
  }

}

