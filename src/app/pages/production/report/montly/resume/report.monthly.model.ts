export interface MonthlyReport {
    id_line: null | number,
    id_month: null | number,
    year: string,
    img_performance: File | null,
    img_material_yield: File | null,
    img_red_area: File | null,
    img_issue: File | null,
    img_next_plan: File | null,
    img_other: File | null;
    id_status: null | number;
    }