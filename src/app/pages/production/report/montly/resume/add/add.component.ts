import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { MonthlyReport } from '../report.monthly.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;
  dataMonth: any;
  dataActivityById:any;
  selectedFiles: { file: File, type: string }[] = [];
  idLine:any;
  imageUrl: any;
  id_report_monthly: any;
  report_monthly: any;
  idParam: any;


  newMonthlyReport : MonthlyReport = {
    id_line: null,
    id_month: null,
    year: '',
    img_performance: null,
    img_material_yield: null,
    img_red_area: null,
    img_issue: null,
    img_next_plan: null,
    img_other: null,
    id_status: null
  }

  constructor(
    private apiService: ApiService,
    private router: Router,
    private RestApiService: RestApiService,
    private route:ActivatedRoute
    ) {
  }

  ngOnInit(): void {
    this.getAllMonth();
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    if (this.idParam) {
      this.getActivityMonthById(this.idParam);
    }
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
    console.log(this.selectedFiles);
    
  }

  insertReportMonthly(dataMonthly: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataMonthly.img_performance = res.uploadedFiles[0];
          dataMonthly.img_material_yield = res.uploadedFiles[1];
          dataMonthly.img_red_area = res.uploadedFiles[2];
          dataMonthly.img_issue = res.uploadedFiles[3];
          dataMonthly.img_next_plan = res.uploadedFiles[4];
          dataMonthly.img_other = res.uploadedFiles[5];
          
          this.newMonthlyReport.id_line = this.idLine
          this.newMonthlyReport.id_status = 2
          this.apiService.insertActivityMonthly(dataMonthly).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_report_monthly = res.data[0];
                this.report_monthly = dataMonthly;
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.apiService.insertActivityMonthly(dataMonthly).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_report_monthly = res.data[0];
            this.report_monthly = dataMonthly;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  

  onInsertData() {
    if (this.selectedFiles.length > 0) {
        this.insertReportMonthly(this.newMonthlyReport);
        Swal.fire({
            title: 'Data Added',
            text: 'Data has been successfully added.',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then((result) => {
          if (result.isConfirmed) {
              this.router.navigate(['production/report/monthly/resume'], {queryParams: {lineId: this.idLine}});
          }
      });;
    } else {
        Swal.fire({
            title: 'No Files Selected',
            text: 'Please select at least one file before adding data.',
            icon: 'warning',
            confirmButtonText: 'OK',
        });
    }
}

updateMonthlyReport(dataWeekly: any) {
  if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
          formData.append('files', fileData.file, fileData.file.name);
          formData.append('type', fileData.type);
      });

      this.RestApiService.uploadMultipleImage(formData).subscribe({
          next: (res: any) => {
              this.imageUrl = res.uploadedFiles;
              const performanceFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-performance');
              const materialFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-material');
              const redFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-redarea');
              const issueFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-issue');
              const nextPlanFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-nextplan');
              const otherFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-other');

              if (performanceFileIndex !== -1) {
                  dataWeekly.img_performance = res.uploadedFiles[performanceFileIndex];
              }
              if (materialFileIndex !== -1) {
                  dataWeekly.img_material_yield = res.uploadedFiles[materialFileIndex];
              }
              if (redFileIndex !== -1) {
                dataWeekly.img_red_area = res.uploadedFiles[redFileIndex];
              }
              if (issueFileIndex !== -1) {
                dataWeekly.img_issue = res.uploadedFiles[issueFileIndex];
              }
              if (nextPlanFileIndex !== -1) {
                dataWeekly.img_next_plan = res.uploadedFiles[nextPlanFileIndex];
              }
              if (otherFileIndex !== -1) {
                dataWeekly.img_other = res.uploadedFiles[otherFileIndex];
              }

              this.apiService.updateActivityMonthly(this.idParam, dataWeekly).subscribe(
                  (res: any) => {
                      if (res.status == true) {
                          this.newMonthlyReport = dataWeekly;
                      }
                  },
                  (error) => {
                      console.error(error)
                  }
              );
          },
          error: (err) => console.error(err),
          complete: () => {
              this.selectedFiles = [];
          }
      });
  } else {
      this.apiService.updateActivityMonthly(this.idParam, dataWeekly).subscribe(
          (res: any) => {
              if (res.status == true) {
                  this.newMonthlyReport = dataWeekly;
              }
          },
          (error) => {
              console.error(error)
          }
      );
    }
}

onUpdate() {
    this.updateMonthlyReport(this.newMonthlyReport);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
}

  getAllMonth(){
    this.RestApiService.getAllMonth().subscribe(
    (res:any) => {
      this.dataMonth = res.data;
    })
  }

  getActivityMonthById(id:any){
    this.apiService.getByIdResume(id).subscribe(
    (res:any) => {
      this.newMonthlyReport = res.data[0];
      console.log(this.newMonthlyReport);
      
    })
  }

  back(){
    this.router.navigate(['/production/report/monthly/resume'], {queryParams: {lineId: this.idLine}});
  }
}