import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GmpAddComponent } from './gmp-add.component';

describe('GmpAddComponent', () => {
  let component: GmpAddComponent;
  let fixture: ComponentFixture<GmpAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GmpAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GmpAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
