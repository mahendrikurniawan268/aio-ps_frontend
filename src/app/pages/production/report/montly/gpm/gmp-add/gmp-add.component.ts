import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { GMP } from '../gmp.model';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-gmp-add',
  templateUrl: './gmp-add.component.html',
  styleUrls: ['./gmp-add.component.scss']
})
export class GmpAddComponent implements OnInit{
  
  public gmp:any;
  id_gmp:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  dataGmp:any;
  dataArea: any [] = [];
  area: any;
  idLine: any;
  idArea: any;

  newGmp : GMP = {
    id_line: null,
    id_section: null,
    date: '',
    pending: '',
    follow_up: '',
    img_before: '',
    img_after: ''
  }

  constructor(
    private router: Router,
    private apiService: ApiService,
    private RestApiService: RestApiService,
    private http: HttpClient,
    private route:ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const idToEdit = params['id'];
      this.id_gmp = idToEdit
      if (idToEdit) {
        this.getGmpById(idToEdit);
      }
    });
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
    this.getArea();
  }

  getArea(){
    this.RestApiService.getAllArea().subscribe(
      (res: any) => {
        this.dataArea = res.data;
        this.area = this.dataArea.filter(item => item.id_line == this.idLine) 
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getGmpById(id:any){
    this.apiService.getByIdGmp(id).subscribe(
    (res:any) => {
      this.newGmp = res.data[0]
      this.newGmp.date = this.convertDate(this.newGmp.date);
    })
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  insertGmp(dataGmp: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataGmp.img_before = res.uploadedFiles[0];
          dataGmp.img_after = res.uploadedFiles[1];
          
          this.newGmp.id_line = this.idLine
          this.apiService.insertGmp(dataGmp).subscribe(
            (res: any) => {
              if (res.status == true) {
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.apiService.insertGmp(dataGmp).subscribe(
        (res: any) => {
          if (res.status == true) {
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  
  
  onInsertData() {
    if (this.selectedFiles.length > 0) {
        this.insertGmp(this.newGmp);
        Swal.fire({
            title: 'Data Added',
            text: 'Data has been successfully added.',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                this.router.navigate(['production/report/monthly/gmp'], {queryParams: {lineId: this.idLine}});
            }
        });
    } else {
        Swal.fire({
            title: 'No Files Selected',
            text: 'Please select at least one file before adding data.',
            icon: 'warning',
            confirmButtonText: 'OK',
        });
    }
}

  forBack() {
    this.router.navigate(['production/report/monthly/gmp'], {queryParams: {lineId: this.idLine}});
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  updateGmp(dataGmp: any) {
    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });

        this.RestApiService.uploadMultipleImage(formData).subscribe({
            next: (res: any) => {
                this.imageUrl = res.uploadedFiles;
                const beforeFileIndex = this.selectedFiles.findIndex(file => file.type === 'img-before');
                const afterFileIndex = this.selectedFiles.findIndex(file => file.type === 'img-after');

                if (beforeFileIndex !== -1) {
                    dataGmp.img_before = res.uploadedFiles[beforeFileIndex];
                }
                if (afterFileIndex !== -1) {
                    dataGmp.img_after = res.uploadedFiles[afterFileIndex];
                }

                this.apiService.updateGmp(this.id_gmp, dataGmp).subscribe(
                    (res: any) => {
                        if (res.status == true) {
                            this.newGmp = dataGmp;
                        }
                    },
                    (error) => {
                        console.error(error)
                    }
                );
            },
            error: (err) => console.error(err),
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.apiService.updateGmp(this.id_gmp, dataGmp).subscribe(
            (res: any) => {
                if (res.status == true) {
                    this.newGmp = dataGmp;
                }
            },
            (error) => {
                console.error(error)
            }
        );
      }
  }

  onUpdate() {
      this.updateGmp(this.newGmp);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
      this.router.navigate(['production/report/monthly/gmp'], {queryParams: {lineId: this.idLine}});
  }
}

