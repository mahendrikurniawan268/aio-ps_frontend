import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';
import { GMP } from './gmp.model';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-gpm',
  templateUrl: './gpm.component.html',
  styleUrls: ['./gpm.component.scss']
})
export class GpmComponent implements OnInit{
  index: number = 0;
  dataGmp: any[] = [];
  dataGmpByLine:any;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  idLine:any;
  idArea:any;

  newGmp : GMP = {
    id_line: null,
    id_section: null,
    date: '',
    pending: '',
    follow_up: '',
    img_before: '',
    img_after: ''
  }

  currentPage = 1;
  itemsPerPage = 10;

  constructor(private router:Router, private apiService:ApiService, private authService:AuthenticationService, private lightbox : Lightbox,
    private route: ActivatedRoute){}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
      this.getGmp();
    }})
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  openLightboxBefore(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Before',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxAfter(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation After',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImgFileBefore(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileAfter(file: any) {
    return environment.API_URL + '/image/' + file
  }
  
  onAddData(){
    this.router.navigate(['production/report/monthly/gmp/add'], {queryParams: {lineId: this.idLine}})
  }

  onEditData(id:any) {
    this.router.navigate([`production/report/monthly/gmp/edit/${id}`], {queryParams: {lineId: this.idLine}});
  }

  getGmp(){
    this.apiService.getTabelViewGmp().subscribe(
      (res:any) => {
        this.dataGmp = res.data;
        this.dataGmpByLine = this.dataGmp.filter(item => item.id_line == this.idLine)
        this.preData = [...this.dataGmpByLine];
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateGmp(dataGmpByLine: any, id:any) {
    this.apiService.updateGmp(id, dataGmpByLine).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.newGmp = dataGmpByLine;
          this.getGmp()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateGmp(deleteData, id);
      }
    });
  }

  search() {
    this.dataGmpByLine = this.preData.filter((item: { area: string; pending: string; follow_up: string; date: string;}) => {
      return item.area.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.pending.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.follow_up.toLowerCase().includes(this.searchText.toLowerCase()) ||
             this.extractMonthFromDate(item.date).toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataGmpByLine.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataGmpByLine = this.preData.filter((item: { date: string; }) => {
      const itemDate = new Date(item.date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.paginateData();
  }
}
