export interface GMP {
    id_line: number | null;
    id_section: number | null;
    date: string;
    pending: string;
    follow_up: string;
    img_before: string;
    img_after: string;
}