import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GpmComponent } from './gpm.component';

describe('GpmComponent', () => {
  let component: GpmComponent;
  let fixture: ComponentFixture<GpmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GpmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
