import { Component, QueryList, ViewChildren } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ApiService } from 'src/app/core/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-daily-history',
  templateUrl: './daily-history.component.html',
  styleUrls: ['./daily-history.component.scss'],
  providers: [DecimalPipe]
})

export class DailyHistoryComponent {
  searchTerm: string = ''; 
  breadCrumbItems!: Array<{}>;
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  abnormalData: any;
  DataById: any;
  totalPages: number = 0;
  filteredHistoryData: any[] = [];
  historyData:any;
  id: number | undefined;
  idLine:any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  
  constructor(
    public apiservice:ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getAllHistory()
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/dashboard-prod'},
      { label: 'Daily', link: '/production/report/daily/proses'},
      { label: 'Proses', link: '/production/report/daily/proses'},
      { label: 'History', active: true }
    ];

    this.route.params.subscribe(params => {
      if ('id' in params) {
        this.id = params['id'];
      }
    });
  }

  getByIdHistory(id: any) {
    this.apiservice.getByIdHistory(id).subscribe({
      next: (res: any) => {
        this.DataById = res.data
        this.router.navigate(['production/report/daily/history/', id], { queryParams: {lineId : this.idLine}});
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }

  addReport(){
    this.router.navigate(['production/report/daily/proses'], {queryParams:{ lineId: this.idLine}})
  }

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          status: 'nonactive',
        };
        this.apiservice.updatereset(id, data).subscribe({
          next: (res:any) => {
            this.modalService.dismissAll();
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          },
        
      } )
        
      }
    });
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filteredHistoryData = this.historyData;
    } else {
      const searchString = this.searchTerm.toLowerCase();
      this.filteredHistoryData = this.historyData.filter((data: { date: string; title: string; }) => {
        const dateMatch = data.date && data.date.toString().toLowerCase().includes(searchString);
        const lotNumberMatch = data.title && data.title.toLowerCase().includes(searchString);
        return  dateMatch || lotNumberMatch;
      });
    }
  }

  getAllHistory() {
    this.apiservice.getAllHistory().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.historyData = res.data.filter((item: any) => item.status === 'active' && item.id_line == this.idLine);
          this.filteredHistoryData = this.historyData;
          this.totalRecords = this.filteredHistoryData.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }  

  filter() {
    this.filteredHistoryData = this.historyData.filter((item: { date: string;}) => {
      const itemDate = new Date(item.date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;
      
      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.setPaginationData();
  }

  getShowingText(): string {
    const startIndex = (this.page - 1) * this.pageSize + 1;
    const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
    return `Showing ${startIndex} - ${endIndex}`;
  }

  onPageSizeChange() {
    this.page = 1;
    this.setPaginationData();
  }

  setPaginationData() {
    this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
  }
}



