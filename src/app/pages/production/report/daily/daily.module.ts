import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';

import { DailyRoutingModule } from './daily-routing.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbTypeaheadModule, NgbPaginationModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ArchwizardModule } from 'angular-archwizard';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ColorPickerModule } from 'ngx-color-picker';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { UiSwitchModule } from 'ngx-ui-switch';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ProductionModule } from '../../production.module';
import { DailyReportComponent } from './daily-report/daily-report.component';
import { DailyActivityComponent } from './daily-activity/daily-activity.component';
import { DailyHistoryComponent } from './daily-history/daily-history.component';
import { EksportsComponent } from './eksports/eksports.component';

import lottie from 'lottie-web';
import { defineElement } from 'lord-icon-element';


@NgModule({
  declarations: [
    DailyReportComponent,
    DailyHistoryComponent,
    EksportsComponent,
    DailyActivityComponent,

  ],
  imports: [
    CommonModule,
    DailyRoutingModule,
    ProductionModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    NgbTypeaheadModule,
    HttpClientModule,
    FlatpickrModule,
    SimplebarAngularModule,
    NgbPaginationModule,
    DatePipe,
    NgbNavModule,
    NgSelectModule,
    UiSwitchModule,
    ColorPickerModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    NgxSliderModule,
    ArchwizardModule,
    DropzoneModule,
    AutocompleteLibModule,
],

providers: [
    DecimalPipe,
    provideNgxMask(),
],
schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class DailyModule { 
  constructor() {
    defineElement(lottie.loadAnimation);
}
}
