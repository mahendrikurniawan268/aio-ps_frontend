import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-daily-activity',
  templateUrl: './daily-activity.component.html',
  styleUrls: ['./daily-activity.component.scss']
})
export class DailyActivityComponent implements OnInit{
  breadCrumbItems!: Array<{}>;
  dailyActivity: any [] = [];
  dailyActivityByLine : any;
  activityAdd!: UntypedFormGroup;
  activityEdit!: UntypedFormGroup;

  ///pages
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';
  filterdata: any[] = [];
  dataEdit: any[]=[];
  submitted = false;
  activityId: any;
  idLine:any;
  status: string = '';
  startDate: string = '';
  endDate: string = '';

    //roll akses
    // userData: boolean = false;
    isAdmin: boolean = false;
    isEmployee: boolean = false;
    isSpv: boolean = false;
  data: any;
  currentDate: any;

  constructor(
    private modalService: NgbModal,
    private formBuilder: UntypedFormBuilder, 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private router: Router,
    private route : ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getTabelView()
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/production/report/daily'},
      { label: 'daily', link: '/production/report/daily'},
      { label: 'activity', active: true }
    ];

    this.activityEdit = this.formBuilder.group({
      id_activity: [null],
      id_line: [null],
      start_date:[null],
      end_date:[null],
      id_pic: [null],
      issue: [null],
      task: [null],
      status:[null],
      actual_start:[null],
      actual_end:[null]
    });
  }

  calculateStatus(start_date: Date, end_date: Date, actual_start: string, actual_end: string): string {
    start_date.setHours(start_date.getHours() - start_date.getTimezoneOffset() / 60);
    end_date.setHours(end_date.getHours() - end_date.getTimezoneOffset() / 60);
    const actualStartDate = actual_start !== '0000-00-00' ? new Date(actual_start) : null;
    const actualEndDate = actual_end !== '0000-00-00' ? new Date(actual_end) : null;
    const currentDate = new Date();

    if (actualStartDate && actualEndDate) {
        return 'closed';
    } else if (currentDate.toISOString().split('T')[0] < start_date.toISOString().split('T')[0]) {
        return 'planning';
    } else if (currentDate.toISOString().split('T')[0] === start_date.toISOString().split('T')[0] &&
        currentDate.toISOString().split('T')[0] <= end_date.toISOString().split('T')[0]) {
        return 'onprogress';
    } else if (!actualStartDate && currentDate > end_date) {
        return 'pending';
    } else {
        return 'pending';
    }
}

  getTabelView() {
    this.apiService.getTabelViewDaily().subscribe({
      next: (res: any) => {
        if (res.status) {
          if (res.data) {
            this.dailyActivity = res.data;
            this.dailyActivityByLine = this.dailyActivity.filter(item => item.id_line == this.idLine)

            if (Array.isArray(this.dailyActivity)) {
              this.dailyActivity.forEach((data: any) => {
                const actual_start = data.actual_start instanceof Date ? data.actual_start.toISOString() : data.actual_start;
                const actual_end = data.actual_end instanceof Date ? data.actual_end.toISOString() : data.actual_end;
                data.status = this.calculateStatus(new Date(data.start_date), new Date(data.end_date), actual_start, actual_end);
              });
              this.filterdata = this.dailyActivityByLine;
              this.totalRecords = this.filterdata.length;              
              this.setPaginationData();
            } else {
              console.error('Data Daily Activity tidak valid.');
            }
          } else {
            console.error('Data respon tidak valid.');
          }
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error('Kesalahan dalam permintaan:', err);
        setTimeout(() => {
        }, 1000);
      },
    });
  }

  getStatusLabelClass(status: string): string {
    switch (status) {
        case 'onprogress':
            return 'info'; // On Progress
        case 'planning':
            return 'success'; // Planning
        case 'pending':
            return 'danger'; // Pending
        case 'close':
            return 'secondary';
        default:
            return 'secondary';
    }
}

getStatusLabel(status: string): string {
    switch (status) {
        case 'onprogress':
            return 'On Progress';
        case 'planning':
            return 'Planning';
        case 'pending':
            return 'Pending';
        case 'close':
            return 'Done';
        default:
            return 'Unknown';
    }
}

  AddActivity(){
    this.router.navigate(['/production/activity'], {queryParams:{lineId : this.idLine}});
  }
  
    /**
  * Open modal
  * @param contentEdit modal content
  */
    convertDatabaseDate(dateString: string | null | undefined): string {
      if (!dateString || dateString === '0000-00-00') {
        return '';
      }
      try {
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + date.getDate()).slice(-2);
    
        return `${year}-${month}-${day}`;
      } catch (error) {
        console.error('Kesalahan konversi tanggal:', error);
        return '';
      }
    }
    
    editModal(contentEdit: any, data: any) {
      this.dataEdit = data;
      console.log(this.dataEdit);
      
      this.modalService.open(contentEdit, { size: 'md', centered: true });
      this.activityEdit.get('id_activity')?.setValue(data.id_activity);
      this.activityEdit.get('start_date')?.setValue(this.convertDatabaseDate(data.start_date));
      this.activityEdit.get('end_date')?.setValue(this.convertDatabaseDate(data.end_date));
      this.activityEdit.get('issue')?.setValue(data.issue);
      this.activityEdit.get('task')?.setValue(data.task);
      this.activityEdit.get('actual_start')?.setValue(this.convertDatabaseDate(data.actual_start));
      this.activityEdit.get('actual_end')?.setValue(this.convertDatabaseDate(data.actual_end));
    }
    
    onUpdate() {
      try {
        const id = this.activityEdit.get('id_activity')?.value;
        const issue = this.activityEdit.get('issue')?.value;
        const task = this.activityEdit.get('task')?.value;
        const setStart = this.activityEdit.get('start_date')?.value;
        const setEnd = this.activityEdit.get('end_date')?.value;
        const ActualStart = this.activityEdit.get('actual_start')?.value;
        const ActualEnd = this.activityEdit.get('actual_end')?.value;
        const start_date = new Date(setStart);
        const end_date = new Date(setEnd);
        const actual_start = ActualStart ? this.convertDatabaseDate(ActualStart) : null;
        const actual_end = ActualEnd ? this.convertDatabaseDate(ActualEnd) : null;

        if (isNaN(start_date.getTime()) || isNaN(end_date.getTime()) ||
            (actual_start && isNaN(new Date(actual_start).getTime())) ||
            (actual_end && isNaN(new Date(actual_end).getTime()))) {
          throw new Error('Tanggal yang dimasukkan tidak valid.');
        }
    
        start_date.setHours(start_date.getHours() - start_date.getTimezoneOffset() / 60);
        end_date.setHours(end_date.getHours() - end_date.getTimezoneOffset() / 60);
    
        const data = {
          id_activity: id,
          start_date: start_date.toISOString(),
          end_date: end_date.toISOString(),
          actual_start: actual_start,
          actual_end: actual_end,
          task: task,
          issue: issue,
        };
        this.apiService.updateActivity(id, data).subscribe({
          next: (res: any) => {
            this.modalService.dismissAll();
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      } catch (error) {
        console.error('Kesalahan onUpdate:', error);
      }
    }

      /**
   * position sweet alert
   * @param position modal content
   */
      saveAgain() {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Saving your work...',
          didOpen: () => {
            this.onUpdate();
            setTimeout(() => {
              Swal.close();
            }, 1000);
          },
        });
      }

    formatDate(date: string): string {
      // Ubah string tanggal menjadi objek Date
      const formattedDate = new Date(date);
  
      // Daftar nama bulan
      const monthNames = [
        'Januari', 'Februari', 'Maret', 'April',
        'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
      ];
  
      // Ambil tanggal, bulan, dan tahun
      const day = formattedDate.getDate();
      const monthIndex = formattedDate.getMonth();
      const year = formattedDate.getFullYear();
  
      // Format tanggal dalam bentuk "DD MMMM YYYY"
      return `${day} ${monthNames[monthIndex]} ${year}`;
    }

    confirm(id: any) {
      Swal.fire({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#34c38f',
        cancelButtonColor: '#f46a6a',
        confirmButtonText: 'Yes, delete it!',
      }).then((result) => {
        if (result.value) {
          const data = {
            status: 0,
          };
          console.log(id, data);
          this.apiService.updateActivity(id, data).subscribe({
            next: (res: any) => {
              this.modalService.dismissAll();
              this.ngOnInit();
            },
            error: (err) => {
              console.error('Kesalahan saat mengirim permintaan update:', err);
            }
          });
        }
      });
    }
    
    onSearch(): void {
      if (!this.searchTerm) {
        this.filterdata = this.dailyActivityByLine;
      } else {
        const searchTermLower = this.searchTerm.toLowerCase();
        this.filterdata = this.dailyActivityByLine.filter((data: { name: string, start_date: string, end_date:string, task: string, issue: string }) => {
          const start_date = new Date(data.start_date);
          const end_date = new Date(data.start_date);
          const startDay = new Date(start_date);
          const endDay = new Date(end_date);
          startDay.setDate(startDay.getDate() + 1);
          endDay.setDate(endDay.getDate() + 1);
          return (
            data.name.toString().toLowerCase().includes(searchTermLower) ||
            data.task.toString().toLowerCase().includes(searchTermLower) ||
            startDay.toISOString().slice(0, 10).toString().toLowerCase().includes(searchTermLower) ||
            endDay.toISOString().slice(0, 10).toString().toLowerCase().includes(searchTermLower) ||
            data.issue.toString().toLowerCase().includes(searchTermLower)
          );
        });
      }
    }

    filter() {
      this.filterdata = this.dailyActivityByLine.filter((item: { start_date: string;}) => {
        const itemDate = new Date(item.start_date);
        const startFilterDate = this.startDate ? new Date(this.startDate) : null;
        const endFilterDate = this.endDate ? new Date(this.endDate) : null;
        
        if (startFilterDate) {
          startFilterDate.setDate(startFilterDate.getDate() - 1);
        }
    
        return (!startFilterDate || itemDate >= startFilterDate) &&
               (!endFilterDate || itemDate <= endFilterDate) 
      });
      this.setPaginationData();
    }
    
getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1; // Reset startIndex
  this.endIndex = this.pageSize; // Update endIndex
}

setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}
}

