import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-eksports',
  templateUrl: './eksports.component.html',
  styleUrls: ['./eksports.component.scss'],
  providers: [DecimalPipe]
})

export class EksportsComponent implements OnInit {
  id: number | undefined;
  data: any[] = [];
  totalFinishGood: number = 0;
  totalReject: number = 0;
  totalProductionMinutes: number = 0;
  lineEfficiency: string ='';
  presentaseReject: string ='';
  idLine: any;

  constructor(
    private route: ActivatedRoute,
    private apiservice: ApiService,
    private http: HttpClient,
    private router: Router) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe({ next: (params) => {
      this.idLine = params['lineId'];
    }});

    this.id = Number(this.route.snapshot.paramMap.get('id'));
    const apiUrl = `${environment.API_URL}master/history/${this.id}`;

    this.http.get(apiUrl).subscribe((response: any) => {
      this.data = response.data;
      console.log(this.data);
      
      if (response.data && response.data.length > 0) {
        this.totalFinishGood = response.data[0].total_finish_good;
        this.totalReject = response.data[0].reject;
  
        this.data.forEach((item: any) => {
          const productionHours = this.calculateMinutes(item.production_hours);
          this.totalProductionMinutes += productionHours;
        });

        this.presentaseReject = ((this.totalReject / this.totalFinishGood) * 100).toFixed(2);
        console.log(this.presentaseReject);
        
  
        this.lineEfficiency = ((this.totalFinishGood / (this.totalProductionMinutes * 530)) * 100).toFixed();
      } else {
        console.log('Data Production Hours is not available.');
      }
    });

    this.route.params.subscribe(params => {
      const id = params['id'];
      this.apiservice.getByIdHistory(id).subscribe((response: any) => {
        this.data = response.data;
        console.log(this.data);
        
      }, error => {
        console.error('Error:', error);
      });
    });
  }

  calculateMinutes(productionHours: string): number {
    if (!productionHours) return 0;
    const hoursAndMinutes = productionHours.split(' ');
    const hours = parseInt(hoursAndMinutes[0], 10);
    const minutes = parseInt(hoursAndMinutes[2], 10);
    return hours * 60 + minutes;
  }

  goBack() {
    this.router.navigate(['production/report/daily/history'], { queryParams: { lineId: this.idLine }});
  }
}
