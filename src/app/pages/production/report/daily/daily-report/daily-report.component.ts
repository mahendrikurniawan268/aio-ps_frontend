import { DecimalPipe } from '@angular/common';
import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ApiService } from 'src/app/core/services/api.service';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.component.html',
  styleUrls: ['./daily-report.component.scss'],
  providers: [DecimalPipe]
})

export class DailyReportComponent implements OnInit{
  breadCrumbItems!: Array<{}>;
  submitted = false;
  dailyReportForm!: UntypedFormGroup;
  editDailyReportForm!: UntypedFormGroup;
  ReportFormReset!: UntypedFormGroup;
  dataTitle: any
  dataTotalFinishGood: any
  dataReject: any;
  isConnected: boolean = false;

  //role user
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userRole: any;

    ///pages
    totalRecords: any; 
    page: number = 1;
    pageSize : number = 5;
    startIndex: number = 1;
    endIndex: number = this.pageSize;
    totalPages: number = 0;

    //data
    searchTerm: string = '';
    filterdata: any[] = [];
    activityId: any;
    userData: any;
    dailyData:any [] = [];
    dailyDataByLine:any;
    dailyDataById:any;
    dataEdit: any[]=[];
    fiterReportDaily: any[] = [];
    idLine: any;
    editDataId: any;
  
  constructor(
    private modalService: NgbModal,
    private formBuilder: UntypedFormBuilder, 
    private apiService: ApiService, 
    private AuthenticationService: AuthenticationService,
    private route : ActivatedRoute,
    private router:Router) {
  }

  ngOnInit(): void {
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.userData = this.AuthenticationService.getUserData();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.dailyReportForm = this.formBuilder.group({
      id_line: this.idLine,
      lot_number: [null],
      id_shift_leaders: [null],
      production_hours: [null],
      start: [null],
      finish: [null],
      remark: [null]
    })
    this.editDailyReportForm = this.formBuilder.group({
      id_line: this.idLine,
      lot_number: [null],
      id_shift_leaders: [null],
      production_hours: [null],
      start: [null],
      finish: [null],
      remark: [null]
    })
    this.ReportFormReset = this.formBuilder.group({
      title: [null],
      total_finish_good: [null], 
      reject: [null],
      status: 'active',
    })
    this.getAllDaily()
    this.getByUser()
  }

  onUpdate() {
    const formData = this.editDailyReportForm.value;
    this.apiService.updatedaily(this.editDataId ,formData).subscribe({
      next: (res: any) => {
        if (res.status) {
          this.modalService.dismissAll();
          this.ngOnInit();
        }
      },
      error: (err: any) => {
        console.error(err);
      }
    });
  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(content: any) {
    this.submitted = false;
    this.modalService.open(content, { size: 'md', centered: true });
  }
    /**
  * Open modal
  * @param contentEdit modal content
  */
    editModal(contentEdit: any, data: any) {
      this.dataEdit = data
      this.editDataId = data.id_daily_report
      this.modalService.open(contentEdit, { size: 'md', centered: true });
      this.editDailyReportForm.get('lot_number')?.setValue(data.lot_number) 
      this.editDailyReportForm.get('id_shift_leaders')?.setValue(data.id_shift_leaders)   
      this.editDailyReportForm.get('finish_good')?.setValue(data.finish_good)      
      this.editDailyReportForm.get('start')?.setValue(data.start)    
      this.editDailyReportForm.get('finish')?.setValue(data.finish)    
      this.editDailyReportForm.get('remark')?.setValue(data.remark) 
      this.updateCalculateProductionHours();
    }
      /**
   * Open modal
   * @param contentLot modal content
   */
  ModalReset(contentLot: any) {
    this.submitted = false;
    this.modalService.open(contentLot, { size: 'md', centered: true });
  }

    onSubmit(){
      this.apiService.insertdaily(this.dailyReportForm.value).subscribe({
        next: (res: any) => {
          if (res.status) {
            this.modalService.dismissAll()
            this.ngOnInit()
          }         
        },
        error: (err: any) => console.error(err)
      });
    }

  onReset(){
    this.apiService.resetdaily(this.dailyData).subscribe({
      next: (res: any) => {
        console.log(res.data);
        const id = res.data
        const data = {id_line: this.idLine, title: this.dataTitle, total_finish_good: this.dataTotalFinishGood, reject:this.dataReject,  status: 'active'}
        this.apiService.updatereset(id, data).subscribe({
          next: (res:any) => {
            this.modalService.dismissAll()
            this.ngOnInit()
          },
          error: (err) => {
            console.log('error',err);
          }
        })
      }
      ,
      error: (err: any) => console.error(err)
    });
  }

  insertCalculateProductionHours() {
    const startTime = this.dailyReportForm.get('start')?.value;
    const endTime = this.dailyReportForm.get('finish')?.value;
  
    if (startTime && endTime) {
      const startTimeParts = startTime.split(':');
      const endTimeParts = endTime.split(':');
      const startHour = parseInt(startTimeParts[0], 10);
      const startMinute = parseInt(startTimeParts[1], 10);
      const endHour = parseInt(endTimeParts[0], 10);
      const endMinute = parseInt(endTimeParts[1], 10);
  
      let totalMinutesStart = startHour * 60 + startMinute;
      let totalMinutesEnd = endHour * 60 + endMinute;
  
      if (totalMinutesStart > totalMinutesEnd) {
        totalMinutesEnd += 24 * 60;
      }
  
      const differenceMinutes = totalMinutesEnd - totalMinutesStart;
      if (differenceMinutes === 0) {
        const productionHours = '24 Hours'; // Set to 24 hours
        this.dailyReportForm.get('production_hours')?.setValue(productionHours);
      } else {
        const hours = Math.floor(differenceMinutes / 60);
        const minutes = differenceMinutes % 60;
        const productionHours = `${hours} Hours ${minutes} minutes`;
        this.dailyReportForm.get('production_hours')?.setValue(productionHours);
      }
    }
  }   

  updateCalculateProductionHours() {
    const startTime = this.editDailyReportForm.get('start')?.value;
    const endTime = this.editDailyReportForm.get('finish')?.value;
  
    if (startTime && endTime) {
      const startTimeParts = startTime.split(':');
      const endTimeParts = endTime.split(':');
      const startHour = parseInt(startTimeParts[0], 10);
      const startMinute = parseInt(startTimeParts[1], 10);
      const endHour = parseInt(endTimeParts[0], 10);
      const endMinute = parseInt(endTimeParts[1], 10);

      let totalMinutesStart = startHour * 60 + startMinute;
      let totalMinutesEnd = endHour * 60 + endMinute;
  
      if (totalMinutesStart > totalMinutesEnd) {
        totalMinutesEnd += 24 * 60;
      }

      const differenceMinutes = totalMinutesEnd - totalMinutesStart;
      if (differenceMinutes === 0) {
        const productionHours = '24 Hours'; // Set to 24 hours
        this.editDailyReportForm.get('production_hours')?.setValue(productionHours);
      } else {
        const hours = Math.floor(differenceMinutes / 60);
        const minutes = differenceMinutes % 60;
        const productionHours = `${hours} Hours ${minutes} minutes`;
        this.editDailyReportForm.get('production_hours')?.setValue(productionHours);
      }
    }
  } 

  getAllDaily() {
      this.isConnected = true;
      this.apiService.getTabelViewProcess().subscribe({
        next: (res: any) => {
          if (res.status) {
            this.dailyData = res.data;
            this.dailyDataByLine = this.dailyData.filter(item => item.id_line == this.idLine)
            this.fiterReportDaily = this.dailyDataByLine;
            this.totalRecords = this.fiterReportDaily.length;
            this.setPaginationData();
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
              this.isConnected = false;
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        },
      });
    }

    getByIdDaily(id:any) {
      this.isConnected = true;
      this.apiService.getByIdDaily(id).subscribe({
        next: (res: any) => {
          if (res.status) {
            this.dailyDataById = res.data
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
              this.isConnected = false;
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        },
      });
    }

    getByUser() {
      this.isConnected = true;
      this.apiService.getAllUsers().subscribe({
        next: (res: any) => {
          if (res.status) {
            this.userRole = res.data.filter((user: any) => user.role_id === 4);
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
              this.isConnected = false;
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        },
      });
    }

    getShowingText(): string {
      const startIndex = (this.page - 1) * this.pageSize + 1;
      const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
      return `Showing ${startIndex} - ${endIndex}`;
    }
  
    onPageSizeChange() {
      this.page = 1;
      this.setPaginationData();
    }
    
    setPaginationData() {
      this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
    }

    onBack(){
      this.router.navigate(['production/report/daily/history'], {queryParams:{ lineId: this.idLine}})
    }
}
