import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyReportComponent } from './daily-report/daily-report.component';
import { DailyHistoryComponent } from './daily-history/daily-history.component';
import { EksportsComponent } from './eksports/eksports.component';
import { DailyActivityComponent } from './daily-activity/daily-activity.component';

const routes: Routes = [
  {
    path:'',
    component: DailyReportComponent,
  },
  {
    path:'proses',
    component: DailyReportComponent,
  },

  {
    path:'history',
    component: DailyHistoryComponent,
  },

  {
    path:'history/:id',
    component:  EksportsComponent,
  },

  {
    path:'activity',
    component:  DailyActivityComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyRoutingModule { }
