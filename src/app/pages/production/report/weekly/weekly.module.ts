import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeeklyRoutingModule } from './weekly-routing.module';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask';
import { ProductionModule } from '../../production.module';
import { ReportModule } from '../report.module';
import { ResumeComponent } from './rakor-mom/resume/resume.component';
import { NextplanComponent } from './rakor-mom/nextplan/nextplan.component';
import { UpdateTalComponent } from './rakor-mom/update-tal/update-tal.component';
import { WeklyReportComponent } from './wekly-report/wekly-report.component';
import { ResumeAddComponent } from './rakor-mom/resume-add/resume-add.component';
import { ResumeUpdateComponent } from './rakor-mom/resume-update/resume-update.component';
import { ResumeMomComponent } from './rakor-mom/resume-mom/resume-mom.component';
import { RakorMomDetailComponent } from './rakor-mom/rakor-mom-detail/rakor-mom-detail.component';
import { WeeklyAddComponent } from './weekly-add/weekly-add.component';
import { WeeklyViewComponent } from './weekly-view/weekly-view.component';
import { MappingComponent } from './mapping/mapping.component';
import { MappingAddComponent } from './mapping/mapping-add/mapping-add.component';


@NgModule({
  declarations: [
    WeklyReportComponent,
    ResumeComponent,
    NextplanComponent,
    UpdateTalComponent,
    ResumeAddComponent,
    ResumeUpdateComponent,
    ResumeMomComponent,
    RakorMomDetailComponent,
    WeeklyAddComponent,
    WeeklyViewComponent,
    MappingComponent,
    MappingAddComponent,
  ],

  imports: [
    CommonModule,
    WeeklyRoutingModule,
    ProductionModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    ReportModule,
  ]
})
export class WeeklyModule { }
