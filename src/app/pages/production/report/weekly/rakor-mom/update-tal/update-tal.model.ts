export interface NextPlan {
    task: string;
    due_date: string;
    id_pic: number | null;
    actual: string;
    remark: string;
    id_rakormom : number | null;
}