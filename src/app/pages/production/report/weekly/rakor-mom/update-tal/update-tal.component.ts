import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NextPlan } from './update-tal.model';


@Component({
  selector: 'app-update-tal',
  templateUrl: './update-tal.component.html',
  styleUrls: ['./update-tal.component.scss']
})
export class UpdateTalComponent implements OnInit{
  public nextplan:any [] = [];

  //roll akses
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userData: any;

  //data
  filterdataupdate: any[] = [];
  projectname: string[] = [];

  isConnected: boolean | undefined;
  userRole: any [] = [];
  userRoleByLine:any;
  dataNextPlan: any;
  updateId: any;
  dataEdit: any;
  idParam:any;
  idLine:any;

  newNextPlan: NextPlan = {
    task: '',
    id_pic: null,
    due_date: '',
    actual: '',
    remark: '',
    id_rakormom: null,
  };
  
  constructor(
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private router: Router,
    private route : ActivatedRoute
    ) {
  }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getByUser();
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
  }

    formatDate(date: string): string {
      const formattedDate = new Date(date);
      const monthNames = [
        'Januari', 'Februari', 'Maret', 'April',
        'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
      ];
      const day = formattedDate.getDate();
      const monthIndex = formattedDate.getMonth();
      const year = formattedDate.getFullYear();
      return `${day} ${monthNames[monthIndex]} ${year}`;
    }

    getByUser() {
      this.isConnected = true;
      this.apiService.getAllUsers().subscribe({
        next: (res: any) => {
          if (res.status) {
            this.userRole = res.data
            this.userRoleByLine = this.userRole.filter(item => item.role_id === 4)
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
              this.isConnected = false;
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        },
      });
    }

  insertNextPlan(index: number){
    const data = this.nextplan[index];
    const dataNextPlan = {
      task: data.task,
      id_pic: data.id_pic,
      due_date: data.due_date,
      actual: data.actual,
      remark: data.remark,
      id_rakormom: this.idParam
    };
  
    if (data.id_next_plan) {
      this.apiService.updateNextPlan(data.id_next_plan, dataNextPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertNextPlan(dataNextPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_next_plan = res.data[0];
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  }  
  
  async onSubmit() {
    await Promise.all(this.nextplan.map((data: any, index: number) => {
      return this.insertNextPlan(index);
    }));
      Swal.fire({
        icon: 'success',
        title: 'Added Successfully',
        text: 'Added has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
      this.router.navigate(['production/report/weekly/rakor/resume'], {queryParams: {lineId: this.idLine}})
  }

  async addData() {
    if(this.nextplan.length === 0){
      const lastNo = this.nextplan.length > 0 ? Math.max(...this.nextplan.map(item => item.no)) : 0;
      this.nextplan.push({
        no: lastNo + 1,
        task: '',
        id_pic: '',
        due_date: '',
        actual: '',
        remark: '',
        id_rakormom: this.idParam
      });
    } else{
      this.nextplan.forEach((data: any, index: number) => {
        if (!data.isUpdated) {
          this.insertNextPlan(index);
        }
      });
      const lastNo = this.nextplan.length > 0 ? Math.max(...this.nextplan.map(item => item.no)) : 0;
      this.nextplan.push({
        no: lastNo + 1,
        task: '',
        id_pic: '',
        due_date: '',
        actual: '',
        remark: '',
        id_rakormom: this.idParam
      });
    }
  }
}


