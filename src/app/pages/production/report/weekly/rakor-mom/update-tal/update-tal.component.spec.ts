import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTalComponent } from './update-tal.component';

describe('UpdateTalComponent', () => {
  let component: UpdateTalComponent;
  let fixture: ComponentFixture<UpdateTalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateTalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateTalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
