import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DropzoneDirective, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ActionPlan } from '../nextplan/nextplan.model';
import { NextPlan } from '../update-tal/update-tal.model';

@Component({
  selector: 'app-resume-update',
  templateUrl: './resume-update.component.html',
  styleUrls: ['./resume-update.component.scss']
})
export class ResumeUpdateComponent implements OnInit {
  breadCrumbItems!: Array<{}>;
  public actionplan:any [] = [];
  public nextplan:any [] = [];
  fileName: any = [];
  username: any[] = [];
  isConnected: boolean = true;
  update!: UntypedFormGroup;
  userData: any [] = [];
  userRoleByLine: any
  oldImages: string[] = [];
  img_rakormom: any =[];
  isAdmin: boolean | undefined;
  isEmployee: boolean | undefined;
  isSpv: boolean | undefined;
  data: any;
  idParam:any;
  idLine:any;

  newActionPlan: ActionPlan = {
    description: '',
    id_pic: null,
    due_date: '',
    remark: '',
    id_rakormom: null,
  };

  newNextPlan: NextPlan = {
    task: '',
    id_pic: null,
    due_date: '',
    actual: '',
    remark: '',
    id_rakormom: null,
  };
  
  constructor(
    private formBuilder: FormBuilder, 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  isEditingProblem = false;
  startEditingProblem() {
    this.isEditingProblem = true;
  }
  cancelEditingProblem() {
    this.isEditingProblem = false;
  }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getActionPlanByRakorId(this.idParam)
    this.getNextPlanByRakorId(this.idParam)
    this.getByUser();
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();

    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.apiService.getByIdRakorMOM(id).subscribe((response: any) => {
          this.data = response.data[0];
          const participantsArray = this.data.participants.split(',');
          this.update.patchValue({
            id_rakormom: this.data.id_rakormom,
            participants: participantsArray,
            date: this.data.date,
            place: this.data.place,
            img_rakormom: this.img_rakormom,
            project_name: this.data.project_name,
            points_discussed: this.data.points_discussed,
            status: this.data.status,
          });
        }, error => {
          console.error('Error:', error);
        });
        
      }
    });

    this.update = this.formBuilder.group({
      id_rakormom: [null],
      participants: [null],
      date: [null],
      place: [null],
      img_rakormom: [null],
      project_name: [null],
      points_discussed: [null],
      status: [null],
    })
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_rakormom}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
    autoProcessQueue: true,
    clickable: '.dropzone_sec',
    maxFilesize: 3, 
    maxFiles: 5,
  };
  
  dropzoneResponse: any;

  async addDataActionPlan() {
    if (this.actionplan.length === 0){
      const lastNo = this.actionplan.length > 0 ? Math.max(...this.actionplan.map(item => item.no)) : 0;
      this.actionplan.push({
        no: lastNo + 1,
        description: '',
        id_pic: '',
        due_date: '',
        remark: '',
        id_rakormom: this.idParam
      });
    } else {
      this.actionplan.forEach((data: any, index: number) => {
        if (!data.isUpdated) {
          this.insertActionPlan(index);
        }
      });
      const lastNo = this.actionplan.length > 0 ? Math.max(...this.actionplan.map(item => item.no)) : 0;
      this.actionplan.push({
        no: lastNo + 1,
        description: '',
        id_pic: '',
        due_date: '',
        remark: '',
        id_rakormom: this.idParam
      });
    }
  }

  async addDataNextPlan() {
    if (this.nextplan.length === 0){
      const lastNo = this.nextplan.length > 0 ? Math.max(...this.nextplan.map(item => item.no)) : 0;
    this.nextplan.push({
      no: lastNo + 1,
      task: '',
      id_pic: '',
      due_date: '',
      actual: '',
      remark: '',
      id_rakormom: this.idParam
    });
    } else {
      this.nextplan.forEach((data: any, index: number) => {
        if (!data.isUpdated) {
          this.insertNextPlan(index);
        }
      });
      const lastNo = this.nextplan.length > 0 ? Math.max(...this.nextplan.map(item => item.no)) : 0;
      this.nextplan.push({
        no: lastNo + 1,
        task: '',
        id_pic: '',
        due_date: '',
        actual: '',
        remark: '',
        id_rakormom: this.idParam
      });
    }
  }

  insertActionPlan(index: number){
    const data = this.actionplan[index];
    const dataActionPlan = {
      description: data.description,
      id_pic: data.id_pic,
      due_date: data.due_date,
      remark: data.remark,
      id_rakormom: this.idParam
    };
  
    if (data.id_action_plan) {
      this.apiService.updateActionPlan(data.id_action_plan, dataActionPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertActionPlan(dataActionPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_action_plan = res.data[0];
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  } 

  insertNextPlan(index: number){
    const data = this.nextplan[index];
    const dataNextPlan = {
      task: data.task,
      id_pic: data.id_pic,
      due_date: data.due_date,
      actual: data.actual,
      remark: data.remark,
      id_rakormom: this.idParam
    };
  
    if (data.id_next_plan) {
      this.apiService.updateNextPlan(data.id_next_plan, dataNextPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertNextPlan(dataNextPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_next_plan = res.data[0];
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  }

  onUpdate() {
    const id = this.update.get('id_rakormom')?.value;
    const points_discussed = document.getElementById('points_discussed') as HTMLTextAreaElement;
    const project_name = this.update.value.project_name;
    const participants = this.update.get('participants')?.value;
    const date = this.update.get('date')?.value;
    const place = this.update.value.place;
    const participantsString = Array.isArray(participants) ? participants.join(',') : '';
    if (this.img_rakormom.length === 0) {
      this.img_rakormom = this.data.img_rakormom ? this.data.img_rakormom.split(',') : [];
    }

    const postData = {
        id_rakormom: id,
        project_name: project_name,
        participants: participantsString,
        date: date,
        place: place,
        points_discussed: points_discussed.value,
        img_rakormom: this.img_rakormom.join(','),
    };
            this.apiService.updateRakorMOM(id,postData).subscribe({
              next: (res: any) => {
                if (res.status) {
                  this.actionplan.forEach((data: any, index:number) => {
                    this.insertActionPlan(index);
                  });
                  this.nextplan.forEach((data: any, index:number) => {
                    this.insertNextPlan(index);
                  });
                  Swal.fire({
                    icon: 'success',
                    title: 'Update Successfully',
                    text: 'Update has been successfully.',
                    timer: 1500,
                    showConfirmButton: false,
                  });
                  this.ngOnInit();
                }
              },
              error: (err: any) => {
                if (err.error && err.error.code === 'ER_DUP_ENTRY') {
                  Swal.fire({
                    title: 'Oops...',
                    text: 'Nama proyek sudah ada di sisi server. Mohon pilih nama proyek yang berbeda.!',
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: 'rgb(243, 78, 78)',
                  })
                  return
                } else {
                  console.error(err);
                }
              }
  })
}

getActionPlanByRakorId(id:any){
  this.apiService.getActionPlanByRakorId(id).subscribe(
    (res:any) =>{
      this.actionplan = res.data;
      this.convertDateFields(this.actionplan)
  })
}

getNextPlanByRakorId(id:any){
  this.apiService.getNextPlanByRakorId(id).subscribe(
    (res:any) =>{
      this.nextplan = res.data;
      this.convertDateFields(this.nextplan)
  })
}

onUploadImgRakorMOM(event: DropzoneEvent) {
  this.dropzoneResponse = event[1];
  if (this.dropzoneResponse !== undefined) {
    this.fileName = this.dropzoneResponse.filename;
    this.img_rakormom.push(this.fileName);
  }
}

getByUser(): void {
  this.apiService.getAllUsers().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.username = res.data
        this.userRoleByLine = this.username.filter(item => item.role_id === 3 || item.role_id === 4)
      } else {
        console.error(`${res.data.message}`);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      }
    },
    error: (err: any) => {
      console.error(err);
      setTimeout(() => {
        this.isConnected = false;
      }, 1000);
    },
  });
}

  convertDateFields(data: any[]) {
    if (data) {
      for (let item of data) {
        if (item.due_date) {
          item.due_date = this.convertDate(item.due_date);
        }
        if (item.start_date) {
          item.start_date = this.convertDate(item.start_date);
        }
        if (item.end_date) {
          item.end_date = this.convertDate(item.end_date);
        }
      }
    }
  }

  onDeleteActionPlan(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          appear: 'nonactive',
        };
        this.apiService.updateActionPlan(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onDeleteNextPlan(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          appear: 'nonactive',
        };
        this.apiService.updateNextPlan(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  goBack(){
    this.router.navigate(['production/report/weekly/rakor/resume'], {queryParams:{lineId: this.idLine}});
  }
}  
