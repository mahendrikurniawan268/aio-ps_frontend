import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeMomComponent } from './resume-mom.component';

describe('ResumeMomComponent', () => {
  let component: ResumeMomComponent;
  let fixture: ComponentFixture<ResumeMomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumeMomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResumeMomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
