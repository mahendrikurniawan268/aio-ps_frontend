import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RakorMomDetailComponent } from './rakor-mom-detail.component';

describe('RakorMomDetailComponent', () => {
  let component: RakorMomDetailComponent;
  let fixture: ComponentFixture<RakorMomDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RakorMomDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RakorMomDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
