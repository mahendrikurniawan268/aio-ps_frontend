import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-rakor-mom-detail',
  templateUrl: './rakor-mom-detail.component.html',
  styleUrls: ['./rakor-mom-detail.component.scss']
})
export class RakorMomDetailComponent implements OnInit {
  breadCrumbItems!: Array<{}>;
  isConnected: boolean | undefined;
  imageUrl = `${environment.API_URL}${environment.getImageRakor}`;
  resumeDetailId: any;
  dataUpdateTal: any;

  constructor(
    public apiservice: ApiService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  id: number | undefined;
  data: any;
  dataNextPlan: any[]=[];
  dataUpdate: any;
  idLine:any;

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.route.params.subscribe(params => {
      const id = params['id'];

      // Get data from getByIdRakorMOM
      this.apiservice.getByIdRakorMOM(id).subscribe({
        next: (res: any) => {
          this.data = res.data[0];
          this.data.project_name = this.data.project_name;
        this.data.participants = this.data.participants;
        this.data.date = this.data.date;
        this.data.points_discussed = this.data.points_discussed;
        this.data.img_rakormom = this.data.img_rakormom.split(',');
          console.log('Data from getByIdRakorMOM:', this.data);
        },
        error: (err: any) => {
          console.error('Error while fetching data from getByIdRakorMOM:', err);
        }
      });

      // Get data from getJoinNextPlan
      this.apiservice.getActionPlanViewPic(id).subscribe({
        next: (res: any) => {
          this.dataNextPlan = res.data;
          console.log(this.dataNextPlan);
          
        },
        error: (err: any) => {
          console.error('Error while fetching data from getJoinNextPlan:', err);
        }
      });
      this.apiservice.getNextPlanViewPic(id).subscribe({
        next: (res: any) => {
          this.dataUpdateTal = res.data;
        },
        error: (err: any) => {
          console.error('Error while fetching data from getJoinUpdate:', err);
        }
      });
    });
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

  formatTime(time: string): string {
    const timeParts = time.split(':');
    const hours = timeParts[0];
    const minutes = timeParts[1];
    return `${hours}:${minutes}`;
  }

  goBack(){
    this.router.navigate(['production/report/weekly/rakor/resume'], {queryParams: {lineId: this.idLine}});
  }
}

