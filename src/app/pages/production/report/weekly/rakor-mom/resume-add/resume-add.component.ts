import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { DropzoneComponent, DropzoneConfigInterface, DropzoneDirective } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Route, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resume-add',
  templateUrl: './resume-add.component.html',
  styleUrls: ['./resume-add.component.scss'],
  providers: [DropzoneDirective],
})
export class ResumeAddComponent implements OnInit {

  breadCrumbItems!: Array<{}>;
  fileName: any = [];
  username: string[] = [];
  isConnected: boolean = true;
  rakorMOMAdd!: UntypedFormGroup;
  userData: any = [];
  img_rakormom: any =[];
  ceknameproject: any;
  idLine:any;

  constructor(
    private formBuilder: FormBuilder, 
    private apiService: ApiService,
    private route : ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.getByUser();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})

    this.username = this.userData.name
    this.rakorMOMAdd = this.formBuilder.group({
      project_name: [null, [Validators.required]],
      participants: [null],
      date: [null],
      place: [null],
      points_discussed: [null],
      img_rakormom: [null],
    });
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_rakormom}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
    autoProcessQueue: true,
    clickable: '.dropzone_sec',
    maxFilesize: 3, 
    maxFiles: 5,
  };
  
  dropzoneResponse: any;

  onUploadImgRakorMOM(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename;
      this.img_rakormom.push(this.fileName);
    }
  }
  
  initializeForm(): void {
    this.rakorMOMAdd = this.formBuilder.group({
      project_name: [''],
      participants: [''],
      date: [''],
      place: [''],
      points_discussed: [''],
      img_rakormom: [''],
    });
  }

  getByUser(): void {
    this.apiService.getAllUsers().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.username = res.data
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      },
    });
  }
  
  onFileChange(event: any): void {
    const fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.apiService.insertImageRakor(file).subscribe(
        response => {
          console.log('File berhasil diunggah', response);
        },
        error => {
          console.error('Terjadi kesalahan saat mengunggah file', error);
        }
      );
    }
  }

  onSubmit() {
    const points_discussed = document.getElementById('points_discussed') as HTMLTextAreaElement;
    const project_name = this.rakorMOMAdd.value.project_name;
    const participants = this.rakorMOMAdd.get('participants')?.value;
    const date = this.rakorMOMAdd.get('date')?.value;
    const place = this.rakorMOMAdd.value.place;
    const participantsString = Array.isArray(participants) ? participants.join(',') : '';
    const postData = {
      project_name: project_name,
      id_line: this.idLine,
      participants: participantsString,
      date: date,
      place: place,
      points_discussed: points_discussed.value,
      img_rakormom: this.img_rakormom.join(','),
    };

    this.apiService.insertRakorMOM(postData).subscribe({
        next: (res: any) => {
            if (res.status) {
                const newId = res.data;
                console.log(newId);
                this.router.navigate([`production/report/weekly/rakor/nextplan/${newId}`], {queryParams: {lineId: this.idLine}});
            }
        },
        error: (err: any) => {
            if (err.error && err.error.code === 'ER_DUP_ENTRY') {
                Swal.fire({
                    title: 'Oops...',
                    text: 'Nama proyek sudah ada di sisi server. Mohon pilih nama proyek yang berbeda.!',
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: 'rgb(243, 78, 78)',
                })
                return
            } else {
                console.error(err);
            }
        }
    });
}


  formatDate(date: string): string {
    const formattedDate = new Date(date);

    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

    /**
   * cancel sweet alert
   * @param confirm modal content
   */
    confirm() {
      this.onSubmit();
    }
}  
