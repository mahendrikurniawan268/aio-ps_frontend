import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit{
  UpdateStatus!: UntypedFormGroup;
  breadCrumbItems!: Array<{}>;
  submitted = false;
  searchTerm: string = ''; 

  //pages
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;

  //roll akses
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  idLine: any;

  //data
  filterdatarakor: any[] = [];
  resumeId: any[] = [];
  username: any[] = [];
  dataRakor: any[] = [];
  dataRakorByLine: any;
  resumeDetailId: any;
  nextplanId: any;
  updateId: any;
  startDate: string = '';
  endDate: string = '';
  constructor(
    private apiService: ApiService,
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private formBuilder: UntypedFormBuilder, 
    ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getAllRakorMOM();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/dashboard-prod' },
      { label: 'Weekly', link: '/dashboard-prod' },
      { label: 'Rakor MOM', link: '/dashboard-prod' },
      { label: 'Resume MOM', active: true},
    ];

    this.UpdateStatus = this.formBuilder.group({
      status: [null],
    })
  }

    formatDate(date: string): string {
      const formattedDate = new Date(date);
      const monthNames = [
        'Januari', 'Februari', 'Maret', 'April',
        'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
      ];
      const day = formattedDate.getDate();
      const monthIndex = formattedDate.getMonth();
      const year = formattedDate.getFullYear();
      return `${day} ${monthNames[monthIndex]} ${year}`;
    }

        /**
   * Open modal
   * @param content modal content
   */
        openModal(content: any) {
          this.submitted = false;
          this.modalService.open(content, { size: 'md', centered: true });
        }

    getAllRakorMOM() {
      this.apiService.getAllRakorMOM().subscribe({
        next: (res: any) => {
          if (res.status) {
            this.dataRakor = res.data.filter((item: any) => item.appear === 'active');
            this.dataRakorByLine = this.dataRakor.filter(item => item.id_line == this.idLine)
            this.filterdatarakor = this.dataRakorByLine;
            this.totalRecords = this.filterdatarakor.length;
            this.setPaginationData();
          } else {
            console.error(`${res.data.message}`);
            setTimeout(() => {
            }, 1000);
          }
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        },
      });
    }

    getByIdRakorMOM(id: any) {
      this.apiService.getByIdRakorMOM(id).subscribe({
        next: (res: any) => {
          this.resumeDetailId = res;
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        }
      });
    }

    getAllJoinRakormom(id: any) {
      this.apiService.getByIdRakorMOM(id).subscribe({
        next: (res: any) => {
          this.resumeDetailId = res;
          this.router.navigate(['production/report/weekly/rakor/resume/detail/', id], {queryParams: {lineId: this.idLine}, state: { data: this.resumeDetailId }});
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        }
      });
      this.apiService.getActionPlanByRakorId(id).subscribe({
        next: (res: any) => {
          this.nextplanId = res;
          console.log('log next dapat',this.nextplanId);
          this.router.navigate(['production/report/weekly/rakor/resume/detail/', id], {queryParams: {lineId: this.idLine}, state: { data: this.nextplanId } });
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        }
      });
      this.apiService.getNextPlanByRakorId(id).subscribe({
        next: (res: any) => {
          this.updateId = res;
          console.log('log update dapat',this.updateId);
          this.router.navigate(['production/report/weekly/rakor/resume/detail/', id], {queryParams: {lineId: this.idLine}, state: { data: this.updateId } });
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        }
      });
    }
    
    getByIdRakorMOMEdit(id: any) {
      console.log(id);
      this.apiService.getByIdRakorMOM(id).subscribe({
        next: (res: any) => {
          this.resumeId = res;
          console.log(this.resumeId);
          this.router.navigate(['production/report/weekly/rakor/resume/', id], {queryParams: {lineId: this.idLine}, state: { data: this.resumeId } });
        },
        error: (err: any) => {
          console.error(err);
          setTimeout(() => {
          }, 1000);
        }
      });
    }

AddActivity(){
  this.router.navigate(['/production/report/weekly/rakor/resume/add'], {queryParams: {lineId: this.idLine}});
}

confirm(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        appear: 'nonactive',
      };
      this.apiService.updateRakorMOM(id, data).subscribe({
        next: (res: any) => {
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onUpdate(id: any) {
  Swal.fire({
    title: 'Are you sure you want to change your status?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, do it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        status: 'closed',
      };
      this.apiService.updateRakorMOM(id, data).subscribe({
        next: (res: any) => {
          console.log('Respon', res);
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onSearch(): void {
  if (!this.searchTerm) {
    this.filterdatarakor = this.dataRakorByLine;
  } else {
    const searchTermLower = this.searchTerm.toLowerCase();
    this.filterdatarakor = this.dataRakorByLine.filter((data: 
      { project_name: string, 
        participants: string, 
        date: string, 
        remark: string, 
        actual: string 
      }) => {
      return (
        (data.project_name && data.project_name.toString().toLowerCase().includes(searchTermLower)) ||
        (data.participants && data.participants.toString().toLowerCase().includes(searchTermLower)) ||
        (data.date && this.isDateMatching(data.date, searchTermLower)) ||
        (data.remark && data.remark.toString().toLowerCase().includes(searchTermLower)) ||
        (data.actual && data.actual.toString().toLowerCase().includes(searchTermLower))
      );
    });
  }
}

filter() {
  this.filterdatarakor = this.dataRakorByLine.filter((item: { date: string;}) => {
    const itemDate = new Date(item.date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;
    
    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.setPaginationData();
}

isDateMatching(dateString: string, searchTerm: string): boolean {
  try {
    const date = new Date(dateString);
    if (!isNaN(date.getTime())) {
      return date.toISOString().toLowerCase().includes(searchTerm);
    }
  } catch (error) {
    console.error('Error parsing date:', error);
  }
  return false;
}


getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1;
  this.endIndex = this.pageSize;
}

setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

}


