import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NextplanComponent } from './nextplan.component';

describe('NextplanComponent', () => {
  let component: NextplanComponent;
  let fixture: ComponentFixture<NextplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NextplanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NextplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
