export interface ActionPlan {
    description: string;
    due_date: string;
    id_pic: number | null;
    remark: string;
    id_rakormom : number | null;
}