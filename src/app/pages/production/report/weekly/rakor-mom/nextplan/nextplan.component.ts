import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';
import { ActionPlan } from './nextplan.model';

@Component({
  selector: 'app-nextplan',
  templateUrl: './nextplan.component.html',
  styleUrls: ['./nextplan.component.scss']
})
export class NextplanComponent  implements OnInit{
  public actionplan:any [] = [];

  //roll akses
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userData: any;
  dataActionPlan: any;
  filterdataActionPlan: any;
  pctionplanId: any;
  userRole: any [] = [];
  userRoleByLine:any
  isConnected: boolean | undefined;
  idParam:any;
  idLine:any;

  newActionPlan: ActionPlan = {
    description: '',
    id_pic: null,
    due_date: '',
    remark: '',
    id_rakormom: null,
  };
  
  constructor(
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private router: Router,
    private route : ActivatedRoute, 
    ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getByUser();
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
  }

  insertActionPlan(index: number){
    const data = this.actionplan[index];
    const dataActionPlan = {
      description: data.description,
      id_pic: data.id_pic,
      due_date: data.due_date,
      remark: data.remark,
      id_rakormom: this.idParam
    };
  
    if (data.id_action_plan) {
      this.apiService.updateActionPlan(data.id_action_plan, dataActionPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertActionPlan(dataActionPlan).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_action_plan = res.data[0];
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  }  
  
  async onSubmit() {
    await Promise.all(this.actionplan.map((data: any, index: number) => {
      return this.insertActionPlan(index);
    }));
    this.router.navigate([`production/report/weekly/rakor/update-tal/${this.idParam}`], {queryParams: {lineId: this.idLine}})
  }

  async addData() {
    if (this.actionplan.length === 0){
      const lastNo = this.actionplan.length > 0 ? Math.max(...this.actionplan.map(item => item.no)) : 0;
      this.actionplan.push({
        no: lastNo + 1,
        description: '',
        id_pic: '',
        due_date: '',
        remark: '',
        id_rakormom: this.idParam
      });
    } else {
      this.actionplan.forEach((data: any, index: number) => {
        if (!data.isUpdated) {
          this.insertActionPlan(index);
        }
      });
      const lastNo = this.actionplan.length > 0 ? Math.max(...this.actionplan.map(item => item.no)) : 0;
      this.actionplan.push({
        no: lastNo + 1,
        description: '',
        id_pic: '',
        due_date: '',
        remark: '',
        id_rakormom: this.idParam
      });
    }
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

  getByUser() {
    this.isConnected = true;
    this.apiService.getAllUsers().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.userRole = res.data
          this.userRoleByLine = this.userRole.filter(item => item.role_id === 4)
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      },
    });
  }
}



