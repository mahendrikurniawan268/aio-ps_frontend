
import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-wekly-report',
  templateUrl: './wekly-report.component.html',
  styleUrls: ['./wekly-report.component.scss']
})
export class WeklyReportComponent implements OnInit{
  breadCrumbItems!: Array<{}>;
  submitted = false;

  //pages
  totalRecords: any; 
  page: number = 1;
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;
  totalPages: number = 0;
  searchTerm: string = '';

  //roll akses
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;

  //data
  filterdata: any;
  dataWeekly: any[] = [];
  dataWeeklyByLine: any;
  weeklyId: any;
  idLine:any;
  startDate: string = '';
  endDate: string = '';
  
  constructor(
    private apiService: ApiService,
    private authService: AuthenticationService,
    private router: Router, private route:ActivatedRoute
    ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getAllWeeklyReport();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/dashboard-prod' },
      { label: 'Weekly', link: '/dashboard-prod' },
      { label: 'Activity', active: true},
    ];
  }

    formatDate(date: string): string {
      const formattedDate = new Date(date);
      const monthNames = [
        'Januari', 'Februari', 'Maret', 'April',
        'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
      ];
  
      // Ambil tanggal, bulan, dan tahun
      const day = formattedDate.getDate();
      const monthIndex = formattedDate.getMonth();
      const year = formattedDate.getFullYear();
  
      // Format tanggal dalam bentuk "DD MMMM YYYY"
      return `${day} ${monthNames[monthIndex]} ${year}`;
    }
    
    getAllWeeklyReport(){
      this.apiService.getTabelViewWeekly().subscribe(
        (res:any) => {
          this.dataWeekly = res.data;
          this.dataWeeklyByLine = this.dataWeekly.filter(item => item.id_line == this.idLine)
          this.filterdata = this.dataWeeklyByLine;
          this.setPaginationData();
        },
        (error:any) => {
          console.error(error)
        }
      );
    }

  getByIdFollow(id: any) {
    this.apiService.getByIdFollowup(id).subscribe({
      next: (res: any) => {
        this.weeklyId = res;
        this.router.navigate(['production/report/weekly/followup', id], { state: { data: this.weeklyId } });
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

AddActivity(){
  this.router.navigate(['/production/report/weekly/activity/add'], { queryParams: {lineId: this.idLine}});
}

onEdit(id: number){
  this.router.navigate([`/production/report/weekly/activity/add/`, id], { queryParams: {lineId: this.idLine}});
}

onView(id:any){
  this.router.navigate([`/production/report/weekly/activity/view/`], { queryParams: { idParam: id, lineId: this.idLine }});
}

confirm(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        status: 0,
      };
      this.apiService.updateActivityWeekly(id, data).subscribe({
        next: (res: any) => {
          console.log('Respon', res);
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onDone(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        id_status: 2,
      };
      this.apiService.updateActivityWeekly(id, data).subscribe({
        next: (res: any) => {
          console.log('Respon', res);
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onRevisi(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#34c38f',
    cancelButtonColor: '#f46a6a',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.value) {
      const data = {
        id_status: 3,
      };
      this.apiService.updateActivityWeekly(id, data).subscribe({
        next: (res: any) => {
          console.log('Respon', res);
          this.ngOnInit();
        },
        error: (err) => {
          console.error('Kesalahan saat mengirim permintaan update:', err);
        }
      });
    }
  });
}

onSearch(): void {
  if (!this.searchTerm) {
    this.filterdata = this.dataWeeklyByLine;
  } else {
    const searchTermLower = this.searchTerm.toLowerCase();
    this.filterdata = this.dataWeeklyByLine.filter((data: 
      { name: string, 
        task: string, 
        due_start: string, 
        remark: string, 
        next_plan: string ,
        action_plan: string
      }) => {
      return (
        (data.name && data.name.toString().toLowerCase().includes(searchTermLower)) ||
        (data.task && data.task.toString().toLowerCase().includes(searchTermLower)) ||
        (data.due_start && this.isDateMatching(data.due_start, searchTermLower)) ||
        (data.remark && data.remark.toString().toLowerCase().includes(searchTermLower)) ||
        (data.action_plan && data.action_plan.toString().toLowerCase().includes(searchTermLower)) ||
        (data.next_plan && data.next_plan.toString().toLowerCase().includes(searchTermLower))
      );
    });
  }
}

filter() {
  this.filterdata = this.dataWeeklyByLine.filter((item: { start_date: string;}) => {
    const itemDate = new Date(item.start_date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;
    
    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.setPaginationData();
}

isDateMatching(dateString: string, searchTerm: string): boolean {
  try {
    const date = new Date(dateString);
    if (!isNaN(date.getTime())) {
      return date.toISOString().toLowerCase().includes(searchTerm);
    }
  } catch (error) {
    console.error('Error parsing date:', error);
  }
  return false;
}

getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1; // Reset startIndex
  this.endIndex = this.pageSize; // Update endIndex
}

setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

getStatusLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'info'; // New
      case 2:
          return 'secondary'; // On Progress
      case 3:
          return 'danger'; // Done
      default:
          return 'secondary';
  }
}

getStatusLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'On Progress';
      case 2:
          return 'Done';
      case 3:
          return 'Revisi';
      default:
          return 'Unknown';
  }
}

}


