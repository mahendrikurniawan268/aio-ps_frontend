import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';
import { ApiService } from 'src/app/core/services/api.service';
import { Mapping } from './mapping.model';

@Component({
  selector: 'app-mapping',
  templateUrl: './mapping.component.html',
  styleUrls: ['./mapping.component.scss']
})
export class MappingComponent implements OnInit{
  index: number = 0;
  dataMapping: any[] = [];
  dataMappingByLine:any;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  idLine:any;
  idArea:any;

  newMapping : Mapping = {
    id_line: null,
    date: '',
    img_mapping: ''
  }

  currentPage = 1;
  itemsPerPage = 10;

  constructor(private router:Router, private apiService:ApiService, private authService:AuthenticationService, private lightbox : Lightbox,
    private route: ActivatedRoute){}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
      this.getMapping();
    }})
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  openLightboxMapping(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Mapping',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImgMapping(file: any) {
    return environment.API_URL + '/image/' + file
  }
  
  onAddData(){
    this.router.navigate(['production/report/weekly/mapping/add'], {queryParams: {lineId: this.idLine}})
  }

  onEditData(id:any) {
    this.router.navigate([`production/report/weekly/mapping/edit/${id}`], {queryParams: {lineId: this.idLine}});
  }

  getMapping(){
    this.apiService.getAllMapping().subscribe(
      (res:any) => {
        this.dataMapping = res.data;
        this.dataMappingByLine = this.dataMapping.filter(item => item.id_line == this.idLine)
        this.preData = [...this.dataMappingByLine];
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateMapping(dataMappingByLine: any, id:any) {
    this.apiService.updateMapping(id, dataMappingByLine).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.newMapping = dataMappingByLine;
          this.getMapping()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateMapping(deleteData, id);
      }
    });
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataMappingByLine.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataMappingByLine = this.preData.filter((item: { date: string; }) => {
      const itemDate = new Date(item.date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.paginateData();
  }
}
