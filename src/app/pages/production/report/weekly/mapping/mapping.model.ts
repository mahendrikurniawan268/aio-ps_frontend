export interface Mapping {
    id_line: number | null;
    date: string;
    img_mapping: string;
}