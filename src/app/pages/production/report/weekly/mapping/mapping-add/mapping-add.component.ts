import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ApiService } from 'src/app/core/services/api.service';
import { Mapping } from '../mapping.model';

@Component({
  selector: 'app-mapping-add',
  templateUrl: './mapping-add.component.html',
  styleUrls: ['./mapping-add.component.scss']
})
export class MappingAddComponent implements OnInit{
  
  public mapping:any;
  id_mapping:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  dataMapping:any;
  dataArea: any [] = [];
  area: any;
  idLine: any;
  idArea: any;

  newMapping : Mapping = {
    id_line: null,
    date: '',
    img_mapping: ''
  }

  constructor(
    private router: Router,
    private apiService: ApiService,
    private RestApiService: RestApiService,
    private http: HttpClient,
    private route:ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const idToEdit = params['id'];
      this.id_mapping = idToEdit
      if (idToEdit) {
        this.getMappingById(idToEdit);
      }
    });
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
  }

  getMappingById(id:any){
    this.apiService.getByIdMapping(id).subscribe(
    (res:any) => {
      this.newMapping = res.data[0]
      this.newMapping.date = this.convertDate(this.newMapping.date);
    })
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  insertMapping(dataMapping: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataMapping.img_mapping = res.uploadedFiles[0];
          
          this.newMapping.id_line = this.idLine
          this.apiService.insertMapping(dataMapping).subscribe(
            (res: any) => {
              if (res.status == true) {
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.apiService.insertMapping(dataMapping).subscribe(
        (res: any) => {
          if (res.status == true) {
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  
  
  onInsertData() {
    if (this.selectedFiles.length > 0) {
        this.insertMapping(this.newMapping);
        Swal.fire({
            title: 'Data Added',
            text: 'Data has been successfully added.',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                this.router.navigate(['production/report/weekly/mapping'], {queryParams: {lineId: this.idLine}});
            }
        });
    } else {
        Swal.fire({
            title: 'No Files Selected',
            text: 'Please select at least one file before adding data.',
            icon: 'warning',
            confirmButtonText: 'OK',
        });
    }
}

  forBack() {
    this.router.navigate(['production/report/weekly/mapping'], {queryParams: {lineId: this.idLine}});
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  updateMapping(dataMapping: any) {
    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });

        this.RestApiService.uploadMultipleImage(formData).subscribe({
            next: (res: any) => {
                this.imageUrl = res.uploadedFiles;
                const mappingFileIndex = this.selectedFiles.findIndex(file => file.type === 'img-mapping');

                if (mappingFileIndex !== -1) {
                    dataMapping.img_mapping = res.uploadedFiles[mappingFileIndex];
                }

                this.apiService.updateMapping(this.id_mapping, dataMapping).subscribe(
                    (res: any) => {
                        if (res.status == true) {
                            this.newMapping = dataMapping;
                        }
                    },
                    (error) => {
                        console.error(error)
                    }
                );
            },
            error: (err) => console.error(err),
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.apiService.updateMapping(this.id_mapping, dataMapping).subscribe(
            (res: any) => {
                if (res.status == true) {
                    this.newMapping = dataMapping;
                }
            },
            (error) => {
                console.error(error)
            }
        );
      }
  }

  onUpdate() {
      this.updateMapping(this.newMapping);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
  }
}
