import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAddComponent } from './mapping-add.component';

describe('MappingAddComponent', () => {
  let component: MappingAddComponent;
  let fixture: ComponentFixture<MappingAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MappingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
