import { Component } from '@angular/core';
import { FormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { WeeklyReport } from './weekly.report.model';

@Component({
  selector: 'app-weekly-add',
  templateUrl: './weekly-add.component.html',
  styleUrls: ['./weekly-add.component.scss']
})
export class WeeklyAddComponent {
  breadCrumbItems!: Array<{}>;
  imageUrl: any
  id_report_weekly:any;
  report_weekly:any;
  activityAdd!: UntypedFormGroup;
  selectedFiles: { file: File, type: string }[] = [];
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userData: any;
  username: any [] = [];
  userByRole:any;
  isConnected: boolean = false;
  data:any;
  idLine:any;
  idParam: any;

  newWeeklyReport : WeeklyReport = {
    id_line: null,
    id_pic: null,
    issue: '',
    task: '',
    start_date: '',
    end_date: '',
    remark: '',
    action_plan: '',
    next_plan: '',
    id_status: null,
    img_before: null,
    img_after: null
  } 

  constructor(
    private modalService: NgbModal,
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private RestApiService: RestApiService) {
  }

  ngOnInit(): void {
    this.getByUser();
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    if(this.idParam){
      this.getAllReportById(this.idParam)
    }
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.username = this.userData.name
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  insertReportWeekly(dataWeekly: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataWeekly.img_before = res.uploadedFiles[0];
          dataWeekly.img_after = res.uploadedFiles[1];
          
          this.newWeeklyReport.id_line = this.idLine
          this.newWeeklyReport.id_status = 1
          this.apiService.insertActivityWeekly(dataWeekly).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_report_weekly = res.data[0];
                this.report_weekly = dataWeekly;
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.apiService.insertActivityWeekly(dataWeekly).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_report_weekly = res.data[0];
            this.report_weekly = dataWeekly;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  

  onInsertData() {
    if (this.selectedFiles.length > 0) {
        this.insertReportWeekly(this.newWeeklyReport);
        Swal.fire({
            title: 'Data Added',
            text: 'Data has been successfully added.',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then((result) => {
          if (result.isConfirmed) {
              this.router.navigate(['production/report/weekly/activity'], {queryParams: {lineId: this.idLine}});
          }
      });;
    } else {
        Swal.fire({
            title: 'No Files Selected',
            text: 'Please select at least one file before adding data.',
            icon: 'warning',
            confirmButtonText: 'OK',
        });
    }
}

updateWeeklyReport(dataWeekly: any) {
  if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
          formData.append('files', fileData.file, fileData.file.name);
          formData.append('type', fileData.type);
      });

      this.RestApiService.uploadMultipleImage(formData).subscribe({
          next: (res: any) => {
              this.imageUrl = res.uploadedFiles;
              const beforeFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-before');
              const afterFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-after');

              if (beforeFileIndex !== -1) {
                  dataWeekly.img_before = res.uploadedFiles[beforeFileIndex];
              }
              if (afterFileIndex !== -1) {
                  dataWeekly.img_after = res.uploadedFiles[afterFileIndex];
              }

              this.apiService.updateActivityWeekly(this.idParam, dataWeekly).subscribe(
                  (res: any) => {
                      if (res.status == true) {
                          this.newWeeklyReport = dataWeekly;
                      }
                  },
                  (error) => {
                      console.error(error)
                  }
              );
          },
          error: (err) => console.error(err),
          complete: () => {
              this.selectedFiles = [];
          }
      });
  } else {
      this.apiService.updateActivityWeekly(this.idParam, dataWeekly).subscribe(
          (res: any) => {
              if (res.status == true) {
                  this.newWeeklyReport = dataWeekly;
              }
          },
          (error) => {
              console.error(error)
          }
      );
    }
}

onUpdate() {
    this.updateWeeklyReport(this.newWeeklyReport);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
}

getByUser() {
  this.apiService.getAllUsers().subscribe(
    (res: any) => {
        this.username = res.data;
        this.userByRole = this.username.filter(item => item.role_id == 2 || item.role_id == 3 || item.role_id == 4) 
    },   
    (error: any) => {
      console.error(error);    
  });
}

getAllReportById(id:any) {
  this.apiService.getByIdWeekly(id).subscribe(
    (res: any) => {
        this.newWeeklyReport = res.data[0];
    },   
    (error: any) => {
      console.error(error);    
  });
}

goBack(){
  this.router.navigate(['production/report/weekly/activity'], { queryParams: {lineId: this.idLine}})
}

}
