import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyAddComponent } from './weekly-add.component';

describe('WeeklyAddComponent', () => {
  let component: WeeklyAddComponent;
  let fixture: ComponentFixture<WeeklyAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeeklyAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WeeklyAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
