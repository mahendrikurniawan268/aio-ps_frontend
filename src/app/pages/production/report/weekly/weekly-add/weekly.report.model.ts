export interface WeeklyReport {
    id_line: null | number,
    id_pic: null | number,
    issue: string,
    task: string,
    start_date: string,
    end_date: string,
    remark: string,
    action_plan: string,
    next_plan: string,
    id_status: null | number,
    img_before: File | null,
    img_after: File | null;
    }