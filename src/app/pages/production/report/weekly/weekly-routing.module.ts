import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeklyReportComponent } from './wekly-report/wekly-report.component';
import { FollowupComponent } from '../../followup/followup.component';
import { ResumeComponent } from './rakor-mom/resume/resume.component';
import { UpdateTalComponent } from './rakor-mom/update-tal/update-tal.component';
import { NextplanComponent } from './rakor-mom/nextplan/nextplan.component';
import { ResumeAddComponent } from './rakor-mom/resume-add/resume-add.component';
import { ResumeUpdateComponent } from './rakor-mom/resume-update/resume-update.component';
import { RakorMomDetailComponent } from './rakor-mom/rakor-mom-detail/rakor-mom-detail.component';
import { WeeklyAddComponent } from './weekly-add/weekly-add.component';
import { WeeklyViewComponent } from './weekly-view/weekly-view.component';
import { MappingComponent } from './mapping/mapping.component';
import { MappingAddComponent } from './mapping/mapping-add/mapping-add.component';

const routes: Routes = [
  {
    path: 'followup/:id',
    component: FollowupComponent,
  },
  {
    path: 'activity',
    component: WeklyReportComponent,
  },
  {
    path: 'activity/add',
    component: WeeklyAddComponent,
  },
  {
    path: 'activity/add/:id',
    component: WeeklyAddComponent,
  },
  {
    path: 'activity/view',
    component: WeeklyViewComponent,
  },
  {
    path: 'rakor/resume',
    component: ResumeComponent,
  },
  {
    path: 'rakor/resume/add',
    component: ResumeAddComponent,
  },
  {
    path: 'rakor/resume/:id',
    component: ResumeUpdateComponent,
  },
  {
    path: 'rakor/nextplan',
    component: NextplanComponent,
  },
  {
    path: 'rakor/nextplan/:id',
    component: NextplanComponent,
  },
  {
    path: 'rakor/update-tal',
    component: UpdateTalComponent,
  },
  {
    path: 'rakor/update-tal/:id',
    component: UpdateTalComponent,
  },
  {
    path: 'rakor/resume/detail/:id',
    component: RakorMomDetailComponent,
  },
  {
    path: 'mapping',
    component: MappingComponent,
  },
  {
    path: 'mapping/add',
    component: MappingAddComponent,
  },
  {
    path: 'mapping/edit/:id',
    component: MappingAddComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeeklyRoutingModule { }
