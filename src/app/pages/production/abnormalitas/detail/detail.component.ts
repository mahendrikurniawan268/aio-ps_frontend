import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  breadCrumbItems!: Array<{}>;
  isConnected: boolean | undefined;
  imageUrl = `${environment.API_URL}${environment.getImageAbnormal}`;
  record_time: any;

  constructor(
    public apiservice: ApiService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  id: number | undefined;
  data: any;
  idLine:any;

  ngOnInit(): void {

    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Abnormal', link: '/production/abnormal'},
      { label: 'Print Report', active: true },
    ];

    this.route.params.subscribe(params => {
      const id = params['id'];
      this.route.queryParams.subscribe({next: (params) => {
        this.idLine = params['lineId']
      }})
      this.apiservice.getByIdabnormal(id).subscribe((response: any) => {
        this.data = response.data[0]
        this.data.section = this.data.section;
        this.data.img_problem = this.data.img_problem.split(',');
        this.data.img_cause = this.data.img_cause.split(',');
        this.data.img_taken_action = this.data.img_taken_action.split(',');
        this.data.img_capa = this.data.img_capa.split(',');
        this.calculateRecordTime();
      }, error => {
        console.error('Error:', error);
      });
    }); 
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }
  formatTime(time: string): string {
    const timeParts = time.split(':');
    const hours = timeParts[0];
    const minutes = timeParts[1];
    return `${hours}:${minutes}`;
  }

  calculateRecordTime() {
    const startTimeParts = this.data.start_time.split(':');
    const endTimeParts = this.data.end_time.split(':');
    const startHour = parseInt(startTimeParts[0], 10);
    const startMinute = parseInt(startTimeParts[1], 10);
    const endHour = parseInt(endTimeParts[0], 10);
    const endMinute = parseInt(endTimeParts[1], 10);
    const totalMinutesStart = startHour * 60 + startMinute;
    const totalMinutesEnd = endHour * 60 + endMinute;
    const differenceMinutes = totalMinutesEnd - totalMinutesStart;
    const hours = Math.floor(differenceMinutes / 60);
    const minutes = differenceMinutes % 60;
    this.record_time = `${hours} Hours ${minutes} minute`;
  }

  goBack(){
    this.router.navigate(['/production/abnormal'], {queryParams: {lineId:this.idLine}});
  }

  getAreaLabel(id_area: number): string {
    switch (id_area) {
        case 1:
            return 'Preparation';
        case 2:
            return 'Filling';
        case 3:
            return 'Packing';
        case 4:
            return 'Preparation';
        case 5:
            return 'Injection';
        case 5:
            return 'Blow';
        case 5:
            return 'Filling';
        case 5:
            return 'Packing';
        default:
            return 'Unknown';
    }
  }

}

