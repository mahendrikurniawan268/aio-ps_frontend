import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;
  
  breadCrumbItems!: Array<{}>;
  fileName: any =[];
  img_problem : any = [];
  img_cause: any = [];
  img_taken_action:any = []
  img_capa:any = []
  userData: any;
  dataArea: any [] = [];
  area: any;
  dataMachine: any [] = [];
  machine: any;
  isAdmin: boolean | undefined;
  isEmployee: boolean | undefined;
  isSpv: boolean | undefined;
  data: any;
  AbnormalEdit!: UntypedFormGroup;
  dataAbnormalById: any;
  username: any;
  idLine:any;
  idParam:any;

  constructor(
    private formBuilder: FormBuilder, 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private RestApiService: RestApiService) {}

  isEditingProblem = false;
  startEditingProblem() {
    this.isEditingProblem = true;
  }
  cancelEditingProblem() {
    this.isEditingProblem = false;
  }

  isEditingCause = false;
  startEditingCause() {
    this.isEditingCause = true;
  }
  cancelEditingCause() {
    this.isEditingCause = false;
  }

  isEditingCapaTion = false;
  startEditingCapaTion() {
    this.isEditingCapaTion = true;
  }
  cancelEditingCapaTion() {
    this.isEditingCapaTion = false;
  }

  isEditingCapaTive = false;
  startEditingCapaTive() {
    this.isEditingCapaTive = true;
  }
  cancelEditingCapaTive() {
    this.isEditingCapaTive = false;
  }


  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getArea();
    this.getMachine();
    this.getAbnormalById(this.idParam)
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.username = this.userData.name;
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_abnormal}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
  };
  dropzoneResponse: any;

  onUploadImgProblem(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_problem.push(this.fileName)
      this.dataAbnormalById.img_problem = this.img_problem
    }
  }

  onUploadImgCause(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_cause.push(this.fileName);
      this.dataAbnormalById.img_cause = this.img_cause
    }
  }
  onUploadImgCapaCurrention(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_taken_action.push(this.fileName)
      this.dataAbnormalById.img_taken_action = this.img_taken_action
    }
  }
  onUploadImgCapaCurrentive(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_capa.push(this.fileName)
      this.dataAbnormalById.img_capa = this.img_capa
    }
  }

  getAbnormalById(id:any){
    this.apiService.getByIdabnormal(id).subscribe(
    (res:any) => {
      this.dataAbnormalById = res.data[0]
      this.dataAbnormalById.due_date = this.convertDate(this.dataAbnormalById.due_date);
    })
  }

  getArea(){
    this.RestApiService.getAllArea().subscribe(
      (res: any) => {
        this.dataArea = res.data;
        this.area = this.dataArea.filter(item => item.id_line == this.idLine)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getMachine(){
    this.RestApiService.getAllMachine().subscribe(
      (res: any) => {
        this.dataMachine = res.data;
        this.machine = this.dataMachine.filter(item => item.id_line == this.idLine)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  onSubmit(){
    this.apiService.updateAbnormal(this.idParam, this.dataAbnormalById).subscribe(
      (res: any) => {
        Swal.fire({
          title: 'Success',
          text: 'Abnormal data updated successfully!',
          icon: 'success',
          confirmButtonColor: '#34c38f'
        })
      },
      (error: any) => {
        console.error(error);
        Swal.fire({
          title: 'Error',
          text: 'Failed to update abnormal data. Please try again later.',
          icon: 'error',
          confirmButtonColor: '#f46a6a'
        });
      }
    );
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  back(){
    this.router.navigate(['/production/abnormal'],  {queryParams:{lineId : this.idLine}});
  }

}
