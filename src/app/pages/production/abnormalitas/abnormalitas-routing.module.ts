import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { DetailComponent } from './detail/detail.component';
import { UpdateComponent } from './update/update.component';
import { HasilInvestigasiComponent } from './hasil-investigasi/hasil-investigasi.component';
import { AddHasilInvestigasiComponent } from './hasil-investigasi/add-hasil-investigasi/add-hasil-investigasi.component';
import { AddSubinvestigasiComponent } from './hasil-investigasi/add-subinvestigasi/add-subinvestigasi.component';
import { AddSubinvestigasiDetailComponent } from './hasil-investigasi/add-subinvestigasi-detail/add-subinvestigasi-detail.component';
import { ViewHasilInvestigasiComponent } from './hasil-investigasi/view-hasil-investigasi/view-hasil-investigasi.component';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent
  },
  {
    path: 'add',
    component: AddComponent
  },
  {
    path: 'detail/:id',
    component: DetailComponent
  },

  {
    path: 'update/:id',
    component: UpdateComponent
  },
  {
    path: 'hasil-investigasi',
    component: HasilInvestigasiComponent
  },
  {
    path: 'hasil-investigasi/add',
    component: AddHasilInvestigasiComponent
  },
  {
    path: 'hasil-investigasi/edit/:id',
    component: AddHasilInvestigasiComponent
  },
  {
    path: 'hasil-investigasi/add-subinvestigasi/:id',
    component: AddSubinvestigasiComponent
  },
  {
    path: 'hasil-investigasi/add-subinvestigasi-detail/:id',
    component: AddSubinvestigasiDetailComponent
  },
  {
    path: 'hasil-investigasi/view/:id',
    component: ViewHasilInvestigasiComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbnormalitasRoutingModule { }
