import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-hasil-investigasi',
  templateUrl: './hasil-investigasi.component.html',
  styleUrls: ['./hasil-investigasi.component.scss']
})
export class HasilInvestigasiComponent {
  searchTerm: string = ''; 
  breadCrumbItems!: Array<{}>;
  totalRecords: any; 
  page: number = 1;
  startDate: string = '';
  endDate: string = '';
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;

  investigasiData: any [] = [];
  investigasiDataByLine:any;
  DataById: any;
  id:any;
  filteredInvestigasiData: any[] = [];
  totalPages: number = 0;
  idLine:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;

  constructor(public apiservice:ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.getTabelView()
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  onView(id:any){
    this.router.navigate([`production/abnormal/hasil-investigasi/view/${id}`], {queryParams: {lineId: this.idLine}});
  }

  onAddData(){
    this.router.navigate(['production/abnormal/hasil-investigasi/add'], {queryParams: {lineId: this.idLine}});
  }

  onEditData(id:any){
    this.router.navigate([`production/abnormal/hasil-investigasi/edit/${id}`], {queryParams: {lineId: this.idLine}});
  }

  getTabelView() {
    this.apiservice.getAllInvestigasi().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.investigasiData = res.data;
          this.investigasiDataByLine = this.investigasiData.filter(item => item.id_line == this.idLine)
          this.filteredInvestigasiData = this.investigasiDataByLine;
          this.totalRecords = this.filteredInvestigasiData.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
        }
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          status: 0,
        };
        this.apiservice.updateInvestigasi(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onDone(id: any) {
    Swal.fire({
      title: 'Are you sure completed this task?',
      text: 'You won\'t be completed this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes!',
    }).then((result) => {
      if (result.value) {
        const data = {
          id_status: 2,
        };
        this.apiservice.updateInvestigasi(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onRevisi(id: any) {
    Swal.fire({
      title: 'Are you Revisi this task?',
      text: 'You won\'t be Revisi this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes!',
    }).then((result) => {
      if (result.value) {
        const data = {
          id_status: 3,
        };
        this.apiservice.updateInvestigasi(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filteredInvestigasiData = this.investigasiDataByLine;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filteredInvestigasiData = this.investigasiDataByLine.filter((data: { name_product: string, lot_number: string, abnormalitas: string }) => {
        return (
          data.name_product.toString().toLowerCase().includes(searchTermLower) ||
          data.lot_number.toString().toLowerCase().includes(searchTermLower) ||
          data.abnormalitas.toString().toLowerCase().includes(searchTermLower)
        );
      });
      this.setPaginationData();
    }
  }

  filter() {
    this.filteredInvestigasiData = this.investigasiDataByLine.filter((item: { date_production: string; }) => {
      const itemDate = new Date(item.date_production);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.setPaginationData();
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'info'; // New
        case 2:
            return 'secondary'; // On Progress
        case 3:
            return 'primary'; // Done
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'On Proggress';
        case 2:
            return 'Done';
        case 3:
            return 'Revisi';
        case 4:
            return 'Pending';
        case 5:
            return 'Revisi';
        default:
            return 'Unknown';
    }
}

formatDate(date: string): string {
  const formattedDate = new Date(date);
  const monthNames = [
    'Januari', 'Februari', 'Maret', 'April',
    'Mei', 'Juni', 'Juli', 'Agustus',
    'September', 'Oktober', 'November', 'Desember'
  ];
  const day = formattedDate.getDate();
  const monthIndex = formattedDate.getMonth();
  const year = formattedDate.getFullYear();
  return `${day} ${monthNames[monthIndex]} ${year}`;
}

getShowingText(): string {
  const startIndex = (this.page - 1) * this.pageSize + 1;
  const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
  return `Showing ${startIndex} - ${endIndex}`;
}

onPageSizeChange() {
  this.startIndex = 1;
  this.endIndex = this.pageSize;
}

setPaginationData() {
  this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
}

}