import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lightbox } from 'ngx-lightbox';
import { ApiService } from 'src/app/core/services/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-view-hasil-investigasi',
  templateUrl: './view-hasil-investigasi.component.html',
  styleUrls: ['./view-hasil-investigasi.component.scss']
})
export class ViewHasilInvestigasiComponent implements OnInit{
  constructor(
    public apiservice: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private lightbox: Lightbox
  ) {}

  idParam: any;
  idLine: any;
  dataInspection: any;
  dataSubInspectionById: any;
  dataSubInspectionDetailById: any;
  imageUrl = `${environment.API_URL}${environment.getImageAbnormal}`;
  
  ngOnInit(){
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getInspection(this.idParam);
    this.getSubInspection(this.idParam);
    this.getSubInspectionDetail(this.idParam);
  }

  openLightbox(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImg(file: string): string {
    return `${this.imageUrl}${file}`;
  }

  getInspection(id:any){
    this.apiservice.getInvestigasiById(id).subscribe(
      (res: any) => {
        this.dataInspection = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getSubInspection(id:any){
    this.apiservice.getSubInvestigasiByInvestigasiId(id).subscribe(
      (res: any) => {
        this.dataSubInspectionById = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getSubInspectionDetail(id:any){
    this.apiservice.getSubInvestigasiDetailBySubInvestigasiId(id).subscribe(
      (res: any) => {
        this.dataSubInspectionDetailById = res.data[0];
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

  getHasilWithLineBreaks(hasil_inspeksi: string): string {
    if (hasil_inspeksi) {
        return hasil_inspeksi.replace(/,\s*/g, '<br><br>').replace(/\n/g, '<br>');
    }
    return '';
  }

  getAnalisaWithLineBreaks(analisa_penyebab: string): string {
    if (analisa_penyebab) {
        return analisa_penyebab.replace(/,\s*/g, '<br><br>').replace(/\n/g, '<br>');
    }
    return '';
  }

  getAkibatWithLineBreaks(akibat: string): string {
    if (akibat) {
        return akibat.replace(/,\s*/g, '<br><br>').replace(/\n/g, '<br>');
    }
    return '';
  }

  getCurrectionWithLineBreaks(currection_action: string): string {
    if (currection_action) {
        return currection_action.replace(/,\s*/g, '<br><br>').replace(/\n/g, '<br>');
    }
    return '';
  }

  getPreventiveWithLineBreaks(preventive_action: string): string {
    if (preventive_action) {
        return preventive_action.replace(/,\s*/g, '<br><br>').replace(/\n/g, '<br>');
    }
    return '';
  }

  goBack(){
    this.router.navigate(['production/abnormal/hasil-investigasi'], {queryParams: {lineId: this.idLine}});
  }
}
