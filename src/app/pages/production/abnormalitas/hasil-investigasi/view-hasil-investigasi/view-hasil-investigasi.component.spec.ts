import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHasilInvestigasiComponent } from './view-hasil-investigasi.component';

describe('ViewHasilInvestigasiComponent', () => {
  let component: ViewHasilInvestigasiComponent;
  let fixture: ComponentFixture<ViewHasilInvestigasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewHasilInvestigasiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewHasilInvestigasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
