import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HasilInvestigasiComponent } from './hasil-investigasi.component';

describe('HasilInvestigasiComponent', () => {
  let component: HasilInvestigasiComponent;
  let fixture: ComponentFixture<HasilInvestigasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HasilInvestigasiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HasilInvestigasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
