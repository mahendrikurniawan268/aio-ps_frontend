export interface Investigasi {
    id_line: number | null;
    name_product: string;
    date_production: string;
    lot_number: string;
    abnormalitas: string;
    id_status: number | null;
}