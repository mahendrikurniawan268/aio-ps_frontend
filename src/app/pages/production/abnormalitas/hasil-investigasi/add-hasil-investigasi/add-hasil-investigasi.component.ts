import { Component, ViewChild } from '@angular/core';
import { FormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { SubInvestigasiDetail } from '../add-subinvestigasi-detail/add-subinvestigasi-detail.model';
import { SubInvestigasi } from '../add-subinvestigasi/add-subinvestigasi.model';
import { Investigasi } from './hasil-investigasi.model';

@Component({
  selector: 'app-add-hasil-investigasi',
  templateUrl: './add-hasil-investigasi.component.html',
  styleUrls: ['./add-hasil-investigasi.component.scss']
})

export class AddHasilInvestigasiComponent {
  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;

  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userData: any;
  username: any [] = [];
  userByRole:any;
  isConnected: boolean = false;
  data:any;
  idLine:any;
  id_investigasi:any;
  dataInvestigasi:any;
  idInvestigasi:any;
  public subinvestigasi:any [] = [];
  idParam:any;
  fileName: any =[];
  upload_img : any = [];
  investigasiById:any;
  investigasiDetailById:any;
  dataSubInvestigasiDetail:any;

  newInvestigasi : Investigasi = {
    id_line: null,
    name_product: '',
    date_production: '',
    lot_number: '',
    abnormalitas: '',
    id_status: null,
  }

  newSubInvestigasi : SubInvestigasi = {
    start_time: '',
    end_time: '',
    explanation: '',
    id_investigasi: null,
  }

  newSubInvestigasiDetail : SubInvestigasiDetail ={
    id_pic : null,
    due_date: '',
    analisa_penyebab : '',
    akibat : '',
    hasil_inspeksi: '',
    currection_action: '',
    preventive_action: '',
    upload_img: [],
    id_subinvestigasi: null
  }

  constructor( 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getByUser();
    this.getInspection(this.idParam);
    this.getSubInspection(this.idParam);
    this.getSubInspectionDetail(this.idParam);
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
  }

  ngAfterViewInit(): void {
    this.dropzone.config = this.dropzoneConfig;
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_abnormal}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
  };
  dropzoneResponse: any;

  onUploadImg(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.upload_img.push(this.fileName)
    }
    this.newSubInvestigasiDetail.upload_img = this.upload_img.join(', ')
  }

  insertInvestigasi(dataInvestigasi: any) {
    this.apiService.insertInvestigasi(dataInvestigasi).subscribe(
      (res: any) => {
        if(res.status == true){
          this.idInvestigasi = res.data[0];
          this.dataInvestigasi = dataInvestigasi
          this.id_investigasi = this.idInvestigasi
          this.router.navigate([`production/abnormal/hasil-investigasi/add-subinvestigasi/${this.id_investigasi}`], { queryParams: {lineId: this.idLine}})
        }
      },
      (error) => {
        console.error(error)
    })
  }

  updateInvestigasi(dataInvestigasi: any) {
    this.apiService.updateInvestigasi(this.idParam, dataInvestigasi).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.dataInvestigasi = dataInvestigasi;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  insertSubInvestigasi(index: number){
    const data = this.subinvestigasi[index];
    const dataSubInvestigasi = {
      start_time: data.start_time,
      end_time: data.end_time,
      explanation: data.explanation,
      id_investigasi: this.idParam
    };
  
    if (data.id) {
      this.apiService.updateSubInvestigasi(data.id, dataSubInvestigasi).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertSubInvestigasi(dataSubInvestigasi).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_investigasi = res.data[0];
            data.isUpdated = true;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  } 

  updateSubInvestigasiDetail(dataSubInvestigasiDetail: any) {
    this.apiService.updateSubInvestigasiDetail(this.idParam, dataSubInvestigasiDetail).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.dataSubInvestigasiDetail = dataSubInvestigasiDetail;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate() {
    this.updateInvestigasi(this.newInvestigasi);
    this.subinvestigasi.forEach((subInvestigasi, index) => {
      this.insertSubInvestigasi(index);
    });
    console.log(this.newSubInvestigasiDetail);
    this.updateSubInvestigasiDetail(this.newSubInvestigasiDetail);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  }

  onAddData(){
    this.newInvestigasi.id_line = this.idLine
    this.newInvestigasi.id_status =  1
    this.insertInvestigasi(this.newInvestigasi)
  }

  addData() {
    const lastNo = this.subinvestigasi.length > 0 ? Math.max(...this.subinvestigasi.map(item => item.no)) : 0;
    this.subinvestigasi.push({
      no: lastNo + 1,
      start_time: '',
      end_time: '',
      explanation: '',
      id_investigasi: this.idParam
    });
  }
  
  getByUser() {
    this.isConnected = true;
    this.apiService.getAllUsers().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.username = res.data;
          this.userByRole = this.username.filter(item => item.role_id === 3 || item.role_id === 4)
        } else {
          console.error(`${res.data.message}`);
          setTimeout(() => {
            this.isConnected = false;
          }, 1000);
        }
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      },
    });
  }

  getInspection(id:any){
    this.apiService.getInvestigasiById(id).subscribe(
      (res: any) => {
        this.newInvestigasi = res.data[0];
        this.investigasiById = this.newInvestigasi
        this.investigasiById.date_production = this.convertDate(this.investigasiById.date_production)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getSubInspection(id:any){
    this.apiService.getSubInvestigasiByInvestigasiId(id).subscribe(
      (res: any) => {
        this.subinvestigasi = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getSubInspectionDetail(id:any){
    this.apiService.getSubInvestigasiDetailOnUpdate(id).subscribe(
      (res: any) => {
        this.newSubInvestigasiDetail = res.data[0];
        this.investigasiDetailById = this.newSubInvestigasiDetail
        this.investigasiDetailById.due_date = this.convertDate(this.investigasiDetailById.due_date)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  goBack(){
    this.router.navigate(['production/abnormal/hasil-investigasi'], { queryParams: {lineId: this.idLine}})
  }

}
