import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHasilInvestigasiComponent } from './add-hasil-investigasi.component';

describe('AddHasilInvestigasiComponent', () => {
  let component: AddHasilInvestigasiComponent;
  let fixture: ComponentFixture<AddHasilInvestigasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddHasilInvestigasiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddHasilInvestigasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
