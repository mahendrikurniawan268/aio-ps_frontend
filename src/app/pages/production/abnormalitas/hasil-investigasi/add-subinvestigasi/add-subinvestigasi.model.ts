export interface SubInvestigasi{
    start_time : string;
    end_time : string;
    explanation : string;
    id_investigasi : number | null;
}