import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { SubInvestigasi } from './add-subinvestigasi.model';

@Component({
  selector: 'app-add-subinvestigasi',
  templateUrl: './add-subinvestigasi.component.html',
  styleUrls: ['./add-subinvestigasi.component.scss']
})
export class AddSubinvestigasiComponent implements OnInit{
  public subinvestigasi:any [] = [];

  //roll akses
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  userData: any;
  dataSubInvestigasi: any;
  filterdataSubInvestigasi: any;
  subInvestigasiId: any;
  isConnected: boolean | undefined;
  idParam:any;
  idLine:any;

  newSubInvestigasi : SubInvestigasi = {
    start_time: '',
    end_time: '',
    explanation: '',
    id_investigasi: null,
  }
  
  constructor(
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private router: Router,
    private route : ActivatedRoute, 
    ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
  }

  insertSubInvestigasi(index: number){
    const data = this.subinvestigasi[index];
    const dataSubInvestigasi = {
      start_time: data.start_time,
      end_time: data.end_time,
      explanation: data.explanation,
      id_investigasi: this.idParam
    };
  
    if (data.id) {
      this.apiService.updateSubInvestigasi(data.id, dataSubInvestigasi).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.isUpdated = true;
            this.router.navigate([`production/abnormal/hasil-investigasi/add-subinvestigasi-detail/${dataSubInvestigasi.id_investigasi}`], {queryParams: {lineId: this.idLine}})
          }
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.apiService.insertSubInvestigasi(dataSubInvestigasi).subscribe(
        (res: any) => {
          if (res.status === true) {
            data.id_investigasi = res.data[0];
            data.isUpdated = true;
            this.router.navigate([`production/abnormal/hasil-investigasi/add-subinvestigasi-detail/${dataSubInvestigasi.id_investigasi}`], {queryParams: {lineId: this.idLine}})
          }
        },
        (error) => {
          console.error(error);
        }
      );
    };
  }  
  
  onSubmit() {
    this.subinvestigasi.forEach((data: any, index:number) => {
      this.insertSubInvestigasi(index);
    });
  }

  addData() {
    const lastNo = this.subinvestigasi.length > 0 ? Math.max(...this.subinvestigasi.map(item => item.no)) : 0;
    this.subinvestigasi.push({
      no: lastNo + 1,
      start_time: '',
      end_time: '',
      explanation: '',
      id_investigasi: this.idParam
    });
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }
}
