import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubinvestigasiComponent } from './add-subinvestigasi.component';

describe('AddSubinvestigasiComponent', () => {
  let component: AddSubinvestigasiComponent;
  let fixture: ComponentFixture<AddSubinvestigasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSubinvestigasiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddSubinvestigasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
