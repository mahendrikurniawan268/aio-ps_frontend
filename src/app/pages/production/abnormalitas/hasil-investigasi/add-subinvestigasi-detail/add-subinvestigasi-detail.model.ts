export interface SubInvestigasiDetail {
    currection_action: string;
    preventive_action: string;
    id_pic : null | number;
    due_date: string;
    analisa_penyebab: string;
    akibat: string;
    hasil_inspeksi: string;
    upload_img: string [];
    id_subinvestigasi: null | number;
}