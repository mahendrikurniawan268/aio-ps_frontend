import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { SubInvestigasiDetail } from './add-subinvestigasi-detail.model';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { environment } from 'src/environments/environment';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-add-subinvestigasi-detail',
  templateUrl: './add-subinvestigasi-detail.component.html',
  styleUrls: ['./add-subinvestigasi-detail.component.scss']
})
export class AddSubinvestigasiDetailComponent implements OnInit{
  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;

  idParam:any;
  idLine:any;
  namePic:any [] = [];
  picEmployee: any;
  fileName: any =[];
  upload_img : any = [];
  dataSubInvestigasiDetail : any;

  newSubInvestigasiDetail : SubInvestigasiDetail ={
    id_pic : null,
    due_date: '',
    analisa_penyebab : '',
    akibat : '',
    hasil_inspeksi: '',
    currection_action: '',
    preventive_action: '',
    upload_img: [],
    id_subinvestigasi: null
  }

  constructor(
    private router: Router,
    private apiService: ApiService,
    private RestApiService: RestApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(){
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getNamePic();
  }

  ngAfterViewInit(): void {
    this.dropzone.config = this.dropzoneConfig;
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_abnormal}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
  };
  dropzoneResponse: any;

  onUploadImg(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.upload_img.push(this.fileName)
    }
    this.newSubInvestigasiDetail.upload_img = this.upload_img.join(', ')
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 3 || item.role_id === 4)     
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  insertSubInvestigasiDetail(dataSubInvestigasiDetail: any) {
    this.apiService.insertSubInvestigasiDetail(dataSubInvestigasiDetail).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.dataSubInvestigasiDetail = res.data
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onAddData(){
    this.newSubInvestigasiDetail.id_subinvestigasi = this.idParam
    this.insertSubInvestigasiDetail(this.newSubInvestigasiDetail)
    Swal.fire({
      icon: 'success',
      title: 'Added Data Successfully',
      text: 'Added has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
    this.router.navigate(['production/abnormal/hasil-investigasi'], {queryParams: {lineId: this.idLine}});
  }
}
