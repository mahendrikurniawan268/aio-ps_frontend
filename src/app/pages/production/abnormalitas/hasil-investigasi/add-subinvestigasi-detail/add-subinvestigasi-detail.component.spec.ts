import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubinvestigasiDetailComponent } from './add-subinvestigasi-detail.component';

describe('AddSubinvestigasiDetailComponent', () => {
  let component: AddSubinvestigasiDetailComponent;
  let fixture: ComponentFixture<AddSubinvestigasiDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSubinvestigasiDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddSubinvestigasiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
