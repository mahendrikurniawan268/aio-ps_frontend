import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})

export class ViewComponent {
  searchTerm: string = ''; 
  breadCrumbItems!: Array<{}>;
  totalRecords: any; 
  page: number = 1;
  startDate: string = '';
  endDate: string = '';
  pageSize : number = 5;
  startIndex: number = 1;
  endIndex: number = this.pageSize;

  abnormalData: any [] = [];
  abnormalDataByLine:any;
  DataById: any;
  id:any;
  filteredAbnormalData: any[] = [];
  totalPages: number = 0;
  idLine:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;

  constructor(public apiservice:ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.apiservice.getByIdabnormal(id).subscribe((res: any) => {
        });
      }
    });
    this.getTabelView()
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  formatDate(date: string): string {
    const formattedDate = new Date(date);
    const monthNames = [
      'Januari', 'Februari', 'Maret', 'April',
      'Mei', 'Juni', 'Juli', 'Agustus',
      'September', 'Oktober', 'November', 'Desember'
    ];
    const day = formattedDate.getDate();
    const monthIndex = formattedDate.getMonth();
    const year = formattedDate.getFullYear();
    return `${day} ${monthNames[monthIndex]} ${year}`;
  }

  getShowingText(): string {
    const startIndex = (this.page - 1) * this.pageSize + 1;
    const endIndex = Math.min(this.page * this.pageSize, this.totalRecords);
    return `Showing ${startIndex} - ${endIndex}`;
  }

  onPageSizeChange() {
    this.startIndex = 1;
    this.endIndex = this.pageSize;
  }

  setPaginationData() {
    this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
  }

  getTabelView() {
    this.apiservice.getTabelViewAbnormal().subscribe({
      next: (res: any) => {
        if (res.status) {
          this.abnormalData = res.data;
          this.abnormalDataByLine = this.abnormalData.filter(item => item.id_line == this.idLine)
          this.filteredAbnormalData = this.abnormalDataByLine;
          this.totalRecords = this.filteredAbnormalData.length;
          this.setPaginationData();
        } else {
          console.error(`${res.data.message}`);
        }
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }

  onAddData(){
    this.router.navigate(['production/abnormal/add'], {queryParams: {lineId: this.idLine}});
  }

  getByIdAbnormal(id: any) {
    this.apiservice.getByIdabnormal(id).subscribe({
      next: (res: any) => {
        this.DataById = res;
        this.router.navigate(['production/abnormal/detail/', id], {queryParams:{lineId : this.idLine}, state: { data: this.DataById }});
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

  getByIdAbnormalEdit(id: any) {
    this.apiservice.getByIdabnormal(id).subscribe({
      next: (res: any) => {
        this.DataById = res;
        this.router.navigate(['production/abnormal/update', id], {queryParams:{lineId : this.idLine}, state: { data: this.DataById }});
      },
      error: (err: any) => {
        console.error(err);
        setTimeout(() => {
        }, 1000);
      }
    });
  }

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        const data = {
          status: 0,
        };
        this.apiservice.updateAbnormal(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onDone(id: any) {
    Swal.fire({
      title: 'Are you sure completed this task?',
      text: 'You won\'t be completed this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes!',
    }).then((result) => {
      if (result.value) {
        const data = {
          id_status: 2,
        };
        this.apiservice.updateAbnormal(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onRevisi(id: any) {
    Swal.fire({
      title: 'Are you Revisi this task?',
      text: 'You won\'t be Revisi this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes!',
    }).then((result) => {
      if (result.value) {
        const data = {
          id_status: 3,
        };
        this.apiservice.updateAbnormal(id, data).subscribe({
          next: (res: any) => {
            this.ngOnInit();
          },
          error: (err) => {
            console.error('Kesalahan saat mengirim permintaan update:', err);
          }
        });
      }
    });
  }

  onSearch(): void {
    if (!this.searchTerm) {
      this.filteredAbnormalData = this.abnormalDataByLine;
    } else {
      const searchTermLower = this.searchTerm.toLowerCase();
      this.filteredAbnormalData = this.abnormalDataByLine.filter((data: { problem: string, cause: string, taken_action: string, capa: string, area: string }) => {
        return (
          data.area.toString().toLowerCase().includes(searchTermLower) ||
          data.problem.toString().toLowerCase().includes(searchTermLower) ||
          data.cause.toString().toLowerCase().includes(searchTermLower) ||
          data.taken_action.toString().toLowerCase().includes(searchTermLower) ||
          data.capa.toString().toLowerCase().includes(searchTermLower)
        );
      });
      this.setPaginationData();
    }
  }

  filter() {
    this.filteredAbnormalData = this.abnormalDataByLine.filter((item: { due_date: string;}) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;
      
      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.setPaginationData();
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'info'; // New
        case 2:
            return 'secondary'; // On Progress
        case 3:
            return 'primary'; // Done
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'On Proggress';
        case 2:
            return 'Done';
        case 3:
            return 'Revisi';
        case 4:
            return 'Pending';
        case 5:
            return 'Revisi';
        default:
            return 'Unknown';
    }
}

}

