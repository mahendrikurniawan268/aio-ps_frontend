import { Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DropzoneEvent } from 'ngx-dropzone-wrapper/lib/dropzone.interfaces';
import { ApiService } from 'src/app/core/services/api.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;
  
  breadCrumbItems!: Array<{}>;
  fileName: any =[];
  img_problem : any = [];
  img_cause: any = [];
  img_taken_action:any = []
  img_capa:any = []
  img_action: any =[];
  idLine :any;

  dataArea : any [] = [];
  area: any;
  dataMachine : any [] = [];
  machine: any;

  form = {
    id_line: '',
    id_area: null,
    id_machine: null,
    lot_number: '',
    due_date: '',
    start_time: '',
    end_time: '',
    problem: '', 
    cause: '',
    taken_action: '',
    capa: '',
    id_status: null
  }

  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private RestApiService: RestApiService
    ) {}

  ngOnInit(): void {
    this.getArea();
    this.getMachine();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
  }

  ngAfterViewInit(): void {
    this.dropzone.config = this.dropzoneConfig;
  }

  dropzoneConfig: DropzoneConfigInterface = {
    url: `${environment.API_URL}${environment.Image_abnormal}`,
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    acceptedFiles: 'image/*',
  };
  dropzoneResponse: any;

  onUploadImgProblem(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_problem.push(this.fileName)
    }
  }

  onUploadImgCause(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_cause.push(this.fileName);
    }
  }
  onUploadImgCapaCurrention(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_taken_action.push(this.fileName)
    }
  }
  onUploadImgCapaCurrentive(event: DropzoneEvent) {
    this.dropzoneResponse = event[1];
    if (this.dropzoneResponse !== undefined) {
      this.fileName = this.dropzoneResponse.filename
      this.img_capa.push(this.fileName)
    }
  }

  getArea(){
    this.RestApiService.getAllArea().subscribe(
      (res: any) => {
        this.dataArea = res.data;
        this.area = this.dataArea.filter(item => item.id_line == this.idLine)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getMachine(){
    this.RestApiService.getAllMachine().subscribe(
      (res: any) => {
        this.dataMachine = res.data;
        this.machine = this.dataMachine.filter(item => item.id_line == this.idLine)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  onSubmit(){
    if (this.img_problem.length === 0 || this.img_cause.length === 0 || this.img_taken_action.length === 0 || this.img_capa.length === 0) {
        Swal.fire({
          icon: 'warning',
          title: 'Incomplete Form',
          text: 'Please complete all image uploads.',
          confirmButtonColor: '#28a745'
        });
        return;
    }

    const formData = {
      id_line: this.idLine,
      id_area: this.form.id_area,
      id_machine: this.form.id_machine,
      lot_number: this.form.lot_number,
      due_date: this.form.due_date,
      start_time: this.form.start_time,
      end_time: this.form.end_time,
      problem: this.form.problem,
      cause: this.form.cause,
      taken_action: this.form.taken_action,
      capa: this.form.capa,
      id_status: 1,
      img_problem: this.img_problem,
      img_cause: this.img_cause,
      img_taken_action: this.img_taken_action, 
      img_capa: this.img_capa 
    };

    this.apiService.insertAbnormal(formData).subscribe(
      (response: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'Abnormal data has been successfully saved.',
          confirmButtonColor: '#28a745'
        }).then(() => {
          this.router.navigate(['/production/abnormal'], {queryParams:{lineId : this.idLine}});
        });
      },
      (error: any) => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Failed to save abnormal data. Please try again later.',
          confirmButtonColor: '#dc3545'
        });
        console.error('Error saving abnormal data:', error);
      }
    );
  }


  back(){
    this.router.navigate(['/production/abnormal'], {queryParams:{lineId : this.idLine}});
  }

}