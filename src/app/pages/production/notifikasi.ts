// notification.component.ts

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  template: `
    <div *ngIf="notificationMessage" class="notification">
      {{ notificationMessage }}
    </div>
  `,
  styles: [
    `
      .notification {
        position: fixed;
        top: 10px;
        right: 10px;
        padding: 10px;
        background-color: #ff0000;
        color: #ffffff;
        border-radius: 5px;
        z-index: 999;
      }
    `,
  ],
})
export class NotificationComponent implements OnInit {
  notificationMessage: string | null = null;
  ngOnInit() {
  }
}
