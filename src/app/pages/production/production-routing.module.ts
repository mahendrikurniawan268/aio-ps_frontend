import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyappsComponent } from './myapps/myapps.component';
import { MachineComponent } from './machine/machine.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { ActivityComponent } from './activity/activity.component';
import { FollowupComponent } from './followup/followup.component';

const routes: Routes = [
  {
    path: 'myapps',
    component: MyappsComponent
  },
  {
    path: 'machine',
    component: MachineComponent,
    canActivate: [AuthGuard],
    data: { allowedRoles: [1,2] }
  },
  { 
    path: 'activity', 
    component: ActivityComponent,
  },
  { 
    path: 'followup', 
    component: FollowupComponent,
  },
  {
    path: '', loadChildren: () => import('./dashboards/dashboards.module').then(m => m.DashboardsModule)
  },
  {
    path: 'schedule', loadChildren: () => import('./calendar/calendar.module').then(m => m.CalendarModule)
  },
  {
    path: 'report', loadChildren: () => import('./report/report.module').then(m => m.ReportModule)
  },
  {
    path: 'abnormal', loadChildren: () => import('./abnormalitas/abnormalitas.module').then(m => m.AbnormalitasModule)
  },
  {
    path: 'kartustock', loadChildren: () => 
    import('./kartustock/kartustock.module').
    then(m => m.KartustockModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
