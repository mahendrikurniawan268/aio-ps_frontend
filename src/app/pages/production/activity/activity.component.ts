import { Component } from '@angular/core';
import { FormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/core/services/api.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent {
  breadCrumbItems!: Array<{}>;

  activityAdd!: UntypedFormGroup;
  isAdmin: boolean = false;
  isEmployee: boolean = false;
  isSpv: boolean = false;
  needFollowUp: boolean = false;
  userData: any;
  username: any [] = [];
  userByRole:any;
  isConnected: boolean = false;
  data:any;
  idLine:any;

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder, 
    private apiService: ApiService,
    private AuthenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.getByUser();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.userData = this.AuthenticationService.getUserData();
    this.isAdmin = this.AuthenticationService.isAdmin();
    this.isEmployee = this.AuthenticationService.isEmployee();
    this.isSpv = this.AuthenticationService.isSpv();
    this.breadCrumbItems = [
      { label: 'Production', link: '/dashboard-prod' },
      { label: 'Report', link: '/production/report/daily'},
      { label: 'activity', active: true }
    ];

     // Validation
    this.username = this.userData.name
    this.activityAdd = this.formBuilder.group({
      id_line: this.idLine,
      start_date: [null],
      end_date: [null],
      id_pic: [null],
      issue: [null],
      task: [null],
      actual_start:[null],
      actual_end:[null]
    });
  }

  onSubmit() {
    if (this.activityAdd.valid) {
      const formData = { ...this.activityAdd.value };
  
      if (Array.isArray(formData.pic)) {
        formData.pic = formData.pic.join(', ');
      }
  
      formData.start_date = formData.start_date ? new Date(formData.start_date).toISOString() : null;
      formData.end_date = formData.end_date ? new Date(formData.end_date).toISOString() : null;
  
      formData.actual_start = formData.actual_start ? new Date(formData.actual_start).toISOString() : '0000-00-00';
      formData.actual_end = formData.actual_end ? new Date(formData.actual_end).toISOString() : '0000-00-00';
  
      if (formData.start_date !== null && formData.end_date !== null) {
        const currentDate = new Date();
        let startDate = new Date(formData.start_date);
        let endDate = new Date(formData.end_date);

        startDate.setHours(startDate.getHours() + 7);
        endDate.setHours(endDate.getHours() + 7);

        if (!formData.actual_start || !formData.actual_end) {
          formData.actual_start = '0000-00-00';
          formData.actual_end = '0000-00-00';
        }
      }
      this.apiService.insertActivity(formData).subscribe({
        next: (res: any) => {
          if (res.status) {
            this.modalService.dismissAll();
            this.ngOnInit();
          }
        },
        error: (err: any) => console.error(err),
      });
    }
  }

        /**
   * position sweet alert
   * @param position modal content
   */
        save() {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Saving your work...',
            didOpen: () => {
              this.onSubmit();
              this.router.navigate(['production/report/daily/activity'], { queryParams: {lineId: this.idLine}})
            },
          });
        }

getByUser() {
  this.isConnected = true;
  this.apiService.getAllUsers().subscribe({
    next: (res: any) => {
      if (res.status) {
        this.username = res.data;
        this.userByRole = this.username.filter(item => item.role_id == 2 || item.role_id == 3 || item.role_id == 4)
        console.log(this.userByRole);
        
      } else {
        console.error(`${res.data.message}`);
        setTimeout(() => {
          this.isConnected = false;
        }, 1000);
      }
    },
    error: (err: any) => {
      console.error(err);
      setTimeout(() => {
        this.isConnected = false;
      }, 1000);
    },
  });
}

goBack(){
  this.router.navigate(['production/report/daily/activity'], { queryParams: {lineId: this.idLine}})
}

}
