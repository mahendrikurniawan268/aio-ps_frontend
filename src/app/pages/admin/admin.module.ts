import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import { NgbAccordionModule, NgbCollapseModule, NgbDropdownModule, NgbModule, NgbNavModule, NgbPaginationModule, NgbProgressbarModule, NgbRatingModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SimplebarAngularModule } from 'simplebar-angular';
import { SharedModule } from 'src/app/shared/shared.module';

// Calendar package
import { FullCalendarModule } from '@fullcalendar/angular';
import { DatePipe } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgSelectModule } from '@ng-select/ng-select';
import { CountToModule } from 'angular-count-to';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxMaskDirective,  provideNgxMask } from 'ngx-mask';
import { LOCALE_ID } from '@angular/core';
import { ListApprovalComponent } from './list-approval/list-approval.component';
import { LightboxModule } from 'ngx-lightbox';
import { TaskHeadToSpvComponent } from './task-spv/task-head-to-spv/task-head-to-spv.component';
import { TaskHeadToSpvAddDataComponent } from './task-spv/task-head-to-spv/task-head-to-spv-add-data/task-head-to-spv-add-data.component';
import { TaskSpvToEmployeeComponent } from './task-spv/task-head-to-spv/task-spv-to-employee/task-spv-to-employee.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { AppGatewayAdminComponent } from './app-gateway-admin/app-gateway-admin.component';
import { AppGatewayAdminModalComponent } from './app-gateway-admin/app-gateway-admin-modal/app-gateway-admin-modal.component';
import { HssPpicComponent } from './hss/hss-ppic/hss-ppic.component';
import { HssProComponent } from './hss/hss-pro/hss-pro.component';
import { HssScheduleComponent } from './hss/hss-schedule/hss-schedule.component';
import { HssScheduleAddDataComponent } from './hss/hss-schedule/hss-schedule-add-data/hss-schedule-add-data.component';
import { HssScheduleViewComponent } from './hss/hss-schedule/hss-schedule-view/hss-schedule-view.component';
import { AccesLineComponent } from './acces-line/acces-line.component';

@NgModule({
  declarations: [

  
    ListApprovalComponent,
          TaskHeadToSpvComponent,
          TaskHeadToSpvAddDataComponent,
          TaskSpvToEmployeeComponent,
          DashboardAdminComponent,
          AppGatewayAdminComponent,
          AppGatewayAdminModalComponent,
          HssPpicComponent,
          HssProComponent,
          HssScheduleComponent,
          HssScheduleAddDataComponent,
          HssScheduleViewComponent,
          AccesLineComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    DropzoneModule,
    ScrollSpyModule.forRoot(),
    NgbDropdownModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    FullCalendarModule,
    ReactiveFormsModule,
    SharedModule,
    DatePipe,
    FormsModule,
    NgbTooltipModule,
    NgbDropdownModule,
    NgbAccordionModule,
    NgbProgressbarModule,
    NgbNavModule,
    NgbPaginationModule,
    NgbCollapseModule,
    FeatherModule.pick(allIcons),
    FlatpickrModule.forRoot(),
    SimplebarAngularModule,
    CountToModule,
    NgApexchartsModule,
    LeafletModule,
    NgSelectModule,
    NgxUsefulSwiperModule,
    NgxMaskDirective, 
    NgbModule,
    NgApexchartsModule,
    SimplebarAngularModule,

  ],

  exports: [
    NgbModule,
    MdbModalModule,
    FormsModule,
    DropzoneModule,
    NgbDropdownModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    LightboxModule,
    FullCalendarModule,
    ReactiveFormsModule,
    DatePipe,
    SimplebarAngularModule,
    SharedModule,
    NgbPaginationModule
  ],
  providers: [
    DatePipe,
    provideNgxMask(),
    { provide: LOCALE_ID, useValue: 'id-ID' },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminModule { }
