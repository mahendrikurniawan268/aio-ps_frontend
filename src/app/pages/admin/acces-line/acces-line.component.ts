import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { AccesLine } from './acces-line.model';

@Component({
  selector: 'app-acces-line',
  templateUrl: './acces-line.component.html',
  styleUrls: ['./acces-line.component.scss']
})
export class AccesLineComponent implements OnInit {

  searchText1:string = '';
  searchText2:string = '';
  dataForCurrentPage1: any [] = [];
  dataForCurrentPage2: any [] = [];
  preData1:any;
  preData2:any;
  index = 0;
  currentPage1 = 1;
  itemsPerPage1 = 10;
  currentPage2 = 1;
  itemsPerPage2 = 10;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  isTableView: boolean = false;
  namePic: any [] = []
  namePicSpv: any [] = []
  dataAcces: any = [];
  dataHistoryAcces: any[] = [];
  idAcces:any;

  newAccesLine : AccesLine = {
    name: '',
    email: '',
    hp_number: '',
    company_name: '',
    country: '',
    visit_category: '',
    aio_pic: '',
    email_pic: '',
    visit_date: '',
    notes: '',
    id_status: null,
    status: null
  }

  constructor(private router:Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private modalService : NgbModal){}
  
    ngOnInit(): void {
      this.getAccesLine();
      this.getHistoryAcces();
      this.getNameSpv();
      this.userData = this.authService.getUserData();
      this.isAdmin = this.authService.isAdmin();
      this.isSpv = this.authService.isSpv();
      this.isEmployee = this.authService.isEmployee();
    }

    clearFormData() {
      this.newAccesLine = {
        name: '',
        email: '',
        hp_number: '',
        company_name: '',
        country: '',
        visit_category: '',
        aio_pic: '',
        email_pic: '',
        visit_date: '',
        notes: '',
        id_status: null,
        status: null
      }
    }

    onSelectedViewCheck(event: any) {
      if (event.target.id == 'btnList') {
        this.isTableView = false
        this.router.navigate(["/menu/acces-line"], {
          queryParams: { tableList: false },
        });
      } else if (event.target.id == 'btnHistory') {
        this.isTableView = true
        this.router.navigate(["/menu/acces-line"], {
          queryParams: { tableHistory: true },
        });
      }
    }

    openApprove(approve: any, item: any, id: any) {
      this.clearFormData();
      this.newAccesLine = { ...item };
      this.idAcces = id;
      this.modalService.open(approve, { size: 'md', centered: true }).result.then(
        (result) => {
          if (result === 'Close click') {
            this.clearFormData();
          }
        },
        (reason) => {
          this.clearFormData();
          console.log(`Dismissed with reason: ${reason}`);
        }
      );
    }

  /**
   * Open modal
   * @param reject modal content
   */
  openReject(reject: any, item:any, id:any) {
    this.clearFormData();
    this.newAccesLine = { ...item };
    this.idAcces = id;
    this.modalService.open(reject, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );;
  }

  onApprove(id:any){
    this.newAccesLine.id_status = 2
    this.updateAccesLine(this.newAccesLine, id);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    this.modalService.dismissAll();
    }

  onReject(id:any){
    this.newAccesLine.id_status = 3
    this.newAccesLine.status = 0
    this.updateAccesLine(this.newAccesLine, id);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    this.modalService.dismissAll();
    }

  onDone(id:any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Is the visit over?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const doneData = { id_status: 4, status: 0, id: id };
        this.updateDoneAndDelete(doneData, id)
      }
    });
  }

        /**
   * Open modal
   * @param add_acces modal content
   */
    addAcces(add_acces: any) {
      this.modalService.open(add_acces, { size: 'lg', centered: true });
    }

    insertAccesLine(dataAccesLine: any) {
      this.RestApiService.insertAccesLine(dataAccesLine).subscribe(
        (res: any) => {
          this.dataAcces = res.data;
          this.getAccesLine();
          this.getHistoryAcces();
        })
    }

    onSubmit(){
      this.newAccesLine.id_status = 1;
      this.insertAccesLine(this.newAccesLine);
      Swal.fire({
        icon: 'success',
        title: 'Add Data Successfully',
        text: 'Add has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    this.modalService.dismissAll();
    }

    updateAccesLine(dataAccesLine:any, id:any){
      this.RestApiService.updateAccesLine(this.idAcces, dataAccesLine).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.newAccesLine = dataAccesLine;
            this.getAccesLine();
            this.getHistoryAcces();
          }
        },
        (error) => {
          console.error(error)
        }
      )
    }

    updateDoneAndDelete(dataAccesLine:any, id:any){
      this.RestApiService.updateAccesLine(id, dataAccesLine).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.newAccesLine = dataAccesLine;
            this.getAccesLine();
            this.getHistoryAcces();
          }
        },
        (error) => {
          console.error(error)
        }
      )
    }

    onUpdate(){
      this.updateAccesLine(this.newAccesLine, this.idAcces);
        Swal.fire({
          icon: 'success',
          title: 'Update Successfully',
          text: 'Update has been successfully.',
          timer: 1500,
          showConfirmButton: false,
        });
      this.modalService.dismissAll();
      }

    onDelete(id:any){
      Swal.fire({
        title: 'Confirmation',
        text: 'Are you sure you want to delete this data?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oke',
        cancelButtonText: 'Cancel',
        cancelButtonColor: '#d33',
        customClass: {
          popup: 'custom-swal-text',
        },
      }).then((result) => {
        if (result.isConfirmed) {
          const deleteData = { status: 0 };
          this.updateDoneAndDelete(deleteData, id)
        }
      });
    }

    getAccesLine(){
      this.RestApiService.getAllAccesLine().subscribe(
        (res:any) => {
          this.dataAcces = res.data;
          this.preData1 = [...this.dataAcces];
          this.paginateData1();
        },
        (error:any) => {
          console.error(error)
        }
      );
    }

    getHistoryAcces(){
      this.RestApiService.getHistoryAcces().subscribe(
        (res:any) => {
          this.dataHistoryAcces = res.data[0];
          this.preData2 = [...this.dataHistoryAcces];
          this.paginateData2();
        },
        (error:any) => {
          console.error(error)
        }
      );
    }

    getNameSpv(){
      this.RestApiService.getAllMasterUser().subscribe(
        (res: any) => {
          this.namePic = res.data;
          this.namePicSpv = this.namePic.filter(item => item.role_id === 1 || item.role_id === 2)
        },
        (error: any) => {
          console.error(error);
        }
      );
    }

    onPic(event: any) {
      const picName = event.name;
      const selectedPic = this.namePicSpv.find(item => item.name === picName);
      if (selectedPic) {
        this.newAccesLine.aio_pic = selectedPic.name;
        this.newAccesLine.email_pic = selectedPic.email;
      }
    }

    search1() {
      this.dataAcces = this.preData1.filter((item: { name: string; company_name: string;}) => {
        return item.name.toLowerCase().includes(this.searchText1.toLowerCase()) ||
               item.company_name.toLowerCase().includes(this.searchText1.toLowerCase());
      });
      this.paginateData1();
    }
  
    paginateData1() {
      const startIndex = (this.currentPage1 - 1) * this.itemsPerPage1;
      const endIndex = startIndex + this.itemsPerPage1;
      this.dataForCurrentPage1 = this.dataAcces.slice(startIndex, endIndex);
    }
  
    onPageChange1(newPage: number) {
      this.currentPage1 = newPage;
      this.paginateData1();
    }
  
    search2() {
      this.dataHistoryAcces = this.preData2.filter((item: { name: string; company_name: string; }) => {
        return item.name.toLowerCase().includes(this.searchText2.toLowerCase()) ||
               item.company_name.toLowerCase().includes(this.searchText2.toLowerCase());
      });
      this.paginateData2();
    }
  
    paginateData2() {
      const startIndex = (this.currentPage2 - 1) * this.itemsPerPage2;
      const endIndex = startIndex + this.itemsPerPage2;
      this.dataForCurrentPage2 = this.dataHistoryAcces.slice(startIndex, endIndex);
    }
  
    onPageChange2(newPage: number) {
      this.currentPage2 = newPage;
      this.paginateData2();
    }
  
    getStatusAdminClass(id_status: number): string {
      switch (id_status) {
          case 1:
              return 'success'; 
          case 2:
              return 'secondary'; 
          case 3:
              return 'danger';
          case 4:
              return 'secondary';
          default:
              return 'secondary';
      }
  }
  
    getStatusAdmin(id_status: number): string {
        switch (id_status) {
            case 1:
                return 'New';
            case 2:
                return 'Approved';
            case 3:
                return 'Reject';
            case 4:
                return 'Done';
            default:
                return 'Unknown';
        }
    }

    getStatusOtherClass(id_status: number): string {
      switch (id_status) {
        case 1:
          return 'secondary'; 
      case 2:
          return 'success'; 
      case 3:
          return 'danger';
      case 4:
          return 'primary';
      default:
          return 'secondary';
      }
  }
  
    getStatusOther(id_status: number): string {
        switch (id_status) {
          case 1:
            return 'Waiting';
        case 2:
            return 'Approved';
        case 3:
            return 'Reject';
        case 4:
            return 'Done';
        default:
            return 'Unknown';
      }
    }

    convertDate(date: string): string {
      if (date) {
        const inputDate = new Date(date);
        inputDate.setDate(inputDate.getDate() + 1);
        const formattedDate = inputDate.toISOString().split('T')[0];
        return formattedDate;
      }
      return '';
    }
}
