export interface AccesLine {
    name: string;
    email: string;
    hp_number: string;
    company_name: string;
    country: string;
    visit_category: string;
    aio_pic: string;
    email_pic: string;
    visit_date: string;
    notes: string;
    id_status: null | number;
    status:null | number;
  }