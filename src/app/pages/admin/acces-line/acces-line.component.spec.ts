import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesLineComponent } from './acces-line.component';

describe('AccesLineComponent', () => {
  let component: AccesLineComponent;
  let fixture: ComponentFixture<AccesLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccesLineComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccesLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
