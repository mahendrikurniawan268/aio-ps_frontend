import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppGatewayAdminComponent } from './app-gateway-admin.component';

describe('AppGatewayAdminComponent', () => {
  let component: AppGatewayAdminComponent;
  let fixture: ComponentFixture<AppGatewayAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppGatewayAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppGatewayAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
