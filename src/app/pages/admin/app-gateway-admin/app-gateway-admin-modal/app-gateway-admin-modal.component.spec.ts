import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppGatewayAdminModalComponent } from './app-gateway-admin-modal.component';

describe('AppGatewayAdminModalComponent', () => {
  let component: AppGatewayAdminModalComponent;
  let fixture: ComponentFixture<AppGatewayAdminModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppGatewayAdminModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppGatewayAdminModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
