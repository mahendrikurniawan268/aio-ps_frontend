export interface TaskSpvToEmployee {
    response: string;
    switch_to: string;
    email_employee: string;
    file: string;
    file_employee:string;
    notes_spv: string;
    notes_m1:string;
    notes_employe: string;
    revisi:string;
    id_status: number | null;
    id_status_spv: number | null;
    id_task_spv: number | null
}