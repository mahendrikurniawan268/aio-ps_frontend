export interface TaskSpv {
    task: string;
    user: string;
    file_task: string;
    send_to: string;
    email_user: string;
    email_spv: string;
    due_date: string;
    id_status: number | null;
    id_priority: number | null;
    notes: string;
    revisi: string;
}