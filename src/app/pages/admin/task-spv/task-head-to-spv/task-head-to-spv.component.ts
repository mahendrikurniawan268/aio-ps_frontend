import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { TaskHeadToSpvAddDataComponent } from './task-head-to-spv-add-data/task-head-to-spv-add-data.component';
import { TaskSpv } from './task-head-to-spv.model';
import { TaskSpvToEmployee } from './task-spv-to-employee.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-task-head-to-spv',
  templateUrl: './task-head-to-spv.component.html',
  styleUrls: ['./task-head-to-spv.component.scss']
})
export class TaskHeadToSpvComponent implements OnInit{
  @ViewChild('edit_task') edit_task!: ElementRef;

  tableColumns = ['Task', 'Owner', 'Due Date', 'Status', 'Priority', 'Notes', 'Details'];
  tableColumns2 = ['Response', 'Swich To', 'File', 'Notes', 'Details']
  titlePage = 'Improvement List';
  index: number = 0;
  dataTaskSpv:any = [];
  dataSpvToEmploye:any [] = [];
  public subtask:any = [];
  spv_to_employee:any;
  idSpv : any;
  task_spv:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  preData:any;
  searchText:string = '';
  startDate: string = '';
  endDate: string = '';
  selectedFiles: { file: File, type: string }[] = [];
  selectedFilesTask: { file: File, type: string }[] = [];
  pdf_url:any
  namePic: any [] = []
  namePicSpv: any [] = []
  nameEmployee:any [] = []
  dataDatePriority: any []=[]
  idTask:any;
  idDetail:any;

  newTaskSpv: TaskSpv = {
    task: '',
    user: '',
    file_task: '',
    send_to: '',
    email_user: '',
    email_spv: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    revisi: '',
  };

  newTaskSpvToEmployee: TaskSpvToEmployee = {
    response: '',
    switch_to: '',
    email_employee: '',
    file: '',
    file_employee: '',
    notes_spv: '',
    notes_employe: '',
    notes_m1: '',
    revisi:'',
    id_status:null,
    id_status_spv: null,
    id_task_spv: null,
  };

  constructor(private router : Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private modalService:NgbModal){}

  ngOnInit(): void {
    this.getTaskSpv();
    this.getSpvToEmployee();
    this.getNameSpv();
    if (this.idTask) {
      this.getTaskSpvById(this.idTask);
    }
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
  }

  clearFormData(){
    this.newTaskSpv = {
      task: '',
      user: '',
      file_task: '',
      send_to: '',
      email_user: '',
      email_spv: '',
      due_date: '',
      id_status: null,
      id_priority: null,
      notes: '',
      revisi: '',
    };
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  filterredDataSubTask(taskId: number): any[] {
    let subtaskData = this.dataSpvToEmploye.filter(data => data.id_task_spv == taskId)
    return subtaskData
  }

  subTask = [
    { source: 'task', task_id:1 },
    { source: 'subtask1', id_task:1 },
    { source: 'subtask2', id_task:2 }
  ]

  getTaskSpvById(id:any){
    this.RestApiService.getTaskSpvById(id).subscribe(
      (res:any) => {
        this.newTaskSpv = res.data[0];
        this.newTaskSpv.due_date = this.convertDate(this.newTaskSpv.due_date);
      })
  }
  
  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  getTaskSpv(){
    this.RestApiService.getAllTaskSpv().subscribe(
      (res:any) => {
        this.dataTaskSpv = res.data;        
        this.preData = [...this.dataTaskSpv]
        this.dataDatePriority = [...this.dataTaskSpv]
        this.checkDatePriority();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getNameSpv(){
    this.RestApiService.getAllMasterUser().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicSpv = this.namePic.filter(item => item.role_id === 2)
        this.nameEmployee = this.namePic.filter(item => item.role_id === 3 || item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getSpvToEmployee(){
    this.RestApiService.getAllTaskSpvDetail().subscribe(
      (res:any) => {
        this.dataSpvToEmploye = res.data; 
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onSendTo(event: any) {
    const sendName = event.name;
    const selectedSigner = this.namePicSpv.find(item => item.name === sendName);
    if (selectedSigner) {
      this.newTaskSpv.send_to = selectedSigner.name;
      this.newTaskSpv.email_spv = selectedSigner.email;
    }
  }

  onSwitchTo(event: any) {
    const sendName = event.name;
    const selectedSigner = this.nameEmployee.find(item => item.name === sendName);
    if (selectedSigner) {
      this.newTaskSpvToEmployee.switch_to = selectedSigner.name;
      this.newTaskSpvToEmployee.email_employee = selectedSigner.email;
    }
  }

  /**
   * Open modal
   * @param switch_to modal content
   */
  openModal(switch_to: any, id:any) {
    this.modalService.open(switch_to, { size: 'md', centered: true });
    this.dataTaskSpv.id = id
    this.newTaskSpvToEmployee.id_task_spv = id
  }

  updateTask(dataTaskSpv: any, id:any) {
    this.RestApiService.updateTaskSpv(id, dataTaskSpv).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTaskSpv = dataTaskSpv;
          this.getTaskSpv();
          this.getSpvToEmployee();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  isOverDate(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Is this task complete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const done = { id_status: 4 };
        this.updateTask(done, id)
      }
    });
  }
  
  onDeleteTask(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateTask(deleteData, id)
      }
    });
  }

updateSpvToEmployee(id: any) {
    const item = this.dataSpvToEmploye.find((item: any) => item.id === id);
    if (!item) return;

    const dataSpvToEmployee = { ...item };

    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });

        this.RestApiService.uploadMultiplePdf(formData).subscribe({
            next: (res: any) => {
                if (res && res.filename) {
                    this.pdf_url = [res.filename];
                    dataSpvToEmployee.file = this.pdf_url[0];
                    this.updateTaskSpvDetail(dataSpvToEmployee, id);
                }
            },
            error: (err) => {
                console.error(err);
            },
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.updateTaskSpvDetail(dataSpvToEmployee, id);
    }
}

updateTaskSpvDetail(dataSpvToEmploye: any, id: any) {
    this.RestApiService.updateTaskSpvDetail(id, dataSpvToEmploye).subscribe(
        (res: any) => {
            if (res.status == true) {
               this.newTaskSpvToEmployee = dataSpvToEmploye;
               this.getTaskSpv();
               this.getSpvToEmployee();
            }
        },
        (error) => {
            console.error(error);
        }
    );
}

onFileSelected(event: any, type: string) {
  const file = event.target.files[0];
  this.selectedFiles.push({ file, type });
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to upload this file?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      this.updateSpvToEmployee(type);
      Swal.fire({
        title: 'Success',
        text: 'File has been uploaded successfully',
        icon: 'success',
        customClass: {
          popup: 'custom-swal-text',
        },
        timer: 1500,
        timerProgressBar: true, 
        showConfirmButton: false 
      });
    }
  });
}

  insertTaskSpvDetail(dataSpvToEmploye:any){
    this.RestApiService.insertTaskSpvDetail(dataSpvToEmploye).subscribe(
      (res:any) => {
        if (res.status == true){
          this.idSpv = res.data;
          this.spv_to_employee= dataSpvToEmploye
          this.getSpvToEmployee();
          this.getTaskSpv();
        }
      })
    }

  insertSpvToEmployee(dataSpvToEmploye:any){
    const text = 'Task Recived'
    dataSpvToEmploye.response = text;
    const switch_to = '-'
    dataSpvToEmploye.switch_to = switch_to
    dataSpvToEmploye.status = 1;

    this.RestApiService.insertTaskSpvDetail(dataSpvToEmploye).subscribe(
      (res:any) => {
        if (res.status == true){
          this.idSpv = res.data;
          this.spv_to_employee= dataSpvToEmploye
          this.getSpvToEmployee();
          this.getTaskSpv();
        }
      })
  }

  reciveSpv(id:any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to recive this task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        this.dataTaskSpv.id = id
        this.newTaskSpvToEmployee.id_task_spv = id
        this.newTaskSpvToEmployee.id_status = 1
        this.insertSpvToEmployee(this.newTaskSpvToEmployee); 
        const updateStatus = { id_status: 2 };
        this.updateTask(updateStatus, id)
        this.getTaskSpv();
        this.getSpvToEmployee();
      }
    });
  } 

  switchSpvToEmployee(dataSpvToEmploye:any){
    const text = 'Task Switched'
    dataSpvToEmploye.response = text;
    dataSpvToEmploye.status = 1;
    dataSpvToEmploye.id_status = 1;
    dataSpvToEmploye.id_status_spv = 1;

    this.RestApiService.insertTaskSpvDetail(dataSpvToEmploye).subscribe(
      (res:any) => {
        if (res.status == true){
          this.idSpv = res.data;
          this.spv_to_employee= dataSpvToEmploye
          this.getTaskSpv();
          this.getSpvToEmployee();
        }
      })
  }

onSwitch(id: any) {
  this.switchSpvToEmployee(this.newTaskSpvToEmployee)
    Swal.fire({
      title: 'Success',
      text: 'Task has been switched successfully',
      icon: 'success',
      timer: 1500,
      timerProgressBar: true,
      showConfirmButton: false
    });
    this.getTaskSpv();
    this.getSpvToEmployee();
    const updateStatus = { id_status: 2 };
    this.updateTask(updateStatus, id)
    this.newTaskSpvToEmployee = {
      response: '',
      switch_to: '',
      email_employee: '',
      file: '',
      file_employee: '',
      notes_spv: '',
      notes_employe: '',
      notes_m1: '',
      revisi:'',
      id_status:null,
      id_status_spv: null,
      id_task_spv: null,
    };
    this.modalService.dismissAll();
}

  /**
   * Open modal
   * @param notes_spv_done modal content
   */
  notesSpvDone(notes_spv_done: any, item:any, id: any) {
    this.newTaskSpvToEmployee = { ...item };
    this.idDetail = id;
    this.modalService.open(notes_spv_done, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param notes_m1_done modal content
   */
  notesM1Done(notes_m1_done: any, item:any, id: any) {
    this.newTaskSpvToEmployee = { ...item };
    this.idDetail = id;
    this.modalService.open(notes_m1_done, { size: 'md', centered: true });
  }

    /**
 * Open modal
 * @param revisi_employee modal content
 */
    revisiEmployee(revisi_employee: any, item:any, id: any) {
      this.newTaskSpvToEmployee = { ...item };
      this.idDetail = id;
      this.modalService.open(revisi_employee, { size: 'md', centered: true });
    }

  onDoneTaskSpv() {
    this.newTaskSpvToEmployee.id_status_spv = 3;
    this.newTaskSpvToEmployee.id_status = 3
    this.newTaskSpvToEmployee.revisi = ''
    this.updateTaskSpvDetail(this.newTaskSpvToEmployee, this.idDetail);
    Swal.fire({
      title: 'Success',
      text: 'Task has been completed successfully!',
      icon: 'success',
      customClass: {
        popup: 'custom-swal-text',
      },
      timer: 1000, 
      timerProgressBar: true,
      showConfirmButton: false
    });
    this.modalService.dismissAll();
  }

  onTaskM1Done() {
    this.newTaskSpvToEmployee.id_status_spv = 3;
    this.newTaskSpvToEmployee.id_status = 3
    this.newTaskSpvToEmployee.revisi = ''
    this.updateTaskSpvDetail(this.newTaskSpvToEmployee, this.idDetail);
    Swal.fire({
      title: 'Success',
      text: 'Task has been completed successfully!',
      icon: 'success',
      customClass: {
        popup: 'custom-swal-text',
      },
      timer: 1000, 
      timerProgressBar: true,
      showConfirmButton: false
    });
    this.modalService.dismissAll();
  }

  onDeleteFileSpv(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this file?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { file: '', id_status_spv: 2, notes_spv: '' };
        this.updateTaskSpvDetail(deleteData, id)
      }
    });
  } 

  search() {
    this.dataTaskSpv = this.preData.filter((item: { task: string; send_to: string; due_date: string; }) => {
      return item.task.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.send_to.toLowerCase().includes(this.searchText.toLowerCase()) ||
             this.extractMonthFromDate(item.due_date).toLowerCase().includes(this.searchText.toLowerCase());
    });
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataTaskSpv = this.preData.filter((item: { due_date: string; }) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        case 5:
            return 'primary';
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Over Date';
        case 5:
          return 'Revisi';
        default:
            return 'Unknown';
    }
}

getPriorityLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'success'; 
      case 2:
          return 'warning'; 
      case 3:
          return 'orange'; 
      case 4:
          return 'danger';
      default:
          return 'secondary';
  }
}

getPriorityLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'Low';
      case 2:
          return 'Medium';
      case 3:
          return 'High';
      case 4:
          return 'Urgent';
      default:
          return 'Unknown';
  }
}

getPriorityId(priority: string): number {
  switch (priority) {
    case 'Urgent':
      return 4; 
    case 'High':
      return 3; 
    case 'Medium':
      return 2; 
    case 'Low':
      return 1; 
    default:
      return 1; 
  }
}

checkDate(due_date: string): string {
  const currentDate = new Date();
  const taskDueDate = new Date(due_date);
  const differenceInTime = taskDueDate.getTime() - currentDate.getTime();
  const differenceInDays = differenceInTime / (1000 * 3600 * 24);

  if (differenceInDays <= 0) {
    return 'Urgent';
  } else if (differenceInDays <= 6) {
    return 'High';
  } else if (differenceInDays <= 29) {
    return 'Medium';
  } else {
    return 'Low';
  }
}

checkDatePriority() {
  this.dataDatePriority.forEach(task => {
    const priority = this.checkDate(task.due_date);
    const newPriorityId = this.getPriorityId(priority);
    
    if (task.id_priority !== newPriorityId) {
      const newDueDate = new Date(task.due_date);
      newDueDate.setDate(newDueDate.getDate() + 1);
      task.due_date = newDueDate.toISOString().split('T')[0];
      task.id_priority = newPriorityId;
      this.updateTask(task, task.id);
    }

    const currentDate = new Date();
    const taskDueDate = new Date(task.due_date);
    const differenceInTime = currentDate.getTime() - taskDueDate.getTime();
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);

    if (differenceInDays > 1 && (task.id_status === 1 || task.id_status === 2)) {
      const newDueDate = new Date(task.due_date);
      newDueDate.setDate(newDueDate.getDate() + 1); 
      task.due_date = newDueDate.toISOString().split('T')[0];
      task.id_status = 4;
      this.updateTask(task, task.id);
    }
  });
}

  /**
   * Open modal
   * @param add_task modal content
   */
  addTask(add_task: any) {
    this.modalService.open(add_task, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param revisi modal content
   */
  revisiTask(revisi: any, task:any, id: any) {
    this.clearFormData();

    this.newTaskSpv = { ...task };
    this.newTaskSpv.due_date = this.convertDate(task.due_date)
    this.idTask = id;
    this.modalService.open(revisi, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  /**
 * Open modal
 * @param edit_task modal content
 */
  editTask(edit_task: any, task:any, id: any) {
    this.clearFormData()

    this.newTaskSpv = { ...task };
    this.newTaskSpv.due_date = this.convertDate(task.due_date)
    this.idTask = id;
    this.modalService.open(edit_task, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

insertTaskSpv(dataTask: any) {
  if (this.selectedFilesTask.length > 0) {
    const formData = new FormData();
    this.selectedFilesTask.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
    });

    this.RestApiService.uploadMultiplePdf(formData).subscribe({
      next: (res: any) => {
        if (res && res.filename) {
          this.pdf_url = [res.filename];
          dataTask.file_task = this.pdf_url[0];
          this.insertTaskFile(dataTask);
        }
      },
      error: (err) => console.error(err),
      complete: () => {
        this.selectedFilesTask = [];
      }
    });
  } else {
    this.insertTaskFile(dataTask);
  }
}

insertTaskFile(dataTask: any) {
  const today = new Date();
  const dueDate = new Date(dataTask.due_date);
  const checkInTime = dueDate.getTime() - today.getTime();
  const checkInDays = checkInTime / (1000 * 3600 * 24);

  if (checkInDays <= 0) {
    dataTask.id_priority = 4;
  } else if (checkInDays <= 6) {
    dataTask.id_priority = 3;
  } else if (checkInDays <= 29) {
    dataTask.id_priority = 2;
  } else {
    dataTask.id_priority = 1;
  }

  this.RestApiService.insertTaskSpv(dataTask).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.idTask = res.data[0];
        this.task_spv = dataTask;
        this.getTaskSpv();
      }
    });
}

onFileTaskSelected(event: any, type: string) {
  const file = event.target.files[0];
  this.selectedFilesTask.push({ file, type });
  console.log(this.selectedFilesTask);
}

onSubmit(){
  this.newTaskSpv.id_status = 1
  this.newTaskSpv.user = this.userData.name
  this.newTaskSpv.email_user = this.userData.email
  this.insertTaskSpv(this.newTaskSpv)
  Swal.fire({
    icon: 'success',
    title: 'Add Task Successfully',
    text: 'Add Task has been successfully.',
    timer: 1500,
    showConfirmButton: false,
  });
  this.modalService.dismissAll();
  this.getTaskSpv();
  this.newTaskSpv = {
    task: '',
    user: '',
    file_task: '',
    send_to: '',
    email_user: '',
    email_spv: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    revisi: '',
  };
}

updateTaskSpvWithFile(dataTask: any) {
  if (this.selectedFilesTask.length > 0) {
    const formData = new FormData();
    this.selectedFilesTask.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
    });

    this.RestApiService.uploadMultiplePdf(formData).subscribe({
      next: (res: any) => {
        if (res && res.filename) {
          this.pdf_url = [res.filename];
          dataTask.file_task = this.pdf_url[0];
          this.updateTaskSpv(dataTask);
        }
      },
      error: (err) => console.error(err),
      complete: () => {
        this.selectedFilesTask = [];
      }
    });
  } else {
    this.updateTaskSpv(dataTask);
  }
}

updateTaskSpv(dataTask:any){
this.RestApiService.updateTaskSpv(this.idTask, dataTask).subscribe(
  (res: any) => {
    if (res.status == true) {
      this.dataTaskSpv = dataTask;
      this.getTaskSpv();
    }
  },
  (error) => {
    console.error(error)
  }
)
}

onUpdate(){
this.updateTaskSpvWithFile(this.newTaskSpv);
  Swal.fire({
    icon: 'success',
    title: 'Update Successfully',
    text: 'Update has been successfully.',
    timer: 1500,
    showConfirmButton: false,
  });
this.modalService.dismissAll();
}

onRevisionEmployee(){
  this.newTaskSpvToEmployee.id_status_spv = 5
  this.newTaskSpvToEmployee.id_status = 2
  this.newTaskSpvToEmployee.notes_employe = ''
  this.updateTaskSpvDetail(this.newTaskSpvToEmployee, this.idDetail);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
  }

onRevision(){
  this.newTaskSpv.id_status = 5
  this.updateTaskSpvWithFile(this.newTaskSpv);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
  }

  isDone(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Is this task complete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const done = { id_status: 3 };
        this.updateTask(done, id)
        console.log(id);
      }
    });
  }

}
