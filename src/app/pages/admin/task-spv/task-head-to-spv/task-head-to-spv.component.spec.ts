import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskHeadToSpvComponent } from './task-head-to-spv.component';

describe('TaskHeadToSpvComponent', () => {
  let component: TaskHeadToSpvComponent;
  let fixture: ComponentFixture<TaskHeadToSpvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskHeadToSpvComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaskHeadToSpvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
