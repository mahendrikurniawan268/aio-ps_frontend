import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskHeadToSpvAddDataComponent } from './task-head-to-spv-add-data.component';

describe('TaskHeadToSpvAddDataComponent', () => {
  let component: TaskHeadToSpvAddDataComponent;
  let fixture: ComponentFixture<TaskHeadToSpvAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskHeadToSpvAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaskHeadToSpvAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
