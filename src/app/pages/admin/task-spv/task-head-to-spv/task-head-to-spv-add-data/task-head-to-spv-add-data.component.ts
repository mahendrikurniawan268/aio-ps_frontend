import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { TaskSpv } from '../task-head-to-spv.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-task-head-to-spv-add-data',
  templateUrl: './task-head-to-spv-add-data.component.html',
  styleUrls: ['./task-head-to-spv-add-data.component.scss']
})
export class TaskHeadToSpvAddDataComponent implements OnInit{
  @Input() taskSpv: any = {}
  @Output() dataInserted: EventEmitter<any> = new EventEmitter<any>();
  
  public idParam:any;
  task_spv:any;
  idTask:any;
  statusData: any[] = [];
  priorityData: any[] = [];
  dataTaskSpv:any;
  namePic: any [] = [];
  namePicSpv: any [] = [];
  namePicEmployee: any [] = [];

  newTaskSpv: TaskSpv = {
    task: '',
    user: '',
    file_task: '',
    send_to: '',
    email_user: '',
    email_spv: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    revisi: '',
  };

  constructor(private router:Router, private RestApiService:RestApiService, private route:ActivatedRoute,
    private modalService:NgbModal){}

  ngOnInit(): void {
    this.getNameSpv();
    this.getNameEmployee();
    if (this.idTask) {
      this.getTaskSpvById(this.idTask);
    }
  } 

  closeEventModal() {
    this.modalService.dismissAll();
  }

  getNameSpv(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicSpv = this.namePic.filter(item => item.role_id === 2)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getNameEmployee(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicEmployee = this.namePic.filter(item => item.role_id === 3)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  forBack(){
    this.router.navigate(['task/task-head-to-spv'])
  }

  insertTaskSpv(dataTask:any){
    const today = new Date();
    const dueDate = new Date(dataTask.due_date);

    const checkInTime = dueDate.getTime() - today.getTime();
    const checkInDays = checkInTime / (1000 * 3600 * 24);

    if (checkInDays <= 0) {
      dataTask.id_priority = 4;
    } else if (checkInDays <= 6) {
      dataTask.id_priority = 3;
    } else if (checkInDays <= 29) {
      dataTask.id_priority = 2;
    } else {
      dataTask.id_priority = 1;
    }

    this.RestApiService.insertTaskSpv(dataTask).subscribe(
      (res:any) => {
        if (res.status == true){
          this.idTask = res.data[0]
          this.task_spv = dataTask
          this.task_spv.id = this.idTask
          this.dataInserted.emit(this.task_spv);
        }
      })
  }

updateTaskSpv(dataTask:any){
  this.RestApiService.updateTaskSpv(this.idTask, dataTask).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.dataTaskSpv = dataTask;
      }
    },
    (error) => {
      console.error(error)
    }
  )
}

onUpdate(){
  this.updateTaskSpv(this.newTaskSpv);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
  this.modalService.dismissAll();
}

onSubmit(){
  this.insertTaskSpv(this.newTaskSpv)
  Swal.fire({
    icon: 'success',
    title: 'Add Task Successfully',
    text: 'Add Task has been successfully.',
    timer: 1500,
    showConfirmButton: false,
  });
  this.modalService.dismissAll();
  this.getTaskSpv();
}

getTaskSpv(){
  this.RestApiService.getAllTaskSpv().subscribe(
    (res:any) => {
      this.dataTaskSpv = res.data;
    },
    (error:any) => {
      console.error(error)
    }
  );
}

getTaskSpvById(id:any){
  this.RestApiService.getTaskSpvById(id).subscribe(
    (res:any) => {
      this.newTaskSpv = res.data[0];
      this.newTaskSpv.due_date = this.convertDate(this.newTaskSpv.due_date);
      console.log(this.taskSpv);
    })
}

convertDate(date: string): string {
  if (date) {
    const inputDate = new Date(date);
    inputDate.setDate(inputDate.getDate() + 1);
    const formattedDate = inputDate.toISOString().split('T')[0];
    return formattedDate;
  }
  return '';
}
}

