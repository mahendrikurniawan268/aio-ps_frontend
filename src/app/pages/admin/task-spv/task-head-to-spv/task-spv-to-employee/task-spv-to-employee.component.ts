import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { TaskSpv } from '../task-head-to-spv.model';
import { TaskSpvToEmployee } from '../task-spv-to-employee.model';
import { TaskHeadToSpvAddDataComponent } from '../task-head-to-spv-add-data/task-head-to-spv-add-data.component';

@Component({
  selector: 'app-task-spv-to-employee',
  templateUrl: './task-spv-to-employee.component.html',
  styleUrls: ['./task-spv-to-employee.component.scss']
})
export class TaskSpvToEmployeeComponent implements OnInit{
  tableColumns = ['Task', 'Owner', 'Due Date', 'Status', 'Priority', 'Notes', 'Details'];
  tableColumns2 = ['Response', 'Swich To', 'File', 'Notes', 'Details']
  titlePage = 'Improvement List';
  index: number = 0;
  dataForCurrentPage: any = [];
  dataTaskEmployee:any = [];
  dataSpvToEmploye:any[] = [];
  dataApprovalBySwitchTo: any = [];
  public subtask:any = [];
  spv_to_employee:any;
  idTaskEmploye : any;
  updateNotesSpv:any = [];
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  preData:any [] = [];
  searchText:string = '';
  startDate: string = '';
  endDate: string = '';
  selectedFiles: { file: File, type: string }[] = [];
  pdf_url:any
  namePic: any [] = []
  namePicSpv: any [] = []
  currentPage = 1;
  itemsPerPage = 10;
  searchData: any;

  newTaskSpv: TaskSpv = {
    task: '',
    user: '',
    file_task: '',
    send_to: '',
    email_user: '',
    email_spv: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    revisi: '',
  };

  newTaskSpvToEmployee: TaskSpvToEmployee = {
    response: '',
    switch_to: '',
    email_employee: '',
    file: '',
    file_employee: '',
    notes_spv: '',
    notes_employe: '',
    notes_m1: '',
    revisi:'',
    id_status:null,
    id_status_spv: null,
    id_task_spv: null,
  };

  constructor(private router : Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private modalService:NgbModal){}

  ngOnInit(): void {
    this.getTaskEmployee();
    this.getSpvToEmployee();
    this.paginateData();
    if (this.idTaskEmploye) {
      this.getTaskSpvToEmployeById(this.idTaskEmploye);
    }
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  subTask = [
    { source: 'task', task_id:1 },
    { source: 'subtask1', id_task:1 },
    { source: 'subtask2', id_task:2 }
  ]

  getTaskEmployee(){
    this.RestApiService.getTaskEmployee().subscribe(
      (res:any) => {
        this.dataTaskEmployee = res.data[0];
        this.preData = [...this.dataTaskEmployee]
        this.dataApprovalBySwitchTo = this.preData.filter(item => item.switch_to === this.userData.name)
        this.searchData = this.dataApprovalBySwitchTo
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getSpvToEmployee(){
    this.RestApiService.getAllTaskSpvDetail().subscribe(
      (res:any) => {
        this.dataSpvToEmploye = res.data; 
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getTaskSpvToEmployeById(id:any){
    this.RestApiService.getTaskSpvDetailById(id).subscribe(
      (res:any) => {
        this.newTaskSpvToEmployee = res.data[0];
      })
  }

  updateTask(dataTaskEmployee: any, id:any) {
    this.RestApiService.updateTaskSpv(id, dataTaskEmployee).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTaskSpv = dataTaskEmployee;
          this.getTaskEmployee();
          
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  isRecive(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to recive this task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const updateStatus = { id_status: 2, id_status_spv: 2 };
        this.updateTaskSpvDetail(updateStatus, id)
        this.getTaskEmployee();
      }
    });
  }

  isDone(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Is this task complete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const done = { id_status: 3 };
        this.updateTaskSpvDetail(done, id)
      }
    });
  }

  isChangeFile(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Change File?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const done = { file_employee: '', id_status: 2 };
        this.updateTaskSpvDetail(done, id)
      }
    });
  }

  /**
 * Open modal
 * @param notes_employe_done modal content
 */
  notesSpvDone(notes_employe_done: any, id: any) {
    this.newTaskSpvToEmployee = this.dataSpvToEmploye.find(item => item.id === id);
    this.idTaskEmploye = id
    console.log(this.newTaskSpvToEmployee);
    if (this.newTaskSpvToEmployee) {
    } else {
        console.error("Data tidak ditemukan untuk ID yang dipilih");
    }
    this.modalService.open(notes_employe_done, { size: 'md', centered: true });
}

  onDoneTaskEmploye() {
    this.newTaskSpvToEmployee.id_status = 3;
    this.updateTaskSpvDetail(this.newTaskSpvToEmployee, this.idTaskEmploye);
    Swal.fire({
      title: 'Success',
      text: 'Task has been completed successfully!',
      icon: 'success',
      customClass: {
        popup: 'custom-swal-text',
      },
      timer: 1000, 
      timerProgressBar: true,
      showConfirmButton: false
    });
    this.modalService.dismissAll();
  }

updateSpvToEmployee(id: any) {
    const item = this.dataSpvToEmploye.find((item: any) => item.id === id);
    if (!item) return;

    const dataSpvToEmployee = { ...item };

    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });
        this.RestApiService.uploadMultiplePdf(formData).subscribe({
            next: (res: any) => {
                if (res && res.filename) {
                    this.pdf_url = [res.filename];
                    dataSpvToEmployee.file_employee = this.pdf_url[0];
                    this.updateTaskSpvDetail(dataSpvToEmployee, id);
                }
            },
            error: (err) => {
                console.error(err);
            },
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.updateTaskSpvDetail(dataSpvToEmployee, id);
    }
}

updateTaskSpvDetail(dataSpvToEmploye: any, id: any) {
    this.RestApiService.updateTaskSpvDetail(id, dataSpvToEmploye).subscribe(
        (res: any) => {
            if (res.status == true) {
               this.newTaskSpvToEmployee = dataSpvToEmploye;
               this.getTaskEmployee();
               this.getSpvToEmployee();
            }
        },
        (error) => {
            console.error(error);
        }
    );
}

onFileSelected(event: any, type: string) {
  const file = event.target.files[0];
  this.selectedFiles.push({ file, type });
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to upload this file?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      this.updateSpvToEmployee(type);
      Swal.fire({
        title: 'Success',
        text: 'File has been uploaded successfully',
        icon: 'success',
        customClass: {
          popup: 'custom-swal-text',
        },
        timer: 1500,
        timerProgressBar: true, 
        showConfirmButton: false 
      });
    }
  });
}

  search() {
    this.dataApprovalBySwitchTo = this.searchData.filter((item: { task: string; send_to: string; due_date: string; }) => {
      return item.task.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.send_to.toLowerCase().includes(this.searchText.toLowerCase()) ||
             this.extractMonthFromDate(item.due_date).toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataApprovalBySwitchTo = this.searchData.filter((item: { task: string; owner: string; due_date: string; }) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataApprovalBySwitchTo.slice(startIndex, endIndex);
  }
  
  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; 
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Late';
        default:
            return 'Unknown';
    }
}

getStatusLabelClassSpv(id_status_spv: number): string {
  switch (id_status_spv) {
      case 1:
          return 'success'; // New
      case 2:
          return 'info'; // On Progress
      case 3:
          return 'secondary'; // Done
      case 4:
          return 'danger'; // Late
      case 5:
          return 'primary';
      default:
          return 'secondary';
  }
}

getStatusLabelSpv(id_status_spv: number): string {
  switch (id_status_spv) {
      case 1:
          return 'New';
      case 2:
          return 'On Progress';
      case 3:
          return 'Done';
      case 4:
          return 'Over Date';
      case 5:
        return 'Revisi';
      default:
          return 'Unknown';
  }
}

getPriorityLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'success'; 
      case 2:
          return 'warning'; 
      case 3:
          return 'orange'; 
      case 4:
          return 'danger';
      default:
          return 'secondary';
  }
}

getPriorityLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'Low';
      case 2:
          return 'Medium';
      case 3:
          return 'High';
      case 4:
          return 'Urgent';
      default:
          return 'Unknown';
  }
}

}