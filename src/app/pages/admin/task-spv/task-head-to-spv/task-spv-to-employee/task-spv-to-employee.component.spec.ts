import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskSpvToEmployeeComponent } from './task-spv-to-employee.component';

describe('TaskSpvToEmployeeComponent', () => {
  let component: TaskSpvToEmployeeComponent;
  let fixture: ComponentFixture<TaskSpvToEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskSpvToEmployeeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaskSpvToEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
