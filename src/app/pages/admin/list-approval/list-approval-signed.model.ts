export interface ApprovalSigned {
    file_signed: string;
    notes: string;
    id_status_signed: number | null;
    id_approval: number | null;
  }