export interface Pro {
    name: string;
    email: string;
    date: string;
    pro_description: string;
    file: string;
    file_signed: string;
    signer: string;
    email_signer: string;
    notes: string;
    id_status:number | null;
  }