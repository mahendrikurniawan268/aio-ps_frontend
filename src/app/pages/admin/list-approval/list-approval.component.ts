import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import { ApprovalSigned } from './list-approval-signed.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Pro } from './pro.model';

@Component({
  selector: 'app-list-approval',
  templateUrl: './list-approval.component.html',
  styleUrls: ['./list-approval.component.scss']
})
export class ListApprovalComponent implements OnInit{
  tableColumns = ['No', 'Name', 'Departement', 'Title','File', 'Status', 'Details'];

  searchText1:string = '';
  searchText2:string = '';
  dataForCurrentPage1: any [] = [];
  dataForCurrentPage2: any [] = [];
  preData1:any [] = [];
  preData2:any;
  index = 0;
  currentPage1 = 1;
  itemsPerPage1 = 10;
  currentPage2 = 1;
  itemsPerPage2 = 10;
  dataSigned: any = []
  dataApproval: any = []
  dataPro: any = []
  dataApprovalBySigner: any = []
  dataPdf:any;
  pdf_url:any
  idPro:any
  id_signed:any;
  selectedFiles: { file: File, type: string }[] = [];
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  searchData: any;
  isTableView: boolean = false;

  newApprovalSigned : ApprovalSigned = {
    file_signed: '',
    notes: '',
    id_status_signed: null,
    id_approval: null
  }

  newPro: Pro = {
    name: '',
    email: '',
    date: '',
    pro_description: '',
    file: '',
    file_signed: '',
    signer: 'Dian Alpiansyah',
    email_signer: 'dalpiansyah@aio.co.id',
    notes: '',
    id_status: 1
  };

  constructor(private router:Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private modalService : NgbModal){}

  ngOnInit(): void {
    this.getApproval();
    this.getPro();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id == 'btnApproval') {
      this.isTableView = false
      this.router.navigate(["/menu/list-approval"], {
        queryParams: { tableApproval: false },
      });
    } else if (event.target.id == 'btnPro') {
      this.isTableView = true
      this.router.navigate(["/menu/list-approval"], {
        queryParams: { tablePro: true },
      });
    }
  }

  /**
   * Open modal
   * @param approve modal content
   */
  openApprove(approve: any, id:any) {
    this.dataApproval.id = id;
    this.newApprovalSigned.id_approval = id
    this.modalService.open(approve, { size: 'md', centered: true });
}

  /**
   * Open modal
   * @param reject modal content
   */
  openReject(reject: any, id:any) {
    this.dataApproval.id = id;
    this.newApprovalSigned.id_approval = id
    this.modalService.open(reject, { size: 'md', centered: true });
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  getApproval(){
    this.RestApiService.getAllApproval().subscribe(
      (res:any) => {
        this.dataApproval = res.data;
        this.preData1 = [...this.dataApproval];
        this.dataApprovalBySigner = this.preData1.filter(item => item.signer === this.userData.name)
        this.searchData = this.dataApprovalBySigner
        console.log(this.dataApprovalBySigner);

        this.paginateData1();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertApprovalSigned(dataFile: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
      this.RestApiService.uploadMultiplePdf(formData).subscribe({
        next: (res: any) => {
          if (res && res.filename) { 
            this.pdf_url = [res.filename];
            dataFile.file_signed = this.pdf_url[0];
          this.RestApiService.insertApprovalSigned(dataFile).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_signed = res.data;
                this.dataSigned = dataFile;
              }
            },
            (error) => {
              console.error(error);
            }
          );
          }
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.RestApiService.insertApprovalSigned(dataFile).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_signed = res.data;
            this.dataSigned = dataFile;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  
  
  onApprove(id:any) {
    this.newApprovalSigned.id_status_signed = 3;
    this.insertApprovalSigned(this.newApprovalSigned);
    Swal.fire({
      title: 'File Added',
      text: 'File has been successfully added.',
      icon: 'success',
      confirmButtonText: 'OK',
    })
    .then((result) => {
      if (result.isConfirmed) {
        const updateData = { id_status: 3 };
        this.updateApproval(updateData, id);
      }
    });
    this.newApprovalSigned = {
      file_signed: '',
      notes: '',
      id_status_signed: null,
      id_approval: null
    };
    this.modalService.dismissAll();
  }

  updateApproval(dataFile: any, id:any) {
    this.RestApiService.updateApproval(id, dataFile).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.dataApproval = dataFile;
          this.getApproval();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onReject(id:any){
    this.newApprovalSigned.id_status_signed = 2;
    this.insertApprovalSigned(this.newApprovalSigned);
    Swal.fire({
      title: 'Reject Success!',
      text: 'Reject has been successfully.',
      icon: 'success',
      confirmButtonText: 'OK',
    })
    .then((result) => {
      if (result.isConfirmed) {
        const updateData = { id_status: 2 };
        this.updateApproval(updateData, id);
      }
    });
    this.newApprovalSigned = {
      file_signed: '',
      notes: '',
      id_status_signed: null,
      id_approval: null
    };
    this.modalService.dismissAll();
  }

  getPro(){
    this.RestApiService.getAllPro().subscribe(
      (res:any) => {
        this.dataPro = res.data;
        this.preData2 = [...this.dataPro];
        this.paginateData2();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateProWithFile(dataPro: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
        formData.append('files', fileData.file, fileData.file.name);
        formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultiplePdf(formData).subscribe({
        next: (res: any) => {
          if (res && res.filename) {
            this.pdf_url = [res.filename];
            dataPro.file_signed = this.pdf_url[0];
            this.updatePro(dataPro);
          }
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.updatePro(dataPro);
    }
  }
  
  updatePro(dataPro:any){
  const originalDate = new Date(dataPro.date);
  originalDate.setDate(originalDate.getDate() + 1);
  const newDate = originalDate.toISOString().split('T')[0];
  dataPro.date = newDate;
  this.RestApiService.updatePro(this.idPro, dataPro).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.dataPro = dataPro;
        this.getPro();
      }
    },
    (error) => {
      console.error(error)
    }
  )
  }  
  
  onApprovePro() {
    this.newPro.id_status = 3;
    this.updateProWithFile(this.newPro);
    Swal.fire({
      title: 'File Added',
      text: 'File has been successfully added.',
      icon: 'success',
      confirmButtonText: 'OK',
    });
    this.modalService.dismissAll();
  }

  onRejectPro(){
    this.newPro.id_status = 2;
    this.updateProWithFile(this.newPro);
    Swal.fire({
      title: 'File Added',
      text: 'File has been successfully added.',
      icon: 'success',
      confirmButtonText: 'OK',
    });
    this.modalService.dismissAll();
  }

   /**
   * Open modal
   * @param approvePro modal content
   */
   openApprovePro(approvePro: any, pro:any, id: any) {
    this.newPro = { ...pro };
    console.log(this.newPro);
    
    this.idPro = id;
    console.log(this.idPro);

    this.modalService.open(approvePro, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param rejectPro modal content
   */
  openRejectPro(rejectPro: any, pro:any,  id:any) {
    this.newPro = { ...pro };
    this.idPro = id;
    this.modalService.open(rejectPro, { size: 'md', centered: true });
  }

  search1() {
    this.dataApprovalBySigner = this.searchData.filter((item: { name: string; departement: string; title: string; }) => {
      return item.name.toLowerCase().includes(this.searchText1.toLowerCase()) ||
             item.departement.toLowerCase().includes(this.searchText1.toLowerCase()) ||
             item.title.toLowerCase().includes(this.searchText1.toLowerCase());
    });
    this.paginateData1();
  }

  paginateData1() {
    const startIndex = (this.currentPage1 - 1) * this.itemsPerPage1;
    const endIndex = startIndex + this.itemsPerPage1;
    this.dataForCurrentPage1 = this.dataApprovalBySigner.slice(startIndex, endIndex);
  }

  onPageChange1(newPage: number) {
    this.currentPage1 = newPage;
    this.paginateData1();
  }

  search2() {
    this.dataPro = this.preData2.filter((item: { name: string; pro_description: string; }) => {
      return item.name.toLowerCase().includes(this.searchText2.toLowerCase()) ||
             item.pro_description.toLowerCase().includes(this.searchText2.toLowerCase());
    });
    this.paginateData2();
  }

  paginateData2() {
    const startIndex = (this.currentPage2 - 1) * this.itemsPerPage2;
    const endIndex = startIndex + this.itemsPerPage2;
    this.dataForCurrentPage2 = this.dataPro.slice(startIndex, endIndex);
  }

  onPageChange2(newPage: number) {
    this.currentPage2 = newPage;
    this.paginateData2();
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'danger'; // On Progress
        case 3:
            return 'secondary'; // Done
        default:
            return 'secondary';
    }
}

  getStatusLabel(id_status: number): string {
      switch (id_status) {
          case 1:
              return 'New';
          case 2:
              return 'Reject';
          case 3:
              return 'Done';
          default:
              return 'Unknown';
      }
  }
}
