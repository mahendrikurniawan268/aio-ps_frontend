import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadApproval } from './upload-approval.model';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-upload-approval',
  templateUrl: './upload-approval.component.html',
  styleUrls: ['./upload-approval.component.scss']
})
export class UploadApprovalComponent implements OnInit{

  constructor(private RestApiService:RestApiService, private router:Router,
    private authService:AuthenticationService){}
  
  @ViewChild(DropzoneComponent) dropzone!: DropzoneComponent;

  departementOptions = ['AL4', 'CAN', 'PET', 'GBL', 'IT', 'OC3','QA', 'Technical', 'Other'];
  departementSearchKeyword: string = '';
  dataSigner:any [] = [];
  user:any [] = [];
  fileName: any =[];
  selectedFiles: { file: File, type: string }[] = [];
  pdf_url:any
  id_approval:any;
  dataApproval:any;
  signerSelected:any;
  userData:any = [];

  newUploadApproval : UploadApproval = {
    name: '',
    email: '',
    departement: '',
    title: '',
    file_upload: '',
    email_signer: '',
    signer: ''
  }

  ngOnInit(): void {
    this.getSigner();
    this.userData = this.authService.getUserData();
  }

  onDepartementSelect(selectedItem: string) {
    this.newUploadApproval.departement = selectedItem;
  }

  getSigner(){
    this.RestApiService.getAllMasterUser().subscribe(
      (res:any) => {
        this.user = res.data
        this.dataSigner = this.user.filter(item => item.role_id == 1 || item.role_id == 2)
    })
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    if (!file || file.type !== 'application/pdf') {
      Swal.fire({
        title: 'Error!',
        text: 'File Not PDF, Change Your File.',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      event.target.value = null;
      return;
    }
    this.selectedFiles.push({ file, type });
  }

  onSignerChange(event: any) {
    const signerName = event.name;
    const selectedSigner = this.dataSigner.find(item => item.name === signerName);
    if (selectedSigner) {
      this.newUploadApproval.signer = selectedSigner.name
      this.newUploadApproval.email_signer = selectedSigner.email;
    }
}

  insertApproval(dataFile: any) {
    const formData = new FormData();
    this.selectedFiles.forEach(fileData => {
    formData.append('files', fileData.file, fileData.file.name);
    formData.append('type', fileData.type);
    });

    this.RestApiService.uploadMultiplePdf(formData).subscribe({
      next: (res: any) => {
        if (res && res.filename) { 
          this.pdf_url = [res.filename];
          dataFile.file_upload = this.pdf_url[0];
        this.RestApiService.insertApproval(dataFile).subscribe(
          (res: any) => {
            if (res.status == true) {
              this.id_approval = res.data;
              this.dataApproval = dataFile;
            }
          },
          (error) => {
            console.error(error);
          }
        );
        }
      },
      error: (err) => console.error(err),
      complete: () => {
        this.selectedFiles = [];
      }
    });
  } 
  
  onInsertData() {
    if (this.selectedFiles.length === 0) {
      Swal.fire({
        title: 'File Empty',
        text: 'Please select a file before submitting.',
        icon: 'warning',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      });
      return;
    }
    this.newUploadApproval.name = this.userData.name;
    this.newUploadApproval.email = this.userData.email;
    this.insertApproval(this.newUploadApproval);
    Swal.fire({
      title: 'File Added',
      text: 'File has been successfully added.',
      icon: 'success',
      confirmButtonText: 'OK',
      allowOutsideClick: false
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['task/signed-approval']);
      }
    });
  }

}
