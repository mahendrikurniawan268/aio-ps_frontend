export interface UploadApproval {
    name: string;
    email: string;
    departement: string;
    title: string;
    file_upload: string;
    email_signer: string;
    signer: string;
  }