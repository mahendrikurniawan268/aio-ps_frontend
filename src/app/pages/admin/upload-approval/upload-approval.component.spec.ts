import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadApprovalComponent } from './upload-approval.component';

describe('UploadApprovalComponent', () => {
  let component: UploadApprovalComponent;
  let fixture: ComponentFixture<UploadApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadApprovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
