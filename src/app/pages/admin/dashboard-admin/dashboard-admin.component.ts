import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { CommonService } from 'src/app/core/services/common.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit{
  tableColumns = ['Name', 'Total Task', 'Presentase On Time', 'Value'];
  constructor(private RestApiService: RestApiService, private authService:AuthenticationService, private common: CommonService) {
    this.month = new Date().getMonth() + 1
    this.year = new Date().getFullYear()
    this.fromDate = this.getDefaultFromDate();
    this.toDate = this.getDefaultToDate();
    this.datePlaceholder = `${common.getMonthName(this.month)} ${this.year}`
  }

  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  status7: any;
  employee: any;
  simplePieChart: any;
  proPieChart: any;

  //Task Spv
  dataAllTaskSpv: any[] = [];
  dataTaskSpv:any [] = []
  dataTaskSpvByName:any [] = []
  dataAchievements:any []= []
  countDataAllTaskSpv: number = 0;
  countDataAllTaskSpvByName: number = 0;
  deadlineDataTaskSpv: any[] = [];
  countDataDeadlineTaskSpv: number = 0;
  //task
  countYetToStart: number = 0;
  countOnProgress: number = 0;
  countCompleted: number = 0;
  countOverDate: number = 0;
  //byname
  countYetToStartByName: number = 0;
  countOnProgressByName: number = 0;
  countCompletedByName: number = 0;
  countOverDateByName: number = 0;

  //approval
  dataAllApproval:any [] = [];
  dataApprovalByName:any [] = [];
  countNewData: number = 0
  countApproveData: number = 0
  countRejectData: number = 0

  //pro
  dataPro:any [] = [];
  countNewDataPro: number = 0
  countApproveDataPro: number = 0
  countRejectDataPro: number = 0

  month: number
  monthBefore!: number
  year: number
  yearBefore!: number

  fromDate: string;
  toDate: string;
  datePlaceholder: string
  dateRange = {from: new Date(), to: new Date()}

  async ngOnInit() {
  await this.getTaskSpv(this.fromDate, this.toDate);
  await this.getAchievementsSpv(this.fromDate, this.toDate);
  this.getAllApproval();
  this.getAllPro(this.fromDate, this.toDate);
  this.getTaskSpvByName(this.fromDate, this.toDate);
  this.userData = this.authService.getUserData();
  this.isAdmin = this.authService.isAdmin();
  this.isSpv = this.authService.isSpv();
  this.isEmployee = this.authService.isEmployee();

  this._simplePieChart('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]');
  this._proPieChart('["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]');
  this._status7('["--vz-danger", "#FFF798", "--vz-warning", "--vz-secondary"]');
  this._employee('["--vz-danger", "#FFF798", "--vz-warning", "--vz-secondary"]');
  }

  getDefaultFromDate(): string {
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const fromDate = `${year}-${monthFilter}-01`
    return fromDate
  }

  getDefaultToDate(): string {
    const getLastDayOfMonth = (year: number, month: number) => {
      month -= 1
      return new Date(year, month + 1, 0).getDate();
    }
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const lastDayOfMonth = getLastDayOfMonth(year, month)
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const toDate = `${year}-${monthFilter}-${lastDayOfMonth}`
    return toDate
  }

 onChangeDateRange(event: any) {
  const value = event.target.value as string;
  const datesArray = value.split(' to ');

  let newFromDate = '';
  let newToDate = '';

  if (datesArray.length >= 1) {
    newFromDate = datesArray[0];
    newToDate = datesArray[datesArray.length - 1];
  }

  if (newFromDate !== this.fromDate || newToDate !== this.toDate) {
    this.fromDate = newFromDate;
    this.toDate = newToDate;
    this.ngOnInit();
  }
}

  getValueLabelClass(presentase: number): string {
    if (presentase >= 80) {
        return "star-5";
    } else if (presentase >= 60) {
        return "star-4";
    } else if (presentase >= 40) {
        return "star-3";
    } else if (presentase >= 20) {
        return "star-2";
    } else {
        return "star-1";
    }
}

  getAllPro(from: string, to:string){
    this.RestApiService.getProByFilter(from, to).subscribe(
      (res: any) => {
        this.dataPro = res.data;
        this.countNewData = this.dataPro.filter(item => item.id_status === 1).length
        this.countApproveData = this.dataPro.filter(item => item.id_status === 3).length
        this.countRejectData = this.dataPro.filter(item => item.id_status === 2).length

        this._proPieChart('["--vz-success", "--vz-primary", "--vz-danger", "--vz-info"]');
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  _proPieChart(colors:any) {
    const NewData = this.countNewData
    const ApproveData = this.countApproveData
    const RejectData = this.countRejectData
    
    colors = this.getChartColorsArray(colors);
    this.proPieChart = {
      series: [NewData, ApproveData, RejectData],
      chart: {
        height: 242,
        type: "pie",
      },
      labels: ["New", "Approve", "Reject"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
      colors: colors,
    };
  }

  getAllApproval() {
    this.RestApiService.getAllApproval().subscribe(
      (res:any) => {
      this.dataAllApproval = res.data;
      this.dataApprovalByName = this.dataAllApproval.filter(item => item.signer === this.userData.name)
      this.countNewData = this.dataApprovalByName.filter(item => item.id_status === 1).length
      this.countApproveData = this.dataApprovalByName.filter(item => item.id_status === 3).length
      this.countRejectData = this.dataApprovalByName.filter(item => item.id_status === 2).length

      this._simplePieChart('["--vz-success", "--vz-primary", "--vz-danger", "--vz-info"]');
    })
  }

  _simplePieChart(colors:any) {
    const NewData = this.countNewData
    const ApproveData = this.countApproveData
    const RejectData = this.countRejectData
    
    colors = this.getChartColorsArray(colors);
    this.simplePieChart = {
      series: [NewData, ApproveData, RejectData],
      chart: {
        height: 242,
        type: "pie",
      },
      labels: ["New", "Approve", "Reject"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
      colors: colors,
    };
  }  

  async getTaskSpv(from: string, to:string) {
    try {
      const res: any = await this.RestApiService.getTaskSpvByFilter(from, to).toPromise();
      this.dataTaskSpv = res.data;
      this.countDataAllTaskSpv = this.dataTaskSpv.length;
      this.updateTaskCounts();  
    } catch (error) {
      console.error(error);
    }
  }

  getTaskSpvByName(from: string, to:string) {
    this.RestApiService.getTaskSpvByFilter(from, to).subscribe(
      (res:any) => {
        this.dataTaskSpv = res.data;
        this.dataTaskSpvByName = this.dataTaskSpv.filter(item => item.send_to === this.userData.name)
        this.countDataAllTaskSpvByName = this.dataTaskSpvByName.length;  
        this.updateScheduleCountsByName()
        
        this._employee('["--vz-danger", "#FFF798", "--vz-warning", "--vz-secondary"]');
      }
    )
  }

  async getAchievementsSpv(from: string, to:string) {
    try {
      const res: any = await this.RestApiService.getAchievementsSpv(from, to).toPromise();
      this.dataAchievements = res.data;
      this.dataAchievements.forEach(item => {
        item.presentase = (item.ontime_task / item.total_task) * 100;
    });
    } catch (error) {
      console.error(error);
    }
  }

  updateTaskCounts(){
    const statusIds = [1, 2, 3, 4];

    const taskCounts: any = {};
    statusIds.forEach((statusId: number) => {
      const count = this.dataTaskSpv.filter((item: any) => item.id_status === statusId).length;
      taskCounts[statusId] = count;
    });

    this.countYetToStart = taskCounts[1] || 0;
    this.countOnProgress = taskCounts[2] || 0;
    this.countCompleted = taskCounts[3] || 0;
    this.countOverDate = taskCounts[4] || 0;
    }

  updateScheduleCountsByName(){
    const statusIds = [1, 2, 3, 4];

    const taskCounts: any = {};
    statusIds.forEach((statusId: number) => {
      const count = this.dataTaskSpvByName.filter((item: any) => item.id_status === statusId).length;
      taskCounts[statusId] = count;
    });

    this.countYetToStartByName = taskCounts[1] || 0;
    this.countOnProgressByName = taskCounts[2] || 0;
    this.countCompletedByName = taskCounts[3] || 0;
    this.countOverDateByName = taskCounts[4] || 0;
    }

    /**
 *  Status7
 */
    setstatusvalue(value: any) {
      if (value == 'all') {
        this.status7.series = [125, 42, 58, 89]
      }
      if (value == '7') {
        this.status7.series = [25, 52, 158, 99]
      }
      if (value == '30') {
        this.status7.series = [35, 22, 98, 99]
      }
      if (value == '90') {
        this.status7.series = [105, 32, 68, 79]
      }
    }

    // Chart Colors Set
  private getChartColorsArray(colors: any) {
    colors = JSON.parse(colors);
    return colors.map(function (value: any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
        if (color) {
          color = color.replace(" ", "");
          return color;
        }
        else return newValue;;
      } else {
        var val = value.split(',');
        if (val.length == 2) {
          var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
          rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
          return rgbaColor;
        } else {
          return newValue;
        }
      }
    });
  }
  
    _status7(colors: any) {
      colors = this.getChartColorsArray(colors);
      const dataDoneOnTime = this.dataTaskSpv.filter(item => item.id_status === 3);
      const dataDoneOverDate = this.dataTaskSpv.filter(item => item.id_status === 4);
      const onTimeCount = dataDoneOnTime.length;
      const overDate = dataDoneOverDate.length;

      this.status7= {
        series: [onTimeCount, overDate],
        labels: ["On Time", "Over Date"],
        chart: {
          type: "donut",
          height: 250,
          toolbar: {
            show: true,
          },
        },
        plotOptions: {
          pie: {
            offsetX: 0,
            offsetY: 0,
            donut: {
              size: "90%",
              labels: {
                show: false,
              }
            },
          },
        },
        dataLabels: {
          enabled: false,
        },
        legend: {
          show: false,
        },
        stroke: {
          lineCap: "round",
          width: 0
        },
        colors: colors
      };
    }

    _employee(colors: any) {
      colors = this.getChartColorsArray(colors);
      const dataDoneOnTime = this.dataTaskSpvByName.filter(item => item.id_status === 3);
      const dataDoneOverDate = this.dataTaskSpvByName.filter(item => item.id_status === 4);
      const onTimeCount = dataDoneOnTime.length;
      const overDate = dataDoneOverDate.length;

      this.employee= {
        series: [onTimeCount, overDate],
        labels: ["On Time", "Over Date"],
        chart: {
          type: "donut",
          height: 230,
          toolbar: {
            show: true,
          },
        },
        plotOptions: {
          pie: {
            offsetX: 0,
            offsetY: 0,
            donut: {
              size: "90%",
              labels: {
                show: false,
              }
            },
          },
        },
        dataLabels: {
          enabled: false,
        },
        legend: {
          show: false,
        },
        stroke: {
          lineCap: "round",
          width: 0
        },
        colors: colors
      };
    }

    getStatusLabelClass(presentase: number): string {
      if (presentase >= 80) {
          return 'success';
      } else if (presentase >= 50 && presentase < 80) {
          return 'warning';
      } else {
          return 'danger';
      }
  }

  getProgressBarType(presentase: number): string {
    if (presentase >= 80) {
        return 'success';
    } else if (presentase >= 50 && presentase < 80) {
        return 'warning';
    } else if (presentase >= 30 && presentase < 50) {
        return 'orange';
    } else {
        return 'danger';
    }
}
}
