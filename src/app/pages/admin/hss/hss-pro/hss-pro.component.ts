import { Component, OnInit, ViewChild, TemplateRef, AfterViewInit} from '@angular/core';
import { ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';

// Calendar option
import { CalendarOptions, EventClickArg, EventApi } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import { EventInput } from '@fullcalendar/core';

// BootStrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
// Sweet Alert
import Swal from 'sweetalert2';

// Calendar Services
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { Pro } from './hss-pro.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-hss-pro',
  templateUrl: './hss-pro.component.html',
  styleUrls: ['./hss-pro.component.scss']
})
export class HssProComponent implements OnInit {

  public data_pro: any = {};

  newPro: Pro = {
    name: '',
    email: '',
    date: '',
    pro_description: '',
    file: '',
    file_signed: '',
    signer: 'Dian Alpiansyah',
    email_signer: 'dalpiansyah@aio.co.id',
    id_status: 1
  };

  id_pro:any;
  dataPro : any = [];
  selectedFiles: { file: File, type: string }[] = [];
  dataSchedule: any
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  namePic: any;
  pdf_url:any

  // bread crumb items
  breadCrumbItems!: Array<{}>;

  // calendar
  calendarEvents : EventInput[] = [];
  editEvent: any;
  formEditData!: UntypedFormGroup;
  newEventDate: any;
  category!: any[];
  submitted = false;

  // Calendar click Event
  formData!: UntypedFormGroup;
  @ViewChild('editmodalShow') editmodalShow!: TemplateRef<any>;
  @ViewChild('modalShow') modalShow !: TemplateRef<any>;

  constructor(private modalService: NgbModal, private formBuilder: UntypedFormBuilder, private router : Router,
    private route: ActivatedRoute, private RestApiService: RestApiService, private authService:AuthenticationService,
    private datePipe:DatePipe) { }

  ngOnInit(): void {
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();    
    // Validation
    this.formData = this.formBuilder.group({
      title: ['', [Validators.required]],
      category: ['', [Validators.required]],
      location: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required]
    });

    this._fetchData();
  }
  private _fetchData() {
    this.getAllPro();
  }

  /***
  * Calender Set
  */
  calendarOptions: CalendarOptions = {
    plugins: [
      interactionPlugin,
      dayGridPlugin,
      timeGridPlugin,
      listPlugin,
    ],
    headerToolbar: {
      left: 'dayGridMonth,dayGridWeek,dayGridDay',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: this.calendarEvents || this.calendarEvents,
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.openModal.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    eventDidMount: this.addIconToEvent.bind(this),
  };
  currentEvents: EventApi[] = [];

  /**
   * Event add modal
   */
  openModal(event?: any) {
    this.submitted = false;
    this.newEventDate = event,
      this.formBuilder.group({
        editDate: this.newEventDate.date
      })
    this.modalService.open(this.modalShow, { centered: true });
  }

  addIconToEvent(info: any): void {
    const event = info.event;
    if (event.extendedProps && event.extendedProps.id_status === 3) {
      const iconElement = document.createElement('i');
      iconElement.className = 'bi bi-check-circle text-white';
      iconElement.style.fontSize = '15px';
      iconElement.style.marginRight = '10px';
  
      const eventElement: HTMLElement = info.el as HTMLElement;
      eventElement.appendChild(iconElement);
    } else if (event.extendedProps && event.extendedProps.id_status === 2) {
      const iconElement = document.createElement('i');
      iconElement.className = 'bi bi-x-circle text-white';
      iconElement.style.fontSize = '15px';
      iconElement.style.marginRight = '10px';
  
      const eventElement: HTMLElement = info.el as HTMLElement;
      eventElement.appendChild(iconElement);
    }
  }  

  /**
   * Event click modal show
   */
  handleEventClick(clickInfo: EventClickArg) {
    this.editEvent = clickInfo.event._def.publicId;
    const formattedDate = this.datePipe.transform(clickInfo.event.start, 'dd-MM-yyyy');
    this.formEditData = this.formBuilder.group({
      id: clickInfo.event.id,
      name: clickInfo.event._def.extendedProps['name'],
      description: clickInfo.event.title,
      file_signed: clickInfo.event._def.extendedProps['file_signed'],
      notes: clickInfo.event._def.extendedProps['notes'],
      date: formattedDate,
    }); 
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  flatpickrOptions: any = {
    dateFormat: 'd-m-Y'
  };

  /**
   * Events bind in calander
   * @param events events
   */
  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
    // this.changeDetector.detectChanges();
  }

  /**
   * Close event modal
   */
  closeEventModal() {
  this.modalService.dismissAll();
  }

  /**
   * Event Data Get
   */
  get form() {
    return this.formData.controls;
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  deleteEventData() {
    this.editEvent.remove();
    this.modalService.dismissAll();
  }

  insertProWithFile(dataPro: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
        formData.append('files', fileData.file, fileData.file.name);
        formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultiplePdf(formData).subscribe({
        next: (res: any) => {
          if (res && res.filename) {
            this.pdf_url = [res.filename];
            dataPro.file = this.pdf_url[0];
            this.insertPro(dataPro);
          }
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.insertPro(dataPro);
    }
  }

  insertPro(dataPro: any) {
    this.RestApiService.insertPro(dataPro).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.id_pro = res.data[0];
          this.data_pro = dataPro;

          const eventToAdd = {
            date: this.data_pro.date,
            description: this.data_pro.description,
            file: this.data_pro.file,
          };
          this.calendarEvents.push(eventToAdd);
          this.calendarOptions.initialEvents = this.calendarEvents;
          
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updatePro(dataPro: any, id:any) {
    let publicId = this.editEvent
    this.RestApiService.updatePro(publicId, dataPro).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.data_pro = dataPro;
          this.data_pro.id = this.id_pro
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  getAllPro(){
    this.RestApiService.getAllPro().subscribe(
      (res: any) => {
        this.dataPro = res.data;
        this.calendarEvents = this. transformDataScheduleWeekly(this.dataPro);
        this.calendarOptions.events = this.calendarEvents;
        this.dataPro.date = this.convertDate(this.dataPro.date);
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  onFileTaskSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  transformDataScheduleWeekly(data: any[]): any[] {
    const calendarEvents: any[] = [];
    for (const item of data) {
      const event = {
        id: item.id,
        name: item.name,
        title: item.pro_description,
        date: item.date,
        file_signed: item.file_signed,
        notes: item.notes,
        id_status: item.id_status
      };
      console.log(event);
      calendarEvents.push(event);
    }
    return calendarEvents;
  }
  
  onClick() {
    this.newPro.name = this.userData.name
    this.newPro.email = this.userData.email
    this.insertProWithFile(this.newPro);
    this.modalService.dismissAll()
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!!',
      icon: 'success',
      timer: 1500, 
      showConfirmButton: false, 
    }).then(() => {
      setTimeout(() => {
        window.location.reload();
      },);
    });

    this.newPro = {
      name: '',
      email: '',
      date: '',
      pro_description: '',
      file: '',
      file_signed: '',
      signer: '',
      email_signer: '',
      id_status: null
    };
    this.getAllPro()
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updatePro(deleteData, id);
        this.modalService.dismissAll()
        window.location.reload()
      }
    });
  }

  updateData() {
    const originalEventDate = this.editEvent.start;
    this.formEditData.patchValue({ date: originalEventDate });
    this.updatePro(this.formEditData.value, this.editEvent);
    this.modalService.dismissAll();
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    }).then(() => {
      setTimeout(() => {
        window.location.reload();
      },);
    });
  }

  onCompleted(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Is The Task Finished?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { follow_up: 1 };
        this.updatePro(deleteData, id);
        this.modalService.dismissAll()
        window.location.reload()
      }
    });
  }
  
  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}
