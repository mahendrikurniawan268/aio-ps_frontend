import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HssProComponent } from './hss-pro.component';

describe('HssProComponent', () => {
  let component: HssProComponent;
  let fixture: ComponentFixture<HssProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HssProComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HssProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
