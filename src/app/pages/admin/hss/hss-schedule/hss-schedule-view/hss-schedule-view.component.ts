import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-hss-schedule-view',
  templateUrl: './hss-schedule-view.component.html',
  styleUrls: ['./hss-schedule-view.component.scss']
})
export class HssScheduleViewComponent implements OnInit {

  // bread crumb items
  breadCrumbItems!: Array<{}>;
  basicTimelineChart: any;
  differentColorChart: any;
  multiSeriesTimelineChart: any;
  advancedTimelineChart: any;
  multipleSeriesChart: any;
  idSchedule:any;
  dataSchedule:any [] = [];
  public idParam:any;
  isConnected: boolean = true;
  isTableView: boolean = false;
  isTimelineView: boolean = true;
  isTableList: boolean = false;

  month: number
  monthBefore!: number
  year: number
  yearBefore!: number

  fromDate: string;
  toDate: string;
  dateRange = {from: new Date(), to: new Date()}

  constructor(private RestApiService:RestApiService, private route:ActivatedRoute, private router: Router) {
    this.differentColorChart = {};
    this.month = new Date().getMonth() + 1
    this.year = new Date().getFullYear()
    this.fromDate = this.getDefaultFromDate();
    this.toDate = this.getDefaultToDate();
   }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getSchedule(this.fromDate, this.toDate);
  }

  getDefaultFromDate(): string {
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const fromDate = `${year}-${monthFilter}-01`
    return fromDate
  }

  getDefaultToDate(): string {
    const getLastDayOfMonth = (year: number, month: number) => {
      month -= 1
      return new Date(year, month + 1, 0).getDate();
    }
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const lastDayOfMonth = getLastDayOfMonth(year, month)
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const toDate = `${year}-${monthFilter}-${lastDayOfMonth}`
    return toDate
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id === 'btnCalendar') {
      this.isTableView = true;
      this.isTableList = false;
      this.isTimelineView = false;
      this.router.navigate(["menu/hss/schedule"], {
        queryParams: { tableCalendar: true },
      });
    } else if (event.target.id === 'btnList') {
      this.isTableView = false;
      this.isTableList = true;
      this.isTimelineView = false;
      this.router.navigate(["menu/hss/schedule/list"], {
        queryParams: { tableList: true },
      });
    } else if (event.target.id === 'btnTimeline') {
      this.isTableView = false;
      this.isTableList = false;
      this.isTimelineView = true;
      this.router.navigate(["menu/hss/schedule/view"], {
        queryParams: { timeline: true },
      });
    }
  }

  // Chart Colors Set
  private getChartColorsArray(colors:any) {
    colors = JSON.parse(colors);
    return colors.map(function (value:any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) {
            color = color.replace(" ", "");
            return color;
            }
            else return newValue;;
        } else {
            var val = value.split(',');
            if (val.length == 2) {
                var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
                rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
                return rgbaColor;
            } else {
                return newValue;
            }
        }
    });
  }

  private _differentColorChart() {
    const colors = this.getChartColorsArray('["--vz-primary", "--vz-danger", "--vz-success", "--vz-warning", "--vz-info"]');
    const filteredDataSubTask = this.dataSchedule.filter(schedule => schedule.status !== 0);
     const timelineData = filteredDataSubTask.map((schedule, index) => {
    let startDate = new Date(schedule.start_date).getTime();
    let endDate = new Date(schedule.end_date).getTime();

    // Jika start_date dan end_date adalah hari yang sama, tambahkan satu hari pada end_date
    if (startDate === endDate) {
      endDate += 24 * 60 * 60 * 1000; // Tambah satu hari
    }

    return {
      x: schedule.task,
      y: [
        startDate,
        endDate,
      ],
      fillColor: colors[index % colors.length],
    };
  });
    const chartHeight = Math.max(330, timelineData.length * 40); 
    this.differentColorChart = {
      series: [
        {
          data: timelineData,
        },
      ],
      chart: {
        height: chartHeight,
        type: "rangeBar",
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
        }
      },
      plotOptions: {
        bar: {
          horizontal: true,
          distributed: true,
          dataLabels: {
            hideOverflowingLabels: false,
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any, opts: any) {
          var label = opts.w.globals.labels[opts.dataPointIndex];
          var a = moment(val[0]);
          var b = moment(val[1]);
          var diff = b.diff(a, "days")+ 1;
          return label + ": " + diff + (diff > 1 ? " days" : " day");
        },
      },
      xaxis: {
        type: "datetime",
      },
      yaxis: {
        show: true
      },
    };
  }
  

  getSchedule(from: string, to: string){
    this.RestApiService.getScheduleProductionByFilter(from, to).subscribe(
      (res:any) => {
        this.dataSchedule = res.data;
        this._differentColorChart();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  forBack(){
    this.router.navigate(['menu/hss/schedule'])
  }

}
