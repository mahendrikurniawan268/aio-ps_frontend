import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HssScheduleViewComponent } from './hss-schedule-view.component';

describe('HssScheduleViewComponent', () => {
  let component: HssScheduleViewComponent;
  let fixture: ComponentFixture<HssScheduleViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HssScheduleViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HssScheduleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
