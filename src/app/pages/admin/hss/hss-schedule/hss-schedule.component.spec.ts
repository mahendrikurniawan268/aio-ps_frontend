import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HssScheduleComponent } from './hss-schedule.component';

describe('HssScheduleComponent', () => {
  let component: HssScheduleComponent;
  let fixture: ComponentFixture<HssScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HssScheduleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HssScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
