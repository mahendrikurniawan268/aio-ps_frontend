export interface ScheduleDetail {
    name: string;
    date: string;
    start_time: null | number;
    end_time: null | number;
    plan: string;
    actual: string;
  }