import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HssScheduleAddDataComponent } from './hss-schedule-add-data.component';

describe('HssScheduleAddDataComponent', () => {
  let component: HssScheduleAddDataComponent;
  let fixture: ComponentFixture<HssScheduleAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HssScheduleAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HssScheduleAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
