import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
// Calendar option
import { CalendarOptions, EventClickArg, EventApi, EventInput,} from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
// BootStrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
// Sweet Alert
import Swal from 'sweetalert2';
import { ApiService } from 'src/app/core/services/api.service';
import { CommonService } from 'src/app/core/services/common.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-hss-schedule',
  templateUrl: './hss-schedule.component.html',
  styleUrls: ['./hss-schedule.component.scss']
})
export class HssScheduleComponent implements OnInit {
  calendarEvents: EventInput[] = [];
  calendarOptions: CalendarOptions = {
    plugins: [
      interactionPlugin,
      dayGridPlugin,
      timeGridPlugin,
      listPlugin,
    ],
    headerToolbar: {
      left: 'dayGridMonth,dayGridWeek,dayGridDay',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
    },

    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: this.calendarEvents,
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    dateClick: this.openModal.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this)
  };

  breadCrumbItems!: Array<{}>;
  editEvent: any;
  formEditData!: UntypedFormGroup;
  submitted = false;
  scheduleData: any;
  isConnected: boolean | undefined;
  id: any
  editEventId!: string;
  DataById!: any;
  formDataSchedule!: UntypedFormGroup;
  eventData: any[] = []
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  isTableList: boolean = false;
  isTableView: boolean = true;
  isTimelineView: boolean = false;

  taskData: any
  loading: boolean = false
  newEventDate: any = { startStr: '' };
  idLine:any;

  category = [
  {
      name: 'Important',
      value: '#FF0000',
      className:''
  },
  {
      name: 'High',
      value: '#FF820F',
      className:''
  },
  {
      name: 'Medium',
      value: '#FFDE00',
      className:''
  },
  {
      name: 'Low',
      value: '#0012FF',
      className:''
  },
  ]
    @ViewChild('editmodalShow') editmodal!: TemplateRef<any>;
    @ViewChild('modalShow') openmodal !: TemplateRef<any>;

  constructor(
    private modalService: NgbModal, 
    private formBuilder: UntypedFormBuilder,
    public apiService: ApiService,
    public common: CommonService,
    private authService: AuthenticationService,
    private router:Router,
    private route:ActivatedRoute,
    private RestApiService : RestApiService) {}

    async ngOnInit() {
      this.userData = this.authService.getUserData();
      this.isAdmin = this.authService.isAdmin();
      this.isSpv = this.authService.isSpv();
      this.isPlanner = this.authService.isPlanner();
      this.isEmployee = this.authService.isEmployee();
      await this.getAllSchedule().finally(() => this.loading = false);

    this.formDataSchedule = this.formBuilder.group({
      task: ['', [Validators.required]],
      category: ['', [Validators.required]],
      location: ['', [Validators.required]],
      description: ['', [Validators.required]],
      start_date: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required]
    });

    this.formEditData = this.formBuilder.group({
      editTitle: [null], // Nilai default untuk semua bidang
      editCategory: [null],
      editLocation: [null],
      editDescription: [null],
      editStartDate: [null],
      editEndDate: [null],
      editStart: [null],
      editEnd: [null],
    });
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id === 'btnCalendar') {
      this.isTableView = true;
      this.isTableList = false;
      this.isTimelineView = false;
      this.router.navigate(["menu/hss/schedule"], {
        queryParams: { tableCalendar: true },
      });
    } else if (event.target.id === 'btnList') {
      this.isTableView = false;
      this.isTableList = true;
      this.isTimelineView = false;
      this.router.navigate(["menu/hss/schedule/list"], {
        queryParams: { tableList: true },
      });
    } else if (event.target.id === 'btnTimeline') {
      this.isTableView = false;
      this.isTableList = false;
      this.isTimelineView = true;
      this.router.navigate(["menu/hss/schedule/view"], {
        queryParams: { timeline: true },
      });
    }
  }

  currentEvents: EventApi[] = [];

  openModal(event?: any) {
    if (this.isAdmin) {
      this.newEventDate = event;
      this.modalService.open(this.openmodal, { centered: true });
    } else {
      alert('You do not have permission to perform this action.');
    }
  }
  

  closeEventModal() {
    this.formDataSchedule = this.formBuilder.group({
      task: '',
      category: '',
      location: '',
      description: '',
      start_date: '',
      start_time: '',
      end_time: ''
    });
    this.modalService.dismissAll();
  }

  get form() {
    return this.formDataSchedule.controls;
  }

  /**
   * Events bind in calander
   * @param events events
   */
  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

onSubmit() {
  if (this.formDataSchedule.valid) {
    const formData = { ...this.formDataSchedule.value };
    if (!formData.start_date.to) {
      formData.start_date.to = formData.start_date.from;
    }
    const eventStartDate = new Date(formData.start_date.from);
    const eventEndDate = new Date(formData.start_date.to);
    eventStartDate.setDate(eventStartDate.getDate() + 1);
    eventEndDate.setDate(eventEndDate.getDate() + 1);
    formData.start_date = eventStartDate;
    formData.end_date = eventEndDate;
    this.RestApiService.insertScheduleProduction(formData).subscribe({
      next: (res: any) => {
        if (res.status) {
          this.modalService.dismissAll();
          window.location.reload();
        }
      },
      error: (err: any) => console.error(err)
    });
  }
}

async getAllSchedule() {
  return new Promise((resolve, reject) => {
    this.RestApiService.getAllScheduleProduction().subscribe({
      next: (res: any) => {
        const data: any[] = res.data;
        const itemData: any[] = [];
        for (let item of data) {
          if (!itemData.includes(item.id)) {
            itemData.push(item.id);
          }
        }
        if (itemData.length > 0) {
          data.forEach((schedule: any) => {
            const selectedCategory = this.category.find(cat => cat.name === schedule.category);
            if (selectedCategory) {
                const serverStartDate = new Date(schedule.start_date);
                const serverEndDate = new Date(schedule.end_date);
                // Mengonversi zona waktu ke "Asia/Jakarta"
                serverStartDate.setHours(serverStartDate.getHours() + 7);
                serverEndDate.setHours(serverEndDate.getHours() + 7);
                this.eventData.push({
                    id: schedule.id,
                    start: serverStartDate,
                    end: serverEndDate,
                    title: `${schedule.task}`,
                    allDay: false,
                    backgroundColor: selectedCategory.value,
                    allData: {
                        category: schedule.category,
                        description: schedule.description,
                        end_date: serverEndDate.toISOString(),
                        end_time: schedule.end_time,
                        task: schedule.task,
                        id: schedule.id,
                        location: schedule.location,
                        start_date: serverStartDate.toISOString(),
                        start_time: schedule.start_time,
                    }
                });
            }
        });
        }
      },
      complete: () => {
        this.calendarOptions.events = this.eventData;
        resolve(true);
      }
    });
  });
}


handleEventClick(clickInfo: EventClickArg) {
  this.editEventId = clickInfo.event.id;
  this.editEvent = clickInfo.event;
  this.getByIdSchedule(this.editEventId);

}

getByIdSchedule(id: any) {
  this.RestApiService.getScheduleProductionById(id).subscribe({
    next: (res: any) => {
      this.DataById = res.data[0];
      console.log(this.DataById);
      
      const startDate = new Date(this.DataById.start_date);
      const endDate = new Date(this.DataById.end_date);
      const startDateWIB = new Date(this.DataById.start_date);
      startDateWIB.setHours(startDateWIB.getHours());
      const endDateWIB = new Date(this.DataById.end_date);
      endDateWIB.setHours(endDateWIB.getHours());
      const startTimeFormatted = this.DataById.start_time;
      const endTimeFormatted = this.DataById.end_time;
      this.formEditData.get('editTitle')?.setValue(this.DataById.task || '');
      this.formEditData.get('editCategory')?.setValue(this.DataById.category || '');
      this.formEditData.get('editLocation')?.setValue(this.DataById.location || '');
      this.formEditData.get('editDescription')?.setValue(this.DataById.description || '');

      this.formEditData.get('editStartDate')?.setValue(startDate);
      this.formEditData.get('editEndDate')?.setValue(endDate);
      this.formEditData.get('editStart')?.setValue(startTimeFormatted);
      this.formEditData.get('editEnd')?.setValue(endTimeFormatted);
      this.modalService.open(this.editmodal, { centered: true });
    },
    error: (err: any) => {
      console.error(err);
      setTimeout(() => {
      }, 1000);
    },
  });
}

editEventSave() {
  if (this.formEditData.valid) {
    const updatedData = this.formEditData.value;
    const updatedEventData = {
      task: updatedData.editTitle,
      category: updatedData.editCategory,
      location: updatedData.editLocation,
      description: updatedData.editDescription,
      start_date: new Date(updatedData.editStartDate), // Konversi ke objek Date
      end_date: updatedData.editEndDate ? new Date(updatedData.editEndDate) : null, // Konversi ke objek Date atau beri nilai null jika tidak ada
      start_time: updatedData.editStart,
      end_time: updatedData.editEnd,
    };
    // Handle zona waktu di sini sesuai kebutuhan
    // Misalnya, jika Anda ingin menghitung dengan zona waktu "Asia/Jakarta":
    updatedEventData.start_date.setHours(updatedEventData.start_date.getHours() - 7); // Kurangi 7 jam
    if (updatedEventData.end_date) {
      updatedEventData.end_date.setHours(updatedEventData.end_date.getHours() - 7);
    }

    if (updatedEventData.start_date) {
      updatedEventData.start_date.setDate(updatedEventData.start_date.getDate() + 1);
    }
    if (updatedEventData.end_date) {
      updatedEventData.end_date.setDate(updatedEventData.end_date.getDate() + 1);
    }
    this.RestApiService.updateScheduleProduction(this.DataById.id, updatedEventData).subscribe(
      (res) => {
        this.modalService.dismissAll();
          window.location.reload();
      },
      (error) => {
        console.error('Error updating event', error);
      }
    );
  }
}

  confirm(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be delete this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        this.deleteEventData(id);
      }
    });
  }

  deleteEventData(id: any) {
    const updatedData = { status: 0 }; 
    this.RestApiService.updateScheduleProduction(id, updatedData).subscribe({
      next: (res: any) => {
        if (res.status) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Event has been deleted',
            showConfirmButton: false,
            timer: 1000,
          });
          this.modalService.dismissAll();
          window.location.reload();
        }
      },
      error: (err: any) => console.error(err),
    });
  }
  

  position() {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Event has been saved',
      showConfirmButton: false,
      timer: 1000,
    });
  }
}
