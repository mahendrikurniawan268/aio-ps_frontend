import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import { Ppic } from './hss-ppic.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-hss-ppic',
  templateUrl: './hss-ppic.component.html',
  styleUrls: ['./hss-ppic.component.scss']
})
export class HssPpicComponent implements OnInit{
  searchText:string = '';
  dataForCurrentPage: any = [];
  selectedFiles: { file: File, type: string }[] = [];
  preData:any [] = [];
  dataPPic: any [] = [];
  ppicData:any = [];
  id_Ppic:any;
  pdf_url:any;
  index = 0;
  currentPage = 1;
  itemsPerPage = 10;
  deleteData:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  searchData:any

  newPpic : Ppic = {
    name: '',
    title: '',
    file: '',
    notes: ''
  }

  constructor(private router:Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private modalService:NgbModal){}

  ngOnInit(): void {
    this.getPpic();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
  }

  /**
   * Open modal
   * @param add_ppic modal content
   */
  addTask(add_ppic: any) {
    this.modalService.open(add_ppic, { size: 'md', centered: true });
  }

   /**
 * Open modal
 * @param edit_ppic modal content
 */
   editPpic(edit_ppic: any, item:any, id: any) {
    this.newPpic = { ...item };
    this.id_Ppic = id;
    this.modalService.open(edit_ppic, { size: 'md', centered: true });
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  getPpic(){
    this.RestApiService.getAllPpic().subscribe(
      (res:any) => {
        this.dataPPic = res.data;
        this.searchData = [...this.dataPPic];
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertPpic(dataFile: any) {
    const formData = new FormData();
    this.selectedFiles.forEach(fileData => {
    formData.append('files', fileData.file, fileData.file.name);
    formData.append('type', fileData.type);
    });

    this.RestApiService.uploadMultiplePdf(formData).subscribe({
      next: (res: any) => {
        if (res && res.filename) { 
          this.pdf_url = [res.filename];
          dataFile.file = this.pdf_url[0];
        this.RestApiService.insertPpic(dataFile).subscribe(
          (res: any) => {
            if (res.status == true) {
              this.id_Ppic = res.data;
              this.dataPPic = dataFile;
              this.getPpic();
            }
          },
          (error) => {
            console.error(error);
          }
        );
        }
      },
      error: (err) => console.error(err),
      complete: () => {
        this.selectedFiles = [];
      }
    });
  } 

  onSubmit(){
    if (this.selectedFiles.length === 0) {
      Swal.fire({
        title: 'File Empty',
        text: 'Please select a file before submitting.',
        icon: 'warning',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      });
      return;
    }
    this.newPpic.name = this.userData.name
    this.insertPpic(this.newPpic)
    Swal.fire({
      icon: 'success',
      title: 'Add Task Successfully',
      text: 'Add Task has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
    this.modalService.dismissAll();
    this.getPpic();
    this.newPpic = {
      name: '',
      title: '',
      file: '',
      notes: ''
    };
  }

  onFileTaskSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
    console.log(this.selectedFiles);
  }

  updatePpicWithFile(dataPpic: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
        formData.append('files', fileData.file, fileData.file.name);
        formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultiplePdf(formData).subscribe({
        next: (res: any) => {
          if (res && res.filename) {
            this.pdf_url = [res.filename];
            dataPpic.file = this.pdf_url[0];
            this.updatePpic(dataPpic, this.id_Ppic);
          }
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.updateTaskSpv(dataPpic);
    }
  }

  updateTaskSpv(dataPpic:any){
    this.RestApiService.updatePpic(this.id_Ppic, dataPpic).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.ppicData = dataPpic;
          this.getPpic();
        }
      },
      (error) => {
        console.error(error)
      }
    )
    }

  onUpdate(){
    this.updatePpicWithFile(this.newPpic);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
    this.modalService.dismissAll();
    }

  updatePpic(dataPpic: any, id:any) {
    this.RestApiService.updatePpic(id, dataPpic).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.deleteData = dataPpic;
          this.getPpic()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updatePpic(deleteData, id);
      }
    });
  }

  search() {
    this.dataPPic = this.searchData.filter((item: { name: string; title: string; }) => {
      return item.name.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.title.toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataPPic.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }
}
