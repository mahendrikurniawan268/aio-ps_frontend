export interface Ppic {
    name: string;
    title: string;
    file: string;
    notes: string;
  }