import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HssPpicComponent } from './hss-ppic.component';

describe('HssPpicComponent', () => {
  let component: HssPpicComponent;
  let fixture: ComponentFixture<HssPpicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HssPpicComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HssPpicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
