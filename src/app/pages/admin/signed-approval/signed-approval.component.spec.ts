import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignedApprovalComponent } from './signed-approval.component';

describe('SignedApprovalComponent', () => {
  let component: SignedApprovalComponent;
  let fixture: ComponentFixture<SignedApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignedApprovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignedApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
