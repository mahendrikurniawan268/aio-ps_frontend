import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-signed-approval',
  templateUrl: './signed-approval.component.html',
  styleUrls: ['./signed-approval.component.scss']
})
export class SignedApprovalComponent implements OnInit{
  tableColumns = ['No', 'Name', 'Departement', 'Title','File', 'Status','Notes', 'Details'];

  searchText:string = '';
  dataForCurrentPage: any = [];
  preData:any [] = [];
  index = 0
  currentPage = 1;
  itemsPerPage = 10;
  dataApprovalSigned: any = []
  dataApprovalByName: any = []
  deleteData:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  searchData:any

  constructor(private router:Router, private RestApiService:RestApiService, private authService:AuthenticationService){}

  ngOnInit(): void {
    this.getApprovalSigned();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
  }

  getPdf(file: any) {
    return environment.API_URL + 'pdf/download/' + file
  }

  getApprovalSigned(){
    this.RestApiService.getApprovalSigned().subscribe(
      (res:any) => {
        this.dataApprovalSigned = res.data[0];
        this.preData = [...this.dataApprovalSigned];
        this.dataApprovalByName = this.preData.filter(item => item.name === this.userData.name)
        this.searchData = this.dataApprovalByName
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateApproval(approval_signed: any, id:any) {
    this.RestApiService.updateApproval(id, approval_signed).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.deleteData = approval_signed;
          this.getApprovalSigned()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateApproval(deleteData, id);
      }
    });
  }

  search() {
    this.dataApprovalByName = this.searchData.filter((item: { name: string; departement: string; title: string; }) => {
      return item.name.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.departement.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.title.toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataApprovalByName.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'warning'; // New
        case 2:
            return 'danger'; // On Progress
        case 3:
            return 'success'; // Done
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'Process';
        case 2:
            return 'Reject';
        case 3:
            return 'Done';
        default:
            return 'Unknown';
    }
}
}