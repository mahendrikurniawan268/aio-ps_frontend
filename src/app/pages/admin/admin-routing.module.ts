import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignedApprovalComponent } from './signed-approval/signed-approval.component';
import { UploadApprovalComponent } from './upload-approval/upload-approval.component';
import { ListApprovalComponent } from './list-approval/list-approval.component';
import { TaskHeadToSpvComponent } from './task-spv/task-head-to-spv/task-head-to-spv.component';
import { TaskHeadToSpvAddDataComponent } from './task-spv/task-head-to-spv/task-head-to-spv-add-data/task-head-to-spv-add-data.component';
import { TaskSpvToEmployeeComponent } from './task-spv/task-head-to-spv/task-spv-to-employee/task-spv-to-employee.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { AppGatewayAdminComponent } from './app-gateway-admin/app-gateway-admin.component';
import { HssPpicComponent } from './hss/hss-ppic/hss-ppic.component';
import { HssProComponent } from './hss/hss-pro/hss-pro.component';
import { HssScheduleComponent } from './hss/hss-schedule/hss-schedule.component';
import { HssScheduleAddDataComponent } from './hss/hss-schedule/hss-schedule-add-data/hss-schedule-add-data.component';
import { HssScheduleViewComponent } from './hss/hss-schedule/hss-schedule-view/hss-schedule-view.component';
import { AccesLineComponent } from './acces-line/acces-line.component';

const routes: Routes = [
    {
    path: 'acces-line',
    component: AccesLineComponent
    },
    {
    path: 'signed-approval',
    component: SignedApprovalComponent
    },
    {
    path: "list-approval",
    component: ListApprovalComponent,
    },
    {
    path: "upload-approval",
    component: UploadApprovalComponent
    },
    {
      path: "task-head-to-spv",
      component: TaskHeadToSpvComponent
    },
    {
      path: "head-to-spv/add-data",
      component: TaskHeadToSpvAddDataComponent
    },
    {
      path: "spv-to-employee",
      component: TaskSpvToEmployeeComponent
    },
    {
      path: "dashboard-admin",
      component: DashboardAdminComponent
    },
    {
      path: "apps-gateway/admin",
      component: AppGatewayAdminComponent
    },
    {
      path: "hss/ppic",
      component: HssPpicComponent
    },
    {
      path: "hss/pro",
      component: HssProComponent
    },
    {
      path: "hss/schedule",
      component: HssScheduleComponent
    },
    {
      path: "hss/schedule/add-data",
      component: HssScheduleAddDataComponent
    },
    {
      path: "hss/schedule/list",
      component: HssScheduleAddDataComponent
    },
    {
      path: "hss/schedule/view",
      component: HssScheduleViewComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
