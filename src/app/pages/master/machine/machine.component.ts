import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild, ElementRef } from '@angular/core';
import { Machine } from './machine.model';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  @ViewChild('editMachine') editMachine!: ElementRef;

  tableColumns = ['No', 'Machine', 'Area', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  currentPage = 1;
  itemsPerPage = 50;

  newMachine : Machine = {
    machine: '',
    id_line: null,
    id_area: null
  }

  selectedMachine: Machine = {
    machine: '',
    id_line: null,
    id_area: null
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idMachine: any;
  delete_machine: any [] = [];
  data_machine: any;
  dataMachine: any [] = [];
  dataMachineByLine: any;
  namePic: any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  searchText:string = '';
  preData: any;
  dataForCurrentPage: any[] = [];
  idLine:any;
  dataArea:any[] = [];
  area: any;

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) {}
  
  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'];
    }})
    this.getMachine();
    this.getAreaSection();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  /**
   * Open modal
   * @param machine modal content
   */
  openModal(machine: any) {
    this.modalService.open(machine, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param EditMachine modal content
   */
  openEditModal(editMachine: any, machine: any, id:any) {
    this.selectedMachine = { ...machine };
    this.idMachine = id
    this.modalService.open(this.editMachine, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  clearFormData(){
    this.newMachine = {
      machine: '',
      id_line: null,
      id_area: null
    }
  }

  onClick() {
    this.newMachine.id_line = this.idLine
    console.log(this.newMachine)
    this.insertMachine(this.newMachine);
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.modalService.dismissAll();
  }

  getMachine(){
    this.RestApiService.getAllMachine().subscribe(
      (res:any) => {
        this.dataMachine = res.data;
        this.dataMachineByLine = this.dataMachine.filter(item => item.id_line == this.idLine)
        this.preData = [...this.dataMachineByLine];
        this.data_machine = [...this.dataMachineByLine]
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertMachine(dataMachine: any) {
    this.RestApiService.insertMachine(dataMachine).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idMachine = res.data[0]
          this.data_machine = dataMachine;
          this.data_machine.id = this.idMachine
          this.getMachine()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateMachine(dataMachine: any, id:any) {
    this.RestApiService.updateMachine(id, dataMachine).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.data_machine = dataMachine;
          this.getMachine()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate() {
    this.updateMachine(this.selectedMachine, this.idMachine);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text',
      },
    });
    this.modalService.dismissAll();
  }

deleteMachine(MachineData: any, id:any) {
  this.RestApiService.updateMachine(id, MachineData).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.delete_machine = MachineData;
        this.getMachine()
      }
    },
    (error) => {
      console.error(error)
    }
  )
}

getAreaSection(){
  this.RestApiService.getAllArea().subscribe(
    (res: any) => {
      this.dataArea = res.data;
      this.area = this.dataArea.filter(item => item.id_line == this.idLine) 
    },
    (error: any) => {
      console.error(error);
    }
  );
}

onDelete(id:any){
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to delete this data?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oke',
    cancelButtonText: 'Cancel',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const deleteData = { status: 0 };
      this.deleteMachine(deleteData, id);
    }
  });
}

search() {
  this.dataMachineByLine = this.preData.filter((item: { machine: string;}) => {
    return (item.machine.toLowerCase().includes(this.searchText.toLowerCase()));
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.dataMachineByLine.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}

getArea(id_area: number): string {
  switch (id_area) {
      case 1:
          return 'Preparation'; 
      case 2:
          return 'Filling'; 
      case 3:
          return 'Packing';
      case 4:
          return 'Preparation'; 
      case 5:
          return 'Injection'; 
      case 6:
          return 'Blow';
      case 7:
          return 'Filling'; 
      case 8:
          return 'Packing';
      default:
          return 'Unknown';
  }
}
}