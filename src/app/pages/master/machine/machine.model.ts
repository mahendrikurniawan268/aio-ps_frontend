export interface Machine {
    machine: string;
    id_line: number | null;
    id_area: number | null;
}