import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionLocationComponent } from './function-location.component';

describe('FunctionLocationComponent', () => {
  let component: FunctionLocationComponent;
  let fixture: ComponentFixture<FunctionLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunctionLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FunctionLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
