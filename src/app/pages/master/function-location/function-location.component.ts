import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild, ElementRef } from '@angular/core';
import { FunctionLocation } from './function-location.model';

@Component({
  selector: 'app-function-location',
  templateUrl: './function-location.component.html',
  styleUrls: ['./function-location.component.scss']
})
export class FunctionLocationComponent implements OnInit {
  @ViewChild('editFunloc') editFunloc!: ElementRef;

  tableColumns = ['No', 'Function Location', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  currentPage = 1;
  itemsPerPage = 50;

  newFunctionLocation : FunctionLocation = {
    function_location: '',
    line: '',
    id_line: null
  }

  selectedFunctionLocation: FunctionLocation = {
    function_location: '',
    line: '',
    id_line: null
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idFunloc: any;
  delete_funloc: any [] = [];
  data_funloc: any;
  dataFunloc: any [] = [];
  dataFunlocByLine: any;
  namePic: any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  searchText:string = '';
  preData: any;
  dataForCurrentPage: any[] = [];
  idLine:any;

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) {}
  
  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'];
    }})
    console.log(this.idLine);
    this.getFunloc();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  /**
   * Open modal
   * @param funloc modal content
   */
  openModal(funloc: any) {
    this.modalService.open(funloc, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param EditFunloc modal content
   */
  openEditModal(editFunloc:any, funloc:any, id:any) {
    this.selectedFunctionLocation = { ...funloc };
    this.idFunloc = id;
    this.modalService.open(editFunloc, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  clearFormData(){
    this.newFunctionLocation = {
      function_location: '',
      line: '',
      id_line: null
    }
  }

  onClick() {
    this.newFunctionLocation.id_line = this.idLine;
    if (this.idLine == 1) {
      this.newFunctionLocation.line = 'can';
    } else if (this.idLine == 2) {
      this.newFunctionLocation.line = 'pet';
    }
    console.log('newFunctionLocation:', this.newFunctionLocation);
    this.insertFunloc(this.newFunctionLocation);
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.modalService.dismissAll();
  }  

  getFunloc(){
    this.RestApiService.getAllFunctionLocation().subscribe(
      (res:any) => {
        this.dataFunloc = res.data;
        this.dataFunlocByLine = this.dataFunloc.filter(item => item.id_line == this.idLine)
        this.preData = [...this.dataFunlocByLine];
        this.data_funloc = [...this.dataFunlocByLine]
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertFunloc(dataFunloc: any) {
    this.RestApiService.insertFunloc(dataFunloc).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idFunloc = res.data[0]
          this.data_funloc = dataFunloc;
          this.data_funloc.id = this.idFunloc
          this.getFunloc()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateFunloc(dataFunloc: any, id:any) {
    this.RestApiService.updateFunloc(id, dataFunloc).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.data_funloc = dataFunloc;
          this.getFunloc()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate() {
    console.log(this.selectedFunctionLocation);
    console.log(this.idFunloc);
    this.updateFunloc(this.selectedFunctionLocation, this.idFunloc);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text',
      },
    });
    this.modalService.dismissAll();
  }

deleteFunloc(funlocData: any, id:any) {
  this.RestApiService.updateFunloc(id, funlocData).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.delete_funloc = funlocData;
        this.getFunloc()
      }
    },
    (error) => {
      console.error(error)
    }
  )
}

onDelete(id:any){
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to delete this data?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oke',
    cancelButtonText: 'Cancel',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const deleteData = { status: 0 };
      this.deleteFunloc(deleteData, id);
    }
  });
}

search() {
  this.dataFunlocByLine = this.preData.filter((item: { function_location: string;}) => {
    return (item.function_location.toLowerCase().includes(this.searchText.toLowerCase()));
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.dataFunlocByLine.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}
}
