export interface FunctionLocation {
    function_location: string;
    line: string;
    id_line: number | null;
}