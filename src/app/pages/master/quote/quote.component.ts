import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Quote } from './quote.model';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
  @ViewChild('editQuote') editQuote!: ElementRef;

  tableColumns = ['No', 'Quote', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  currentPage = 1;
  itemsPerPage = 10;

  newQuote : Quote = {
  quote: '',
  }

  selectedQuote: Quote = {
    quote: '',
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idQuote: any;
  delete_quote: any [] = [];
  data_quote: any;
  dataQuote: any;
  namePic: any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  searchText:string = '';
  preData: any;
  dataForCurrentPage: any[] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) {}
  
  ngOnInit(): void {
    this.getQuote();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  /**
   * Open modal
   * @param quote modal content
   */
  openModal(quote: any) {
    this.modalService.open(quote, { size: 'md', centered: true });
  }

  /**
   * Open modal
   * @param Editquote modal content
   */
  openEditModal(quote: Quote) {
    this.selectedQuote = { ...quote };
    this.modalService.open(this.editQuote, { size: 'md', centered: true });
  }

  onClick() {
    this.insertQuote(this.newQuote);
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.modalService.dismissAll();
  }

  getQuote(){
    this.RestApiService.getAllQuote().subscribe(
      (res:any) => {
        this.dataQuote = res.data;
        this.preData = [...this.dataQuote];
        this.data_quote = [...this.dataQuote]
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertQuote(dataQuote: any) {
    this.RestApiService.insertQuote(dataQuote).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idQuote = res.data[0]
          this.data_quote = dataQuote;
          this.data_quote.id = this.idQuote
          this.getQuote()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateQuote(dataQuote: any, id:any) {
    this.RestApiService.updateQuote(id, dataQuote).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.data_quote = dataQuote;
          this.getQuote()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate() {
    this.updateQuote(this.selectedQuote, this.selectedQuote.id);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text',
      },
    });
    this.modalService.dismissAll();
  }

deleteQuote(quoteData: any, id:any) {
  this.RestApiService.updateQuote(id, quoteData).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.delete_quote = quoteData;
        this.getQuote()
      }
    },
    (error) => {
      console.error(error)
    }
  )
}

onDelete(id:any){
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to delete this data?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oke',
    cancelButtonText: 'Cancel',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const deleteData = { status: 0 };
      this.deleteQuote(deleteData, id);
    }
  });
}

search() {
  this.dataQuote = this.preData.filter((item: { quote: string;}) => {
    return (item.quote.toLowerCase().includes(this.searchText.toLowerCase()));
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.dataQuote.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}
}




