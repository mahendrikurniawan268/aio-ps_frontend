import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Image } from './picture.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss']
})
export class PictureComponent implements OnInit {
  tableColumns = ['No', 'Image', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  currentPage = 1;
  itemsPerPage = 10;
  newImage : Image = {
  image: '',
}

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idImage: any;
  delete_image: any [] = [];
  data_image: any;
  dataImage: any;
  selectedFiles: { file: File, type: string }[] = [];
  userData:any;
  imageUrl: any
  isAdmin: boolean = false;
  isSpv: boolean = false;
  searchText:string = '';
  preData: any;
  dataForCurrentPage: any[] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private modalService: NgbModal
  ) {}
  
  ngOnInit(): void {
    this.getImageLanding();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  getImgFileBefore(file: any) {
    return environment.API_URL + '/image/' + file
  }

  /**
   * Open modal
   * @param quote modal content
   */
  openModal(quote: any) {
    this.modalService.open(quote, { size: 'md', centered: true });
  }

  getImageLanding(){
    this.RestApiService.getAllImageLanding().subscribe(
      (res:any) => {
        this.dataImage = res.data;
        this.preData = [...this.dataImage];
        this.data_image = [...this.dataImage]
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  insertImageLanding(dataImage: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataImage.image = res.uploadedFiles[0];
          this.getImageLanding();
          this.RestApiService.insertImageLanding(dataImage).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.idImage = res.data[0];
                this.data_image = dataImage;
                this.data_image.id = this.idImage;
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      (error: any) => {
        console.error(error);
      }
    }
  }  
  
  onInsertData() {
    this.insertImageLanding(this.newImage);
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.ngOnInit();
  }

deleteImage(imageData: any, id:any) {
  this.RestApiService.updateImageLanding(id, imageData).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.delete_image = imageData;
        this.getImageLanding()
      }
    },
    (error) => {
      console.error(error)
    }
  )
}

onDelete(id:any){
  Swal.fire({
    title: 'Confirmation',
    text: 'Are you sure you want to delete this data?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oke',
    cancelButtonText: 'Cancel',
    cancelButtonColor: '#d33',
    customClass: {
      popup: 'custom-swal-text',
    },
  }).then((result) => {
    if (result.isConfirmed) {
      const deleteData = { status: 0 };
      this.deleteImage(deleteData, id);
    }
  });
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.dataImage.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}
}





