import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Component pages
import { HomeGatewayComponent } from './home-gateway/home-gateway.component';
import { FilMechaPreventiveComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-preventive.component';
import { FilMechaCorectiveComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-corective.component';
import { FilMechaOverHaulComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-haul.component';
import { FilMechaPrevPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-popup/filling-mecha-prev-popup.component';
import { FillingMechaPrevAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-add-data/filling-mecha-prev-add-data.component';
import { FilElecPreventiveComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-preventive.component';
import { FilElecCorectiveComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-corective.component';
import { FilElecOverHaulComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-haul.component';
import { PreMechaPreventiveComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/pre-mecha-preventive.component';
import { PreMechaCorectiveComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/pre-mecha-corective.component';
import { PreMechaOverHaulComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/pre-mecha-over-haul.component';
import { PreElecPreventiveComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/pre-elec-preventive.component';
import { PreElecCorectiveComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/pre-elec-corective.component';
import { PreElecOverHaulComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/pre-elec-over-haul.component';
import { PacMechaPreventiveComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/pac-mecha-preventive.component';
import { PacMechaCorectiveComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/pac-mecha-corective.component';
import { PacMechaOverHaulComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/pac-mecha-over-haul.component';
import { PacElecPreventiveComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/pac-elec-preventive.component';
import { PacElecCorectiveComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/pac-elec-corective.component';
import { PacElecOverHaulComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/pac-elec-over-haul.component';
import { ImprovementComponent } from './maintenance/can/continous-improvement/improvement/improvement.component';
import { ScheduleMechaMonthlyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-monthly/schedule-mecha-monthly.component';
import { ScheduleMechaWeeklyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-weekly/schedule-mecha-weekly.component';
import { ScheduleMechaDailyComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-daily.component';
import { ScheduleElecMonthlyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-monthly/schedule-elec-monthly.component';
import { ScheduleElecWeeklyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-weekly/schedule-elec-weekly.component';
import { ScheduleElecDailyComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-daily.component';
import { ContinousImprovementAddDataComponent } from './maintenance/can/continous-improvement/improvement/continous-improvement-add-data/continous-improvement-add-data.component';
import { PopupDescriptionMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-material/popup-description-material.component';
import { FillingMechaPrevEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-preventive/filling-mecha-prev-edit-data/filling-mecha-prev-edit-data.component';
import { FillingMechaCorAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-add-data/filling-mecha-cor-add-data.component';
import { FillingMechaCorPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-popup/filling-mecha-cor-popup.component';
import { FillingMechaCorMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-material/filling-mecha-cor-material.component';
import { FillingMechaCorEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-corective/filling-mecha-cor-edit-data/filling-mecha-cor-edit-data.component';
import { FillingMechaOverAddDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-add-data/filling-mecha-over-add-data.component';
import { FillingMechaOverPopupComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-popup/filling-mecha-over-popup.component';
import { FillingMechaOverMaterialComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-material/filling-mecha-over-material.component';
import { FillingMechaOverEditDataComponent } from './maintenance/can/master-plan/filling/mechanical/filling-mecha-over-haul/filling-mecha-over-edit-data/filling-mecha-over-edit-data.component';
import { FillingElecPrevAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-add-data/filling-elec-prev-add-data.component';
import { FillingElecPrevPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-popup/filling-elec-prev-popup.component';
import { FillingElecPrevMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-material/filling-elec-prev-material.component';
import { FillingElecPrevEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-preventive/filling-elec-prev-edit-data/filling-elec-prev-edit-data.component';
import { FillingElecCorAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-add-data/filling-elec-cor-add-data.component';
import { FillingElecCorPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-popup/filling-elec-cor-popup.component';
import { FillingElecCorMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-material/filling-elec-cor-material.component';
import { FillingElecCorEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-corective/filling-elec-cor-edit-data/filling-elec-cor-edit-data.component';
import { FillingElecOverAddDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-add-data/filling-elec-over-add-data.component';
import { FillingElecOverPopupComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-popup/filling-elec-over-popup.component';
import { FillingElecOverMaterialComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-material/filling-elec-over-material.component';
import { FillingElecOverEditDataComponent } from './maintenance/can/master-plan/filling/electrical/filling-elec-over-haul/filling-elec-over-edit-data/filling-elec-over-edit-data.component';
import { PackingElecCorAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-add-data/packing-elec-cor-add-data.component';
import { PackingElecCorPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-popup/packing-elec-cor-popup.component';
import { PackingElecCorMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-material/packing-elec-cor-material.component';
import { PackingElecCorEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-corective/packing-elec-cor-edit-data/packing-elec-cor-edit-data.component';
import { PackingElecPrevAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-add-data/packing-elec-prev-add-data.component';
import { PackingElecOverPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-popup/packing-elec-over-popup.component';
import { PackingElecOverMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-material/packing-elec-over-material.component';
import { PackingElecOverEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-edit-data/packing-elec-over-edit-data.component';
import { PackingElecPrevPopupComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-popup/packing-elec-prev-popup.component';
import { PackingElecPrevMaterialComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-material/packing-elec-prev-material.component';
import { PackingElecPrevEditDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-preventive/packing-elec-prev-edit-data/packing-elec-prev-edit-data.component';
import { PackingElecOverAddDataComponent } from './maintenance/can/master-plan/packing/electrical/pac-elec-over-haul/packing-elec-over-add-data/packing-elec-over-add-data.component';
import { PackingMechaPrevAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-add-data/packing-mecha-prev-add-data.component';
import { PackingMechaPrevPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-popup/packing-mecha-prev-popup.component';
import { PackingMechaPrevMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-material/packing-mecha-prev-material.component';
import { PackingMechaPrevEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-preventive/packing-mecha-prev-edit-data/packing-mecha-prev-edit-data.component';
import { PackingMechaCorAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-add-data/packing-mecha-cor-add-data.component';
import { PackingMechaCorPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-popup/packing-mecha-cor-popup.component';
import { PackingMechaCorMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-material/packing-mecha-cor-material.component';
import { PackingMechaCorEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-corective/packing-mecha-cor-edit-data/packing-mecha-cor-edit-data.component';
import { PackingMechaOverAddDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-add-data/packing-mecha-over-add-data.component';
import { PackingMechaOverPopupComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-popup/packing-mecha-over-popup.component';
import { PackingMechaOverMaterialComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-material/packing-mecha-over-material.component';
import { PackingMechaOverEditDataComponent } from './maintenance/can/master-plan/packing/mechanical/pac-mecha-over-haul/packing-mecha-over-edit-data/packing-mecha-over-edit-data.component';
import { PreparationElecPrevAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-add-data/preparation-elec-prev-add-data.component';
import { PreparationElecPrevPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-popup/preparation-elec-prev-popup.component';
import { PreparationElecPrevMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-material/preparation-elec-prev-material.component';
import { PreparationElecPrevEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-preventive/preparation-elec-prev-edit-data/preparation-elec-prev-edit-data.component';
import { PreparationElecCorPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-popup/preparation-elec-cor-popup.component';
import { PreparationElecCorAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-add-data/preparation-elec-cor-add-data.component';
import { PreparationElecCorMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-material/preparation-elec-cor-material.component';
import { PreparationElecCorEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-corective/preparation-elec-cor-edit-data/preparation-elec-cor-edit-data.component';
import { PreparationElecOverAddDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-add-data/preparation-elec-over-add-data.component';
import { PreparationElecOverPopupComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-popup/preparation-elec-over-popup.component';
import { PreparationElecOverMaterialComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-material/preparation-elec-over-material.component';
import { PreparationElecOverEditDataComponent } from './maintenance/can/master-plan/preparation/electrical/pre-elec-over-haul/preparation-elec-over-edit-data/preparation-elec-over-edit-data.component';
import { PreparationMechaPrevAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-add-data/preparation-mecha-prev-add-data.component';
import { PreparationMechaPrevPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-popup/preparation-mecha-prev-popup.component';
import { PreparationMechaPrevMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-material/preparation-mecha-prev-material.component';
import { PreparationMechaPrevEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-preventive/preparation-mecha-prev-edit-data/preparation-mecha-prev-edit-data.component';
import { PreparationMechaCorAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-add-data/preparation-mecha-cor-add-data.component';
import { PreparationMechaCorPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-popup/preparation-mecha-cor-popup.component';
import { PreparationMechaCorMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-material/preparation-mecha-cor-material.component';
import { PreparationMechaCorEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-corective/preparation-mecha-cor-edit-data/preparation-mecha-cor-edit-data.component';
import { PreparationMechaOverAddDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-add-data/preparation-mecha-over-add-data.component';
import { PreparationMechaOverPopupComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-popup/preparation-mecha-over-popup.component';
import { PreparationMechaOverMaterialComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-material/preparation-mecha-over-material.component';
import { PreparationMechaOverEditDataComponent } from './maintenance/can/master-plan/preparation/mechanical/pre-mecha-over-haul/preparation-mecha-over-edit-data/preparation-mecha-over-edit-data.component';
import { ScheduleWeeklyCareComponent } from './maintenance/can/continous-improvement/weekly-care/schedule/schedule-weekly-care/schedule-weekly-care.component';
import { SheduleMasterPlanComponent } from './maintenance/can/master-plan/shedule-master-plan/shedule-master-plan.component';
import { ReportMasterPlanComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan.component';
import { ReportMasterPlanAddDataComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan-add-data/report-master-plan-add-data.component';
import { ReportMasterPlanViewComponent } from './maintenance/can/master-plan/report-master-plan/report-master-plan-view/report-master-plan-view.component';
import { ContinousImprovementEditDataComponent } from './maintenance/can/continous-improvement/improvement/continous-improvement-edit-data/continous-improvement-edit-data.component';
import { ImprovementTimelineComponent } from './maintenance/can/continous-improvement/improvement/improvement-timeline/improvement-timeline.component';
import { ScheduleMechaAddDataComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-add-data/schedule-mecha-add-data.component';
import { ScheduleMechaUnplannedComponent } from './maintenance/can/schedule-maintenance/mechanical/schedule-mecha-daily/schedule-mecha-unplanned/schedule-mecha-unplanned.component';
import { DashboardCanComponent } from './maintenance/can/dashboard/dashboard-can/dashboard-can.component';
import { ScheduleElecAddDataComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-add-data/schedule-elec-add-data.component';
import { ScheduleElecUnplannedComponent } from './maintenance/can/schedule-maintenance/electrical/schedule-elec-daily/schedule-elec-unplanned/schedule-elec-unplanned.component';
import { ProfileComponent } from './user/profile/profile/profile.component';
import { AddUserComponent } from './user/add-user/add-user/add-user.component';
import { AddDataUserComponent } from './user/add-user/add-data-user/add-data-user.component';
import { ListReportComponent } from './maintenance/can/master-plan/list-report/list-report.component';
import { ReportScheduleMaintenanceComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance.component';
import { ReportScheduleMaintenanceAddDataComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance-add-data/report-schedule-maintenance-add-data.component';
import { ReportScheduleMaintenanceViewComponent } from './maintenance/can/schedule-maintenance/report-schedule-maintenance/report-schedule-maintenance-view/report-schedule-maintenance-view.component';
import { ListReportScheduleMaintenanceComponent } from './maintenance/can/schedule-maintenance/list-report-schedule-maintenance/list-report-schedule-maintenance.component';
import { ListReportEditComponent } from './maintenance/can/master-plan/list-report/list-report-edit/list-report-edit.component';
import { ListReportScheduleMaintenanceEditComponent } from './maintenance/can/schedule-maintenance/list-report-schedule-maintenance/list-report-schedule-maintenance-edit/list-report-schedule-maintenance-edit.component';
import { DashboardProdComponent } from './production/dashboards/dashboard-prod/dashboard-prod.component';
import { BudgetingMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance.component';
import { BudgetingRepairMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-maintenance.component';
import { BudgetingMaintenanceMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-maintenance.component';
import { BudgetingOverhaulMaintenanceComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-maintenance.component';
import { BudgetingRepairAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-add-data/budgeting-repair-add-data.component';
import { BudgetingRepairEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-repair-maintenance/budgeting-repair-edit-data/budgeting-repair-edit-data.component';
import { QuoteComponent } from './master/quote/quote.component';
import { PictureComponent } from './master/picture/picture.component';
import { BudgetingMaintenanceAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-add-data/budgeting-maintenance-add-data.component';
import { BudgetingMaintenanceEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-maintenance-maintenance/budgeting-maintenance-edit-data/budgeting-maintenance-edit-data.component';
import { BudgetingOverhaulAddDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-add-data/budgeting-overhaul-add-data.component';
import { BudgetingOverhaulEditDataComponent } from './maintenance/can/budgeting-maintenance/budgeting-overhaul-maintenance/budgeting-overhaul-edit-data/budgeting-overhaul-edit-data.component';
import { ReportWeeklyCareComponent } from './maintenance/can/continous-improvement/weekly-care/report-weekly-care/report-weekly-care.component';
import { FunctionLocationComponent } from './master/function-location/function-location.component';
import { MachineComponent } from './master/machine/machine.component';

const routes: Routes = [
  //CAN Production
  {
    path: 'production', loadChildren: () => import('./production/production.module').then(m => m.ProductionModule)
  },
  //Admin
  {
    path: 'menu', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },

  //PET Maintenance
  {
    path: 'maintenance', loadChildren: () => import('./maintenance/maintenance.module').then(m => m.MaintenanceModule)
  },

  //CAN Maintenance
  {
    path: "report/weekly-care",
    component: ReportWeeklyCareComponent
  },
  {
    path: "dashboard/can",
    component: DashboardCanComponent
  },
  {
    path: "quote",
    component: QuoteComponent
  },
  {
    path: "function-location",
    component: FunctionLocationComponent
  },
  {
    path: "machine",
    component: MachineComponent
  },
  {
    path: "image/landing",
    component: PictureComponent
  },
  {
    path: "dashboard-prod",
    component: DashboardProdComponent
  },
  {
    path: "gateway",
    component: HomeGatewayComponent
  },
  {
    path: "budgeting/total/maintenance",
    component: BudgetingMaintenanceComponent
  },
  {
    path: "budgeting/repair/maintenance",
    component: BudgetingRepairMaintenanceComponent
  },
  {
    path: "budgeting/repair/maintenance/add-data",
    component: BudgetingRepairAddDataComponent
  },
  {
    path: "budgeting/repair/maintenance/add-data/:id",
    component: BudgetingRepairAddDataComponent
  },
  {
    path: "budgeting/repair/maintenance/edit-data",
    component: BudgetingRepairEditDataComponent
  },
  {
    path: "budgeting/repair/maintenance/edit-data/:id",
    component: BudgetingRepairEditDataComponent
  },
  {
    path: "budgeting/maintenance/maintenance",
    component: BudgetingMaintenanceMaintenanceComponent
  },
  {
    path: "budgeting/maintenance/maintenance/add-data",
    component: BudgetingMaintenanceAddDataComponent
  },
  {
    path: "budgeting/maintenance/maintenance/add-data/:id",
    component: BudgetingMaintenanceAddDataComponent
  },
  {
    path: "budgeting/maintenance/maintenance/edit-data",
    component: BudgetingMaintenanceEditDataComponent
  },
  {
    path: "budgeting/maintenance/maintenance/edit-data/:id",
    component: BudgetingMaintenanceEditDataComponent
  },
  {
    path: "budgeting/overhaul/maintenance",
    component: BudgetingOverhaulMaintenanceComponent
  },
  {
    path: "budgeting/overhaul/maintenance/add-data",
    component: BudgetingOverhaulAddDataComponent
  },
  {
    path: "budgeting/overhaul/maintenance/add-data/:id",
    component: BudgetingOverhaulAddDataComponent
  },
  {
    path: "budgeting/overhaul/maintenance/edit-data",
    component: BudgetingOverhaulEditDataComponent
  },
  {
    path: "budgeting/overhaul/maintenance/edit-data/:id",
    component: BudgetingOverhaulEditDataComponent
  },
  {
    path: "schedule/master-plan",
    component: SheduleMasterPlanComponent
  },
  {
    path: "filling/mechanical/preventive",
    component: FilMechaPreventiveComponent
  },
  {
    path: "filling/mechanical/preventive/adddata",
    component: FillingMechaPrevAddDataComponent
  },
  {
    path: "filling/mechanical/preventive/popup",
    component: FilMechaPrevPopupComponent
  },
  {
    path: "filling/mechanical/preventive/popup-description",
    component: PopupDescriptionMaterialComponent
  },
  {
    path: "filling/mechanical/preventive/edit-data/:id",
    component: FillingMechaPrevEditDataComponent
  },
  {
    path: "filling/mechanical/corective",
    component: FilMechaCorectiveComponent
  },
  {
    path: "filling/mechanical/corective/adddata",
    component: FillingMechaCorAddDataComponent
  },
  {
    path: "filling/mechanical/corective/popup",
    component: FillingMechaCorPopupComponent
  },
  {
    path: "filling/mechanical/corective/material",
    component: FillingMechaCorMaterialComponent
  },
  {
    path: "filling/mechanical/corective/edit-data/:id",
    component: FillingMechaCorEditDataComponent
  },
  {
    path: "filling/mechanical/over-haul",
    component: FilMechaOverHaulComponent
  },
  {
    path: "filling/mechanical/over-haul/adddata",
    component: FillingMechaOverAddDataComponent
  },
  {
    path: "filling/mechanical/over-haul/popup",
    component: FillingMechaOverPopupComponent
  },
  {
    path: "filling/mechanical/over-haul/material",
    component: FillingMechaOverMaterialComponent
  },
  {
    path: "filling/mechanical/over-haul/edit-data/:id",
    component: FillingMechaOverEditDataComponent
  },
  {
    path: "filling/electrical/preventive",
    component: FilElecPreventiveComponent
  },
  {
    path: "filling/electrical/preventive/adddata",
    component: FillingElecPrevAddDataComponent
  },
  {
    path: "filling/electrical/preventive/popup",
    component: FillingElecPrevPopupComponent
  },
  {
    path: "filling/electrical/preventive/material",
    component: FillingElecPrevMaterialComponent
  },
  {
    path: "filling/electrical/preventive/edit-data/:id",
    component: FillingElecPrevEditDataComponent
  },
  {
    path: "filling/electrical/corective",
    component: FilElecCorectiveComponent
  },
  {
    path: "filling/electrical/corective/adddata",
    component: FillingElecCorAddDataComponent
  },
  {
    path: "filling/electrical/corective/popup",
    component: FillingElecCorPopupComponent
  },
  {
    path: "filling/electrical/corective/material",
    component: FillingElecCorMaterialComponent
  },
  {
    path: "filling/electrical/corective/edit-data/:id",
    component: FillingElecCorEditDataComponent
  },
  {
    path: "filling/electrical/over-haul",
    component: FilElecOverHaulComponent
  },
  {
    path: "filling/electrical/over-haul/adddata",
    component: FillingElecOverAddDataComponent
  },
  {
    path: "filling/electrical/over-haul/popup",
    component: FillingElecOverPopupComponent
  },
  {
    path: "filling/electrical/over-haul/material",
    component: FillingElecOverMaterialComponent
  },
  {
    path: "filling/electrical/over-haul/edit-data/:id",
    component: FillingElecOverEditDataComponent
  },
  {
    path: "preparation/mechanical/preventive",
    component: PreMechaPreventiveComponent
  },
  {
    path: "preparation/mechanical/preventive/adddata",
    component: PreparationMechaPrevAddDataComponent
  },
  {
    path: "preparation/mechanical/preventive/popup",
    component: PreparationMechaPrevPopupComponent
  },
  {
    path: "preparation/mechanical/preventive/material",
    component: PreparationMechaPrevMaterialComponent
  },
  {
    path: "preparation/mechanical/preventive/edit-data/:id",
    component: PreparationMechaPrevEditDataComponent
  },
  {
    path: "preparation/mechanical/corective",
    component: PreMechaCorectiveComponent
  },
  {
    path: "preparation/mechanical/corective/adddata",
    component: PreparationMechaCorAddDataComponent
  },
  {
    path: "preparation/mechanical/corective/popup",
    component: PreparationMechaCorPopupComponent
  },
  {
    path: "preparation/mechanical/corective/material",
    component: PreparationMechaCorMaterialComponent
  },
  {
    path: "preparation/mechanical/corective/edit-data/:id",
    component: PreparationMechaCorEditDataComponent
  },
  {
    path: "preparation/mechanical/over-haul",
    component: PreMechaOverHaulComponent
  },
  {
    path: "preparation/mechanical/over-haul/adddata",
    component: PreparationMechaOverAddDataComponent
  },
  {
    path: "preparation/mechanical/over-haul/popup",
    component: PreparationMechaOverPopupComponent
  },
  {
    path: "preparation/mechanical/over-haul/material",
    component: PreparationMechaOverMaterialComponent
  },
  {
    path: "preparation/mechanical/over-haul/edit-data/:id",
    component: PreparationMechaOverEditDataComponent
  },
  {
    path: "preparation/electrical/preventive",
    component: PreElecPreventiveComponent
  },
  {
    path: "preparation/electrical/preventive/adddata",
    component: PreparationElecPrevAddDataComponent
  },
  {
    path: "preparation/electrical/preventive/popup",
    component: PreparationElecPrevPopupComponent
  },
  {
    path: "preparation/electrical/preventive/material",
    component: PreparationElecPrevMaterialComponent
  },
  {
    path: "preparation/electrical/preventive/edit-data/:id",
    component: PreparationElecPrevEditDataComponent
  },
  {
    path: "preparation/electrical/corective",
    component: PreElecCorectiveComponent
  },
  {
    path: "preparation/electrical/corective/adddata",
    component: PreparationElecCorAddDataComponent
  },
  {
    path: "preparation/electrical/corective/popup",
    component: PreparationElecCorPopupComponent
  },
  {
    path: "preparation/electrical/corective/material",
    component: PreparationElecCorMaterialComponent
  },
  {
    path: "preparation/electrical/corective/edit-data/:id",
    component: PreparationElecCorEditDataComponent
  },
  {
    path: "preparation/electrical/over-haul",
    component: PreElecOverHaulComponent
  },
  {
    path: "preparation/electrical/over-haul/adddata",
    component: PreparationElecOverAddDataComponent
  },
  {
    path: "preparation/electrical/over-haul/popup",
    component: PreparationElecOverPopupComponent
  },
  {
    path: "preparation/electrical/over-haul/material",
    component: PreparationElecOverMaterialComponent
  },
  {
    path: "preparation/electrical/over-haul/edit-data/:id",
    component: PreparationElecOverEditDataComponent
  },
  {
    path: "packing/mechanical/preventive",
    component: PacMechaPreventiveComponent
  },
  {
    path: "packing/mechanical/preventive/adddata",
    component: PackingMechaPrevAddDataComponent
  },
  {
    path: "packing/mechanical/preventive/popup",
    component: PackingMechaPrevPopupComponent
  },
  {
    path: "packing/mechanical/preventive/material",
    component: PackingMechaPrevMaterialComponent
  },
  {
    path: "packing/mechanical/preventive/edit-data/:id",
    component: PackingMechaPrevEditDataComponent
  },
  {
    path: "packing/mechanical/corective",
    component: PacMechaCorectiveComponent
  },
  {
    path: "packing/mechanical/corective/adddata",
    component: PackingMechaCorAddDataComponent
  },
  {
    path: "packing/mechanical/corective/popup",
    component: PackingMechaCorPopupComponent
  },
  {
    path: "packing/mechanical/corective/material",
    component: PackingMechaCorMaterialComponent
  },
  {
    path: "packing/mechanical/corective/edit-data/:id",
    component: PackingMechaCorEditDataComponent
  },
  {
    path: "packing/mechanical/over-haul",
    component: PacMechaOverHaulComponent
  },
  {
    path: "packing/mechanical/over-haul/adddata",
    component: PackingMechaOverAddDataComponent
  },
  {
    path: "packing/mechanical/over-haul/popup",
    component: PackingMechaOverPopupComponent
  },
  {
    path: "packing/mechanical/over-haul/material",
    component: PackingMechaOverMaterialComponent
  },
  {
    path: "packing/mechanical/over-haul/edit-data/:id",
    component: PackingMechaOverEditDataComponent
  },
  {
    path: "packing/electrical/preventive",
    component: PacElecPreventiveComponent
  },
  {
    path: "packing/electrical/preventive/adddata",
    component: PackingElecPrevAddDataComponent
  },
  {
    path: "packing/electrical/preventive/popup",
    component: PackingElecPrevPopupComponent
  },
  {
    path: "packing/electrical/preventive/material",
    component: PackingElecPrevMaterialComponent
  },
  {
    path: "packing/electrical/preventive/edit-data/:id",
    component: PackingElecPrevEditDataComponent
  },
  {
    path: "packing/electrical/corective",
    component: PacElecCorectiveComponent
  },
  {
    path: "packing/electrical/corective/adddata",
    component: PackingElecCorAddDataComponent
  },
  {
    path: "packing/electrical/corective/popup",
    component: PackingElecCorPopupComponent
  },
  {
    path: "packing/electrical/corective/material",
    component: PackingElecCorMaterialComponent
  },
  {
    path: "packing/electrical/corective/edit-data/:id",
    component: PackingElecCorEditDataComponent
  },
  {
    path: "packing/electrical/over-haul",
    component: PacElecOverHaulComponent
  },
  {
    path: "packing/electrical/over-haul/adddata",
    component: PackingElecOverAddDataComponent
  },
  {
    path: "packing/electrical/over-haul/popup",
    component: PackingElecOverPopupComponent
  },
  {
    path: "packing/electrical/over-haul/material",
    component: PackingElecOverMaterialComponent
  },
  {
    path: "packing/electrical/over-haul/edit-data/:id",
    component: PackingElecOverEditDataComponent
  },
  {
    path: "report/master-plan",
    component: ReportMasterPlanComponent
  },
  {
    path: "report/master-plan/edit-data/:id",
    component: ListReportEditComponent
  },
  {
    path: "report/master-plan/add-data/:id",
    component: ReportMasterPlanAddDataComponent
  },
  {
    path: "report/master-plan/view",
    component: ReportMasterPlanViewComponent
  },
  {
    path: "report/list-report",
    component: ListReportComponent
  },
  {
    path: "report/schedule-maintenance/add",
    component: ReportScheduleMaintenanceComponent
  },
  {
    path: "report/schedule-maintenance/edit-data/:id",
    component: ListReportScheduleMaintenanceEditComponent
  },
  {
    path: "report/schedule-maintenance/add-data/:id",
    component: ReportScheduleMaintenanceAddDataComponent
  },
  {
    path: "report/schedule-maintenance/view",
    component: ReportScheduleMaintenanceViewComponent
  },
  {
    path: "list-report/schedule-maintenance",
    component: ListReportScheduleMaintenanceComponent
  },
  {
    path: "continous/weekly-care/schedule",
    component: ScheduleWeeklyCareComponent
  },
  {
    path: "continous/improvement",
    component: ImprovementComponent
  },
  {
    path: "continous/improvement/add-data",
    component: ContinousImprovementAddDataComponent
  },
  {
    path: "continous/improvement/add-data/:id",
    component: ContinousImprovementAddDataComponent
  },
  {
    path: "continous/improvement/edit-data/:id",
    component: ContinousImprovementEditDataComponent
  },
  {
    path: "continous/improvement/timeline/:id",
    component: ImprovementTimelineComponent
  },
  {
    path: "schedule/mecha/monthly",
    component: ScheduleMechaMonthlyComponent
  },
  {
    path: "schedule/mecha/weekly",
    component: ScheduleMechaWeeklyComponent
  },
  {
    path: "schedule/mecha/daily",
    component: ScheduleMechaDailyComponent
  },
  {
    path: "schedule/mecha/daily/add-data",
    component: ScheduleMechaAddDataComponent
  },
  {
    path: "schedule/mecha/daily/add-data/:id",
    component: ScheduleMechaAddDataComponent
  },
  {
    path: "schedule/mecha/daily/unplanned",
    component: ScheduleMechaUnplannedComponent
  },
  {
    path: "schedule/mecha/daily/unplanned/:id",
    component: ScheduleMechaUnplannedComponent
  },
  {
    path: "schedule/elec/monthly",
    component: ScheduleElecMonthlyComponent
  },
  {
    path: "schedule/elec/weekly",
    component: ScheduleElecWeeklyComponent
  },
  {
    path: "schedule/elec/daily",
    component: ScheduleElecDailyComponent
  },
  {
    path: "schedule/elec/daily/add-data",
    component: ScheduleElecAddDataComponent
  },
  {
    path: "schedule/elec/daily/add-data/:id",
    component: ScheduleElecAddDataComponent
  },
  {
    path: "schedule/elec/daily/unplanned",
    component: ScheduleElecUnplannedComponent
  },
  {
    path: "schedule/elec/daily/unplanned/:id",
    component: ScheduleElecUnplannedComponent
  },
  // {
  //   path: "sparepart/machine",
  //   component: BudgetingRepairDetailDataComponent
  // },
  {
    path: "auth/profile/:id",
    component: ProfileComponent
  },
  {
    path: "auth/add-user",
    component: AddUserComponent
  },
  {
    path: "auth/add-user/add-data",
    component: AddDataUserComponent
  },
  {
    path: "auth/add-user/add-data/:id",
    component: AddDataUserComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
