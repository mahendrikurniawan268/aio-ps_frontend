import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { TokenStorageService } from '../../../../core/services/token-storage.service';
import { User } from '../../add-user/user.model';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userData:any;
  dataMasterUser:any;
  id_masterUser:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  oldPassword: string = '';
  newPassword: string = '';
  confirmPassword: string = '';

  newUser: User = {
    nik: null,
    name: '',
    role_id: '',
    password: '',
    email: '',
  }

  constructor(private TokenStorageService : TokenStorageService, private RestApiService : RestApiService,
    private route:ActivatedRoute){}

  ngOnInit(): void {
    this.userData =  this.TokenStorageService.getUser();  
    this.route.params.subscribe((params) => {
      const idToEdit = params['id'];
      this.id_masterUser = idToEdit
      if (idToEdit) {
        this.getIdMasterUser(idToEdit);
      }
    });
  }

  getImgFilePhoto(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getIdMasterUser(id:any){
    this.RestApiService.getMasterUserById(id).subscribe(
  (res:any) => {
    this.dataMasterUser = res.data[0];
    this.newUser = { ...this.dataMasterUser };
  });
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  getRole(role_id: number): string {
    switch (role_id) {
        case 1:
            return 'Admin';
        case 2:
            return 'Supervisor';
        case 3:
            return 'Employee';
        case 4:
          return 'Other Departement';
        default:
            return 'Guest';
    }
  }

  updateMasterUser(dataMasterUser: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
        formData.append('files', fileData.file, fileData.file.name);
        formData.append('type', fileData.type);
      });

      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataMasterUser.photo = res.uploadedFiles;
          delete dataMasterUser.password;
  
          this.RestApiService.updateMasterUser(this.id_masterUser, dataMasterUser).subscribe(
            (res: any) => {
              if (res.status == true) {
                
                this.newUser = dataMasterUser;
                this.getIdMasterUser(this.id_masterUser);
              }
            },
            (error) => {
              console.error(error)
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      delete dataMasterUser.password;
      this.updateUserData(dataMasterUser)
    }
  }   
  
  updateUserData(dataMasterUser: any) {
    this.RestApiService.updateMasterUser(this.id_masterUser, dataMasterUser).subscribe(
      (res: any) => {
        if (res.status === true) {
          this.newUser = dataMasterUser;
          this.getIdMasterUser(this.id_masterUser);
        }
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  onUpdate() {
      this.updateMasterUser(this.newUser);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
  }

  updatePassword(dataUser:any) {
    if (this.oldPassword !== '' && this.newPassword !== '' && this.confirmPassword !== '') {
      if (this.oldPassword == this.newPassword) {
        Swal.fire({
          title: 'Error',
          text: 'Old Password and New Password is same',
          icon: 'error',
          confirmButtonText: 'OK'
        });
        return;
      }
    
      if (this.newPassword !== this.confirmPassword) {
        Swal.fire({
          title: 'Error',
          text: 'New Password and Confirm Password do not match',
          icon: 'error',
          confirmButtonText: 'OK'
        });
        return;
      }
  
      this.newUser.password = this.newPassword;
    
      this.RestApiService.updateMasterUser(this.id_masterUser, this.newUser).subscribe(
        (res: any) => {
          if (res.status === true) {
            this.oldPassword = '';
            this.newPassword = '';
            this.confirmPassword = '';
            Swal.fire({
              title: 'Notification',
              text: 'Password Updated Successfully!',
              icon: 'success',
              timer: 1500,
              showConfirmButton: false,
              customClass: {
                popup: 'custom-swal-text', 
              },
            });
          }
        },
        (error: any) => {
          console.error(error);
          Swal.fire({
            title: 'Error',
            text: 'Failed to update password. Please try again.',
            icon: 'error',
            confirmButtonText: 'OK'
          });
        }
      );
    }
  };
  

  onUpdatePassword() {
    this.updatePassword(this.newUser);
  } 
}


