import { Component, OnInit} from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { User } from '../user.model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-data-user',
  templateUrl: './add-data-user.component.html',
  styleUrls: ['./add-data-user.component.scss']
})
export class AddDataUserComponent implements OnInit{

  constructor(private RestApiService: RestApiService, private router: Router, 
    private route:ActivatedRoute){}

  dataRole: any [] = [];
  dataUser: any;
  id_user: any;
  idParam:any;
  dataAllUser:any
  errorMessage: string = '';

  newUser: User = {
    nik: null,
    name: '',
    role_id: '',
    password: '',
    email: '',
  }

  ngOnInit(): void {
    this.getRoleUser();
    this.idParam = this.route.snapshot.paramMap.get('id');
    if (this.idParam) {
      this.getMasterUserById(this.idParam);
    }
    this.getAllMasterUser();
  }

  getMasterUserById(id:any){
    this.RestApiService.getMasterUserById(id).subscribe(
    (res:any) => {
      this.newUser = res.data[0];
    })
  }

  getRoleUser(){
    this.RestApiService.getRole().subscribe(
      (res:any) => {
        this.dataRole = res.data[0];
      });
  }

  getAllMasterUser(){
    this. RestApiService.getAllMasterUser().subscribe(
      (res:any) => {
        this.dataAllUser = res.data;
      });
  }

  insertDataUser(dataMasterUser: any){
    this.RestApiService.insertMasterUser(dataMasterUser).subscribe(
      (res:any) => {
        this.dataUser = res.data;
        this.dataUser.id = this.id_user;
        this.getAllMasterUser();
      },
      (error: any) => {
        console.error(error);
        if (error.status === 200) {
          this.errorMessage = error.error.message;
          Swal.fire({
              title: 'Error',
              text: this.errorMessage,
              icon: 'error',
              confirmButtonText: 'OK'
          });
      } else {
      }
      });
  }

  onInsert(form: NgForm) {
    if (form.valid) {
      if (this.idParam) {
        this.updateUser(this.newUser);
      } else {
        this.insertDataUser(this.newUser);
      }  
  
      Swal.fire({
        title: 'Notification',
        text: 'Data Added Successfully!',
        icon: 'success',
        showConfirmButton: true,
        showCancelButton: true, 
        customClass: {
          popup: 'custom-swal-text', 
        },
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigate(['auth/add-user']);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } else {
      Swal.fire({
        title: 'Error',
        text: 'The table is not complete',
        icon: 'error',
        confirmButtonText: 'OK'
      });
      Object.keys(form.controls).forEach(field => {
        const control = form.controls[field];
        control.markAsTouched(); 
      });
    }
  }  

  updateUser(dataMasterUser: any) {
    this.RestApiService.updateMasterUser(this.idParam, dataMasterUser).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.dataUser = dataMasterUser;
          this.getAllMasterUser();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdateUser() {
    this.updateUser(this.newUser);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
  } 

  forBack(){
    this.router.navigate(['/auth/add-user'])
  }
}
