export interface User {
    nik: number | null;
    name: string;
    role_id: string;
    password: string;
    email: string;
}