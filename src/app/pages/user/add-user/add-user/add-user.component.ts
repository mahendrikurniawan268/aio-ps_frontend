import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit{
  tableColumns = ['No', 'Photo', 'NIK', 'Name', 'Role', 'Email', 'Details'];
  titlePage = 'Profile List';
  index: number = 0;
  dataUser: any[] = [];
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;

  currentPage = 1;
  itemsPerPage = 10;

  constructor(private router : Router, private RestApiService:RestApiService){}

  ngOnInit(): void {
    this.getAllUser();
    this.paginateData()
    this.getAllUser()
  }

  getImgFilePhoto(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getAllUser(){
    this.RestApiService.getTabelViewUser().subscribe(
      (res:any) => {
        ;
        this.dataUser = res.data;
        this.preData = [...this.dataUser];
        this.paginateData()
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  addData(){
    this.router.navigate(['/auth/add-user/add-data'])
  }

  onEdit(id:any){
    this.router.navigate([`auth/add-user/add-data/${id}`])
  }

  search() {
    this.dataUser = this.preData.filter((item: { nik: number; name: string; }) => {
      return item.nik.toString().includes(this.searchText.toLowerCase()) ||
             item.name.toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataUser.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  updateMasterUser(dataMasterUser: any, id:any) {
    this.RestApiService.updateMasterUser(id, dataMasterUser).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.dataUser = dataMasterUser;
          this.getAllUser();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateMasterUser(deleteData, id);
      }
    });
  }
}


