import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceRoutingModule } from './maintenance-routing.module';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import { NgbAccordionModule, NgbCollapseModule, NgbDropdownModule, NgbModule, NgbNavModule, NgbPaginationModule, NgbProgressbarModule, NgbRatingModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InViewportModule } from '@thisissoon/angular-inviewport';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SimplebarAngularModule } from 'simplebar-angular';
import { SharedModule } from 'src/app/shared/shared.module';

// Calendar package
import { FullCalendarModule } from '@fullcalendar/angular';
import { DatePipe } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgSelectModule } from '@ng-select/ng-select';
import { CountToModule } from 'angular-count-to';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxMaskDirective,  provideNgxMask } from 'ngx-mask';
import { LOCALE_ID } from '@angular/core';
import { WeeklyCareComponent } from './can/continous-improvement/weekly-care/weekly-care/weekly-care.component';
import { WeeklyCareAddDataComponent } from './can/continous-improvement/weekly-care/weekly-care/weekly-care-add-data/weekly-care-add-data.component';
import { AutocompleteComponent, AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AutonomMaintenanceComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-maintenance.component';
import { AutomonAddPictureComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/automon-add-picture/automon-add-picture.component';
import { AutonomAddDataComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-add-data/autonom-add-data.component';
import { AutonomConfirmComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-confirm/autonom-confirm.component';
import { MasterViewComponent } from './can/master-plan/master-view/master-view.component';
import { MasterAddComponent } from './can/master-plan/master-add/master-add.component';
import { MasterEditComponent } from './can/master-plan/master-edit/master-edit.component';
import { MasterPopupComponent } from './can/master-plan/master-popup/master-popup.component';
import { MasterMaterialComponent } from './can/master-plan/master-material/master-material.component';

@NgModule({
  declarations: [
           WeeklyCareComponent,
           WeeklyCareAddDataComponent,
           AutonomMaintenanceComponent,
           AutomonAddPictureComponent,
           AutonomAddDataComponent,
           AutonomConfirmComponent,
           MasterViewComponent,
           MasterAddComponent,
           MasterEditComponent,
           MasterPopupComponent,
           MasterMaterialComponent
  ],
  imports: [
    CommonModule,
    MaintenanceRoutingModule,
    FormsModule,
    DropzoneModule,
    ScrollSpyModule.forRoot(),
    NgbDropdownModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    FullCalendarModule,
    ReactiveFormsModule,
    SharedModule,
    DatePipe,
    FormsModule,
    NgbTooltipModule,
    NgbDropdownModule,
    NgbAccordionModule,
    NgbProgressbarModule,
    NgbNavModule,
    NgbPaginationModule,
    NgbCollapseModule,
    FeatherModule.pick(allIcons),
    FlatpickrModule.forRoot(),
    SimplebarAngularModule,
    CountToModule,
    NgApexchartsModule,
    LeafletModule,
    NgSelectModule,
    NgxUsefulSwiperModule,
    NgxMaskDirective, 
    NgbModule,
    NgApexchartsModule,
    SimplebarAngularModule,
    AutocompleteLibModule
  ],

  exports: [
    NgbModule,
    MdbModalModule,
    FormsModule,
    DropzoneModule,
    NgbDropdownModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    FullCalendarModule,
    ReactiveFormsModule,
    DatePipe,
    SimplebarAngularModule,
    SharedModule,
    NgbPaginationModule
  ],
  providers: [
    DatePipe,
    provideNgxMask(),
    { provide: LOCALE_ID, useValue: 'id-ID' },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MaintenanceModule { }
