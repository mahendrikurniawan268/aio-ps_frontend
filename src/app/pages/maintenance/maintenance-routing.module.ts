import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutomonAddPictureComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/automon-add-picture/automon-add-picture.component';
import { AutonomAddDataComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-add-data/autonom-add-data.component';
import { AutonomConfirmComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-confirm/autonom-confirm.component';
import { AutonomMaintenanceComponent } from './can/continous-improvement/autonomous-maintenance/autonom-maintenance/autonom-maintenance.component';
import { WeeklyCareAddDataComponent } from './can/continous-improvement/weekly-care/weekly-care/weekly-care-add-data/weekly-care-add-data.component';
import { WeeklyCareComponent } from './can/continous-improvement/weekly-care/weekly-care/weekly-care.component';
import { MasterAddComponent } from './can/master-plan/master-add/master-add.component';
import { MasterEditComponent } from './can/master-plan/master-edit/master-edit.component';
import { MasterPopupComponent } from './can/master-plan/master-popup/master-popup.component';
import { MasterViewComponent } from './can/master-plan/master-view/master-view.component';

const routes: Routes = [
  {
    path: 'pet', loadChildren: () => import('./pet/pet.module').then(m => m.PetModule)
  },
  {
    path: "weekly-care",
    component: WeeklyCareComponent
  },
  {
    path: "weekly-care/add-data",
    component: WeeklyCareAddDataComponent
  },
  {
    path: "weekly-care/edit-data/:id",
    component: WeeklyCareAddDataComponent
  },
  {
    path: "autonom-maintenance",
    component: AutonomMaintenanceComponent
  },
  {
    path: "autonom-maintenance/add-picture",
    component: AutomonAddPictureComponent
  },
  {
    path: "autonom-maintenance/add-picture/:id",
    component: AutomonAddPictureComponent
  },
  {
    path: "autonom-maintenance/add-data/:id",
    component: AutonomAddDataComponent
  },
  {
    path: "autonom-maintenance/confirm",
    component: AutonomConfirmComponent
  },
  {
    path: "master-plan/view",
    component: MasterViewComponent
  },
  {
    path: "master-plan/add",
    component: MasterAddComponent
  },
  {
    path: "master-plan/edit/:id",
    component: MasterEditComponent
  },
  {
    path: "master-plan/popup",
    component: MasterPopupComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
