import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCanComponent } from './dashboard-can.component';

describe('DashboardCanComponent', () => {
  let component: DashboardCanComponent;
  let fixture: ComponentFixture<DashboardCanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardCanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardCanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
