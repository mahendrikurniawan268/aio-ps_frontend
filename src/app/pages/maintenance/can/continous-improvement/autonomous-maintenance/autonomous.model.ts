export interface Autonomous {
    no: number | null;
    task_activity: string;
    periode: string;
    standart: string;
    pic: string;
    date: string;
    remark: string;
    id_picture: number | null;
  }