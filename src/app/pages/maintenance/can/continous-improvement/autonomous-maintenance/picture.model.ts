export interface Picture {
    id_line: null | number;
    id_area: null | number;
    id_machine: null | number;
    picture1: string;
    picture2: string;
    picture3: string;
    picture4: string;
    picture5: string;
  }