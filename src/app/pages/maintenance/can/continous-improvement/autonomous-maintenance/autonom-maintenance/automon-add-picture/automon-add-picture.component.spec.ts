import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomonAddPictureComponent } from './automon-add-picture.component';

describe('AutomonAddPictureComponent', () => {
  let component: AutomonAddPictureComponent;
  let fixture: ComponentFixture<AutomonAddPictureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomonAddPictureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutomonAddPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
