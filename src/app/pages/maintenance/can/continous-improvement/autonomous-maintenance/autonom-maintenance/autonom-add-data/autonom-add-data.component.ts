import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Picture } from '../../picture.model';
import { Autonomous } from '../../autonomous.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-autonom-add-data',
  templateUrl: './autonom-add-data.component.html',
  styleUrls: ['./autonom-add-data.component.scss']
})
export class AutonomAddDataComponent implements OnInit {

  public autonomous:any [] = [];
  public idParam:any;
  public autonomousSaveData: boolean[] = [];
  public currentNo: number = 1;

  newPicture: Picture = {
    id_line: null,
    id_area: null,
    id_machine: null, 
    picture1: '',
    picture2: '',
    picture3: '',
    picture4: '',
    picture5: ''
  };

  newAutonomous: Autonomous = {
    no: null,
    task_activity: '',
    periode: '',
    standart: '',
    pic: '',
    date: '',
    remark: '',
    id_picture: null,
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  id_picture: any;
  id_autonomous: any;
  dataPicture:any;
  autonomousData: Autonomous[] = [];
  namePic: any [] = [];
  picEmployee:any;
  idLine: any;
  idArea: any;
  standartOptions = ['Normal', 'Clean', 'Lubrication', 'Tight', 'No Bending', 'No Burn','No Broken', 'No Blocked', 'No Crack',
  'No Clogging', 'No Damage', 'No Dirty', 'No Function', 'No Scracth', 'No Sincronaise', 'No Standart Level', 'No Short Circuit', 'No Noise', 'No Wear Out', 'No Working'];
  standartSearchKeyword: string = '';
  periodeOptions = ['M', 'W'];
  periodeSearchKeyword: string = '';

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getPictureById(this.idParam);
    this.getNamePic();
    this.getAutonomousByPictureId(this.idParam);
    this.autonomous.forEach((item: any) => {
      item.isUpdated = false;
    });
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
    this.autonomousSaveData = new Array(this.autonomous.length).fill(false);

    this.breadCrumbItems = [{
        label: 'Forms'
      },
      {
        label: 'Form Layout',
        active: true
      }
    ];
  }

  onStandartSelect(selectedItem: string) {
    this.newAutonomous.standart = selectedItem;
  }

  onPeriodeSelect(selectedItem: string) {
    this.newAutonomous.periode = selectedItem;
  }

  getImgFilePicture1(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture2(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture3(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture4(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture5(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  editPicture(id:any){
    this.router.navigate([`maintenance/autonom-maintenance/add-picture/${id}`], {queryParams: {lineId: this.idLine, areaId: this.idArea}})
  }

  async addData() {
    if (this.autonomous.length === 0) {
      const lastNo = 0;
      this.autonomous.push({
        no: lastNo + 1,
        task_activity: '',
        periode: '',
        standart: '',
        pic: '',
        date: '',
        remark: '',
        id_picture: this.idParam,
        isUpdated: false
      });
    } else {
      await Promise.all(this.autonomous.map((data: any) => {
        if (!data.isUpdated) {
          return this.insertOrUpdateAutonomous(data);
        } else {
          return Promise.resolve();
        }
      }));
      const lastNo = Math.max(...this.autonomous.map(item => item.no));
      this.autonomous.push({
        no: lastNo + 1,
        task_activity: '',
        periode: '',
        standart: '',
        pic: '',
        date: '',
        remark: '',
        id_picture: this.idParam,
        isUpdated: false
      });
    }
  }
  
  insertOrUpdateAutonomous(data: any) {
    const dataAutonomous = {
      no: data.no,
      task_activity: data.task_activity,
      periode: data.periode,
      standart: data.standart,
      pic: data.pic,
      date: data.date,
      remark: data.remark,
      id_picture: this.idParam
    };
  
    return new Promise<void>((resolve, reject) => {
      if (data.id) {
        this.RestApiService.updateAutonomous(dataAutonomous, data.id).subscribe(
          (res: any) => {
            if (res.status === true) {
              data.isUpdated = true;
              resolve();
            } else {
              reject('Update failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      } else {
        this.RestApiService.insertAutonomous(dataAutonomous).subscribe(
          (res: any) => {
            if (res.status === true) {
              data.id_autonomous = res.data[0];
              data.isUpdated = true;
              resolve();
            } else {
              reject('Insert failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      }
    });
  }
  
  async onSubmit() {
    const updatePromises = this.autonomous.map(data => {
      if (data.id) {
        return this.insertOrUpdateAutonomous(data);
      } else {
        return Promise.resolve();
      }
    });
    await Promise.all(updatePromises);

    const lastData = this.autonomous[this.autonomous.length - 1];
    if (!lastData.id) {
      await this.insertOrUpdateAutonomous(lastData);
    }
    
    this.router.navigate(['maintenance/autonom-maintenance'], { queryParams: { lineId: this.idLine, areaId: this.idArea } });
    Swal.fire({
      icon: 'success',
      title: 'Successfully!',
      text: 'Data has been successfully added.',
    });
  }

isDataAutonomousChange(id: any) {
  const data = this.autonomous.find((item: any, index: number) => index === id);
  return (
    data &&
    (data.no !== this.newAutonomous.no ||
      data.task_activity !== this.newAutonomous.task_activity ||
      data.periode !== this.newAutonomous.periode ||
      data.standart !== this.newAutonomous.standart ||
      data.pic !== this.newAutonomous.pic ||
      data.date !== this.newAutonomous.date ||
      data.remark !== this.newAutonomous.remark)
  );
}

  getPictureById(id:any){
    this.RestApiService.getPictureById(id).subscribe(
    (res:any) => {
      this.dataPicture = res.data;
      this.convertDateFields(this.dataPicture);
    })
  }

  getAutonomousByPictureId(id:any){
    this.RestApiService.getAutonomousByPictureId(id).subscribe(
    (res:any) => {
      this.autonomous = res.data;
      this.convertDateFields(this.autonomous);
    })
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  convertDateFields(data: any[]) {
    if (data) {
      for (let item of data) {
        if (item.date) {
          item.date = this.convertDate(item.date);
        }
      }
    }
  }

  forBack() {
    if (this.autonomous.length === 0) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Add autonomous data first!',
      });
    } else {
      this.router.navigate(['maintenance/autonom-maintenance'], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
    }
  }
}