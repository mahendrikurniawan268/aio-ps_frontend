import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutonomMaintenanceComponent } from './autonom-maintenance.component';

describe('AutonomMaintenanceComponent', () => {
  let component: AutonomMaintenanceComponent;
  let fixture: ComponentFixture<AutonomMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutonomMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutonomMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
