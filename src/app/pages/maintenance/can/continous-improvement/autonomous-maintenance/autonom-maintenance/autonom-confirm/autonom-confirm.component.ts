import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-autonom-confirm',
  templateUrl: './autonom-confirm.component.html',
  styleUrls: ['./autonom-confirm.component.scss']
})
export class AutonomConfirmComponent implements OnInit {
  autonomous:any;

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  id_picture:any

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private activeModal: NgbActiveModal,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.getAutonomousId(this.id_picture);

    this.breadCrumbItems = [{
        label: 'Forms'
      },
      {
        label: 'Form Layout',
        active: true
      }
    ];
  }

  updateStatus(index: number) {
    const dataToUpdate = this.autonomous[index];
    const updatedStatus = 3;

    const updatedData = {
      id_status: updatedStatus
    };

    this.RestApiService.updateAutonomous(updatedData, dataToUpdate.id).subscribe(
    (res:any) => {
      if (res.status == true) {
        this.autonomous[index].id_status = updatedStatus;
        
      }
    },
    (error) => {
      console.error(error)
    })
  }

  onCompleted(index: number) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Is The Task Finished?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        this.updateStatus(index);
        this.modalService.dismissAll();
        window.location.reload();
      }
    });
  }

  getAutonomousId(id:any){
    this.RestApiService.getAutonomousByPictureId(id).subscribe(
      (res:any) => {
        this.autonomous = res.data
        this.convertDateFields(this.autonomous);
      }
    )
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  convertDateFields(data: any[]) {
    if (data) {
      for (let item of data) {
        if (item.date) {
          item.date = this.convertDate(item.date);
        }
      }
    }
  }

  onClose(){
    this.activeModal.close();
  }
}