import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { Picture } from '../picture.model';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';
import { AutonomConfirmComponent } from './autonom-confirm/autonom-confirm.component';

@Component({
  selector: 'app-autonom-maintenance',
  templateUrl: './autonom-maintenance.component.html',
  styleUrls: ['./autonom-maintenance.component.scss']
})
export class AutonomMaintenanceComponent implements OnInit{
  tableColumns = ['No', 'Picture', 'Area', 'No', 'Task Activity', 'Period', 'Standart', 'PIC', 'Date', 'Remark', 'Details'];
  titlePage = 'Autonomous List';
  index: number = 0;
  dataAutonomous: any[] = [];
  dataByLine : any [] = [];
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  idLine: any;
  idArea:any;

  newPicture : Picture ={
  id_line: null,
  id_area: null,
  id_machine: null, 
  picture1: '',
  picture2: '',
  picture3: '',
  picture4: '',
  picture5: '',
  }

  currentPage = 1;
  itemsPerPage = 10;

  constructor(private router : Router, private RestApiService:RestApiService, 
  private modalService:NgbModal, private authService: AuthenticationService, private lightbox:Lightbox,
  private route:ActivatedRoute){}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
      this.getAll();
    }})
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  openPicture1(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Picture 1',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openPicture2(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Picture 2',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openPicture3(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Picture 3',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openPicture4(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Picture 4',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openPicture5(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Picture 5',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImgFilePicture1(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture2(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture3(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture4(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFilePicture5(file: any) {
    return environment.API_URL + '/image/' + file
  }
  

  getAll(){
    this.RestApiService.getAllAutonomousMaintenance().subscribe(
      (res:any) => {
        this.dataAutonomous = res.data[0];
        this.dataByLine = this.dataAutonomous.filter(item => item.id_line == this.idLine && item.id_area == this.idArea)
        this.preData = [...this.dataByLine];
        this.paginateData(); 
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  addData(){
    this.router.navigate(['maintenance/autonom-maintenance/add-picture'], {queryParams: {lineId: this.idLine, areaId: this.idArea}})
  }

  editData(id:any){
    this.router.navigate([`maintenance/autonom-maintenance/add-data/${id}`], {queryParams: {lineId: this.idLine, areaId: this.idArea}})
  }

  updatePicture(dataPicture: any, id:any) {
    this.RestApiService.updatePicture(id, dataPicture).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newPicture = dataPicture;
          this.getAll()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updatePicture(deleteData, id);
      }
    });
  }

  search() {
    this.dataByLine = this.preData.filter((item: { machine: string; task_activity: string; date: string; }) => {
      const month = this.extractMonthFromDate(item.date);
      return (month && month.toLowerCase().includes(this.searchText.toLowerCase())) ||
             item.machine.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.task_activity.toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataByLine.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    let items:any = []
    this.dataByLine = this.preData.filter((item: {area: string; task_activity: string; date: string;}) => {
      if(item.date.includes('\n')) {
        items.push(...item.date.split('\n'));
      } else {
        items.push(item.date);
      }
      const itemDate = new Date(item.date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;
      
      if(item.date.includes('\n')) {
        let multi_date = item.date.split('\n').map(date => new Date(date));
        let result = false;
        for (let single_date of multi_date) {
          if((!startFilterDate || single_date >= startFilterDate) && 
          (!endFilterDate || single_date <= endFilterDate)){
            result =  true;
          }
        }
        return result
      } else {
        return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
      }
      
    });
    this.paginateData();
  }

  openPopup(id: any) {
    const modalOptions: NgbModalOptions = {
      centered: true,
      size: 'lg', 
    };
  
    const modalRef = this.modalService.open(AutonomConfirmComponent, modalOptions);
    modalRef.componentInstance.id_picture = id;
    modalRef.result.then(
      (result) => {
        
      },
      (reason) => {
        
      }
    );
  }
}