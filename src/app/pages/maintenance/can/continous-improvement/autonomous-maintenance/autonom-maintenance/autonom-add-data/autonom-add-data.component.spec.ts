import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutonomAddDataComponent } from './autonom-add-data.component';

describe('AutonomAddDataComponent', () => {
  let component: AutonomAddDataComponent;
  let fixture: ComponentFixture<AutonomAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutonomAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutonomAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
