import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Picture } from '../../picture.model';

@Component({
  selector: 'app-automon-add-picture',
  templateUrl: './automon-add-picture.component.html',
  styleUrls: ['./automon-add-picture.component.scss']
})
export class AutomonAddPictureComponent implements OnInit{

  public picture:any;
  public autonomous:any;
  id_picture:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  dataPicture:any;
  areaData: any[] = [];
  idParam:any;
  idArea: any;
  idLine: any;
  dataMachine: any [] = [];
  machine: any;

  newPicture : Picture ={
    id_line: null,
    id_area: null,
    id_machine: null, 
    picture1: '',
    picture2: '',
    picture3: '',
    picture4: '',
    picture5: ''
  }

  constructor(private router:Router, private RestApiService:RestApiService, private route:ActivatedRoute) {}

  ngOnInit(): void {
    this.getMachine()
    this.idParam = this.route.snapshot.paramMap.get('id');
    if (this.idParam) {
      this.getPictureById(this.idParam);
    }
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
  }

  getPictureById(id:any){
    this.RestApiService.getPictureById(id).subscribe(
    (res:any) => {
      this.newPicture = res.data[0];
    })
  }

  insertPicture(dataPicture: any) {
  
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
  
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataPicture.picture1 = res.uploadedFiles[0];
          dataPicture.picture2 = res.uploadedFiles[1];
          dataPicture.picture3 = res.uploadedFiles[2];
          dataPicture.picture4 = res.uploadedFiles[3];
          dataPicture.picture5 = res.uploadedFiles[4];
  
          // Setelah berhasil mengunggah file, kirim data report ke server
          this.RestApiService.insertPicture(dataPicture).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_picture = res.data[0];
                this.picture = dataPicture;
                this.picture.id = this.id_picture;
                this.router.navigate([`maintenance/autonom-maintenance/add-data/${this.id_picture}`], {queryParams: {lineId: this.idLine, areaId: this.idArea}})
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.RestApiService.insertPicture(dataPicture).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_picture = res.data[0];
            this.picture = dataPicture;
            this.picture.id = this.id_picture;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  } 

  onInsertData() {
    this.newPicture.id_line = this.idLine
    this.newPicture.id_area = this.idArea
    this.insertPicture(this.newPicture);
  }

  updateDataPicture(dataPicture: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
        formData.append('files', fileData.file, fileData.file.name);
        formData.append('type', fileData.type);
      });

      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;

          const picture1FileIndex = this.selectedFiles.findIndex(file => file.type === 'picture-1');
          const picture2FileIndex = this.selectedFiles.findIndex(file => file.type === 'picture-2');
          const picture3FileIndex = this.selectedFiles.findIndex(file => file.type === 'picture-3');
          const picture4FileIndex = this.selectedFiles.findIndex(file => file.type === 'picture-4');
          const picture5FileIndex = this.selectedFiles.findIndex(file => file.type === 'picture-5');

          if (picture1FileIndex !== -1) {
            dataPicture.picture1 = res.uploadedFiles[picture1FileIndex];
          }
          if (picture2FileIndex !== -1) {
            dataPicture.picture2 = res.uploadedFiles[picture2FileIndex];
          }
          if (picture3FileIndex !== -1) {
            dataPicture.picture3 = res.uploadedFiles[picture3FileIndex];
          }
          if (picture4FileIndex !== -1) {
            dataPicture.picture4 = res.uploadedFiles[picture4FileIndex];
          }
          if (picture5FileIndex !== -1) {
            dataPicture.picture5 = res.uploadedFiles[picture5FileIndex];
          }
  
          this.RestApiService.updatePicture(this.idParam, dataPicture).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.newPicture = dataPicture;
                this.getPictureById(this.idParam);
              }
            },
            (error) => {
              console.error(error)
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.RestApiService.updatePicture(this.idParam, dataPicture).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.newPicture = dataPicture;
            this.getPictureById(this.idParam);
          }
        },
        (error) => {
          console.error(error)
        }
      );
    }
  }   

  onUpdate() {
      this.updateDataPicture(this.newPicture);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
  }
  
  picture1: any
  picture2: any
  picture3: any
  picture4: any

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    if (type == 'picture-1') this.picture1 = file
    if (type == 'picture-2') this.picture2 = file
    if (type == 'picture-3') this.picture3 = file
    if (type == 'picture-4') this.picture4 = file
    this.selectedFiles.push({ file, type });
  }

  forBack() {
    this.router.navigate([`maintenance/autonom-maintenance/add-data/${this.idParam}`], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
  }

  getMachine(){
    this.RestApiService.getAllMachine().subscribe(
      (res: any) => {
        this.dataMachine = res.data;
        this.machine = this.dataMachine.filter(item => item.id_line == this.idLine && item.id_area == this.idArea)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }
}