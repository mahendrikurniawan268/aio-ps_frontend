import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutonomConfirmComponent } from './autonom-confirm.component';

describe('AutonomConfirmComponent', () => {
  let component: AutonomConfirmComponent;
  let fixture: ComponentFixture<AutonomConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutonomConfirmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AutonomConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
