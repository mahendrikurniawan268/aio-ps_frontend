export interface WeeklyCare {
  line: string,
  area: string,
  description: string,
  status_activity: string,
  remark: string,
  result: string,
  genba: string,
  due_date: string,
  category: string,
  sub_category:string,
  documentation_before: File | null,
  documentation_after: File | null;
  }