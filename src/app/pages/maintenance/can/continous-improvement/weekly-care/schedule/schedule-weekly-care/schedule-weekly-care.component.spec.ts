import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleWeeklyCareComponent } from './schedule-weekly-care.component';

describe('ScheduleWeeklyCareComponent', () => {
  let component: ScheduleWeeklyCareComponent;
  let fixture: ComponentFixture<ScheduleWeeklyCareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleWeeklyCareComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleWeeklyCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
