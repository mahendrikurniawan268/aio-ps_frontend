import { Component, OnInit, ViewChild, TemplateRef, AfterViewInit} from '@angular/core';
import { ElementRef } from '@angular/core';

// Calendar option
import { CalendarOptions, EventClickArg, EventApi } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import { EventInput } from '@fullcalendar/core';

// BootStrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
// Sweet Alert
import Swal from 'sweetalert2';

// Calendar Services
import { RestApiService } from "../../../../../../../core/services/rest-api.service";
import { SheduleWeekly } from './schedule-weekly-model';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-schedule-weekly-care',
  templateUrl: './schedule-weekly-care.component.html',
  styleUrls: ['./schedule-weekly-care.component.scss']
})
export class ScheduleWeeklyCareComponent implements OnInit {
  tableColumns = ['No', 'Area', 'Description', 'Date', 'Pic', 'Status'];
  public schedule_weekly: any = {};

  newScheduleWeekly: SheduleWeekly = {
    description: '',
    pic: '',
    date: '',
    id_area: null,
    id_line: null
  };

  id_schedule_weekly:any;
  dataScheduleWeekly : any [] = [];
  dataScheduleWeeklyCan: any;
  tabelView: any [] = [];
  tabelViewByLine: any;
  typeData : any[] = [];
  dataSchedule: any
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  namePic: any [] = [];
  picEmployee:any;

  // calendar
  calendarEvents : EventInput[] = [];
  editEvent: any;
  formEditData!: UntypedFormGroup;
  newEventDate: any;
  category!: any[];
  submitted = false;
  idLine:any;
  dataArea: any [] = [];
  area:any;
  isTableView: boolean = false;

  currentPage = 1;
  itemsPerPage = 10;
  dataForCurrentPage :any [] = [];

  // Calendar click Event
  formData!: UntypedFormGroup;
  @ViewChild('editmodalShow') editmodalShow!: TemplateRef<any>;
  @ViewChild('modalShow') modalShow !: TemplateRef<any>;

  constructor(private modalService: NgbModal, private formBuilder: UntypedFormBuilder, private router : Router,
    private route: ActivatedRoute, private RestApiService: RestApiService, private authService:AuthenticationService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getArea();
    this.getNamePic();
    this.getTabelViewScheduleWeekly();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();

    // Validation
    this.formData = this.formBuilder.group({
      title: ['', [Validators.required]],
      category: ['', [Validators.required]],
      location: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required]
    });

    this._fetchData();
  }
  private _fetchData() {
    this.getAllScheduleWeekly();
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id == 'btnCalendar') {
      this.isTableView = false
      this.router.navigate(["continous/weekly-care/schedule"], {
        queryParams: { tableCalendar: false, lineId: this.idLine },
      });
    } else if (event.target.id == 'btnList') {
      this.isTableView = true
      this.router.navigate(["continous/weekly-care/schedule"], {
        queryParams: { tableList: true, lineId: this.idLine },
      });
    }
  }

  /***
  * Calender Set
  */
  calendarOptions: CalendarOptions = {
    plugins: [
      interactionPlugin,
      dayGridPlugin,
      timeGridPlugin,
      listPlugin,
    ],
    headerToolbar: {
      left: 'dayGridMonth,dayGridWeek,dayGridDay',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: this.calendarEvents || this.calendarEvents,
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.openModal.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    eventDidMount: this.addIconToEvent.bind(this),
  };
  currentEvents: EventApi[] = [];

  /**
   * Event add modal
   */
  openModal(event?: any) {
    this.submitted = false;
    this.newEventDate = event,
      this.formBuilder.group({
        editDate: this.newEventDate.date
      })
    this.modalService.open(this.modalShow, { centered: true });
  }

  addIconToEvent(info:any): void {
    const event = info.event;
    if (event.extendedProps && event.extendedProps.follow_up === 1) {
      const iconElement = document.createElement('i');
      iconElement.className = 'bi bi-check-circle text-white';
      iconElement.style.fontSize = '15px';
      iconElement.style.marginRight = '10px';
      const eventElement: HTMLElement = info.el as HTMLElement;
      eventElement.appendChild(iconElement);
    }
  }

  /**
   * Event click modal show
   */
  handleEventClick(clickInfo: EventClickArg) {
    this.editEvent = clickInfo.event._def.publicId;
    this.formEditData = this.formBuilder.group({
      id: clickInfo.event.id,
      description: clickInfo.event.title,
      pic: clickInfo.event._def.extendedProps['pic'],
      id_area: clickInfo.event._def.extendedProps['id_area'],
      date: clickInfo.event.start,
    });
    console.log(this.formEditData);
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  /**
   * Events bind in calander
   * @param events events
   */
  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
    // this.changeDetector.detectChanges();
  }

  /**
   * Close event modal
   */
  closeEventModal() {
  this.modalService.dismissAll();
  }

  /**
   * Event Data Get
   */
  get form() {
    return this.formData.controls;
  }

  deleteEventData() {
    this.editEvent.remove();
    this.modalService.dismissAll();
  }

  insertScheduleWeekly(dataScheduleWeekly: any) {
    this.RestApiService.insertScheduleWeekly(dataScheduleWeekly).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.id_schedule_weekly = res.data[0];
          this.schedule_weekly = dataScheduleWeekly;
          this.schedule_weekly.id = this.id_schedule_weekly;

          const eventToAdd = {
            title: this.schedule_weekly.description,
            date: this.schedule_weekly.date,
            id_area: this.schedule_weekly.id_area,
            pic: this.schedule_weekly.pic,
            description: this.schedule_weekly.description,
          };
          this.calendarEvents.push(eventToAdd);
          this.calendarOptions.initialEvents = this.calendarEvents;
          
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateSchedule(dataSchedule: any, id:any) {
    let publicId = this.editEvent
    this.RestApiService.updateScheduleWeekly(publicId, dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.schedule_weekly = dataSchedule;
          this.schedule_weekly.id = this.id_schedule_weekly
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  getAllScheduleWeekly(){
    this.RestApiService.getAllScheduleWeekly().subscribe(
      (res: any) => {
        this.dataScheduleWeekly = res.data;
        this.dataScheduleWeeklyCan = this.dataScheduleWeekly.filter(item => item.id_line == this.idLine)
        this.calendarEvents = this. transformDataScheduleWeekly(this.dataScheduleWeeklyCan);
        this.calendarOptions.events = this.calendarEvents;
        this.dataScheduleWeeklyCan.date = this.convertDate(this.newScheduleWeekly.date);
      },
      (error: any) => {
        console.error(error);
      }
    );
  }
  
  getTabelViewScheduleWeekly(){
    this.RestApiService.getTabelViewScheduleWeekly().subscribe(
      (res: any) => {
        this.tabelView = res.data;
        this.tabelViewByLine = this.tabelView.filter(item => item.id_line == this.idLine);
        this.checkAndSetLateStatus();
        this.paginateData();
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getArea(){
    this.RestApiService.getAllArea().subscribe(
      (res: any) => {
        this.dataArea = res.data;
        this.area = this.dataArea.filter(item => item.id_line == this.idLine)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  transformDataScheduleWeekly(data: any[]): any[] {
    const calendarEvents: any[] = [];
    for (const item of data) {
      const event = {
        id: item.id,
        title: item.description,
        date: item.date,
        id_area: item.id_area,
        pic: item.pic,
        status: item.status,
        follow_up: item.follow_up
      };
      calendarEvents.push(event);
    }
    return calendarEvents;
  }

  onReport(id_area: number) {
    if (id_area === 1) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 1, areaId: 1 } });
    } else if (id_area === 2) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 1, areaId: 2 } });
    } else if (id_area === 3) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 1, areaId: 3 } });
    } else if (id_area === 4) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 2, areaId: 4 } });
    } else if (id_area === 5) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 2, areaId: 5 } });
    } else if (id_area === 6) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 2, areaId: 6 } });
    } else if (id_area === 7) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 2, areaId: 7 } });
    } else if (id_area === 8) {
        this.router.navigate(['maintenance/weekly-care'], { queryParams: { lineId: 2, areaId: 8 } });
    } else {
    }
    this.modalService.dismissAll();
}
  
  onClick() {
    this.newScheduleWeekly.id_line = this.idLine
    this.insertScheduleWeekly(this.newScheduleWeekly);
    console.log(this.newScheduleWeekly)
    this.modalService.dismissAll()
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!!',
      icon: 'success',
      timer: 1500, 
      showConfirmButton: false, 
    }).then(() => {
      setTimeout(() => {
        window.location.reload();
      },);
    });

    this.newScheduleWeekly = {
      description: '',
      pic: '',
      date: '',
      id_area: null,
      id_line: null
    };
    this.getAllScheduleWeekly()
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateSchedule(deleteData, id);
        this.modalService.dismissAll()
        window.location.reload()
      }
    });
  }

  updateData() {
    const originalEventDate = this.editEvent.start;
    this.formEditData.patchValue({ date: originalEventDate });
    this.updateSchedule(this.formEditData.value, this.editEvent);
    this.modalService.dismissAll();
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    }).then(() => {
      setTimeout(() => {
        window.location.reload();
      },);
    });
  }

  onCompleted(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Is The Task Finished?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { follow_up: 1 };
        this.updateSchedule(deleteData, id);
        this.modalService.dismissAll()
        window.location.reload()
      }
    });
  }

  checkAndSetLateStatus() {
    const currentDate = new Date(); 
    for (const data of this.tabelView) {
      if (data.follow_up === 0) { 
        const scheduledDate = new Date(data.date); 
        scheduledDate.setDate(scheduledDate.getDate() + 2);
        if (scheduledDate < currentDate) {
          data.follow_up = 2;
  
          this.RestApiService.updateScheduleWeekly(data.id, { follow_up: 2}).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.getTabelViewScheduleWeekly();
              }
            },
            (error) => {
              console.error(error);
            }
          );
        }
      }
    }
  }

  getStatusLabelClass(follow_up: number): string {
    switch (follow_up) {
        case 0:
            return 'success'; // New
        case 1:
            return 'secondary'; // On Progress
        case 2:
            return 'danger'; // Done
        case 3:
            return 'danger'; // Late
        case 4:
          return 'primary'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(follow_up: number): string {
    switch (follow_up) {
        case 0:
            return 'New';
        case 1:
            return 'Done';
        case 2:
            return 'Pending';
        case 3:
            return 'Pending';
        case 4:
          return 'Revisi';
        default:
            return 'Unknown';
    }
}
  
  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.tabelViewByLine.slice(startIndex, endIndex);
  }
  
  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }
}

