export interface SheduleWeekly {
    description: string;
    pic: string;
    date: string;
    id_area: null | number;
    id_line: null | number;
  }