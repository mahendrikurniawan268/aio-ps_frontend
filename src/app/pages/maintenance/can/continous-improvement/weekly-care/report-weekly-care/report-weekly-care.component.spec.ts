import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportWeeklyCareComponent } from './report-weekly-care.component';

describe('ReportWeeklyCareComponent', () => {
  let component: ReportWeeklyCareComponent;
  let fixture: ComponentFixture<ReportWeeklyCareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportWeeklyCareComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportWeeklyCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
