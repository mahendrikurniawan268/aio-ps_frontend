import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-report-weekly-care',
  templateUrl: './report-weekly-care.component.html',
  styleUrls: ['./report-weekly-care.component.scss']
})
export class ReportWeeklyCareComponent implements OnInit{

  dataWeeklyCare:any [] = [];
  filterData:any [] = [];
  startDate: string = '';
  endDate: string = '';
  preData:any;
  simplePieChart: any;
  AllData: number = 0
  countOpenData: number = 0
  countCloseData: number = 0
  idLine:any;
  idArea: any;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
    if (history.state.dataWeeklyCare) {
      this.dataWeeklyCare = history.state.dataWeeklyCare;
      this.preData = [...this.dataWeeklyCare]
      console.log(this.dataWeeklyCare);
    } else {
    }
  }

  _simplePieChart(colors:any) {
    colors = this.getChartColorsArray(colors);
    this.simplePieChart = {
      series: [this.countCloseData, this.countOpenData],
      chart: {
        height: 400,
        type: "pie",
      },
      labels: ["Close", "Open"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
      colors: colors,
    };
  }  

  private getChartColorsArray(colors: any) {
    colors = JSON.parse(colors);
    return colors.map(function (value: any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
        if (color) {
          color = color.replace(" ", "");
          return color;
        }
        else return newValue;;
      } else {
        var val = value.split(',');
        if (val.length == 2) {
          var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
          rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
          return rgbaColor;
        } else {
          return newValue;
        }
      }
    });
  }

  filter() {
    this.filterData = this.preData.filter((item: { area: string; description: string; due_date: string; }) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (endFilterDate) {
        endFilterDate.setDate(endFilterDate.getDate() + 1);
      }else if(startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    console.log(this.filterData);
    
    this.AllData = this.filterData.length
    this.countOpenData = this.filterData.filter(item => !item.documentation_after).length
    this.countCloseData = this.filterData.filter(item => item.documentation_after).length
    this._simplePieChart('["#4F709C", "#E5D283", "--vz-warning", "--vz-danger", "--vz-info"]');
  }

  forBack() {
    this.router.navigate(['maintenance/weekly-care'], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
  }
}
