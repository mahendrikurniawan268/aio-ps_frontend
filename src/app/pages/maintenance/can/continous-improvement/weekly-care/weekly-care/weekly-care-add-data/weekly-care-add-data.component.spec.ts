import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyCareAddDataComponent } from './weekly-care-add-data.component';

describe('WeeklyCareAddDataComponent', () => {
  let component: WeeklyCareAddDataComponent;
  let fixture: ComponentFixture<WeeklyCareAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeeklyCareAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WeeklyCareAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
