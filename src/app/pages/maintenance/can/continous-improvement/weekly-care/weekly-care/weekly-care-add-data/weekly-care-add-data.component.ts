import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Weekly } from '../../weekly.model';

@Component({
  selector: 'app-weekly-care-add-data',
  templateUrl: './weekly-care-add-data.component.html',
  styleUrls: ['./weekly-care-add-data.component.scss']
})
export class WeeklyCareAddDataComponent implements OnInit{
  
  public weekly_care:any;
  id_weekly_care:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  dataWeeklyCare:any;
  namePic:any [] = [];
  picEmployee: any;
  dataMachine: any [] = [];
  machine: any;
  idLine: any;
  idArea: any;
  statusOptions = ['Open', 'Close'];
  statusSearchKeyword: string = '';

  newWeeklyCare : Weekly = {
    id_line: null,
    id_area: null,
    id_machine: null,
    description: '',
    status_activity: '',
    remark: '',
    result: '',
    id_genba: null,
    due_date: '',
    documentation_before: null,
    documentation_after: null,
  }

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private http: HttpClient,
    private route:ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getNamePic();
    this.getMachine();
    this.route.params.subscribe((params) => {
      const idToEdit = params['id'];
      this.id_weekly_care = idToEdit
      if (idToEdit) {
        this.getWeeklyCareById(idToEdit);
      }
    });
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
    }})
  }

  onStatusSelect(selectedItem: any) {
    this.newWeeklyCare.status_activity = selectedItem;
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 3 || item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getMachine(){
    this.RestApiService.getAllMachine().subscribe(
      (res: any) => {
        this.dataMachine = res.data;
        this.machine = this.dataMachine.filter(item => item.id_line == this.idLine && item.id_area == this.idArea)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getWeeklyCareById(id:any){
    this.RestApiService.getWeeklyById(id).subscribe(
    (res:any) => {
      this.newWeeklyCare = res.data[0]
      this.newWeeklyCare.due_date = this.convertDate(this.newWeeklyCare.due_date);
    })
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  insertWeeklyCare(dataWeeklyCare: any) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataWeeklyCare.documentation_before = res.uploadedFiles[0];
          dataWeeklyCare.documentation_after = res.uploadedFiles[1];
          
          this.newWeeklyCare.id_line = this.idLine
          this.newWeeklyCare.id_area = this.idArea
          this.RestApiService.insertWeekly(dataWeeklyCare).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_weekly_care = res.data[0];
                this.weekly_care = dataWeeklyCare;
                this.weekly_care.id_weekly_care = this.id_weekly_care;
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.RestApiService.insertWeekly(dataWeeklyCare).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_weekly_care = res.data[0];
            this.weekly_care = dataWeeklyCare;
            this.weekly_care.id_report_master = this.id_weekly_care;
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }  
  
  onInsertData() {
    if (this.selectedFiles.length > 0) {
        this.insertWeeklyCare(this.newWeeklyCare);
        Swal.fire({
            title: 'Data Added',
            text: 'Data has been successfully added.',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then((result) => {
            if (result.isConfirmed) {
                this.router.navigate(['maintenance/weekly-care'], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
            }
        });
    } else {
        Swal.fire({
            title: 'No Files Selected',
            text: 'Please select at least one file before adding data.',
            icon: 'warning',
            confirmButtonText: 'OK',
        });
    }
}

  forBack() {
    this.router.navigate(['maintenance/weekly-care'], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  updateWeeklySugar(dataWeeklyCare: any) {
    if (this.selectedFiles.length > 0) {
        const formData = new FormData();
        this.selectedFiles.forEach(fileData => {
            formData.append('files', fileData.file, fileData.file.name);
            formData.append('type', fileData.type);
        });

        this.RestApiService.uploadMultipleImage(formData).subscribe({
            next: (res: any) => {
                this.imageUrl = res.uploadedFiles;
                const beforeFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-before');
                const afterFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-after');

                if (beforeFileIndex !== -1) {
                    dataWeeklyCare.documentation_before = res.uploadedFiles[beforeFileIndex];
                }
                if (afterFileIndex !== -1) {
                    dataWeeklyCare.documentation_after = res.uploadedFiles[afterFileIndex];
                }

                this.RestApiService.updateWeekly(this.id_weekly_care, dataWeeklyCare).subscribe(
                    (res: any) => {
                        if (res.status == true) {
                            this.newWeeklyCare = dataWeeklyCare;
                        }
                    },
                    (error) => {
                        console.error(error)
                    }
                );
            },
            error: (err) => console.error(err),
            complete: () => {
                this.selectedFiles = [];
            }
        });
    } else {
        this.RestApiService.updateWeekly(this.id_weekly_care, dataWeeklyCare).subscribe(
            (res: any) => {
                if (res.status == true) {
                    this.newWeeklyCare = dataWeeklyCare;
                }
            },
            (error) => {
                console.error(error)
            }
        );
      }
  }

  onUpdate() {
      this.updateWeeklySugar(this.newWeeklyCare);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
  }
}
