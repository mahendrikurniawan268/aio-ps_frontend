import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';
import { Weekly } from '../weekly.model';

@Component({
  selector: 'app-weekly-care',
  templateUrl: './weekly-care.component.html',
  styleUrls: ['./weekly-care.component.scss']
})
export class WeeklyCareComponent implements OnInit{
  tableColumns = ['No', 'Line', 'Area', 'Description', 'Status', 'Before', 'Remark', 'After', 'Result', 'Genba', 'Date', 'Details'];
  titlePage = 'Weekly Care List';
  index: number = 0;
  dataWeeklyCare: any[] = [];
  dataWeeklyCareByLine:any [] = [];
  id_weekly_care:any;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  startDate: string = '';
  endDate: string = '';
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  idLine:any;
  idArea:any;

  newWeeklyCare : Weekly = {
    id_line: null,
    id_area: null,
    id_machine: null,
    description: '',
    status_activity: '',
    remark: '',
    result: '',
    id_genba: null,
    due_date: '',
    documentation_before: null,
    documentation_after: null,
  }

  currentPage = 1;
  itemsPerPage = 10;

  constructor(private router:Router, private RestApiService:RestApiService, private authService:AuthenticationService, private lightbox : Lightbox,
    private route: ActivatedRoute){}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idArea = params['areaId'];
      this.getWeeklyCare();
    }})
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  openLightboxBefore(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation Before',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  openLightboxAfter(imageSrc: string): void {
    const image = [{
      src: imageSrc,
      caption: 'Documentation After',
      thumb: '' 
    }];
    this.lightbox.open(image, 0, {
      showDownloadButton: true,
      showZoom: true
    });
  }

  getImgFileBefore(file: any) {
    return environment.API_URL + '/image/' + file
  }

  getImgFileAfter(file: any) {
    return environment.API_URL + '/image/' + file
  }

  reportWeeklyCare(){
    this.router.navigate(['report/weekly-care'], {queryParams: {lineId: this.idLine, areaId: this.idArea}, state: { dataWeeklyCare: this.dataWeeklyCareByLine } });
    }

  onAddData(){
    this.router.navigate(['maintenance/weekly-care/add-data'], {queryParams: {lineId: this.idLine, areaId: this.idArea}})
  }

  onEditData(id:any) {
    this.router.navigate([`maintenance/weekly-care/edit-data/${id}`], {queryParams: {lineId: this.idLine, areaId: this.idArea}});
  }

  getWeeklyCare(){
    this.RestApiService.getAllWeeklyCare().subscribe(
      (res:any) => {
        this.dataWeeklyCare = res.data[0];
        this.dataWeeklyCareByLine = this.dataWeeklyCare.filter(item => item.id_line == this.idLine && item.id_area == this.idArea)
        console.log(this.dataWeeklyCareByLine);
        
        this.preData = [...this.dataWeeklyCareByLine];
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateWeekly(dataWeeklyCareByLine: any, id:any) {
    this.RestApiService.updateWeekly(id, dataWeeklyCareByLine).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.newWeeklyCare = dataWeeklyCareByLine;
          this.getWeeklyCare()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateWeekly(deleteData, id);
      }
    });
  }

  search() {
    this.dataWeeklyCareByLine = this.preData.filter((item: { machine: string; description: string; due_date: string; status_activity: string;}) => {
      return item.machine.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.description.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.status_activity.toLowerCase().includes(this.searchText.toLowerCase()) ||
             this.extractMonthFromDate(item.due_date).toLowerCase().includes(this.searchText.toLowerCase());
    });
    this.paginateData();
  }

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataWeeklyCareByLine.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataWeeklyCareByLine = this.preData.filter((item: { area: string; description: string; due_date: string; }) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
    this.paginateData();
  }
}