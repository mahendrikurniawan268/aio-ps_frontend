export interface Weekly {
    id_line: null | number,
    id_area: null | number,
    id_machine: null | number;
    description: string,
    status_activity: string,
    remark: string,
    result: string,
    id_genba: null | number,
    due_date: string,
    documentation_before: File | null,
    documentation_after: File | null;
    }