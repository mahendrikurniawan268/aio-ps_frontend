import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinousImprovementEditDataComponent } from './continous-improvement-edit-data.component';

describe('ContinousImprovementEditDataComponent', () => {
  let component: ContinousImprovementEditDataComponent;
  let fixture: ComponentFixture<ContinousImprovementEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContinousImprovementEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContinousImprovementEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
