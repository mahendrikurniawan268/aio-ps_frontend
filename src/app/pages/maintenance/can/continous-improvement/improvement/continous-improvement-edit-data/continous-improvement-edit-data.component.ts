import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { SubTaskImprovement } from '../subtask.model';
import { TaskImrovement } from '../task.model';

@Component({
  selector: 'app-continous-improvement-edit-data',
  templateUrl: './continous-improvement-edit-data.component.html',
  styleUrls: ['./continous-improvement-edit-data.component.scss']
})
export class ContinousImprovementEditDataComponent implements OnInit{

  public subtask:any [] = [];
  public task: any = {};
  public idParam:any;
  public subtaskSaveData: boolean[] = [];
  statusData: any[] = [];
  taskById:any;
  dataTask:any[] = [];
  dataTaskCan:any [] = [];
  idLine:any;

  newTaskImprovement: TaskImrovement = {
    task: '',
    owner: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    id_line: null
  };

  newSubTaskImprovement: SubTaskImprovement = {
    subtask: '',
    id_status: null,
    due_date: '',
    start_date: '',
    end_date: '',
    notes: '',
    id_task: null,
  };

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.subtask.forEach((item: any) => {
      item.isUpdated = false;
    });
    this.getSubTaskByTaskId(this.idParam)
    this.getStatusSub()
    this.subtaskSaveData = new Array(this.subtask.length).fill(false);
  }
  
  forBack() {
    this.router.navigate(['continous/improvement'], { queryParams:{lineId: this.idLine}});
  }

  async addSubTask() {
    if (this.subtask.length === 0) {
      this.subtask.push({
        subtask: '',
        id_status: null,
        due_date: '',
        start_date: '',
        end_date: '',
        notes: '',
        id_task: this.idParam,
        isUpdated: false
      });
    } else {
      await Promise.all(this.subtask.map((data: any) => {
        if (!data.isUpdated) {
          return this.insertOrUpdateSubTask(data);
        } else {
          return Promise.resolve();
        }
      }));
      this.subtask.push({
        subtask: '',
        id_status: null,
        due_date: '',
        start_date: '',
        end_date: '',
        notes: '',
        id_task: this.idParam,
        isUpdated: false
      });
    }
  }
  
  insertOrUpdateSubTask(data: any) {
    const dataSubTask = {
      subtask: data.subtask,
      id_status: data.id_status,
      due_date: data.due_date,
      start_date: data.start_date,
      end_date: data.end_date,
      notes: data.notes,
      id_task: this.idParam
    };
  
    return new Promise<void>((resolve, reject) => {
      if (data.id) {
        this.RestApiService.updateSubTask(data.id, dataSubTask).subscribe(
          (res: any) => {
            if (res.status === true) {
              data.isUpdated = true;
              resolve();
            } else {
              reject('Update failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      } else {
        this.RestApiService.insertSubTaskImprovement(dataSubTask).subscribe(
          (res: any) => {
            if (res.status === true) {
              data.id = res.data[0];
              data.isUpdated = true;
              resolve();
            } else {
              reject('Insert failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      }
    });
  }
  
  async onSubmit() {
    this.subtask.sort((a, b) => a.index - b.index);
    await Promise.all(this.subtask.map((data: any) => this.insertOrUpdateSubTask(data)));
    await this.updateTaskStatusBasedOnSubtasks();
    Swal.fire({
      icon: 'success',
      title: 'Success',
      text: 'Data has been successfully submitted.',
      showCancelButton: false,
      confirmButtonText: 'OK',
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['continous/improvement'], { queryParams: { lineId: this.idLine } });
      }
    });
  }

  updateTaskStatusBasedOnSubtasks() {
    const subtasksWithStatus1 = this.subtask.some(subtask => subtask.id_status == 1);
    const subtasksWithStatus2 = this.subtask.some(subtask => subtask.id_status == 2);
    const allSubtasksWithStatus3 = this.subtask.every(subtask => subtask.id_status == 3);
    const subtasksWithStatus4 = this.subtask.some(subtask => subtask.id_status == 4);
  
    let newTaskStatus: number | null = null;
  
    if (subtasksWithStatus1) {
      newTaskStatus = 2;
    } else if (subtasksWithStatus2) {
      newTaskStatus = 2;
    } else if (allSubtasksWithStatus3) {
      newTaskStatus = 3;
    } else if (subtasksWithStatus4) {
      newTaskStatus = 2;
    }
    
    if (newTaskStatus !== null && this.task.id_status !== newTaskStatus) {
      this.task.id_status = newTaskStatus;
      this.RestApiService.updateTask(this.idParam, this.task).subscribe(
        (res: any) => {
          if (res.status === true) {
            this.getTask();
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
}

isDataSubtaskChange(id: any) {
  const data = this.subtask.find((item: any, index: number) => index === id);
  return (
    data &&
    (data.subtask !== this.newSubTaskImprovement.subtask ||
      data.id_status !== this.newSubTaskImprovement.id_status||
      data.due_date !== this.newSubTaskImprovement.due_date ||
      data.start_date !== this.newSubTaskImprovement.start_date||
      data.end_date !== this.newSubTaskImprovement.end_date ||
      data.notes !== this.newSubTaskImprovement.notes)
  );
}

updateTask(dataTaskCan: any, id:any) {
  this.RestApiService.updateTask(id, dataTaskCan).subscribe(
    (res: any) => {
      if (res.status == true) {
        console.log(this.newTaskImprovement)
        this.newTaskImprovement= dataTaskCan;
        this.getTask()
      }
    },
    (error) => {
      console.error(error)
    }
  )
} 

getTask(){
  this.RestApiService.getAllTaskImprovement().subscribe(
    (res:any) => {
      this.dataTask = res.data;
      this.dataTaskCan = this.dataTask.filter(item => item.line == this.idLine)
    },
    (error:any) => {
      console.error(error)
    }
  );
}

  getTaskImprovementById(id:any){
    this.RestApiService.getTaskImprovementById(id).subscribe(
    (res:any) => {
      this.taskById = res.data[0]
    })
  }

  getSubTaskByTaskId(id:any){
    this.RestApiService.getSubTaskByTaskId(id).subscribe(
    (res:any) => {
      this.subtask = res.data;
      this.convertDateFields(this.subtask);
    })
  }

  getStatusSub(){
    this.RestApiService.getStatusSub().subscribe(
      (res: any) => {
        this.statusData = res.data[0];
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  convertDateFields(data: any[]) {
    if (data) {
      for (let item of data) {
        if (item.due_date) {
          item.due_date = this.convertDate(item.due_date);
        }
        if (item.start_date) {
          item.start_date = this.convertDate(item.start_date);
        }
        if (item.end_date) {
          item.end_date = this.convertDate(item.end_date);
        }
      }
    }
  }  
}
