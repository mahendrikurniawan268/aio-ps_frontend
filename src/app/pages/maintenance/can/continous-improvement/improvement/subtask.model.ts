export interface SubTaskImprovement {
    subtask: string;
    id_status: number | null;
    due_date: string;
    start_date: string;
    end_date: string;
    notes: string;
    id_task: number | null;
}