export interface TaskImrovement {
    task: string;
    owner: string;
    due_date: string;
    id_status: number | null;
    id_priority: number | null;
    notes: string;
    id_line: null | number;
}