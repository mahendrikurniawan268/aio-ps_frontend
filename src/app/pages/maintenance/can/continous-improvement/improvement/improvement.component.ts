import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { SubTaskImprovement } from './subtask.model';
import { TaskImrovement } from './task.model';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-improvement',
  templateUrl: './improvement.component.html',
  styleUrls: ['./improvement.component.scss']
})
export class ImprovementComponent implements OnInit{
  tableColumns = ['Task', 'Owner', 'Due Date', 'Status', 'Priority', 'Notes', 'Details'];
  tableColumns2 = ['Subtask', 'Due Date', 'Status', 'Timeline', 'Notes', 'Details']
  titlePage = 'Improvement List';
  index: number = 0;
  dataTask:any[] = [];
  dataTaskByLine:any [] = [];
  dataSubTask:any[] = [];
  dataDatePriority: any [] = []
  dataSubtaskByTask: any;
  allData:any;
  public subtask:any = [];
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  preData:any;
  searchText:string = '';
  startDate: string = '';
  endDate: string = '';
  idLine:any;

  newTaskImprovement: TaskImrovement = {
    task: '',
    owner: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    id_line: null
  };

  newSubTaskImprovement: SubTaskImprovement = {
    subtask: '',
    id_status: null,
    due_date: '',
    start_date: '',
    end_date: '',
    notes: '',
    id_task: null,
  };

  constructor(private router : Router, private RestApiService:RestApiService, private authService:AuthenticationService,
    private route:ActivatedRoute){}

  ngOnInit(): void {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getTask();
    this.getSubTask();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  filterredDataSubTask(taskId: number): any[] {
    let subtaskData = this.dataSubTask.filter(data => data.id_task == taskId)
    return subtaskData
  }

  getTask(){
    this.RestApiService.getAllTaskImprovement().subscribe(
      (res:any) => {
        this.dataTask = res.data;
        this.dataTaskByLine = this.dataTask.filter(item => item.id_line == this.idLine)
        this.preData = [...this.dataTaskByLine]
        this.dataTaskByLine = this.dataTaskByLine.slice().sort((a, b) => b.id - a.id);
        this.dataDatePriority = [...this.dataTask]
        this.checkDatePriority();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getSubTask(){
    this.RestApiService.getAllSubTaskImprovement().subscribe(
      (res:any) => {
        this.dataSubTask = res.data; 
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  checkDate(due_date: string): string {
    const currentDate = new Date();
    const taskDueDate = new Date(due_date);
    const differenceInTime = taskDueDate.getTime() - currentDate.getTime();
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);
  
    if (differenceInDays <= -1) {
      return 'Over Date';
    } else if (differenceInDays <= 0) {
      return 'Urgent';
    } else if (differenceInDays <= 6) {
      return 'High';
    } else if (differenceInDays <= 29) {
      return 'Medium';
    } else {
      return 'Low';
    }
  }

  checkDatePriority() {
    this.dataDatePriority.forEach(task => {
      const priority = this.checkDate(task.due_date);
      const newPriorityId = this.getPriorityId(priority);
      
      if (task.id_priority !== newPriorityId) {
        const newDueDate = new Date(task.due_date);
        newDueDate.setDate(newDueDate.getDate() + 1);
        task.due_date = newDueDate.toISOString().split('T')[0];
        task.id_priority = newPriorityId;
        this.updateTask(task, task.id);
      }
    });
  }

  search() {
    this.dataTaskByLine = this.preData.filter((item: { task: string; owner: string; due_date: string; }) => {
      return item.task.toLowerCase().includes(this.searchText.toLowerCase()) ||
             item.owner.toLowerCase().includes(this.searchText.toLowerCase()) ||
             this.extractMonthFromDate(item.due_date).toLowerCase().includes(this.searchText.toLowerCase());
    });
  }

  extractMonthFromDate(dateString: string): string {
    const date = new Date(dateString);
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[date.getMonth()];
  }

  filter() {
    this.dataTaskByLine = this.preData.filter((item: { task: string; owner: string; due_date: string; }) => {
      const itemDate = new Date(item.due_date);
      const startFilterDate = this.startDate ? new Date(this.startDate) : null;
      const endFilterDate = this.endDate ? new Date(this.endDate) : null;

      if (startFilterDate) {
        startFilterDate.setDate(startFilterDate.getDate() - 1);
      }
  
      return (!startFilterDate || itemDate >= startFilterDate) &&
             (!endFilterDate || itemDate <= endFilterDate) 
    });
  }
  
  viewTimeline(id:any){
    this.router.navigate([`continous/improvement/timeline/${id}`], { queryParams: {lineId: this.idLine}})
  }

  addData(){
    this.router.navigate(['continous/improvement/add-data'], { queryParams: {lineId: this.idLine}})
  }

  editData(id:any){
    this.router.navigate([`continous/improvement/add-data/${id}`], { queryParams: {lineId: this.idLine}})
  }

  editSubtask(id:any){
    this.router.navigate([`continous/improvement/edit-data/${id}`], { queryParams: {lineId: this.idLine}})
  }

  addDataSubtask(idParam:number){
    this.router.navigate([`continous/improvement/edit-data/${idParam}`], { queryParams: {lineId: this.idLine}})
  }

  updateTask(dataTaskByLine: any, id:any) {
    this.RestApiService.updateTask(id, dataTaskByLine).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTaskImprovement= dataTaskByLine;
          this.getTask()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDeleteTask(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateSubTaskByTaskId(deleteData, id);
        this.updateTask(deleteData, id)
      }
    });
  }

  updateSubtask(dataSubTask: any, id:any) {
    this.RestApiService.updateSubTask(id, dataSubTask).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newSubTaskImprovement= dataSubTask;
          this.getSubTask()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  updateSubTaskByTaskId(dataSubTaskByTask:any, taskId:any) {
    this.RestApiService.updateSubTaskByTaskId(taskId, dataSubTaskByTask).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.getTask()
          this.getSubTask()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 
  
  onDeleteSubtask(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateSubtask(deleteData, id);
      }
    });
  }
  
  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Late';
        default:
            return 'Unknown';
    }
}

getPriorityLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'primary'; 
      case 2:
          return 'warning'; 
      case 3:
          return 'orange';
      case 4:
          return 'danger'; 
      case 5:
          return 'danger';
      default:
          return 'secondary';
  }
}

getPriorityLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'Low';
      case 2:
          return 'Medium';
      case 3:
          return 'High';
      case 4:
          return 'Urgent';
      case 5:
          return 'Over Date';
      default:
          return 'Unknown';
  }
}

getPriorityId(priority: string): number {
  switch (priority) {
    case 'Over Date':
      return 5; 
    case 'Urgent':
      return 4; 
    case 'High':
      return 3; 
    case 'Medium':
      return 2; 
    case 'Low':
      return 1; 
    default:
      return 1; 
  }
}

getStatusId(priority: string): number {
  switch (priority) {
    case 'Late':
      return 4; 
    case 'Done':
      return 3; 
    case 'On Progress':
      return 2; 
    case 'New':
      return 1; 
    default:
      return 1; 
  }
}

}
