import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { TaskImrovement } from '../task.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-continous-improvement-add-data',
  templateUrl: './continous-improvement-add-data.component.html',
  styleUrls: ['./continous-improvement-add-data.component.scss']
})
export class ContinousImprovementAddDataComponent implements OnInit{

    public subtask:any = [];  
    public idParam:any;
    task_improvement:any;
    idTask:any;
    subtask_improvement:any;
    idSubTask:any;
    statusData: any[] = [];
    priorityData: any[] = [];
    dataTask:any;
    namePic: any [] = [];
    picSpvAndAdmin: any;
    idLine:any;

    newTaskImprovement : TaskImrovement = {
      task: '',
      owner: '',
      due_date: '',
      id_status: null,
      id_priority: null,
      notes: '',
      id_line: null
    }

    constructor(private router:Router, private RestApiService:RestApiService, private route:ActivatedRoute){}

    ngOnInit(): void {
      this.getNamePic();
      this.route.params.subscribe((params) => {
        const idToEdit = params['id'];
        this.idTask = idToEdit
        if (idToEdit) {
          this.getTaskById(idToEdit);
        }
        this.getStatus();
        this.getPriority();
      });
      this.route.queryParams.subscribe({next: (params) => {
        this.idLine = params['lineId']
      }})
    } 

    getNamePic(){
      this.RestApiService.getNamePic().subscribe(
        (res: any) => {
          this.namePic = res.data;
          this.picSpvAndAdmin = this.namePic.filter(item => item.role_id === 1 || item.role_id === 2)
        },
        (error: any) => {
          console.error(error);
        }
      );
    }

    forBack(){
      this.router.navigate(['continous/improvement'], {queryParams:{lineId: this.idLine}})
    }

    insertTaskImprovement(dataTask:any){
      this.RestApiService.insertTaskImprovement(dataTask).subscribe(
        (res:any) => {
          if (res.status == true){
            this.idTask = res.data[0]
            this.task_improvement = dataTask
            this.task_improvement.id = this.idTask
            this.router.navigate([`continous/improvement/edit-data/${this.idTask}`], { queryParams:{lineId: this.idLine}})
          }
        })
    }

  updateTask(dataTask:any){
    this.RestApiService.updateTask(this.idTask, dataTask).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTaskImprovement = dataTask;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdate(){
    this.updateTask(this.newTaskImprovement);
      Swal.fire({
        icon: 'success',
        title: 'Update Successfully',
        text: 'Update has been successfully.',
        timer: 1500,
        showConfirmButton: false,
      });
  }

  onAddDataTask(){
    this.newTaskImprovement.id_line = this.idLine
    this.newTaskImprovement.id_status = 1
    this.insertTaskImprovement(this.newTaskImprovement)
  }

  getStatus(){
    this.RestApiService.getStatus().subscribe(
      (res: any) => {
        this.statusData = res.data[0];
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getPriority(){
    this.RestApiService.getPriority().subscribe(
      (res: any) => {
        this.priorityData = res.data[0];
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getTaskById(id:any){
    this.RestApiService.getTaskImprovementById(id).subscribe(
      (res:any) => {
        this.newTaskImprovement = res.data[0];
        this.newTaskImprovement.due_date = this.convertDate(this.newTaskImprovement.due_date);
      })
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}
