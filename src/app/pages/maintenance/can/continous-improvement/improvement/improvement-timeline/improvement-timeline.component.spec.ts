import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprovementTimelineComponent } from './improvement-timeline.component';

describe('ImprovementTimelineComponent', () => {
  let component: ImprovementTimelineComponent;
  let fixture: ComponentFixture<ImprovementTimelineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImprovementTimelineComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImprovementTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
