import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-improvement-timeline',
  templateUrl: './improvement-timeline.component.html',
  styleUrls: ['./improvement-timeline.component.scss']
})
export class ImprovementTimelineComponent implements OnInit {

  // bread crumb items
  breadCrumbItems!: Array<{}>;
  basicTimelineChart: any;
  differentColorChart: any;
  multiSeriesTimelineChart: any;
  advancedTimelineChart: any;
  multipleSeriesChart: any;
  dataSubTask:any[] = [];
  idSubtask:any;
  dataTask:any;
  idLine:any;
  public idParam:any;

  constructor(private RestApiService:RestApiService, private route:ActivatedRoute, private router: Router) {
    this.differentColorChart = {};
   }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getTaskImprovementById(this.idParam)
    this.getSubTaskByTaskId(this.idParam)
    

     this.breadCrumbItems = [
      { label: 'Apexcharts' },
      { label: 'Timeline Charts', active: true }
    ];
  }

  // Chart Colors Set
  private getChartColorsArray(colors:any) {
    colors = JSON.parse(colors);
    return colors.map(function (value:any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) {
            color = color.replace(" ", "");
            return color;
            }
            else return newValue;;
        } else {
            var val = value.split(',');
            if (val.length == 2) {
                var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
                rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
                return rgbaColor;
            } else {
                return newValue;
            }
        }
    });
  }

  /**
 * Different Color For Each Bar
 */
  private _differentColorChart() {
    const colors = this.getChartColorsArray('["--vz-primary", "--vz-danger", "--vz-success", "--vz-warning", "--vz-info"]');
    const filteredDataSubTask = this.dataSubTask.filter(task => task.status !== 0);
    const timelineData = filteredDataSubTask.map((task, index) => {
      let startDate = new Date(task.start_date).getTime() + 24 * 60 * 60 * 1000;
      let endDate = new Date(task.end_date).getTime() + 24 * 60 * 60 * 1000;
  
      // Jika start_date dan end_date adalah hari yang sama, tambahkan satu hari pada end_date
      if (startDate === endDate) {
        endDate += 24 * 60 * 60 * 1000; // Tambah satu hari
      }
  
      return {
        x: task.subtask,
        y: [
          startDate,
          endDate,
        ],
        fillColor: colors[index % colors.length],
      };
    });
    const chartHeight = Math.max(330, timelineData.length * 40); 
    this.differentColorChart = {
      series: [
        {
          data: timelineData,
        },
      ],
      chart: {
        height: chartHeight,
        type: "rangeBar",
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
        }
      },
      plotOptions: {
        bar: {
          horizontal: true,
          distributed: true,
          dataLabels: {
            hideOverflowingLabels: false,
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any, opts: any) {
          var label = opts.w.globals.labels[opts.dataPointIndex];
          var a = moment(val[0]);
          var b = moment(val[1]);
          var diff = b.diff(a, "days");
          return label + ": " + diff + (diff > 1 ? " days" : " day");
        },
      },
      xaxis: {
        type: "datetime",
      },
      yaxis: {
        show: true,
      },
    };
  }

  getTaskImprovementById(id:any){
    this.RestApiService.getTaskImprovementById(id).subscribe(
    (res:any) => {
      this.dataTask = res.data;
    })
  }

  getSubTaskByTaskId(id:any){
    this.RestApiService.getSubTaskByTaskId(id).subscribe(
      (res:any) => {
        this.dataSubTask = res.data;
        this._differentColorChart();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }
  forBack(){
    this.router.navigate(['/continous/improvement'], {queryParams:{ lineId : this.idLine}})
  }

}
