import { Component, ViewChild, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { ScheduleMechaWeeklyPopupComponent } from './schedule-mecha-weekly-popup/schedule-mecha-weekly-popup.component';


@Component({
  selector: 'app-schedule-mecha-weekly',
  templateUrl: './schedule-mecha-weekly.component.html',
  styleUrls: ['./schedule-mecha-weekly.component.scss']
})
export class ScheduleMechaWeeklyComponent {
  tableColumns = ['No', 'Function Location', 'Operation Task', 'Description Material', 'Pic', 'Status', 'Date', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  scheduleData: any;
  pendingData:any;

  @ViewChild('myModal') myModal: any; 

  currentPage = 1;
  itemsPerPage = 10;
  startDate: string = '';
  endDate: string = '';

  constructor(private modalService: NgbModal, 
    private route: ActivatedRoute, 
    private router:Router, 
    private RestApiService : RestApiService) {} 
  
  public idParam:any;

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  idMaterial:any;
  dataWeekly:any [] = [];
  dataWeeklyCan:any [] = [];
  dataPending:any [] = [];
  dataPendingCan:any [] = [];
  schedule_maintenance : any;
  searchText1:string = '';
  searchText2:string = '';
  dataForCurrentPage1: any[] = [];
  dataForCurrentPage2: any[] = [];
  preData1:any;
  preData2:any;
  isTableView: boolean = false;

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getWeekly()
    this.getPending()
    this.paginateData1()
    this.paginateData2()
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id == 'btnCompleted') {
      this.isTableView = false
      this.router.navigate(["/schedule/mecha/weekly"], {
        queryParams: { tableCompleted: false },
      });
    } else if (event.target.id == 'btnPending') {
      this.isTableView = true
      this.router.navigate(["/schedule/mecha/weekly"], {
        queryParams: { tablePending: true },
      });
    }
  }

  openPopup(data: any) {
    const modalRef = this.modalService.open(ScheduleMechaWeeklyPopupComponent, { centered: true });
    modalRef.componentInstance.dataWeekly = data;
    modalRef.result.then(
      (result) => {
      },
      (reason) => {
      }
    );
  }

  getWeekly(){
    this.RestApiService.getMechaWeekly().subscribe(
      (res:any) => {
        this.dataWeekly = res.data[0];
        this.dataWeeklyCan = this.dataWeekly.filter(item => item.line === 'can')
        this.preData1 = [...this.dataWeeklyCan];
        this.paginateData1();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getPending(){
    this.RestApiService.getMechaPending().subscribe(
      (res:any) => {
        this.dataPending = res.data[0];
        this.dataPendingCan = this.dataPending.filter(item => item.line === 'can')
        this.preData2 = [...this.dataPendingCan];
        this.paginateData2();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Pending';
        default:
            return 'Unknown';
    }
}

search1() {
  this.dataWeeklyCan = this.preData1.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const month = this.extractMonthFromDate(item.date);
    return (month && month.toLowerCase().includes(this.searchText1.toLowerCase())) ||
           item.function_location.toLowerCase().includes(this.searchText1.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText1.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText1.toLowerCase());
  });
  this.paginateData1();
}

search2() {
  this.dataPendingCan = this.preData2.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const month = this.extractMonthFromDate(item.date);
    return (month && month.toLowerCase().includes(this.searchText2.toLowerCase())) ||
           item.function_location.toLowerCase().includes(this.searchText2.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText2.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText2.toLowerCase());
  });
  this.paginateData2();
}

filter1() {
  this.dataWeeklyCan = this.preData1.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const itemDate = new Date(item.date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;

    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.filter2();
  this.paginateData1();
}

filter2() {
  this.dataPendingCan = this.preData2.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const itemDate = new Date(item.date);
    const startFilterDate = this.startDate ? new Date(this.startDate) : null;
    const endFilterDate = this.endDate ? new Date(this.endDate) : null;

    if (startFilterDate) {
      startFilterDate.setDate(startFilterDate.getDate() - 1);
    }

    return (!startFilterDate || itemDate >= startFilterDate) &&
           (!endFilterDate || itemDate <= endFilterDate) 
  });
  this.paginateData2();
}

paginateData1() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage1 = this.dataWeeklyCan.slice(startIndex, endIndex);
}

paginateData2() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage2 = this.dataPendingCan.slice(startIndex, endIndex);
}

onPageChange1(newPage: number) {
  this.currentPage = newPage;
  this.paginateData1();
}

onPageChange2(newPage: number) {
  this.currentPage = newPage;
  this.paginateData2();
}

extractMonthFromDate(dateString: string): string {
  const date = new Date(dateString);
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', '-August', 'September', 'October', 'November', 'December'
  ];
  return monthNames[date.getMonth()];
}
}
