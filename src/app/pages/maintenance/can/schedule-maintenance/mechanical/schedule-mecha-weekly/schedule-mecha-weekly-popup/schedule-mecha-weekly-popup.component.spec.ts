import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaWeeklyPopupComponent } from './schedule-mecha-weekly-popup.component';

describe('ScheduleMechaWeeklyPopupComponent', () => {
  let component: ScheduleMechaWeeklyPopupComponent;
  let fixture: ComponentFixture<ScheduleMechaWeeklyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaWeeklyPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaWeeklyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
