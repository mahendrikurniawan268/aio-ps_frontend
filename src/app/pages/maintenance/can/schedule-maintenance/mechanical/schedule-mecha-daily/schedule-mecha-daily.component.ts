import { Component, ViewChild, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { ScheduleMechaPopupComponent } from './schedule-mecha-popup/schedule-mecha-popup.component';

@Component({
  selector: 'app-schedule-mecha-daily',
  templateUrl: './schedule-mecha-daily.component.html',
  styleUrls: ['./schedule-mecha-daily.component.scss']
})
export class ScheduleMechaDailyComponent {
  tableColumns = ['No', 'Function Location', 'Operation Task', 'Description Material', 'Pic', 'Status', 'Date', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  scheduleData: any[] = [];

  @ViewChild('myModal') myModal: any; 
  
  currentPage1 = 1;
  itemsPerPage1 = 10;

  currentPage2 = 1;
  itemsPerPage2 = 10;

  constructor(private modalService: NgbModal, 
    private route: ActivatedRoute, 
    private router:Router, 
    private RestApiService : RestApiService,
    private authService: AuthenticationService) {} 
  
  public idParam:any;

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  idMaterial:any;
  dataDaily:any [] = [];
  dataDailyCan:any [] = [];
  dataUnplannedCan:any [] = [];
  dataUnplanned: any [] = [];
  schedule_maintenance : any [] = [];
  searchText1:string = '';
  dataForCurrentPage1: any[] = [];
  preData1:any;
  searchText2:string = '';
  dataForCurrentPage2: any[] = [];
  preData2:any;
  userData:any;
  isAdmin:boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  isTableView: boolean = false;

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getDaily();
    this.getUnplanned();
    this.paginateData1();
    this.paginateData2();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  onSelectedViewCheck(event: any) {
    if (event.target.id == 'btnPlanned') {
      this.isTableView = false
      this.router.navigate(["/schedule/mecha/daily"], {
        queryParams: { tableView: false },
      });
    } else if (event.target.id == 'btnUnplanned') {
      this.isTableView = true
      this.router.navigate(["/schedule/mecha/daily"], {
        queryParams: { tableView: true },
      });
    }
  }

  openPopupDaily(data: any) {
    const modalRef = this.modalService.open(ScheduleMechaPopupComponent, { centered: true });
    modalRef.componentInstance.dataDaily = data;
    modalRef.result.then(
      (result) => {
        
      },
      (reason) => {
        
      }
    );
  }

  openPopupUnplanned(data: any) {
    const modalRef = this.modalService.open(ScheduleMechaPopupComponent, { centered: true });
    modalRef.componentInstance.dataUnplanned = data;
    modalRef.result.then(
      (result) => {
        
      },
      (reason) => {
        
      }
    );
  }

  addDataDaily() {
  this.router.navigate(['schedule/mecha/daily/add-data']);
  }

  addDataUnplanned() {
    this.router.navigate(['schedule/mecha/daily/unplanned']);
  }

  updateSchedule(dataSchedule: any, id:any) {
    this.RestApiService.updateScheduleMaintenance(id, dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.schedule_maintenance = dataSchedule;
          this.getDaily()
          this.getUnplanned()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        window.location.reload()
        this.updateSchedule(deleteData, id);
      }
    });
  }

  getDaily(){
    this.RestApiService.getMechaDaily().subscribe(
      (res:any) => {
        this.dataDaily = res.data[0];
        this.dataDailyCan = this.dataDaily.filter(item => item.line === 'can')
        this.scheduleData = [...this.dataDailyCan];
        this.preData1 = [...this.dataDailyCan];
        this.paginateData1();
        this.checkAndSetLateStatus();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getUnplanned(){
    this.RestApiService.getMechaUnplanned().subscribe(
      (res:any) => {
        this.dataUnplanned = res.data[0];
        this.dataUnplannedCan = this.dataUnplanned.filter(item => item.line === 'can')
        this.scheduleData = [...this.dataUnplannedCan];
        this.preData2 = [...this.dataUnplannedCan];
        this.paginateData2();
        this.checkAndSetLateStatus()
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        case 5:
          return 'primary'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Pending';
        case 5:
          return 'Revisi';
        default:
            return 'Unknown';
    }
}

onReport(){
  this.router.navigate(['report/schedule-maintenance/add'], {queryParams: {lineId: 1}})
}

listReport(){
  this.router.navigate(['list-report/schedule-maintenance'], {queryParams: {lineId: 1}})
}

editPlan(id:any){
  this.router.navigate([`schedule/mecha/daily/unplanned/${id}`])
}

editDaily(id:any){
  this.router.navigate([`schedule/mecha/daily/add-data/${id}`])
}

checkAndSetLateStatus() {
  const currentDate = new Date(); 
  for (const data of this.scheduleData) {
    if (data.id_status === 2 || data.id_status === 1) { 
      const scheduledDate = new Date(data.date); 
      scheduledDate.setDate(scheduledDate.getDate() + 2);
      if (scheduledDate < currentDate) {
        data.id_status = 4;

        this.RestApiService.updateScheduleMaintenance(data.id, { id_status: 4 }).subscribe(
          (res: any) => {
            if (res.status == true) {
              this.getDaily();
              this.getUnplanned();
            }
          },
          (error) => {
            console.error(error);
          }
        );
      }
    }
  }
}

search1() {
  this.dataDailyCan = this.preData1.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const month = this.extractMonthFromDate(item.date);
    return (month && month.toLowerCase().includes(this.searchText1.toLowerCase())) ||
           item.function_location.toLowerCase().includes(this.searchText1.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText1.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText1.toLowerCase());
  });
  this.paginateData1();
}

search2() {
  this.dataUnplannedCan = this.preData2.filter((item: { function_location: string; operation_task: string; date: string; pic: string;}) => {
    const month = this.extractMonthFromDate(item.date);
    return (month && month.toLowerCase().includes(this.searchText2.toLowerCase())) ||
           item.function_location.toLowerCase().includes(this.searchText2.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText2.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText2.toLowerCase());
  });
  this.paginateData2();
}

paginateData1() {
  const startIndex = (this.currentPage1 - 1) * this.itemsPerPage1;
  const endIndex = startIndex + this.itemsPerPage1;
  this.dataForCurrentPage1 = this.dataDailyCan.slice(startIndex, endIndex);
}

paginateData2() {
  const startIndex = (this.currentPage2 - 1) * this.itemsPerPage2;
  const endIndex = startIndex + this.itemsPerPage2;
  this.dataForCurrentPage2 = this.dataUnplannedCan.slice(startIndex, endIndex);
}

onPageChange1(newPage: number) {
  this.currentPage1 = newPage;
  this.paginateData1();
}

onPageChange2(newPage: number) {
  this.currentPage2 = newPage;
  this.paginateData2();
}

extractMonthFromDate(dateString: string): string {
  const date = new Date(dateString);
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', '-August', 'September', 'October', 'November', 'December'
  ];
  return monthNames[date.getMonth()];
}
  
}
