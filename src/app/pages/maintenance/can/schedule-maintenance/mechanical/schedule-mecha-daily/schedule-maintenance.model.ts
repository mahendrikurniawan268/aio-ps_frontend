export interface ScheduleMaintenance {
    function_location: string | null;
    equipment: string;
    operation_task: string;
    description_material: string;
    date: string;
    pic: string[];
    email: string[];
    qty: number | null;
    status_part: string;
    work_center: string;
    duration: string;
    notes: string;
    category: string;
    unplanned: number | null;
    line: string;
  }