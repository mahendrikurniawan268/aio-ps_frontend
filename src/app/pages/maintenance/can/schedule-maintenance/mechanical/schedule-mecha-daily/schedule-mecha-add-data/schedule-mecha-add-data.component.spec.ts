import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaAddDataComponent } from './schedule-mecha-add-data.component';

describe('ScheduleMechaAddDataComponent', () => {
  let component: ScheduleMechaAddDataComponent;
  let fixture: ComponentFixture<ScheduleMechaAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
