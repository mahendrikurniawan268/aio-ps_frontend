import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPopupComponent } from './schedule-mecha-popup.component';

describe('ScheduleMechaPopupComponent', () => {
  let component: ScheduleMechaPopupComponent;
  let fixture: ComponentFixture<ScheduleMechaPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
