import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaUnplannedComponent } from './schedule-mecha-unplanned.component';

describe('ScheduleMechaUnplannedComponent', () => {
  let component: ScheduleMechaUnplannedComponent;
  let fixture: ComponentFixture<ScheduleMechaUnplannedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaUnplannedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaUnplannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
