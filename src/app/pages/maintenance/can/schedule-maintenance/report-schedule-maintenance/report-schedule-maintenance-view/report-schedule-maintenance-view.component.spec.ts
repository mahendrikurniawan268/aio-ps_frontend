import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportScheduleMaintenanceViewComponent } from './report-schedule-maintenance-view.component';

describe('ReportScheduleMaintenanceViewComponent', () => {
  let component: ReportScheduleMaintenanceViewComponent;
  let fixture: ComponentFixture<ReportScheduleMaintenanceViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportScheduleMaintenanceViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportScheduleMaintenanceViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
