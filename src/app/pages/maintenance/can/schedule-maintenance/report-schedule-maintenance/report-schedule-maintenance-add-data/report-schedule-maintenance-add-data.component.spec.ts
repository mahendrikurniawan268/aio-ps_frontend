import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportScheduleMaintenanceAddDataComponent } from './report-schedule-maintenance-add-data.component';

describe('ReportScheduleMaintenanceAddDataComponent', () => {
  let component: ReportScheduleMaintenanceAddDataComponent;
  let fixture: ComponentFixture<ReportScheduleMaintenanceAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportScheduleMaintenanceAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportScheduleMaintenanceAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
