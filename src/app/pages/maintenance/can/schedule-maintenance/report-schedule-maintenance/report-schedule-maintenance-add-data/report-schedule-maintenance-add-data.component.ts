import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ReportMasterPlan } from '../report-master-plan.model';
import Swal from 'sweetalert2';
import { ReportMaster } from '../report-master.model';

@Component({
  selector: 'app-report-schedule-maintenance-add-data',
  templateUrl: './report-schedule-maintenance-add-data.component.html',
  styleUrls: ['./report-schedule-maintenance-add-data.component.scss']
})
export class ReportScheduleMaintenanceAddDataComponent implements OnInit{
  
  public report_master_plan:any;
  public report_master:any;
  idParam:any;
  reportMasterPlanData: any[] = [];
  id_report_master_plan:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any;
  nextNumber: number = 0;
  buttonClicked: boolean = false;
  namePic: any [] = [];
  namePicEmployee:any;
  idLine:any;

  newReportMasterPlan : ReportMasterPlan = {
    activity: '',
    date: '',
    start_time: '',
    end_time: '',
    verification: '',
    documentation_before: null,
    documentation_after: null,
    id_report_master: null
  }

  newReportMaster : ReportMaster = {
    id_area: null,
    pic: '',
    email: '',
    spv: 'Alfan Aditama',
    email_spv: 'mahendrikurniawan268@gmail.com',
    equipment: '',
    wo_number: '',
    description:'',
    start_date: '',
    end_date: '',
    type: 'schedule maintenance',
    id_line: null
  }

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route:ActivatedRoute
  ) {}
  
  ngOnInit(): void {
  this.idParam = this.route.snapshot.paramMap.get('id');
  this.route.queryParams.subscribe({next: (params) => {
    this.idLine = params['lineId']
  }})
  this.getNamePic();
  this.getReportById(this.idParam);
  this.getReportMasterPlanByReportMasterId(this.idParam);
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicEmployee = this.namePic.filter(item => item.role_id == 3 || item.role_id == 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  insertReportMasterPlan(dataReportMasterPlan: any) {
  
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
  
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          dataReportMasterPlan.documentation_before = res.uploadedFiles[0];
          dataReportMasterPlan.documentation_after = res.uploadedFiles[1];
  
          this.RestApiService.insertReportMaster(dataReportMasterPlan).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.id_report_master_plan = res.data[0];
                this.report_master_plan = dataReportMasterPlan;
                this.report_master_plan.id_report_master_plan = this.id_report_master_plan;
                this.getReportMasterPlanByReportMasterId(this.idParam)
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      this.RestApiService.insertReportMaster(dataReportMasterPlan).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.id_report_master_plan = res.data[0];
            this.report_master_plan = dataReportMasterPlan;
            this.report_master_plan.id_report_master_plan = this.id_report_master_plan;
            this.getReportMasterPlanByReportMasterId(this.idParam)
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  } 
  
  getReportById(id:any){
    this.RestApiService.getReportById(id).subscribe(
    (res:any) => {
      this.report_master = res.data[0];      
    })
  }

  getReportMasterPlanByReportMasterId(id: any) {
    this.RestApiService.getReportMasterPlanByReportMasterId(id).subscribe(
    (res: any) => {
      this.reportMasterPlanData = res.data;
    });
  }
  
  onInsertData() {
    this.newReportMasterPlan.id_report_master = this.idParam;
    this.nextNumber++;
    this.buttonClicked = true;
    this.insertReportMasterPlan(this.newReportMasterPlan);
    this.clearForm();

    Swal.fire({
      title: 'Notification',
      text: 'Add Data Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
  }

  forBack() {
    this.router.navigate(['preparation/mechanical/over-haul']);
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  clearForm() {
    this.newReportMasterPlan = {
      activity: '',
      date: '',
      start_time: '',
      end_time: '',
      verification: '',
      documentation_before: null,
      documentation_after: null,
      id_report_master: null
    };
  }

  onSubmit(){
    this.router.navigate(['report/schedule-maintenance/view/'], {queryParams: { idParam: this.idParam, lineId: this.idLine
       },
    state: { 
      newReportMaster: this.newReportMaster,
      newReportMasterPlan: this.newReportMasterPlan }
    });
  }
}

