export interface ReportMasterPlan {
    activity: string,
    date: string,
    start_time: string,
    end_time: string,
    verification: string,
    documentation_before: File | null,
    documentation_after: File | null,
    id_report_master: number | null
  }