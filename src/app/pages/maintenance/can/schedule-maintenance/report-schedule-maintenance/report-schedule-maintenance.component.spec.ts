import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportScheduleMaintenanceComponent } from './report-schedule-maintenance.component';

describe('ReportScheduleMaintenanceComponent', () => {
  let component: ReportScheduleMaintenanceComponent;
  let fixture: ComponentFixture<ReportScheduleMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportScheduleMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportScheduleMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
