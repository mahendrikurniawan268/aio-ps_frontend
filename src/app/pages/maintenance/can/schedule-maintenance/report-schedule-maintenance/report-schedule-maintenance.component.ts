import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ReportMaster } from './report-master.model';

@Component({
  selector: 'app-report-schedule-maintenance',
  templateUrl: './report-schedule-maintenance.component.html',
  styleUrls: ['./report-schedule-maintenance.component.scss']
})
export class ReportScheduleMaintenanceComponent implements OnInit {
  
  public report_master:any;
  id_report_master:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any
  namePic: any [] = [];
  namePicEmployee: any;
  userData: any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee: boolean = false;
  sectionOptions = ['Preparation', 'Filling', 'Packing'];
  sectionSearchKeyword: string = '';
  idLine:any;
  dataArea: any [] = [];
  area: any;

  newReportMaster : ReportMaster = {
    id_area: null,
    pic: '',
    email: '',
    spv: 'Alfan Aditama',
    email_spv: 'mahendrikurniawan268@gmail.com',
    equipment: '',
    wo_number: '',
    description:'',
    start_date: '',
    end_date: '',
    type: 'schedule maintenance',
    id_line: null
  }

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private http: HttpClient,
    private authService: AuthenticationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getArea();
    this.getNamePic();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();   
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicEmployee = this.namePic.filter(item => item.role_id == 3 || item.role_id == 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getArea(){
    this.RestApiService.getAllArea().subscribe(
      (res: any) => {
        this.dataArea = res.data;
        this.area = this.dataArea.filter(item => item.id_line == this.idLine) 
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  insertReportMaster(dataReportMaster: any) {
    this.RestApiService.insertReport(dataReportMaster).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.id_report_master = res.data[0]
          this.report_master = dataReportMaster;
          this.report_master.id = this.id_report_master
          this.router.navigate([`report/schedule-maintenance/add-data/${this.id_report_master}`], { queryParams:{lineId: this.idLine}})
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }
  
  onInsertData() {
    this.newReportMaster.id_line = this.idLine;
    this.newReportMaster.pic = this.userData.name;
    this.newReportMaster.email = this.userData.email;
    this.insertReportMaster(this.newReportMaster);
  }

  forBack() {
    this.router.navigate(['preparation/mechanical/over-haul']);
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

}
