export interface ReportMaster {
    id_area: null | number,
    pic: string,
    email: string,
    spv: string,
    email_spv: string,
    equipment: string,
    wo_number: string,
    description: string,
    start_date: string,
    end_date: string
    type: string,
    id_line: null | number
  }