import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListReportScheduleMaintenanceEditComponent } from './list-report-schedule-maintenance-edit.component';

describe('ListReportScheduleMaintenanceEditComponent', () => {
  let component: ListReportScheduleMaintenanceEditComponent;
  let fixture: ComponentFixture<ListReportScheduleMaintenanceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListReportScheduleMaintenanceEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListReportScheduleMaintenanceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
