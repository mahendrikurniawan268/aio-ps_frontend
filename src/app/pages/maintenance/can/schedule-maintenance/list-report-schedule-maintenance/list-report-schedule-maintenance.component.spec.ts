import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListReportScheduleMaintenanceComponent } from './list-report-schedule-maintenance.component';

describe('ListReportScheduleMaintenanceComponent', () => {
  let component: ListReportScheduleMaintenanceComponent;
  let fixture: ComponentFixture<ListReportScheduleMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListReportScheduleMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListReportScheduleMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
