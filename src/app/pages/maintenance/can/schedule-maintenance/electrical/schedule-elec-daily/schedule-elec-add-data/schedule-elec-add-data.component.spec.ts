import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecAddDataComponent } from './schedule-elec-add-data.component';

describe('ScheduleElecAddDataComponent', () => {
  let component: ScheduleElecAddDataComponent;
  let fixture: ComponentFixture<ScheduleElecAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
