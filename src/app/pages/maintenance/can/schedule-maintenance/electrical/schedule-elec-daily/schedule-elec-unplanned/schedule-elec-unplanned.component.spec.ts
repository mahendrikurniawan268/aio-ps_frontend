import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecUnplannedComponent } from './schedule-elec-unplanned.component';

describe('ScheduleElecUnplannedComponent', () => {
  let component: ScheduleElecUnplannedComponent;
  let fixture: ComponentFixture<ScheduleElecUnplannedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecUnplannedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecUnplannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
