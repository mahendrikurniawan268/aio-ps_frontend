import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ScheduleMaintenance } from '../../../mechanical/schedule-mecha-daily/schedule-maintenance.model';

@Component({
  selector: 'app-schedule-elec-unplanned',
  templateUrl: './schedule-elec-unplanned.component.html',
  styleUrls: ['./schedule-elec-unplanned.component.scss']
})
export class ScheduleElecUnplannedComponent implements OnInit {

  public schedule:any = [];
  public idParam:any;
  public scheduleSaveData: boolean[] = [];
  dataUnplanned:any;

  newScheduleMaintenance : ScheduleMaintenance = {
  function_location: null,
  equipment: '',
  operation_task: '',
  description_material: '',
  date: '',
  pic: [],
  email: [],
  qty: null,
  status_part: '',
  work_center: '',
  duration: '',
  notes: '',
  category: 'Electrical',
  unplanned: 2,
  line: 'can'
}

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idSchedule: any;
  schedule_maintenance: any;
  ScheduleData: ScheduleMaintenance[] = [];
  namePic: any [] = [];
  picEmployee: any [] = [];
  scheduleById: any;
  workCenterOptions = ['Production', 'Maintenance'];
  workCenterSearchKeyword: string = '';
  statusPartOptions = ['Ready Stock', 'Stock Not Available'];
  statusPartrSearchKeyword: string = '';
  functionLocation:any [] = [];
  functionLocationCan: any = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}
  
  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getFunctionLocation();
    this.getNamePic();
    this.getUnplanned();
    if (this.idParam) {
      this.getScheduleMaintenanceById(this.idParam);
    }
  }

  onPic(event: any, type?: string) {
    const selectedPics = event.map((selectedItem: any) => {
      const selectedPic = this.picEmployee.find((item: any) => item.name === selectedItem.name);
      if (selectedPic) {
        return { name: selectedPic.name, email: selectedPic.email };
      }
      return null;
    }).filter((pic: any) => pic !== null);
    const picNames = selectedPics.map((pic: any) => pic.name);
    const emails = selectedPics.map((pic: any) => pic.email);
    if (type !== 'update') {
      this.newScheduleMaintenance.pic = picNames.join(', ');
      this.newScheduleMaintenance.email = emails.join(', ');
    } else {
      this.newScheduleMaintenance.pic = picNames
      this.newScheduleMaintenance.email = emails
    }
  }

  getFunctionLocation(){
    this.RestApiService.getAllFunctionLocation().subscribe(
      (res:any) => {
        this.functionLocation = res.data;
        this.functionLocationCan = this.functionLocation.filter(item => item.line === 'can')
      }
    )
  }

  onWorkCenterSelect(selectedItem: string) {
    this.newScheduleMaintenance.work_center = selectedItem;
  }

  onStatusPartSelect(selectedItem: string) {
    this.newScheduleMaintenance.status_part = selectedItem;
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  forBack() {
    this.router.navigate(['/schedule/elec/daily']);
  }

  onClick() {
    if (this.isDataValid(this.newScheduleMaintenance)) {
      this.insertSchedule(this.newScheduleMaintenance);
      this.router.navigate(['/schedule/elec/daily']);
      Swal.fire({
        title: 'Notification',
        text: 'Data Added Successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      }).then(() => {
        setTimeout(() => {
          window.location.reload();
        },);
      });
    } else {
      Swal.fire({
        title: 'Error',
        text: 'Please complete all fields',
        icon: 'error',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    }
  }
  
  isDataValid(data: any): boolean {
    return (
      data.function_location &&
      data.equipment &&
      data.operation_task &&
      data.description_material &&
      data.date &&
      data.pic &&
      data.email &&
      data.qty &&
      data.status_part &&
      data.work_center &&
      data.duration
    );
  }

  getScheduleMaintenanceById(id:any){
    this.RestApiService.getScheduleMaintenanceById(id).subscribe(
      (res:any) => {
        this.newScheduleMaintenance = res.data[0];
        this.scheduleById = this.newScheduleMaintenance
        this.scheduleById.date = this.convertDate(this.scheduleById.date);
        if (this.scheduleById.pic && this.scheduleById.email) {
          this.scheduleById.pic = (this.scheduleById.pic as string).split(',').map(pic => pic.trim());
          this.newScheduleMaintenance.email = (this.scheduleById.email as string).split(',').map(pic => pic.trim());
        }
      })
  }

  getUnplanned(){
    this.RestApiService.getElecUnplanned().subscribe(
      (res:any) => {
        this.dataUnplanned = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  insertSchedule(dataSchedule: any) {
    this.RestApiService.insertScheduleMaintenance(dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idSchedule = res.data[0]
          this.schedule_maintenance = dataSchedule;
          this.schedule_maintenance.id = this.idSchedule
          this.getUnplanned();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateSchedule(dataSchedule: any) {
    this.RestApiService.updateScheduleMaintenance(this.idParam, dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.schedule_maintenance = dataSchedule;
          this.getUnplanned()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdateSchedule() {
    (this.newScheduleMaintenance.pic as any) = this.newScheduleMaintenance.pic.join(', '),
    (this.newScheduleMaintenance.email as any) = this.newScheduleMaintenance.email.join(', ')
    console.log(this.newScheduleMaintenance)
    this.updateSchedule(this.newScheduleMaintenance);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.getScheduleMaintenanceById(this.idParam)
  } 

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}



