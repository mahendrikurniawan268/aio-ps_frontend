import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPopupComponent } from './schedule-elec-popup.component';

describe('ScheduleElecPopupComponent', () => {
  let component: ScheduleElecPopupComponent;
  let fixture: ComponentFixture<ScheduleElecPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
