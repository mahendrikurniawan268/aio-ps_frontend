import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecWeeklyPopupComponent } from './schedule-elec-weekly-popup.component';

describe('ScheduleElecWeeklyPopupComponent', () => {
  let component: ScheduleElecWeeklyPopupComponent;
  let fixture: ComponentFixture<ScheduleElecWeeklyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecWeeklyPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecWeeklyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
