import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairAddDataComponent } from './budgeting-repair-add-data.component';

describe('BudgetingRepairAddDataComponent', () => {
  let component: BudgetingRepairAddDataComponent;
  let fixture: ComponentFixture<BudgetingRepairAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
