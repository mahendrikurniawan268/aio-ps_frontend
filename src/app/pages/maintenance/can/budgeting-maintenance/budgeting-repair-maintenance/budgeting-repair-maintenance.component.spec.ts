import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairMaintenanceComponent } from './budgeting-repair-maintenance.component';

describe('BudgetingRepairMaintenanceComponent', () => {
  let component: BudgetingRepairMaintenanceComponent;
  let fixture: ComponentFixture<BudgetingRepairMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
