import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairEditDataComponent } from './budgeting-repair-edit-data.component';

describe('BudgetingRepairEditDataComponent', () => {
  let component: BudgetingRepairEditDataComponent;
  let fixture: ComponentFixture<BudgetingRepairEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
