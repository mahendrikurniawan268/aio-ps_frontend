import { Component, ViewChild, OnInit, ChangeDetectorRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';

@Component({
  selector: 'app-budgeting-maintenance',
  templateUrl: './budgeting-maintenance.component.html',
  styleUrls: ['./budgeting-maintenance.component.scss']
})
export class BudgetingMaintenanceComponent implements OnInit {

  @ViewChild('myModal') myModal: any; 
  
  currentPage = 1;
  itemsPerPage = 10;
  repairChart: any;
  dataYear:any;
  maintenanceChart: any;
  idBudgeting:any;
  overhaulChart: any;
  dataBudgeting:any [] = [];

  constructor(
    private route: ActivatedRoute, 
    private RestApiService : RestApiService,
    private authService: AuthenticationService,
    private cdRef: ChangeDetectorRef) {} 
  
  public idParam:any;

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  report_schedule:any;
  preData:any;
  userData:any;
  id_report_master : any;
  isAdmin:boolean = false;
  isSpv: boolean = false;

  async ngOnInit() {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    await this.getAllBudgeting();
  }

  async getAllBudgeting() {
    return new Promise((resolve, reject) => {
      this.RestApiService.getAllBudgeting().subscribe(
        (res: any) => {
          this.dataBudgeting = this.transformData(res.data);
          resolve(true);
        },
        (error: any) => {
          console.error(error);
          reject(error);
        }
      );
    });
  }

  onYearChange(event: any) {
    const selectedYear = event.target.value;
    const budgetingForSelectedMonth = this.dataBudgeting.find((budgeting: any) => budgeting.year === selectedYear);
  
    if (budgetingForSelectedMonth) {
      this.dataYear = budgetingForSelectedMonth;

    this._repairChart(this.dataYear, '["#4F709C", "#E5D283", "#FFF798"]');
    this._maintenanceChart(this.dataYear, '["#4F709C", "#E5D283", "#FFF798"]');
    this._overhaulChart(this.dataYear, '["#4F709C", "#E5D283", "#FFF798"]');
    }
  }  
  
  transformData(originalData: any[]) {
    const transformedData: { [key: string]: { [key: string]: { plan: Array<any>, actual: Array<any> } } } = {};
    
    const dataOri = originalData.filter(item => item.line === 'can')
    dataOri.forEach(item => {
      const year = item.year;
  
      if (!transformedData[year]) {
        transformedData[year] = {};
      }
  
      const category = item.category;
      
      if (!transformedData[year][category]) {
        transformedData[year][category] = {
          plan: [],
          actual: []
        };
      }
  
      transformedData[year][category].plan.push([
        item.januari_plan,
        item.februari_plan,
        item.maret_plan,
        item.april_plan,
        item.mei_plan,
        item.juni_plan,
        item.juli_plan,
        item.agustus_plan,
        item.september_plan,
        item.oktober_plan,
        item.november_plan,
        item.desember_plan,
      ]);
  
      transformedData[year][category].actual.push([
        item.januari_actual,
        item.februari_actual,
        item.maret_actual,
        item.april_actual,
        item.mei_actual,
        item.juni_actual,
        item.juli_actual,
        item.agustus_actual,
        item.september_actual,
        item.oktober_actual,
        item.november_actual,
        item.desember_actual,
      ]);
    });
  
    const resultArray = Object.keys(transformedData).map(year => ({
      year,
      data: transformedData[year]
    }));
  
    return resultArray;
  }  

  getCategoryKeys(data: any): string[] {
    return Object.keys(data);
  }

  private getChartColorsArray(colors:any) {
    colors = JSON.parse(colors);
    return colors.map(function (value:any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) {
            color = color.replace(" ", "");
            return color;
            }
            else return newValue;;
        } else {
            var val = value.split(',');
            if (val.length == 2) {
                var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
                rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
                return rgbaColor;
            } else {
                return newValue;
            }
        }
    });
  }

  _repairChart(dataYear:any, colors: any,) {
    colors = this.getChartColorsArray(colors);
    const actualData: number[] = [];
    const planData: number[] = [];

    const RepairData = dataYear.data.Repair;
    
    if (RepairData) {
        actualData.push(...RepairData.actual[0]);
        planData.push(...RepairData.plan[0]);
    };
        this.repairChart = {
            series: [{
                name: "Planned",
                data: planData,
            },
            {
                name: "Actual",
                data: actualData,
            },
            ],
            chart: {
                height: 350,
                type: "bar",
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: "45%",
                },
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                show: true,
                width: 2,
                colors: ["transparent"],
            },
            colors: colors,
            xaxis: {
                categories: [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec"
                ],
            },
            yaxis: {
                title: {
                    text: "Rp (Rupiah)",
                },
            },
            grid: {
                borderColor: "#f1f1f1",
            },
            fill: {
                opacity: 1,
            },
            tooltip: {
                y: {
                    formatter: function (val: any) {
                        return "Rp " + val.toFixed(0).replace(/\d(?=(\d{3})+$)/g, "$&,");
                    },
                },
            },
        };
    }

    _maintenanceChart(dataYear:any, colors: any,) {
        colors = this.getChartColorsArray(colors);
        const actualData: number[] = [];
        const planData: number[] = [];

        const MaintenanceData = dataYear.data.Maintenance;
        
        if (MaintenanceData) {
            actualData.push(...MaintenanceData.actual[0]);
            planData.push(...MaintenanceData.plan[0]);
        };
            this.maintenanceChart = {
                series: [{
                    name: "Planned",
                    data: planData,
                },
                {
                    name: "Actual",
                    data: actualData,
                },
                ],
                chart: {
                    height: 350,
                    type: "bar",
                    toolbar: {
                        show: false,
                    },
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: "45%",
                    },
                },
                dataLabels: {
                    enabled: false,
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ["transparent"],
                },
                colors: colors,
                xaxis: {
                    categories: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec"
                    ],
                },
                yaxis: {
                    title: {
                        text: "Rp (Rupiah)",
                    },
                },
                grid: {
                    borderColor: "#f1f1f1",
                },
                fill: {
                    opacity: 1,
                },
                tooltip: {
                    y: {
                        formatter: function (val: any) {
                            return "Rp " + val.toFixed(0).replace(/\d(?=(\d{3})+$)/g, "$&,");
                        },
                    },
                },
            };
        }

    
        _overhaulChart(dataYear:any, colors: any,) {
            colors = this.getChartColorsArray(colors);
            const actualData: number[] = [];
            const planData: number[] = [];

            const OverHaulData = dataYear.data.OverHaul;
            if (OverHaulData) {
                actualData.push(...OverHaulData.actual[0]);
                planData.push(...OverHaulData.plan[0]);
            };
                this.overhaulChart = {
                    series: [{
                        name: "Planned",
                        data: planData,
                    },
                    {
                        name: "Actual",
                        data: actualData,
                    },
                    ],
                    chart: {
                        height: 350,
                        type: "bar",
                        toolbar: {
                            show: false,
                        },
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                            columnWidth: "45%",
                        },
                    },
                    dataLabels: {
                        enabled: false,
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ["transparent"],
                    },
                    colors: colors,
                    xaxis: {
                        categories: [
                            "Jan",
                            "Feb",
                            "Mar",
                            "Apr",
                            "May",
                            "Jun",
                            "Jul",
                            "Aug",
                            "Sep",
                            "Oct",
                            "Nov",
                            "Dec"
                        ],
                    },
                    yaxis: {
                        title: {
                            text: "Rp (Rupiah)",
                        },
                    },
                    grid: {
                        borderColor: "#f1f1f1",
                    },
                    fill: {
                        opacity: 1,
                    },
                    tooltip: {
                        y: {
                            formatter: function (val: any) {
                                return "Rp " + val.toFixed(0).replace(/\d(?=(\d{3})+$)/g, "$&,");
                            },
                        },
                    },
                };
            }
  } 

  


