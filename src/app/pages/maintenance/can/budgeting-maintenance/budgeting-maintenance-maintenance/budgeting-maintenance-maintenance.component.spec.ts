import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenanceMaintenanceComponent } from './budgeting-maintenance-maintenance.component';

describe('BudgetingMaintenanceMaintenanceComponent', () => {
  let component: BudgetingMaintenanceMaintenanceComponent;
  let fixture: ComponentFixture<BudgetingMaintenanceMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenanceMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenanceMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
