import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenanceAddDataComponent } from './budgeting-maintenance-add-data.component';

describe('BudgetingMaintenanceAddDataComponent', () => {
  let component: BudgetingMaintenanceAddDataComponent;
  let fixture: ComponentFixture<BudgetingMaintenanceAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenanceAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenanceAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
