import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenanceEditDataComponent } from './budgeting-maintenance-edit-data.component';

describe('BudgetingMaintenanceEditDataComponent', () => {
  let component: BudgetingMaintenanceEditDataComponent;
  let fixture: ComponentFixture<BudgetingMaintenanceEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenanceEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenanceEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
