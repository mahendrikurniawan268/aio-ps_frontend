import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulMaintenanceComponent } from './budgeting-overhaul-maintenance.component';

describe('BudgetingOverhaulMaintenanceComponent', () => {
  let component: BudgetingOverhaulMaintenanceComponent;
  let fixture: ComponentFixture<BudgetingOverhaulMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
