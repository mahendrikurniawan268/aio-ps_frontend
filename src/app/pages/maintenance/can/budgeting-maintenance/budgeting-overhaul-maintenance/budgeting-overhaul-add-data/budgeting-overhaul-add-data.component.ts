import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { BudgetingMaintenance } from '../../budgeting-maintenance.model';

@Component({
  selector: 'app-budgeting-overhaul-add-data',
  templateUrl: './budgeting-overhaul-add-data.component.html',
  styleUrls: ['./budgeting-overhaul-add-data.component.scss']
})
export class BudgetingOverhaulAddDataComponent implements OnInit {

  newBudgetingMaintenance : BudgetingMaintenance = {
    year: '',
    januari_plan:  null,
    januari_actual:  0,
    februari_plan:  null,
    februari_actual:  0,
    maret_plan:  null,
    maret_actual:  0,
    april_plan:  null,
    april_actual:  0,
    mei_plan:  null,
    mei_actual:  0,
    juni_plan:  null,
    juni_actual:  0,
    juli_plan:  null,
    juli_actual:  0,
    agustus_plan:  null,
    agustus_actual:  0,
    september_plan:  null,
    september_actual:  0,
    oktober_plan:  null,
    oktober_actual:  0,
    november_plan:  null,
    november_actual:  0,
    desember_plan:  null,
    desember_actual:  0,
    category: 'OverHaul',
    line: 'can'
}

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idBudgeting: any;
  budgeting_maintenance: any;
  idParam: any;
  dataBudgeting: any;

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}
  
  ngOnInit(): void {
    this.getBudgeting();
    this.idParam = this.route.snapshot.paramMap.get('id');
    if (this.idParam) {
      this.getBudgetingById(this.idParam);
    }
  }

  forBack() {
    this.router.navigate(['/budgeting/overhaul/maintenance']);
  }

  onClick() {
    this.insertBudgeting(this.newBudgetingMaintenance);
    Swal.fire({
      title: 'Notification',
      text: 'Data Added Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    })
  }

  getBudgeting(){
    this.RestApiService.getAllBudgeting().subscribe(
      (res:any) => {
        this.dataBudgeting = res.data;        
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getBudgetingById(id:any){
    this.RestApiService.getBudgetingById(id).subscribe(
      (res:any) => {
        this.newBudgetingMaintenance = res.data[0];
      })
  }

  insertBudgeting(dataBudgeting: any) {
    this.RestApiService.insertBudgeting(dataBudgeting).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idBudgeting = res.data[0]
          this.budgeting_maintenance = this.dataBudgeting;
          this.budgeting_maintenance.id = this.idBudgeting;
          this.router.navigate([`/budgeting/overhaul/maintenance/edit-data/${this.idBudgeting}`])
          this.getBudgeting()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateBudgeting(dataBudgeting: any) {
    this.RestApiService.updateBudgeting(this.idParam, dataBudgeting).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.budgeting_maintenance = dataBudgeting
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdateBudgeting() {
    this.updateBudgeting(this.newBudgetingMaintenance);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
  } 
}
