import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulAddDataComponent } from './budgeting-overhaul-add-data.component';

describe('BudgetingOverhaulAddDataComponent', () => {
  let component: BudgetingOverhaulAddDataComponent;
  let fixture: ComponentFixture<BudgetingOverhaulAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
