import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { SubBudgeting } from '../../subbudgeting.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-budgeting-overhaul-edit-data',
  templateUrl: './budgeting-overhaul-edit-data.component.html',
  styleUrls: ['./budgeting-overhaul-edit-data.component.scss']
})
export class BudgetingOverhaulEditDataComponent implements OnInit {

  constructor(private router:Router,
    private route:ActivatedRoute,private RestApiService: RestApiService){}
  
  public details:any [] = [];
  dataMonth:any;
  budgeting_maintenance:any;
  dataFunctionLocation:any [] = [];
  dataFunctionLocationCan: any = [];
  idParam:any;
  data_subbudgeting:any;
  idSubBudgeting:any;
  budgetingByParamId:any;
  subbudgetingByBudgetingId:any;
  detailsBySubbudgetingId:any;
  dataPlus = 0
  isMonthSelected = false;

  newSubBudgeting : SubBudgeting = {
   month: '',
   id_budgeting: null,
  }

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getAllMonth();
    this.getAllFunctionLocation();
    this.getSubBudgetingByBudgetingId(this.idParam);
    this.getBudgetingByParamId(this.idParam);
    this.newSubBudgeting.id_budgeting = this.idParam;
  }

  forBack(event: Event) {
    this.router.navigate(['/budgeting/overhaul/maintenance']);
    event.preventDefault();
  }

  onMonthChange(event: any) {
    const selectedMonth = event.target.value;
    this.newSubBudgeting.month = selectedMonth;
    this.isMonthSelected = true
    const subBudgetingForSelectedMonth = this.subbudgetingByBudgetingId.find((subBudgeting: any) => subBudgeting.month === selectedMonth);
  
    if (subBudgetingForSelectedMonth) {
      this.idSubBudgeting = subBudgetingForSelectedMonth.id;
      this.details = [];
      this.getDetailByBudgetingId(this.idSubBudgeting);
      this.updateTable();
    } else {
      if (this.subbudgetingByBudgetingId.length > 0) {
        const newId = this.subbudgetingByBudgetingId[this.subbudgetingByBudgetingId.length - 1].id + 1;
        this.insertSubBudgeting({ ...this.newSubBudgeting, id: newId }, () => {
          this.getSubBudgetingByBudgetingId(this.idParam);
        });
      } else {
        this.insertSubBudgeting(this.newSubBudgeting, () => {
          this.getSubBudgetingByBudgetingId(this.idParam);
        });
      }
    }
  }  

  getAllMonth(){
    this.RestApiService.getAllMonth().subscribe(
    (res:any) => {
      this.dataMonth = res.data;
    })
  }

  getBudgetingByParamId(id:any){
    this.RestApiService.getBudgetingById(id).subscribe(
      (res:any) => {
        this.budgetingByParamId = res.data
      }
    )
  }

  getAllFunctionLocation(){
    this.RestApiService.getAllFunctionLocation().subscribe(
    (res:any) => {
      this.dataFunctionLocation = res.data;
      this.dataFunctionLocationCan = this.dataFunctionLocation.filter(item => item.line === 'can')
    })
  }

  getSubBudgetingByBudgetingId(id:any){
    this.RestApiService.getSubBudgetingByBudgetingId(id).subscribe(
      (res:any) => {
        this.subbudgetingByBudgetingId = res.data

        const selectedMonth = this.newSubBudgeting.month;
        const subBudgetingForSelectedMonth = this.subbudgetingByBudgetingId.find((subBudgeting: any) => subBudgeting.month === selectedMonth);
        if (subBudgetingForSelectedMonth) {
          this.idSubBudgeting = subBudgetingForSelectedMonth.id;
          this.getDetailByBudgetingId(this.idSubBudgeting);
        } else {
          this.details = [];
        }
      }
    )
  }

  getDetailByBudgetingId(id:any){
    this.RestApiService.getBudgetingDetailsBySubBudgetingId(id).subscribe(
      (res:any) => {
        this.detailsBySubbudgetingId = res.data
        this.details = this.detailsBySubbudgetingId;
        this.updateTable();
      }
    )
  }

  updateTable() {
  if (this.details && this.details.length > 0) {
    const filteredDetails = this.details.filter((newDetail: any) => newDetail.id_subbudgeting === this.idSubBudgeting);
    const mergedDetails = this.details.map((existingDetail: any) => {
      const newDetail = filteredDetails.find((newDetail: any) => newDetail.id === existingDetail.id);
      return newDetail ? { ...existingDetail, ...newDetail } : existingDetail;
    });
    this.details = mergedDetails;
  }
}

updateBudgeting(dataBudgeting: any) {
  this.RestApiService.updateBudgeting(this.idParam, dataBudgeting).subscribe(
    (res: any) => {
      if (res.status == true) {
        this.budgeting_maintenance = dataBudgeting
      }
    },
    (error) => {
      console.error(error)
    }
  )
}
  
  insertSubBudgeting(dataSubBudgeting: any, callback: () => void) {
    this.RestApiService.insertSubBudgeting(dataSubBudgeting).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idSubBudgeting = res.data[0]
          this.data_subbudgeting = dataSubBudgeting;
          this.data_subbudgeting.id = this.idSubBudgeting
          callback();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  async addDetails() {
    if (this.details.length === 0) {
      this.details.push({
        id_subbudgeting: this.idSubBudgeting,
        function_location: '',
        w1: '',
        w2: '',
        w3: '',
        w4: '',
        index: 1,
        isUpdated: false
      });
    } else {
      await Promise.all(this.details.map((detail: any) => {
        if (!detail.isUpdated) {
          return this.insertDetails(detail);
        } else {
          return Promise.resolve();
        }
      }));
      const newIndex = this.details.length + 1;
      this.details.push({
        id_subbudgeting: this.idSubBudgeting,
        function_location: '',
        w1: '',
        w2: '',
        w3: '',
        w4: '',
        index: newIndex,
        isUpdated: false
      });
    }
  }
    
  insertDetails(detail: any) {
    let dataDetails = {
      function_location: detail.function_location,
      w1: detail.w1,
      w2: detail.w2,
      w3: detail.w3,
      w4: detail.w4,
      id_subbudgeting: this.idSubBudgeting
    };
  
    return new Promise<void>((resolve, reject) => {
      if (detail.id) {
        this.RestApiService.updateBudgetingDetails(detail.id, dataDetails).subscribe(
          (res: any) => {
            if (res.status == true) {
              detail.isUpdated = true;
              resolve();
            } else {
              reject('Update failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      } else {
        this.RestApiService.insertBudgetingDetails(dataDetails).subscribe(
          (res: any) => {
            if (res.status == true) {
              detail.id = res.data[0];
              detail.isUpdated = true;
              resolve();
            } else {
              reject('Insert failed');
            }
          },
          (error) => {
            console.error(error);
            reject(error);
          }
        );
      }
    });
  }
    
  
  async onSubmit() {
    this.details.sort((a, b) => a.index - b.index);
    await Promise.all(this.details.map((detail: any) => this.insertDetails(detail)));
    this.updateBudgetingWithCombinedData();
  
    Swal.fire({
      title: 'Notification',
      text: 'Successfully!',
      icon: 'success',
      showConfirmButton: true,
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then(() => {
      this.router.navigate(['/budgeting/overhaul/maintenance']);
    });
  }
    
    updateBudgetingWithCombinedData() {
      this.dataPlus = 0;
    
      this.details.forEach((detail: any) => {
        const parseOrDefault = (value: any) => {
          const parsedValue = parseInt(value);
          return isNaN(parsedValue) ? 0 : parsedValue;
        };
    
        const w1 = parseOrDefault(detail.w1);
        const w2 = parseOrDefault(detail.w2);
        const w3 = parseOrDefault(detail.w3);
        const w4 = parseOrDefault(detail.w4);
    
        this.dataPlus += w1 + w2 + w3 + w4;
      });
    
      const monthToUpdate = this.newSubBudgeting.month.toLowerCase() + '_actual';
      const budgetingToUpdate = this.budgetingByParamId.find((budgeting: any) => budgeting[monthToUpdate] !== null);
    
      if (budgetingToUpdate) {
        budgetingToUpdate[monthToUpdate] = this.dataPlus;
        this.updateBudgeting(budgetingToUpdate);
      }
    }

  updateSubBudgeting(dataSubBudgeting: any, callback: () => void) {
    this.RestApiService.updateSubBudgeting(this.idSubBudgeting, dataSubBudgeting).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idSubBudgeting = res.data[0];
          this.data_subbudgeting = dataSubBudgeting;
          this.data_subbudgeting.id = this.idSubBudgeting;
          callback();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateDetails() {
    this.details.forEach((detail: any) => {
      if (detail.isUpdated) {
        this.updateDetail(detail);
      } else {
        this.insertDetails(detail);
      }
    });
  }

  updateDetail(detail: any) {
    let dataDetails = {
      function_location: detail.function_location,
      w1: detail.w1,
      w2: detail.w2,
      w3: detail.w3,
      w4: detail.w4,
      id_subbudgeting: this.idSubBudgeting,
    };
  
    this.RestApiService.updateBudgetingDetails(detail.id, dataDetails).subscribe(
      (res: any) => {
        if (res.status == true) {
          detail.isUpdated = true;
          Swal.fire({
            title: 'Notification',
            text: 'Update Data Successfully!',
            icon: 'success',
            timer: 1500,
            showConfirmButton: false,
            customClass: {
              popup: 'custom-swal-text',
            },
          });
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  trackByFunction(index: number, item: any): any {
    return index;
}
  
}
