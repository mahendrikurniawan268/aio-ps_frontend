import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulEditDataComponent } from './budgeting-overhaul-edit-data.component';

describe('BudgetingOverhaulEditDataComponent', () => {
  let component: BudgetingOverhaulEditDataComponent;
  let fixture: ComponentFixture<BudgetingOverhaulEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
