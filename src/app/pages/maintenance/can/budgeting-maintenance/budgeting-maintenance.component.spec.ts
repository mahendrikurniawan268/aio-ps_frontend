import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenanceComponent } from './budgeting-maintenance.component';

describe('BudgetingMaintenanceComponent', () => {
  let component: BudgetingMaintenanceComponent;
  let fixture: ComponentFixture<BudgetingMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
