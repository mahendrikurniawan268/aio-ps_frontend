import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SheduleMasterPlanComponent } from './shedule-master-plan.component';

describe('SheduleMasterPlanComponent', () => {
  let component: SheduleMasterPlanComponent;
  let fixture: ComponentFixture<SheduleMasterPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SheduleMasterPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SheduleMasterPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
