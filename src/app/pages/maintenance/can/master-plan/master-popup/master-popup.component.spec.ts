import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPopupComponent } from './master-popup.component';

describe('MasterPopupComponent', () => {
  let component: MasterPopupComponent;
  let fixture: ComponentFixture<MasterPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MasterPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
