import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'; 
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ActivityGroup } from '../master-add/activity-group.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-master-popup',
  templateUrl: './master-popup.component.html',
  styleUrls: ['./master-popup.component.scss']
})
export class MasterPopupComponent implements OnInit {
  @Input() activityData: any = {}

  constructor(private RestApiService: RestApiService,  
    private activeModal: NgbActiveModal,
    private router: Router, private authService:AuthenticationService,
    private route:ActivatedRoute,
    private modalService: NgbModal) {}
  
    public activity_group: any = {};
    userData:any;
    isAdmin: boolean = false;
    isSpv: boolean = false;
    isPlanner: boolean = false;
    isEmployee: boolean = false;
    idLine: any;
    idType: any;
    idCategory: any;
    idSubcategory: any
    idTask:any;

    newActivityGroup: ActivityGroup = {
      id_line: null,
      function_location: null,
      date: '',
      pic: '',
      email: '',
      equipment: '',
      id_type: null,
      id_category: null,
      id_subcategory: null,
      notes: ''
    };
  
    ngOnInit(): void {
      this.route.queryParams.subscribe({next: (params) => {
        this.idLine = params['lineId'], this.idType = params['typeId'], this.idCategory = params['categoryId'], this.idSubcategory = params['subCategoryId'];
      }})
      this.userData = this.authService.getUserData();
      this.isAdmin = this.authService.isAdmin();
      this.isSpv = this.authService.isSpv();
      this.isPlanner = this.authService.isPlanner();
      this.isEmployee = this.authService.isEmployee();
    }

  getActivityGroupById(id:any){
    this.RestApiService.getActivityGroupById(id).subscribe(
    (res:any) => {
      this.activityData = res.data[0]
    })
  }

  onApprove(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to approve this schedule?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, Approve it!',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.isConfirmed) {
        const approveData = { id_status: 2 };
        this.updateActivityGroup(approveData, id);
        this.activeModal.close();
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}});
        window.location.reload();
      }
    });
  }
  
  onDone(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Is The Task Finished?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const approveData = { id_status: 3 };
        this.updateActivityGroup(approveData, id);
        this.activeModal.close();
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}});
        window.location.reload();
      }
    });
  }

  updateActivityGroup(dataActivityGroup: any, id:any) {
    this.RestApiService.updateActivityGroup(id, dataActivityGroup).subscribe(
      (res: any) => {
        if (res.id_status == true) {
          
          this.activity_group = dataActivityGroup;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onClose(){
    this.activeModal.close();
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }

  clearFormData(){
    this. newActivityGroup = {
      id_line: null,
      function_location: null,
      date: '',
      pic: '',
      email: '',
      equipment: '',
      id_type: null,
      id_category: null,
      id_subcategory: null,
      notes: ''
   }
  }

   /**
 * Open modal
 * @param revisi modal content
 */
   revisiTask(revisi: any, task:any, id: any) {
    this.clearFormData();

    this.newActivityGroup = { ...task };
    console.log(this.newActivityGroup);

    this.idTask = id;
    console.log(this.idTask);
    
    this.modalService.open(revisi, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  onRevision(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to revisi this task?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, Revisi it!',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.isConfirmed) {
        const reviseData = {
          id_status: 5,
          notes: this.newActivityGroup.notes
        };
        this.updateActivityGroup(reviseData, id);
        this.activeModal.close();
      }
      this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}});
      window.location.reload();
    });
  }
}
