import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMasterPlanAddDataComponent } from './report-master-plan-add-data.component';

describe('ReportMasterPlanAddDataComponent', () => {
  let component: ReportMasterPlanAddDataComponent;
  let fixture: ComponentFixture<ReportMasterPlanAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportMasterPlanAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportMasterPlanAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
