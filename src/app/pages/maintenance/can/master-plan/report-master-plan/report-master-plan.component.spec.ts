import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMasterPlanComponent } from './report-master-plan.component';

describe('ReportMasterPlanComponent', () => {
  let component: ReportMasterPlanComponent;
  let fixture: ComponentFixture<ReportMasterPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportMasterPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportMasterPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
