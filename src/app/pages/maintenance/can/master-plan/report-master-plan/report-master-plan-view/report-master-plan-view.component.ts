import { Component, OnInit, ElementRef, ViewChild, Renderer2  } from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import { ReportMaster } from '../report-master.model';
import { ReportMasterPlan } from '../report-master-plan.model';
import { environment } from 'src/environments/environment';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-report-master-plan-view',
  templateUrl: './report-master-plan-view.component.html',
  styleUrls: ['./report-master-plan-view.component.scss']
})
export class ReportMasterPlanViewComponent implements OnInit{
  @ViewChild('content') content!:ElementRef <any>;

  newReportMaster : ReportMaster = {
    id_area: null,
    pic: '',
    email: '',
    spv: 'Alfan Aditama',
    email_spv: 'mahendrikurniawan268@gmail.com',
    equipment: '',
    wo_number: '',
    description: '',
    start_date: '',
    end_date: '',
    type: 'master plan',
    id_line: null,
  }

  newReportMasterPlan : ReportMasterPlan = {
    activity: '',
    date: '',
    start_time: '',
    end_time: '',
    verification: '',
    documentation_before: null,
    documentation_after: null,
    id_report_master: null
  }

  public id_report_master : any
  reportMasterPlanData:any
  idLine:any;
  dataReport:any = []

  constructor(private RestApiService : RestApiService, 
    private route : ActivatedRoute, private router:Router, private renderer: Renderer2, private lightbox:Lightbox){}

    ngOnInit(): void {
      this.route.queryParams.subscribe((params) => {
        const idParam = params['idParam'];      
        if (idParam) {
          this.idLine = params['lineId'];
          this.getReportMasterPlanByReportMasterId(idParam);
          this.getReportMasterById(idParam);
        }
      });
     }
    
     getActivityWithLineBreaks(activity: string): string {
      if (activity) {
          return activity.replace(/,\s*/g, '<br><br>');
      }
      return '';
    }

     openLightboxBefore(imageSrc: string): void {
      const image = [{
        src: imageSrc,
        caption: 'Documentation Before',
        thumb: '' 
      }];
      this.lightbox.open(image, 0, {
        showDownloadButton: true,
        showZoom: true
      });
    }

    openLightboxAfter(imageSrc: string): void {
      const image = [{
        src: imageSrc,
        caption: 'Documentation After',
        thumb: '' 
      }];
      this.lightbox.open(image, 0, {
        showDownloadButton: true,
        showZoom: true
      });
    }

    getImgFileBefore(file: any) {
      return environment.API_URL + '/image/' + file
    }

    getImgFileAfter(file: any) {
      return environment.API_URL + '/image/' + file
    }
    
     getBase64Image(img: any) {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      var ctx: any = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      var dataURL = canvas.toDataURL("image/png");
      return dataURL;
    }

  getReportMasterPlanByReportMasterId(id: any) {
    this.RestApiService.getReportMasterPlanByReportMasterId(id).subscribe((res: any) => {
      this.reportMasterPlanData = res.data;
      
    });
  }
  

  getReportMasterById(id:any){
    this.RestApiService.getReportById(id).subscribe(
    (res:any) => {
      this.dataReport = res.data[0]
    })
  }

  getReportMasterPlanById(id:any){
    this.RestApiService.getReportMasterPlanById(id).subscribe(
    (res:any) => {
      this.newReportMasterPlan = res.data[0];
    })
  }

  async exportToPdf() {
    //start converting to base64
    const imgElements = this.content.nativeElement.querySelectorAll('img');
    let items:any [] = [];

    for (let i = 0; i < 2; i++) {
      for (let element of imgElements) {
        let item: any = {};
        item.image_url = element.getAttribute('src');
        item = await this.convertImageToBase64(item);
        items.push(item);
        this.renderer.setAttribute(element, 'src', item.base64_image);
      }
    }

    const allImagesConverted = items.every((item) => item.base64_image);

    if (allImagesConverted) {
      const pdf = new jsPDF();
      const content = this.content.nativeElement;
  
      html2canvas(content).then((canvas) => {
        const imgData = canvas.toDataURL('image/png');
        
        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = pdf.internal.pageSize.getHeight();
  
        const imageWidth = canvas.width;
        const imageHeight = canvas.height;
  
        const scale = Math.min(pdfWidth / imageWidth, pdfHeight / imageHeight);
  
        const x = 0;
        const y = 10; 
  
        pdf.addImage(imgData, 'PNG', x, y, imageWidth * scale, imageHeight * scale);
  
        pdf.save('report.pdf');
      });
    } else {
      // Atau tambahkan pesan lain sesuai kebutuhan jika tidak semua gambar sudah dikonversi
    }
  }

  forBack(){
    this.router.navigate(['report/list-report'], { queryParams:{lineId: this.idLine}})
  }

  getBase64FromUrl = async () => {
    const data = await fetch(`${environment.API_URL}/image/rt-whireframe%20login%20(2)-1698310707857-486104632.png`);
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob); 
      reader.onloadend = () => {
        const base64data = reader.result;   
        resolve(base64data);
      }
    });
  }

  async convertImageToBase64(item: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if (item && item.image_url) {
        const img = new Image();
        img.setAttribute('crossOrigin', 'anonymous');
        img.src = item.image_url;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          canvas.width = img.width;
          canvas.height = img.height;

          const ctx = canvas.getContext('2d');
          if (ctx) {
            ctx.drawImage(img, 0, 0);

            // Convert the image to base64
            const base64Image = canvas.toDataURL('image/png');

            // Store the base64 image in a new property
            item.base64_image = base64Image;
            // Assuming 'base64_image' is the new property to store base64 data
            
            resolve(item)
          }
        };
      } else {
        reject(false);
      }
    });
  }

  getSection(id_area: number): string {
    switch (id_area) {
        case 1:
            return 'Preparation';
        case 2:
            return 'Filling';
        case 3:
            return 'Packing';
        case 4:
            return 'Preparation';
        case 5:
            return 'Injection';
        case 6:
            return 'Blow';
        case 7:
            return 'Filling';
        case 8:
            return 'Packing';
        default:
            return 'Unknown';
    }
  }
}


