import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMasterPlanViewComponent } from './report-master-plan-view.component';

describe('ReportMasterPlanViewComponent', () => {
  let component: ReportMasterPlanViewComponent;
  let fixture: ComponentFixture<ReportMasterPlanViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportMasterPlanViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportMasterPlanViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
