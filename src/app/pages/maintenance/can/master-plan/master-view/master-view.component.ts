import { Component, ViewChild, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { EventService } from 'src/app/core/services/event.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { MasterPopupComponent } from '../master-popup/master-popup.component';

interface OperationTask {
  task: string;
  descriptions: string[];
}

interface Item {
  function_location: string;
  operation_tasks: OperationTask[];
  pic: string;
  id_status: number;
  date: Date;
}

@Component({
  selector: 'app-master-view',
  templateUrl: './master-view.component.html',
  styleUrls: ['./master-view.component.scss']
})
export class MasterViewComponent {
  tableColumns = ['No', 'Function Location', 'Operation Task', 'Description Material', 'Pic', 'Status', 'Date', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  activityData: any [] = [];
  activityDataByLine: any [] = [];

  @ViewChild('myModal') myModal: any; 

  constructor(private modalService: NgbModal, private route: ActivatedRoute, 
    private router:Router, private RestApiService : RestApiService,
    private authService: AuthenticationService) {} 
  
  public activities:any = [];
  public activity_group: any = {};
  public idParam:any;
  public material:any = [];
  public activitySaveData: boolean[] = [];

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  idMaterial:any;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isEmployee:boolean = false;
  isPlanner: boolean = false;
  currentPage = 1;
  itemsPerPage = 10;
  idLine: any;
  idType: any;
  idCategory: any;
  idSubcategory: any;
  masterData: any [] = [];

  ngOnInit(): void {
      this.route.queryParams.subscribe({next: (params) => {
        this.idLine = params['lineId'], this.idType = params['typeId'], this.idCategory = params['categoryId'], this.idSubcategory = params['subCategoryId'];
        this.getTabelView();
      }})
      this.userData = this.authService.getUserData();
      this.isAdmin = this.authService.isAdmin();
      this.isSpv = this.authService.isSpv();
      this.isPlanner = this.authService.isPlanner();
      this.isEmployee = this.authService.isEmployee();
  }

  listReport(){
    this.router.navigate(['report/list-report'], {queryParams: {lineId: this.idLine}})
  }

  openPopup(data:any) {
    const modalRef = this.modalService.open(MasterPopupComponent, { centered: true });
    modalRef.componentInstance.activityData = data;
    modalRef.result.then(
      (result) => {
      },
      (reason) => {
        
      }
    );
  } 

  addData() {
  this.router.navigate(['maintenance/master-plan/add'], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}});
  }

  extraTasks(currentTasksCount: number): any[] {
    const maxTasksPerRow = 2;
    const remainingTasks = maxTasksPerRow - currentTasksCount;
    if (remainingTasks > 0) {
        return new Array(remainingTasks).fill(null);
    } else {
        return [];
    }
}

  getTabelView() {
    this.RestApiService.getTabelViewMasterPlan().subscribe(
      (res: any) => {
        this.activityData = res.data;
        this.activityDataByLine = this.activityData.filter(item => 
          item.id_line == this.idLine && 
          item.id_type == this.idType && 
          item.id_category == this.idCategory && 
          item.id_subcategory == this.idSubcategory
        );

        this.dataForCurrentPage = this.activityDataByLine.map(item => {
          const tasks = item.operation_task.split('\n');
          const descriptions = item.description_material.split('\n');
          const operation_tasks: OperationTask[] = [];

          tasks.forEach((task: string, index: number) => {
            const taskDescriptions = descriptions.filter((desc: string) => desc.includes(`(${index + 1})`));
            operation_tasks.push({ task, descriptions: taskDescriptions });
          });

          return {
            function_location: item.function_location,
            operation_tasks: operation_tasks,
            pic: item.pic,
            id_status: item.id_status,
            date: new Date(item.date)
          };
        });
        console.log(this.dataForCurrentPage);
        

        this.preData = [...this.activityDataByLine];
        this.masterData = [...this.activityDataByLine];
        this.paginateData();
        this.checkAndSetLateStatus();
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  updateActivityGroup(dataActivityGroup: any, id:any) {
    this.RestApiService.updateActivityGroup(id, dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.activity_group = dataActivityGroup;
          this.getTabelView()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  checkAndSetLateStatus() {
    const currentDate = new Date(); 
    for (const data of this.masterData) {
      if (data.id_status === 2 || data.id_status === 1) { 
        const scheduledDate = new Date(data.date); 
        scheduledDate.setDate(scheduledDate.getDate() + 2);
        if (scheduledDate < currentDate) {
          data.id_status = 4;
  
          this.RestApiService.updateActivityGroup(data.id_activity_group, { id_status: 4 }).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.getTabelView();
              }
            },
            (error) => {
              console.error(error);
            }
          );
        }
      }
    }
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateActivityGroup(deleteData, id);
      }
    });
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        case 5:
          return 'primary'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Pending';
        case 5:
          return 'Revisi';
        default:
            return 'Unknown';
    }
}

onReport(){
  this.router.navigate(['report/master-plan'], {queryParams: {lineId: this.idLine}})
}

editData(id:any){
  this.router.navigate([`maintenance/master-plan/edit/${id}`], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}})
}

search() {
  this.activityDataByLine = this.preData.filter((item: { function_location: string; operation_task: string; description_material: string; pic:string;}) => {
    return item.function_location.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.description_material.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText.toLowerCase());
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.activityDataByLine.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}
  
}