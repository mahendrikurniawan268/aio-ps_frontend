import { Component, ViewChild, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-report',
  templateUrl: './list-report.component.html',
  styleUrls: ['./list-report.component.scss']
})
export class ListReportComponent implements OnInit {
  tableColumns = ['No', 'Section', 'Wo Number', 'Pic', 'Description of Work', 'Date', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  reportData: any [] = [];

  @ViewChild('myModal') myModal: any; 
  
  currentPage = 1;
  itemsPerPage = 10;

  constructor(private modalService: NgbModal, 
    private route: ActivatedRoute, 
    private router:Router, 
    private RestApiService : RestApiService,
    private authService: AuthenticationService) {} 
  
  public idParam:any;

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  searchText:string = '';
  report_schedule:any;
  dataForCurrentPage: any[] = [];
  preData:any;
  userData:any;
  id_report_master : any;
  isAdmin:boolean = false;
  isSpv: boolean = false;
  idLine:any;
  reportMasterplan:any [] = [];

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getReport();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  onReport(){
    this.router.navigate(['report/master-plan'], {queryParams: {lineId: this.idLine}})
  }

  updateReport(dataReportMaster: any, id:any) {
    this.RestApiService.updateReportMaster(id, dataReportMaster).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.report_schedule = dataReportMaster;
          this.getReport()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateReport(deleteData, id);
      }
    });
  }

  getReport(): void {
    this.RestApiService.getReportByAreaLine().subscribe(
      (res: any) => {
        this.reportData = res.data[0];
        this.reportMasterplan = this.reportData.filter(item => item.type === 'master plan' && item.id_line == this.idLine);
        this.preData = [...this.reportMasterplan];
        this.paginateData();
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

viewReport(id:any){
  this.router.navigate([`report/master-plan/view`], {queryParams: { idParam: id, lineId: this.idLine}});
}

editReport(id:any){
  this.router.navigate([`report/master-plan/edit-data/${id}`], { queryParams: { lineId: this.idLine }})
}

search() {
  this.reportMasterplan = this.preData.filter((item: { pic: string; section: string; wo_number: string; 
  description: string; start_date: string; }) => {
    const month = this.extractMonthFromDate(item.start_date);
    return (month && month.toLowerCase().includes(this.searchText.toLowerCase())) ||
           item.pic.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.wo_number.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.description.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.section.toLowerCase().includes(this.searchText.toLowerCase());
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.reportMasterplan.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}

extractMonthFromDate(dateString: string): string {
  const date = new Date(dateString);
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', '-August', 'September', 'October', 'November', 'December'
  ];
  return monthNames[date.getMonth()];
}

onSort(event: any) {
  const sortBy = event.target.getAttribute('listsortable');
  if (sortBy) {
    this.reportMasterplan.sort((a: any, b: any) => {
      const valueA = a[sortBy] ? a[sortBy] : '';
      const valueB = b[sortBy] ? b[sortBy] : '';
      if (valueA < valueB) {
        return -1;
      }
      if (valueA > valueB) {
        return 1;
      }
      return 0;
    });
    this.paginateData();
  }
}
  
}

