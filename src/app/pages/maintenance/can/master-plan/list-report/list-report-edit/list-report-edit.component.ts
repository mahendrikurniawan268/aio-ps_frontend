import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ReportMaster } from '../../report-master-plan/report-master.model';
import { ReportMasterPlan } from '../../report-master-plan/report-master-plan.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-report-edit',
  templateUrl: './list-report-edit.component.html',
  styleUrls: ['./list-report-edit.component.scss']
})
export class ListReportEditComponent {
  
  public report_master:any = [];
  id_report_master:any;
  selectedFiles: { file: File, type: string }[] = [];
  imageUrl: any;
  report_schedule: any;
  idParam:any;
  reportMasterPlanData: any;
  id_report_master_plan:any;
  report_master_plan:any;
  namePic: any [] = [];
  namePicEmployee: any;
  idLine:any;
  dataArea: any [] = []
  area: any;

  newReportMaster : ReportMaster = {
    id_area: null,
    pic: '',
    email: '',
    spv: 'Alfan Aditama',
    email_spv: 'mahendrikurniawan268@gmail.com',
    equipment: '',
    wo_number: '',
    description:'',
    start_date: '',
    end_date: '',
    type: 'master plan',
    id_line: null,
  }

  newReportMasterPlan : ReportMasterPlan = {
    activity: '',
    date: '',
    start_time: '',
    end_time: '',
    verification: '',
    documentation_before: null,
    documentation_after: null,
    id_report_master: null
  }

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private http: HttpClient,
    private route:ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    this.getArea();
    this.getNamePic();
    this.getReportById(this.idParam);
    this.getReportMasterPlanByReportMasterId(this.idParam);
    }

    getArea(){
      this.RestApiService.getAllArea().subscribe(
        (res: any) => {
          this.dataArea = res.data;
          this.area = this.dataArea.filter(item => item.id_line == this.idLine)
        },
        (error: any) => {
          console.error(error);
        }
      );
    }

    getNamePic(){
      this.RestApiService.getNamePic().subscribe(
        (res: any) => {
          this.namePic = res.data;
          this.namePicEmployee = this.namePic.filter(item => item.role_id == 3 || item.role_id == 4)
        },
        (error: any) => {
          console.error(error);
        }
      );
    }

    onAddData(){
      this.router.navigate([`report/master-plan/add-data/${this.idParam}`], {queryParams: {lineId: this.idLine}})
    }

    updateReport(dataReportMaster: any) {
      this.RestApiService.updateReportMaster(this.idParam, dataReportMaster).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.report_schedule = dataReportMaster;
          }
        },
        (error) => {
          console.error(error)
        }
      )
    }
  
    // onUpdateReport() {
    //   this.updateReport(this.report_master);
    //   Swal.fire({
    //     title: 'Notification',
    //     text: 'Data Updated Successfully!',
    //     icon: 'success',
    //     timer: 1500,
    //     showConfirmButton: false,
    //     customClass: {
    //       popup: 'custom-swal-text', 
    //     },
    //   });
    // } 

  forBack() {
    this.router.navigate(['report/list-report'], {queryParams: {lineId: this.idLine}});
  }

  onFileSelected(event: any, type: string) {
    const file = event.target.files[0];
    this.selectedFiles.push({ file, type });
  }

  updateMasterPlan(dataReportMasterPlan: any, index: number) {
    if (this.selectedFiles.length > 0) {
      const formData = new FormData();
  
      this.selectedFiles.forEach(fileData => {
      formData.append('files', fileData.file, fileData.file.name);
      formData.append('type', fileData.type);
      });
  
      this.RestApiService.uploadMultipleImage(formData).subscribe({
        next: (res: any) => {
          this.imageUrl = res.uploadedFiles;
          
          const beforeFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-before');
          const afterFileIndex = this.selectedFiles.findIndex(file => file.type === 'documentation-after');

          if (beforeFileIndex !== -1) {
            dataReportMasterPlan.documentation_before = res.uploadedFiles[beforeFileIndex];
        }
        if (afterFileIndex !== -1) {
            dataReportMasterPlan.documentation_after = res.uploadedFiles[afterFileIndex];
        }
  
          this.RestApiService.updateReportMasterPlan( dataReportMasterPlan.id, dataReportMasterPlan).subscribe(
            (res: any) => {
              if (res.status == true) {
                this.reportMasterPlanData[index] = { ...dataReportMasterPlan };
              }
            },
            (error) => {
              console.error(error);
            }
          );
        },
        error: (err) => console.error(err),
        complete: () => {
          this.selectedFiles = [];
        }
      });
    } else {
      const id_report_master_plan = {...dataReportMasterPlan}
      delete id_report_master_plan["id"]
      this.RestApiService.updateReportMasterPlan(dataReportMasterPlan.id, id_report_master_plan).subscribe(
        (res: any) => {
          if (res.status == true) {
            this.reportMasterPlanData[index] = { ...dataReportMasterPlan };
          }
        },
        (error) => {
          console.error(error);
        }
      );
    }
  } 
  
  getReportById(id:any){
    this.RestApiService.getReportById(id).subscribe(
    (res:any) => {
      this.report_master = res.data[0];
      this.report_master.start_date = this.convertDate(this.report_master.start_date);
      this.report_master.end_date = this.convertDate(this.report_master.end_date);
    })
  }

  getReportMasterPlanByReportMasterId(id: any) {
    this.RestApiService.getReportMasterPlanByReportMasterId(id).subscribe((res: any) => {
      this.reportMasterPlanData = res.data.filter((entry: any) => entry.id);
      this.reportMasterPlanData.forEach((plan: any) => {
        plan.date = this.convertDate(plan.date);
      });
    });
  }

  getReportMasterPlanData(id: any) {
    this.RestApiService.getReportMasterPlanById(id).subscribe((res: any) => {
      this.reportMasterPlanData = res.data;
    });
  }
  
  onUpdate(index:any) {
    const updatedPlan = {...this.reportMasterPlanData[index]};
    this.updateMasterPlan(updatedPlan, index);
    this.updateReport(this.report_master);
    Swal.fire({
      icon: 'success',
      title: 'Update Successfully',
      text: 'Update has been successfully.',
      timer: 1500,
      showConfirmButton: false,
    });
}

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}

