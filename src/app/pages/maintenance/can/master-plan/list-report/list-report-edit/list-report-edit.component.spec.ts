import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListReportEditComponent } from './list-report-edit.component';

describe('ListReportEditComponent', () => {
  let component: ListReportEditComponent;
  let fixture: ComponentFixture<ListReportEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListReportEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListReportEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
