import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecOverAddDataComponent } from './packing-elec-over-add-data.component';

describe('PackingElecOverAddDataComponent', () => {
  let component: PackingElecOverAddDataComponent;
  let fixture: ComponentFixture<PackingElecOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
