import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecPrevPopupComponent } from './packing-elec-prev-popup.component';

describe('PackingElecPrevPopupComponent', () => {
  let component: PackingElecPrevPopupComponent;
  let fixture: ComponentFixture<PackingElecPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
