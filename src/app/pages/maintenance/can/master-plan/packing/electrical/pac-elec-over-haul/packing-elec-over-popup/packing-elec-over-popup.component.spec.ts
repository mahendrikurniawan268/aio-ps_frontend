import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecOverPopupComponent } from './packing-elec-over-popup.component';

describe('PackingElecOverPopupComponent', () => {
  let component: PackingElecOverPopupComponent;
  let fixture: ComponentFixture<PackingElecOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
