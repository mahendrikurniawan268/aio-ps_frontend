import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecOverMaterialComponent } from './packing-elec-over-material.component';

describe('PackingElecOverMaterialComponent', () => {
  let component: PackingElecOverMaterialComponent;
  let fixture: ComponentFixture<PackingElecOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
