import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecCorAddDataComponent } from './packing-elec-cor-add-data.component';

describe('PackingElecCorAddDataComponent', () => {
  let component: PackingElecCorAddDataComponent;
  let fixture: ComponentFixture<PackingElecCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
