import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecPrevAddDataComponent } from './packing-elec-prev-add-data.component';

describe('PackingElecPrevAddDataComponent', () => {
  let component: PackingElecPrevAddDataComponent;
  let fixture: ComponentFixture<PackingElecPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
