import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecPrevEditDataComponent } from './packing-elec-prev-edit-data.component';

describe('PackingElecPrevEditDataComponent', () => {
  let component: PackingElecPrevEditDataComponent;
  let fixture: ComponentFixture<PackingElecPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
