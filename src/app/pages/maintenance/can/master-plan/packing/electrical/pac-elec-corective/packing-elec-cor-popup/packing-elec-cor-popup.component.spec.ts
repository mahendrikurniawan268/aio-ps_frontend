import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecCorPopupComponent } from './packing-elec-cor-popup.component';

describe('PackingElecCorPopupComponent', () => {
  let component: PackingElecCorPopupComponent;
  let fixture: ComponentFixture<PackingElecCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
