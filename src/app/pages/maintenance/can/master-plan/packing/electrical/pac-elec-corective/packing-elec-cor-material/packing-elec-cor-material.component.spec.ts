import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecCorMaterialComponent } from './packing-elec-cor-material.component';

describe('PackingElecCorMaterialComponent', () => {
  let component: PackingElecCorMaterialComponent;
  let fixture: ComponentFixture<PackingElecCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
