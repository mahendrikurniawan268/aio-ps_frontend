import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecCorEditDataComponent } from './packing-elec-cor-edit-data.component';

describe('PackingElecCorEditDataComponent', () => {
  let component: PackingElecCorEditDataComponent;
  let fixture: ComponentFixture<PackingElecCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
