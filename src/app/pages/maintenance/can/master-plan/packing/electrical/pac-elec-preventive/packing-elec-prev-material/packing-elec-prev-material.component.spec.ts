import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecPrevMaterialComponent } from './packing-elec-prev-material.component';

describe('PackingElecPrevMaterialComponent', () => {
  let component: PackingElecPrevMaterialComponent;
  let fixture: ComponentFixture<PackingElecPrevMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecPrevMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecPrevMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
