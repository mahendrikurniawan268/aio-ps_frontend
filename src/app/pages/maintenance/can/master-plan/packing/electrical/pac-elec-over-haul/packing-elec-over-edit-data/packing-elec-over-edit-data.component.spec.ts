import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingElecOverEditDataComponent } from './packing-elec-over-edit-data.component';

describe('PackingElecOverEditDataComponent', () => {
  let component: PackingElecOverEditDataComponent;
  let fixture: ComponentFixture<PackingElecOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingElecOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingElecOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
