import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaOverPopupComponent } from './packing-mecha-over-popup.component';

describe('PackingMechaOverPopupComponent', () => {
  let component: PackingMechaOverPopupComponent;
  let fixture: ComponentFixture<PackingMechaOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
