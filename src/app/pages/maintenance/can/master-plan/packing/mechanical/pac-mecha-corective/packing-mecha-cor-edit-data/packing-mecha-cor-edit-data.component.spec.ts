import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaCorEditDataComponent } from './packing-mecha-cor-edit-data.component';

describe('PackingMechaCorEditDataComponent', () => {
  let component: PackingMechaCorEditDataComponent;
  let fixture: ComponentFixture<PackingMechaCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
