import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaPrevEditDataComponent } from './packing-mecha-prev-edit-data.component';

describe('PackingMechaPrevEditDataComponent', () => {
  let component: PackingMechaPrevEditDataComponent;
  let fixture: ComponentFixture<PackingMechaPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
