import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { Material } from '../packing-mecha-cor-add-data/activity-material.model.group';
import { Activity } from '../packing-mecha-cor-add-data/activity.model';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-packing-mecha-cor-material',
  templateUrl: './packing-mecha-cor-material.component.html',
  styleUrls: ['./packing-mecha-cor-material.component.scss']
})
export class PackingMechaCorMaterialComponent implements OnInit {
  public activities:any = [];
  public activity_group: any = {};
  public material:any = [];
  public materialSaveData: boolean[] = [];

  newActivity: Activity = {
    operation_task: '',
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
    id_activity_group: null
  };

  newActivityMaterial: Material = {
    description_material: '',
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
    id_activity: null
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  id_activity:any
  activityData: Activity[] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    
    
  ) {}
  ngOnInit(): void {
    

    
    this.getActivityMaterialByActivityId(this.id_activity);
    this.activities.forEach((item: any) => {
      item.isUpdated = false;
    });

    this.materialSaveData = new Array(this.material.length).fill(false);

    this.breadCrumbItems = [{
        label: 'Forms'
      },
      {
        label: 'Form Layout',
        active: true
      }
    ];
  }

  addMaterial() {
    this.material.push({});
    
  }

  insertMaterial(index: any) {
    let data = this.material.filter ((x : any, i : any) => {
      return i == index 
    })[0];
    let dataMaterial = {
    description_material: data.description_material,
    qty: data.qty,
    status_part: data.status_part,
    category_activity: data.category_activity,
    work_center: data.work_center,
    duration: data.duration,
    id_activity: this.id_activity
    }
    
    this.RestApiService.insertMaterial(dataMaterial).subscribe(
    (res:any) => {
      if (res.status == true) {
        data.id_activity_material = res.data[0];
        data.isUpdated = true;
        Swal.fire({
          title: 'Notification',
          text: 'Add Data Successfully!',
          icon: 'success',
          timer: 1500,
          showConfirmButton: false,
          customClass: {
            popup: 'custom-swal-text', 
          },
        });
      }
    },
    (error) => {
      console.error(error)
    })
  }

  updateMaterial(index: any) {
    let data = this.material.filter ((x : any, i : any) => {
      return i == index 
    })[0];
    let dataMaterial = {
      description_material: data.description_material,
      qty: data.qty,
      status_part: data.status_part,
      category_activity: data.category_activity,
      work_center: data.work_center,
      duration: data.duration,
      id_activity: this.id_activity
    }
    
    this.RestApiService.updateMaterial(dataMaterial, data.id_activity_material).subscribe(
    (res:any) => {
      if (res.status == true) {
        data.id_activity_material = res.data[0]
        this.materialSaveData[index] = true;
      }
    },
    (error) => {
      console.error(error)
    })
  }

  onUpdateMaterial(id:any){
    if (this.isDataMaterialChange(id)) {
      this.updateMaterial(id)
      Swal.fire({
        title: 'Notification',
        text: 'Data Updated Successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
      this.getActivityMaterialByActivityId(this.id_activity);
    } else {
      Swal.fire({
        title: 'Notification',
        text: "Data is\'n Update",
        icon: 'error',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    }
  }

  isDataMaterialChange(id: any) {
    const data = this.material.find((item: any, index: number) => index === id);
    return (
      data &&
      (data.description_material !== this.newActivityMaterial.description_material)
    );
  }

  getActivityMaterialByActivityId(id:any){
    this.RestApiService.getActivityMaterialByActivityId(id).subscribe(
      (res:any) => {
        
        this.material = res.data
      }
    )
  }


}