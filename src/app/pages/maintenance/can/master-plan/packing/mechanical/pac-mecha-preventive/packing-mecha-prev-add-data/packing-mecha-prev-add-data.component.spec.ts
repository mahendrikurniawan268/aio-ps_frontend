import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaPrevAddDataComponent } from './packing-mecha-prev-add-data.component';

describe('PackingMechaPrevAddDataComponent', () => {
  let component: PackingMechaPrevAddDataComponent;
  let fixture: ComponentFixture<PackingMechaPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
