import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaOverEditDataComponent } from './packing-mecha-over-edit-data.component';

describe('PackingMechaOverEditDataComponent', () => {
  let component: PackingMechaOverEditDataComponent;
  let fixture: ComponentFixture<PackingMechaOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
