import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaCorPopupComponent } from './packing-mecha-cor-popup.component';

describe('PackingMechaCorPopupComponent', () => {
  let component: PackingMechaCorPopupComponent;
  let fixture: ComponentFixture<PackingMechaCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
