import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaOverMaterialComponent } from './packing-mecha-over-material.component';

describe('PackingMechaOverMaterialComponent', () => {
  let component: PackingMechaOverMaterialComponent;
  let fixture: ComponentFixture<PackingMechaOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
