import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaOverAddDataComponent } from './packing-mecha-over-add-data.component';

describe('PackingMechaOverAddDataComponent', () => {
  let component: PackingMechaOverAddDataComponent;
  let fixture: ComponentFixture<PackingMechaOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
