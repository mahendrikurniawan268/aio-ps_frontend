import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaPrevPopupComponent } from './packing-mecha-prev-popup.component';

describe('PackingMechaPrevPopupComponent', () => {
  let component: PackingMechaPrevPopupComponent;
  let fixture: ComponentFixture<PackingMechaPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
