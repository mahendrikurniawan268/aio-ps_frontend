import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaPrevMaterialComponent } from './packing-mecha-prev-material.component';

describe('PackingMechaPrevMaterialComponent', () => {
  let component: PackingMechaPrevMaterialComponent;
  let fixture: ComponentFixture<PackingMechaPrevMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaPrevMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaPrevMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
