import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaCorMaterialComponent } from './packing-mecha-cor-material.component';

describe('PackingMechaCorMaterialComponent', () => {
  let component: PackingMechaCorMaterialComponent;
  let fixture: ComponentFixture<PackingMechaCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
