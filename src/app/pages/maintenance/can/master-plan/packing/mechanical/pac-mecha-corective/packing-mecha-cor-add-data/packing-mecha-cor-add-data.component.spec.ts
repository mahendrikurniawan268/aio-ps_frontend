import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingMechaCorAddDataComponent } from './packing-mecha-cor-add-data.component';

describe('PackingMechaCorAddDataComponent', () => {
  let component: PackingMechaCorAddDataComponent;
  let fixture: ComponentFixture<PackingMechaCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackingMechaCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PackingMechaCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
