import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'; 
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-filling-elec-prev-popup',
  templateUrl: './filling-elec-prev-popup.component.html',
  styleUrls: ['./filling-elec-prev-popup.component.scss']
})
export class FillingElecPrevPopupComponent implements OnInit {
  @Input() activityData: any = {}

  constructor(private RestApiService: RestApiService,  
  private activeModal: NgbActiveModal,
  private router: Router, private authService: AuthenticationService) {}

  public activity_group: any = {};
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;

  ngOnInit(): void {
    
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  getActivityGroupById(id:any){
    this.RestApiService.getActivityGroupById(id).subscribe(
    (res:any) => {
      
      this.activityData = res.data[0]
    })
  }

  onApprove(id:any){
    
    const approveData = {id_status: 2};
    this.updateActivityGroup(approveData, id);

    this.activeModal.close();
    
    this.router.navigate(['filling/electrical/preventive']);

    window.location.reload();
  }

  onDone(id:any){
    
    const approveData = {id_status: 3};
    this.updateActivityGroup(approveData, id);

    this.activeModal.close();
    
    this.router.navigate(['filling/electrical/preventive']);

    window.location.reload();
  }

  updateActivityGroup(dataActivityGroup: any, id:any) {
    this.RestApiService.updateActivityGroup(id, dataActivityGroup).subscribe(
      (res: any) => {
        if (res.id_status == true) {
          
          this.activity_group = dataActivityGroup;
          this.getAllPreMechaCorective();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  getAllPreMechaCorective(){
    this.RestApiService.getPreMechaCorec().subscribe(
      (res:any) => {
        this.activityData = res.data[0];
        
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onClose(){
    this.activeModal.close();
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}
