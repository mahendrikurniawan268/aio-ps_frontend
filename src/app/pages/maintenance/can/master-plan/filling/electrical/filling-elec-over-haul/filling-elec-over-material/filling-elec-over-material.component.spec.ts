import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecOverMaterialComponent } from './filling-elec-over-material.component';

describe('FillingElecOverMaterialComponent', () => {
  let component: FillingElecOverMaterialComponent;
  let fixture: ComponentFixture<FillingElecOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
