import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecCorMaterialComponent } from './filling-elec-cor-material.component';

describe('FillingElecCorMaterialComponent', () => {
  let component: FillingElecCorMaterialComponent;
  let fixture: ComponentFixture<FillingElecCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
