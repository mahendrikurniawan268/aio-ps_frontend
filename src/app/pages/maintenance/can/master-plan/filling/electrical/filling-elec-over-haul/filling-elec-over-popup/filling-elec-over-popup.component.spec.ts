import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecOverPopupComponent } from './filling-elec-over-popup.component';

describe('FillingElecOverPopupComponent', () => {
  let component: FillingElecOverPopupComponent;
  let fixture: ComponentFixture<FillingElecOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
