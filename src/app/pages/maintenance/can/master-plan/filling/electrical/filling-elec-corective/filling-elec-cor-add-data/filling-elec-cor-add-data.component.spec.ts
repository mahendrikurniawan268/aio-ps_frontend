import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecCorAddDataComponent } from './filling-elec-cor-add-data.component';

describe('FillingElecCorAddDataComponent', () => {
  let component: FillingElecCorAddDataComponent;
  let fixture: ComponentFixture<FillingElecCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
