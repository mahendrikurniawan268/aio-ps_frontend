import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecCorEditDataComponent } from './filling-elec-cor-edit-data.component';

describe('FillingElecCorEditDataComponent', () => {
  let component: FillingElecCorEditDataComponent;
  let fixture: ComponentFixture<FillingElecCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
