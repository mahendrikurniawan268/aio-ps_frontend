import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecOverEditDataComponent } from './filling-elec-over-edit-data.component';

describe('FillingElecOverEditDataComponent', () => {
  let component: FillingElecOverEditDataComponent;
  let fixture: ComponentFixture<FillingElecOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
