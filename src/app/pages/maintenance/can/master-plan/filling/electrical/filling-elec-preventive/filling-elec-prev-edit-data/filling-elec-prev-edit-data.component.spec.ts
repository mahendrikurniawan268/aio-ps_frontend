import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecPrevEditDataComponent } from './filling-elec-prev-edit-data.component';

describe('FillingElecPrevEditDataComponent', () => {
  let component: FillingElecPrevEditDataComponent;
  let fixture: ComponentFixture<FillingElecPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
