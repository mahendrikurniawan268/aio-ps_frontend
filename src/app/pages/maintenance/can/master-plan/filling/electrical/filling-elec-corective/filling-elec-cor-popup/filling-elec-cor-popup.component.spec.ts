import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecCorPopupComponent } from './filling-elec-cor-popup.component';

describe('FillingElecCorPopupComponent', () => {
  let component: FillingElecCorPopupComponent;
  let fixture: ComponentFixture<FillingElecCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
