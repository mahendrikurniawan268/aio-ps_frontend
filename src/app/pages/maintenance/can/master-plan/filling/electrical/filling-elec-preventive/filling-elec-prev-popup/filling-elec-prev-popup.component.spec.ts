import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecPrevPopupComponent } from './filling-elec-prev-popup.component';

describe('FillingElecPrevPopupComponent', () => {
  let component: FillingElecPrevPopupComponent;
  let fixture: ComponentFixture<FillingElecPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
