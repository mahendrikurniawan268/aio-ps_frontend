import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecPrevMaterialComponent } from './filling-elec-prev-material.component';

describe('FillingElecPrevMaterialComponent', () => {
  let component: FillingElecPrevMaterialComponent;
  let fixture: ComponentFixture<FillingElecPrevMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecPrevMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecPrevMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
