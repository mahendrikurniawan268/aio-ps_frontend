import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecOverAddDataComponent } from './filling-elec-over-add-data.component';

describe('FillingElecOverAddDataComponent', () => {
  let component: FillingElecOverAddDataComponent;
  let fixture: ComponentFixture<FillingElecOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
