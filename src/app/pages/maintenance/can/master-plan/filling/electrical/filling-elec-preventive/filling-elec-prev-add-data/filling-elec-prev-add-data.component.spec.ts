import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingElecPrevAddDataComponent } from './filling-elec-prev-add-data.component';

describe('FillingElecPrevAddDataComponent', () => {
  let component: FillingElecPrevAddDataComponent;
  let fixture: ComponentFixture<FillingElecPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingElecPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingElecPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
