export interface Material {
  description_material: string;
  qty: number | null;
  status_part: string;
  category_activity: string;
  work_center: string;
  duration: string;
  id_activity: number | null;
}