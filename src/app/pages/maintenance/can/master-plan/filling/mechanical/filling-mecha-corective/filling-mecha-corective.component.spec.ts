import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilMechaCorectiveComponent } from './filling-mecha-corective.component';

describe('FilMechaCorectiveComponent', () => {
  let component: FilMechaCorectiveComponent;
  let fixture: ComponentFixture<FilMechaCorectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilMechaCorectiveComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilMechaCorectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
