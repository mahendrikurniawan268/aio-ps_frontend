import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilMechaPrevPopupComponent } from './filling-mecha-prev-popup.component';

describe('FilMechaPrevPopupComponent', () => {
  let component: FilMechaPrevPopupComponent;
  let fixture: ComponentFixture<FilMechaPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilMechaPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilMechaPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
