import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaCorPopupComponent } from './filling-mecha-cor-popup.component';

describe('FillingMechaCorPopupComponent', () => {
  let component: FillingMechaCorPopupComponent;
  let fixture: ComponentFixture<FillingMechaCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
