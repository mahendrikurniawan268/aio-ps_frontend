import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaPrevEditDataComponent } from './filling-mecha-prev-edit-data.component';

describe('FillingMechaPrevEditDataComponent', () => {
  let component: FillingMechaPrevEditDataComponent;
  let fixture: ComponentFixture<FillingMechaPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
