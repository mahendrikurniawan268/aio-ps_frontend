import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaOverAddDataComponent } from './filling-mecha-over-add-data.component';

describe('FillingMechaOverAddDataComponent', () => {
  let component: FillingMechaOverAddDataComponent;
  let fixture: ComponentFixture<FillingMechaOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
