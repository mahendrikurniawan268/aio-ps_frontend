import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDescriptionMaterialComponent } from './popup-description-material.component';

describe('PopupDescriptionMaterialComponent', () => {
  let component: PopupDescriptionMaterialComponent;
  let fixture: ComponentFixture<PopupDescriptionMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupDescriptionMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopupDescriptionMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
