import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaPrevAddDataComponent } from './filling-mecha-prev-add-data.component';

describe('FillingMechaPrevAddDataComponent', () => {
  let component: FillingMechaPrevAddDataComponent;
  let fixture: ComponentFixture<FillingMechaPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
