import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaOverMaterialComponent } from './filling-mecha-over-material.component';

describe('FillingMechaOverMaterialComponent', () => {
  let component: FillingMechaOverMaterialComponent;
  let fixture: ComponentFixture<FillingMechaOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
