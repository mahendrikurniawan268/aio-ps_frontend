import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaOverEditDataComponent } from './filling-mecha-over-edit-data.component';

describe('FillingMechaOverEditDataComponent', () => {
  let component: FillingMechaOverEditDataComponent;
  let fixture: ComponentFixture<FillingMechaOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
