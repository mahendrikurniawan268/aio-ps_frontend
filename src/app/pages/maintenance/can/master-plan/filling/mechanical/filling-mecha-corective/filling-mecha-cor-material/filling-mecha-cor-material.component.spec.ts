import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaCorMaterialComponent } from './filling-mecha-cor-material.component';

describe('FillingMechaCorMaterialComponent', () => {
  let component: FillingMechaCorMaterialComponent;
  let fixture: ComponentFixture<FillingMechaCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
