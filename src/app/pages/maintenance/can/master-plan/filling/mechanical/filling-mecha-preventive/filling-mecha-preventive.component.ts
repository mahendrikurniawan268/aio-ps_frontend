import { Component, ViewChild, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FilMechaPrevPopupComponent } from './filling-mecha-prev-popup/filling-mecha-prev-popup.component';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-preventive',
  templateUrl: './filling-mecha-preventive.component.html',
  styleUrls: ['./filling-mecha-preventive.component.scss']
})
export class FilMechaPreventiveComponent implements OnInit {
  tableColumns = ['No', 'Function Location', 'Operation Task', 'Description Material', 'Pic', 'Status', 'Date', 'Details'];
  titlePage = 'Activity List';
  index: number = 0;
  activityData: any[] = [];

  @ViewChild('myModal') myModal: any; 

  constructor(private modalService: NgbModal, private route: ActivatedRoute, 
    private router:Router, private RestApiService : RestApiService,
    private authService: AuthenticationService) {} 
  
  public activities:any = [];
  public activity_group: any = {};
  public idParam:any;
  public material:any = [];
  public activitySaveData: boolean[] = [];

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  idMaterial:any;
  searchText:string = '';
  dataForCurrentPage: any[] = [];
  preData:any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  currentPage = 1;
  itemsPerPage = 10;

  ngOnInit(): void {
    

    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getActivityData();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.activities.forEach((item: any) => {
      item.isUpdated = false;});
  }

  listReport(){
    this.router.navigate(['report/list-report'])
  }

  openPopup(data:any) {
    const modalRef = this.modalService.open(FilMechaPrevPopupComponent, { centered: true });
    modalRef.componentInstance.activityData = data;
    modalRef.result.then(
      (result) => {
        
      },
      (reason) => {
        
      }
    );
  } 
  addData() {
  this.router.navigate(['/filling/mechanical/preventive/adddata']);
  }

  getActivityData(){
    this.RestApiService.getTabelViewMasterPlan().subscribe(
      (res:any) => {
        this.activityData = res.data[0];
        this.preData = [...this.activityData];
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateActivityGroup(dataActivityGroup: any, id:any) {
    this.RestApiService.updateActivityGroup(id, dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.activity_group = dataActivityGroup;
          this.getActivityData()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateActivityGroup(deleteData, id);
      }
    });
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'warning'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Late';
        default:
            return 'Unknown';
    }
}

onReport(){
  this.router.navigate(['report/master-plan'])
}

editData(id:any){
  this.router.navigate([`filling/mechanical/preventive/edit-data/${id}`])
}

search() {
  this.activityData = this.preData.filter((item: { function_location: string; operation_task: string; description_material: string; pic:string;}) => {
    return item.function_location.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.operation_task.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.description_material.toLowerCase().includes(this.searchText.toLowerCase()) ||
           item.pic.toLowerCase().includes(this.searchText.toLowerCase());
  });
  this.paginateData();
}

paginateData() {
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.dataForCurrentPage = this.activityData.slice(startIndex, endIndex);
}

onPageChange(newPage: number) {
  this.currentPage = newPage;
  this.paginateData();
}

}
