import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaOverPopupComponent } from './filling-mecha-over-popup.component';

describe('FillingMechaOverPopupComponent', () => {
  let component: FillingMechaOverPopupComponent;
  let fixture: ComponentFixture<FillingMechaOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
