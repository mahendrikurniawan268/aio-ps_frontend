import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaCorEditDataComponent } from './filling-mecha-cor-edit-data.component';

describe('FillingMechaCorEditDataComponent', () => {
  let component: FillingMechaCorEditDataComponent;
  let fixture: ComponentFixture<FillingMechaCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
