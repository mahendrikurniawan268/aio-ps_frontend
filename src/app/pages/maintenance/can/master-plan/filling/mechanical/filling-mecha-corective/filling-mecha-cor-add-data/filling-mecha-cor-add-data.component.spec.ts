import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillingMechaCorAddDataComponent } from './filling-mecha-cor-add-data.component';

describe('FillingMechaCorAddDataComponent', () => {
  let component: FillingMechaCorAddDataComponent;
  let fixture: ComponentFixture<FillingMechaCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillingMechaCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FillingMechaCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
