export interface ActivityGroup {
    id_line: number | null;
    function_location: string | null;
    date: string;
    pic: string;
    email: string;
    equipment: string;
    id_type: number | null;
    id_category: number | null;
    id_subcategory: number | null;
    notes: string;
  }