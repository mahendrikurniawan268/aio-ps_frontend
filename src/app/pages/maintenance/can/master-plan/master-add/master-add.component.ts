import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ActivityGroup } from './activity-group.model';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-master-add',
  templateUrl: './master-add.component.html',
  styleUrls: ['./master-add.component.scss']
})
export class MasterAddComponent implements OnInit {

  public activities:any = [];
  public activity_group: any = {};

  newActivityGroup: ActivityGroup = {
    id_line: null,
    function_location: null,
    date: '',
    pic: '',
    email: '',
    equipment: '',
    id_type: null,
    id_category: null,
    id_subcategory: null,
    notes: ''
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  namePic:any [] = [];
  namePicByRole:any [] = [];
  idLine: any;
  idType: any;
  idCategory: any;
  idSubcategory: any
  functionLocation:any  [] = [];
  functionLocationByLine: any [] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route : ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.getNamePic();
    this.getFunctionLocation();
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId'], this.idType = params['typeId'], this.idCategory = params['categoryId'], this.idSubcategory = params['subCategoryId'];
    }})
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.namePicByRole = this.namePic.filter(item => item.role_id === 3 || item.role_id === 4)        
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getFunctionLocation(){
    this.RestApiService.getAllFunctionLocation().subscribe(
      (res:any) => {
        this.functionLocation = res.data;
        this.functionLocationByLine = this.functionLocation.filter(item => item.id_line == this.idLine)
      }
    )
  }

  onPic(event: any) {
    const picName = event.target.value;
    const selectedPic = this.namePicByRole.find(item => item.name === picName);
    if (selectedPic) {
      this.newActivityGroup.pic = selectedPic.name
      this.newActivityGroup.email = selectedPic.email;
    }
  }

  insertActivityGroup(dataActivityGroup: any) {
    this.newActivityGroup.id_line = this.idLine
    this.newActivityGroup.id_type = this.idType
    this.newActivityGroup.id_category = this.idCategory
    this.newActivityGroup.id_subcategory = this.idSubcategory
    this.RestApiService.insertActivityGroup(dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idActivityGroup = res.data[0]
          this.activity_group = dataActivityGroup;
          this.activity_group.id = this.idActivityGroup
          this.router.navigate([`maintenance/master-plan/edit/${this.idActivityGroup}`], {queryParams:{lineId: this.idLine, typeId: this.idType, categoryId: this.idCategory, subCategoryId: this.idSubcategory}})
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }
  
  onClick() {
    this.insertActivityGroup(this.newActivityGroup);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!!',
      icon: 'success',
      timer: 1500, 
      showConfirmButton: false, 
    });
  }
}
