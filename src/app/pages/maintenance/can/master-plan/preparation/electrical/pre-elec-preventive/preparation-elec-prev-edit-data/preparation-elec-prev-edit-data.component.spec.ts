import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecPrevEditDataComponent } from './preparation-elec-prev-edit-data.component';

describe('PreparationElecPrevEditDataComponent', () => {
  let component: PreparationElecPrevEditDataComponent;
  let fixture: ComponentFixture<PreparationElecPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
