import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecCorEditDataComponent } from './preparation-elec-cor-edit-data.component';

describe('PreparationElecCorEditDataComponent', () => {
  let component: PreparationElecCorEditDataComponent;
  let fixture: ComponentFixture<PreparationElecCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
