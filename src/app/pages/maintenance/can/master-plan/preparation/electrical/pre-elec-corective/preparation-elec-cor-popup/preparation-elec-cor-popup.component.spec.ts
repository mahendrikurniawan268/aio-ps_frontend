import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecCorPopupComponent } from './preparation-elec-cor-popup.component';

describe('PreparationElecCorPopupComponent', () => {
  let component: PreparationElecCorPopupComponent;
  let fixture: ComponentFixture<PreparationElecCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
