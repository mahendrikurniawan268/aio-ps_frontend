import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecCorMaterialComponent } from './preparation-elec-cor-material.component';

describe('PreparationElecCorMaterialComponent', () => {
  let component: PreparationElecCorMaterialComponent;
  let fixture: ComponentFixture<PreparationElecCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
