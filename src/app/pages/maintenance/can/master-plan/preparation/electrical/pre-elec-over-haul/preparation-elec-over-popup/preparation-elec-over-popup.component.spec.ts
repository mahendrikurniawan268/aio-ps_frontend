import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecOverPopupComponent } from './preparation-elec-over-popup.component';

describe('PreparationElecOverPopupComponent', () => {
  let component: PreparationElecOverPopupComponent;
  let fixture: ComponentFixture<PreparationElecOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
