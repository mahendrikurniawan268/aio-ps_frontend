import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecPrevMaterialComponent } from './preparation-elec-prev-material.component';

describe('PreparationElecPrevMaterialComponent', () => {
  let component: PreparationElecPrevMaterialComponent;
  let fixture: ComponentFixture<PreparationElecPrevMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecPrevMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecPrevMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
