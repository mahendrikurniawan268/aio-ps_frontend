import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecOverMaterialComponent } from './preparation-elec-over-material.component';

describe('PreparationElecOverMaterialComponent', () => {
  let component: PreparationElecOverMaterialComponent;
  let fixture: ComponentFixture<PreparationElecOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
