import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecPrevPopupComponent } from './preparation-elec-prev-popup.component';

describe('PreparationElecPrevPopupComponent', () => {
  let component: PreparationElecPrevPopupComponent;
  let fixture: ComponentFixture<PreparationElecPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
