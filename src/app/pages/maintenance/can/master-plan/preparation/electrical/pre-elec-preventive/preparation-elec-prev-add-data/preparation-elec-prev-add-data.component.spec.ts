import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecPrevAddDataComponent } from './preparation-elec-prev-add-data.component';

describe('PreparationElecPrevAddDataComponent', () => {
  let component: PreparationElecPrevAddDataComponent;
  let fixture: ComponentFixture<PreparationElecPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
