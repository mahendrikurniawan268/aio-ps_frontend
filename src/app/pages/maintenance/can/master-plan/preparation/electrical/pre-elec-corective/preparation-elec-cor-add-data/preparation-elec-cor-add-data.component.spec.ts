import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecCorAddDataComponent } from './preparation-elec-cor-add-data.component';

describe('PreparationElecCorAddDataComponent', () => {
  let component: PreparationElecCorAddDataComponent;
  let fixture: ComponentFixture<PreparationElecCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
