import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ActivityGroup } from '../preparation-elec-prev-add-data/activity-group.model';
import { Material } from '../preparation-elec-prev-add-data/activity-material.model.group';
import { Activity } from '../preparation-elec-prev-add-data/activity.model';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { PreparationElecPrevMaterialComponent } from '../preparation-elec-prev-material/preparation-elec-prev-material.component';

@Component({
  selector: 'app-preparation-elec-prev-edit-data',
  templateUrl: './preparation-elec-prev-edit-data.component.html',
  styleUrls: ['./preparation-elec-prev-edit-data.component.scss']
})
export class PreparationElecPrevEditDataComponent implements OnInit {

  public activities:any = [];
  public activity_group: any = {};
  public idParam:any;
  public material:any = [];
  public activitySaveData: boolean[] = [];
  namePic:any;

  newActivityGroup: ActivityGroup = {
    function_location: '',
    date: '',
    pic: '',
    equipment: '',
    type: 'preparation',
    category: 'electrical',
    sub_category: 'preventive',
  };

  newActivity: Activity = {
    operation_task: '',
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
    id_activity_group: null
  };

  newActivityMaterial: Material = {
    description_material: '',
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
    id_activity: null
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  idMaterial:any;
  activityData: Activity[] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}
  ngOnInit(): void {
    this.getNamePic();
    this.idParam = this.route.snapshot.paramMap.get('id');
    this.getActivityGroupById(this.idParam);
    this.getActivityByGroupId(this.idParam);
    this.activities.forEach((item: any) => {
      item.isUpdated = false;});
    this.activitySaveData = new Array(this.activities.length).fill(false);
    this.breadCrumbItems = [{
        label: 'Forms'
      },
      {
        label: 'Form Layout',
        active: true
      }
    ];
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  forBack() {
    this.router.navigate(['preparation/electrical/preventive']);
  }

  insertActivityGroup(dataActivityGroup: any) {
    this.RestApiService.insertActivityGroup(dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idActivityGroup = res.data[0]
          
          this.activity_group = dataActivityGroup;
          this.activity_group.id = this.idActivityGroup
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  updateActivityGroup(dataActivityGroup: any) {
    
    this.RestApiService.updateActivityGroup(this.idParam, dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          
          this.activity_group = dataActivityGroup;
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdateGroup() {
    if (this.isDataGroupChanged()) {
      this.updateActivityGroup(this.newActivityGroup);
  
      Swal.fire({
        title: 'Notification',
        text: 'Data Updated Successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    } else {
      Swal.fire({
        title: 'Notification',
        text: "Data is\'n Update",
        icon: 'error',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    }
  }

  isDataGroupChanged() {
    return (
      this.newActivityGroup == this.newActivityGroup
    );
  }

  addActivity() {
    this.activities.push({});
    
  }

  insertActivity(index: any) {
    let data = this.activities.filter ((x : any, i : any) => {
      return i == index 
    })[0];
    let dataActivity = {
    operation_task: data.operation_task,
    qty: data.qty,
    status_part: data.status_part,
    category_activity: data.category_activity,
    work_center: data.work_center,
    duration: data.duration,
    id_activity_group: this.idParam
    }
    
    this.RestApiService.insertActivity(dataActivity).subscribe(
    (res:any) => {
      if (res.status == true) {
        data.id_activity = res.data[0];
        data.isUpdated = true;

        Swal.fire({
          title: 'Notification',
          text: 'Add Data Successfully!',
          icon: 'success',
          timer: 1500,
          showConfirmButton: false,
          customClass: {
            popup: 'custom-swal-text', 
          },
        });
      }
    },
    (error) => {
      console.error(error)
    })
  }

  updateActivity(index: any) {
    let data = this.activities.filter ((x : any, i : any) => {
      return i == index 
    })[0];
    let dataActivity = {
      operation_task: data.operation_task,
      qty: data.qty,
      status_part: data.status_part,
      category_activity: data.category_activity,
      work_center: data.work_center,
      duration: data.duration,
      id_activity_group: this.idParam
    }
    
    this.RestApiService.updateActivity(dataActivity, data.id_activity).subscribe(
    (res:any) => {
      if (res.status == true) {
        data.id_activity = res.data[0]
      }
    },
    (error) => {
      console.error(error)
    })
  }

  onUpdateActivity(id:any){
    if (this.isDataActivityChange(id)) {
      this.updateActivity(id)
      Swal.fire({
        title: 'Notification',
        text: 'Data Updated Successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
      this.getActivityByGroupId(this.idParam);
    } else {
      Swal.fire({
        title: 'Notification',
        text: "Data is\'n Update",
        icon: 'error',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    }
  }

isDataActivityChange(id: any) {
  const data = this.activities.find((item: any, index: number) => index === id);
  return (
    data &&
    (data.operation_task !== this.newActivity.operation_task ||
      data.qty !== this.newActivity.qty ||
      data.status_part !== this.newActivity.status_part ||
      data.category_activity !== this.newActivity.category_activity ||
      data.work_center !== this.newActivity.work_center ||
      data.duration !== this.newActivity.duration)
  );
}

  getActivityGroupById(id:any){
    this.RestApiService.getActivityGroupById(id).subscribe(
    (res:any) => {
      this.newActivityGroup = res.data[0]
      this.newActivityGroup.date = this.convertDate(this.newActivityGroup.date)
    })
  }

  getActivityByGroupId(id:any){
    this.RestApiService.getActivityByGroupId(id).subscribe(
    (res:any) => {
      
      this.activities = res.data
    })
  }

  getActivityMaterialByActivityId(id:any){
    this.RestApiService.getActivityMaterialByActivityId(id).subscribe(
      (res:any) => {
        
        this.material = res.data
      }
    )
  }

  openPopup(id:any) {
    const modalOptions: NgbModalOptions = {
      centered: true,
      size: 'xl' 
    };
    const modalRef = this.modalService.open(PreparationElecPrevMaterialComponent, modalOptions);
    modalRef.componentInstance.id_activity = id;
    modalRef.result.then(
      (result) => {
        
      },
      (reason) => {
        
      }
    );
  } 

  onSubmit() {
    this.router.navigate(['preparation/electrical/preventive'])
    Swal.fire({
      icon: 'success',
      title: 'Data Added',
      text: 'Data has been successfully added.',
    });
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}

