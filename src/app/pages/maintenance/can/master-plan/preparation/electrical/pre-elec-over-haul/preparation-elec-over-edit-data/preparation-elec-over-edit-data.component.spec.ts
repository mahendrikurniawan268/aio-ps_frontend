import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecOverEditDataComponent } from './preparation-elec-over-edit-data.component';

describe('PreparationElecOverEditDataComponent', () => {
  let component: PreparationElecOverEditDataComponent;
  let fixture: ComponentFixture<PreparationElecOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
