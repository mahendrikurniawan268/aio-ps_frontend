import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationElecOverAddDataComponent } from './preparation-elec-over-add-data.component';

describe('PreparationElecOverAddDataComponent', () => {
  let component: PreparationElecOverAddDataComponent;
  let fixture: ComponentFixture<PreparationElecOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationElecOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationElecOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
