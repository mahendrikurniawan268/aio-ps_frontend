import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaOverPopupComponent } from './preparation-mecha-over-popup.component';

describe('PreparationMechaOverPopupComponent', () => {
  let component: PreparationMechaOverPopupComponent;
  let fixture: ComponentFixture<PreparationMechaOverPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaOverPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaOverPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
