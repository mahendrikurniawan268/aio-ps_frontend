import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaCorMaterialComponent } from './preparation-mecha-cor-material.component';

describe('PreparationMechaCorMaterialComponent', () => {
  let component: PreparationMechaCorMaterialComponent;
  let fixture: ComponentFixture<PreparationMechaCorMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaCorMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaCorMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
