import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ActivityGroup } from './activity-group.model';
import { Material } from './activity-material.model.group';
import { Activity } from './activity.model';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-preparation-mecha-prev-add-data',
  templateUrl: './preparation-mecha-prev-add-data.component.html',
  styleUrls: ['./preparation-mecha-prev-add-data.component.scss']
})
export class PreparationMechaPrevAddDataComponent implements OnInit {

  public activities:any = [];
  public activity_group: any = {};

  newActivityGroup: ActivityGroup = {
    function_location: '',
    date: '',
    pic: '',
    equipment: '',
    type: 'preparation',
    category: 'mechanical',
    sub_category: 'preventive',
  };

  newActivity: Activity = {
    operation_task: '',
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
    id_activity_group: null
  };

  newActivityMaterial: Material = {
    description_material: '',
    id_activity: null,
    qty: null,
    status_part: '',
    category_activity: '',
    work_center: '',
    duration: '',
  };

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idActivityGroup: any;
  idActivity: any;
  activityData: Activity[] = [];
  namePic:any;

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.getNamePic();
    this.breadCrumbItems = [{
        label: 'Forms'
      },
      {
        label: 'Form Layout',
        active: true
      }
    ];
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  insertActivityGroup(dataActivityGroup: any) {
    
    this.RestApiService.insertActivityGroup(dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idActivityGroup = res.data[0]
          this.activity_group = dataActivityGroup;
          this.activity_group.id = this.idActivityGroup
          this.router.navigate([`preparation/mechanical/preventive/edit-data/${this.idActivityGroup}`])
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }
  
  onClick() {
    this.insertActivityGroup(this.newActivityGroup);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!!',
      icon: 'success',
      timer: 1500, 
      showConfirmButton: false, 
    });
  }
}
