import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaPrevAddDataComponent } from './preparation-mecha-prev-add-data.component';

describe('PreparationMechaPrevAddDataComponent', () => {
  let component: PreparationMechaPrevAddDataComponent;
  let fixture: ComponentFixture<PreparationMechaPrevAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaPrevAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaPrevAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
