import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaPrevEditDataComponent } from './preparation-mecha-prev-edit-data.component';

describe('PreparationMechaPrevEditDataComponent', () => {
  let component: PreparationMechaPrevEditDataComponent;
  let fixture: ComponentFixture<PreparationMechaPrevEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaPrevEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaPrevEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
