import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { ActivityGroup } from './activity-group.model';
import { Activity } from './activity.model';

@Component({
  selector: 'app-preparation-mecha-over-add-data',
  templateUrl: './preparation-mecha-over-add-data.component.html',
  styleUrls: ['./preparation-mecha-over-add-data.component.scss']
})
export class PreparationMechaOverAddDataComponent implements OnInit {

  public activities:any = [];
  public activity_group: any = {};
  namePic:any;

  newActivityGroup: ActivityGroup = {
    function_location: '',
    date: '',
    pic: '',
    equipment: '',
    type: 'preparation',
    category: 'mechanical',
    sub_category: 'over haul',
    id_status: 1,
  };

  // bread crumb items
  idActivityGroup: any;
  idActivity: any;
  activityData: Activity[] = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService
  ) {}
  ngOnInit(): void {
   this.getNamePic(); 
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  insertActivityGroup(dataActivityGroup: any) {
    this.RestApiService.insertActivityGroup(dataActivityGroup).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idActivityGroup = res.data[0]
          
          this.activity_group = dataActivityGroup;
          this.activity_group.id = this.idActivityGroup
          this.router.navigate([`preparation/mechanical/over-haul/edit-data/${this.idActivityGroup}`])
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }
  onClick() {
    this.insertActivityGroup(this.newActivityGroup);
  }
}
