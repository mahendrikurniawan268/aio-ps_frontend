import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaOverEditDataComponent } from './preparation-mecha-over-edit-data.component';

describe('PreparationMechaOverEditDataComponent', () => {
  let component: PreparationMechaOverEditDataComponent;
  let fixture: ComponentFixture<PreparationMechaOverEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaOverEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaOverEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
