import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaOverAddDataComponent } from './preparation-mecha-over-add-data.component';

describe('PreparationMechaOverAddDataComponent', () => {
  let component: PreparationMechaOverAddDataComponent;
  let fixture: ComponentFixture<PreparationMechaOverAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaOverAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaOverAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
