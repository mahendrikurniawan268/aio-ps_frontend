import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaPrevMaterialComponent } from './preparation-mecha-prev-material.component';

describe('PreparationMechaPrevMaterialComponent', () => {
  let component: PreparationMechaPrevMaterialComponent;
  let fixture: ComponentFixture<PreparationMechaPrevMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaPrevMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaPrevMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
