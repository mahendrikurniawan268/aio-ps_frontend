import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaCorAddDataComponent } from './preparation-mecha-cor-add-data.component';

describe('PreparationMechaCorAddDataComponent', () => {
  let component: PreparationMechaCorAddDataComponent;
  let fixture: ComponentFixture<PreparationMechaCorAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaCorAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaCorAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
