import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaPrevPopupComponent } from './preparation-mecha-prev-popup.component';

describe('PreparationMechaPrevPopupComponent', () => {
  let component: PreparationMechaPrevPopupComponent;
  let fixture: ComponentFixture<PreparationMechaPrevPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaPrevPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaPrevPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
