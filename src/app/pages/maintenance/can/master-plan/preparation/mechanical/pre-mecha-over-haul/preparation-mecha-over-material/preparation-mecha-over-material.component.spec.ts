import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaOverMaterialComponent } from './preparation-mecha-over-material.component';

describe('PreparationMechaOverMaterialComponent', () => {
  let component: PreparationMechaOverMaterialComponent;
  let fixture: ComponentFixture<PreparationMechaOverMaterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaOverMaterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaOverMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
