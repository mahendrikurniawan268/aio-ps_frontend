import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaCorPopupComponent } from './preparation-mecha-cor-popup.component';

describe('PreparationMechaCorPopupComponent', () => {
  let component: PreparationMechaCorPopupComponent;
  let fixture: ComponentFixture<PreparationMechaCorPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaCorPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaCorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
