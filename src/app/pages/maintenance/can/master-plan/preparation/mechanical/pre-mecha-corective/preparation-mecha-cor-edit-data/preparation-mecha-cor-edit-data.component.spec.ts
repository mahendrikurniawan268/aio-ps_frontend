import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparationMechaCorEditDataComponent } from './preparation-mecha-cor-edit-data.component';

describe('PreparationMechaCorEditDataComponent', () => {
  let component: PreparationMechaCorEditDataComponent;
  let fixture: ComponentFixture<PreparationMechaCorEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparationMechaCorEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreparationMechaCorEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
