import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPetMaintenanceComponent } from './dashboard-pet-maintenance.component';

describe('DashboardPetMaintenanceComponent', () => {
  let component: DashboardPetMaintenanceComponent;
  let fixture: ComponentFixture<DashboardPetMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardPetMaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardPetMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
