import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { CommonService } from 'src/app/core/services/common.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskImrovement } from '../../can/continous-improvement/improvement/task.model';

@Component({
  selector: 'app-dashboard-pet-maintenance',
  templateUrl: './dashboard-pet-maintenance.component.html',
  styleUrls: ['./dashboard-pet-maintenance.component.scss']
})
export class DashboardPetMaintenanceComponent implements OnInit {

  // bread crumb items
  exporting: boolean = false;
  breadCrumbItems!: Array<{}>;
  statData!: any;
  OverviewChart: any;
  ActiveProjects: any;
  MyTask: any;
  TeamMembers: any;
  status7: any;
  simplePieChart: any;
  StackedColumnChart: any;
  distributedColumnChart: any;
  multipleRadialbarChart:any;
  preparationMasterChart: any;
  injectionMasterChart: any;
  blowMasterChart: any;
  fillingMasterChart: any;
  packingMasterChart: any;
  idLine: any;

  //Weekly care
  dataWeeklyCare: any[] = [];
  dataWeeklyCarePet: any[] = [];
  countFindingsFailureMachine:number = 0;
  countDataWeeklyCare: number = 0;

  //Autonomous Maintenance
  countDataAllAutonomous: number = 0;
  dataProgressAutonomous: any [] = [];
  dataProgressAutonomousPet: any [] = [];
  dataPreAutonomous: any[] = [];
  dataInjecAutonomous: any[] = [];
  dataBlowAutonomous: any[] = [];
  dataFilAutonomous: any[] = [];
  dataPacAutonomous: any[] = [];
  progressPreAuto: number = 0;
  progressInjecAuto: number = 0;
  progressBlowAuto: number = 0;
  progressFilAuto: number = 0;
  progressPacAuto: number = 0;

  //Improvement
  dataTask:any[] = [];
  dataTaskByLine:any [] = [];
  dataDatePriority: any [] = [];
  dataForCurrentPage: any[] = [];
  newTaskImprovement: TaskImrovement = {
    task: '',
    owner: '',
    due_date: '',
    id_status: null,
    id_priority: null,
    notes: '',
    id_line: null
  };
  totalImprovement : any;
  currentPage = 1;
  itemsPerPage = 10;

  //Schedule Maintenance
  dataAllScheduleMaintenance: any[] = [];
  dataAllScheduleMaintenancePet: any[] = [];
  dataMechaYetToStart: any;
  dataMechaOnProgress: any;
  dataMechaDone:any;
  dataMechaPending: any;
  dataElecYetToStart: any;
  dataElecOnProgress: any;
  dataElecDone:any;
  dataElecPending: any;
  countDataAllScheduleMaintenance: number = 0;
  newDataScheduleMaintenance: any[] = [];
  newDataScheduleMaintenanceByLine: any;
  countDataNewScheduleMaintenance: number = 0;
  //mechanical
  countYetToStartMechanical: number = 0;
  countOnProgressMechanical: number = 0;
  countCompletedMechanical: number = 0;
  countPendingMechanical: number = 0;
  //Electrical
  countYetToStartElectrical: number = 0;
  countOnProgressElectrical: number = 0;
  countCompletedElectrical: number = 0;
  countPendingElectrical: number = 0;

  //weekly care
  countWeeklyCarePreparation: number = 0;
  countWeeklyCareInjection: number = 0;
  countWeeklyCareBlow: number = 0;
  countWeeklyCareFilling: number = 0;
  countWeeklyCarePacking: number = 0;

  //Schedule Weekly
  dataScheduleWeekly: any[] = [];
  dataScheduleWeeklyByLine: any [] = [];
  countDataScheduleWeekly:number = 0;
  dataSchedulePre: any[] = [];
  dataScheduleInjec: any[] = [];
  dataScheduleBlow: any[] = [];
  dataScheduleFill: any[] = [];
  dataSchedulePac: any[] = [];

   //Master Plan
   dataAllMasterPlan: any[] = [];
   dataAllMasterPlanByLine: any[] = [];
   countDataMasterPlan: number = 0;
   dataMasterPreparation: any[] = [];
   dataMasterInjection: any[] = [];
   dataMasterBlow: any[] = [];
   dataMasterFilling: any[] = [];
   dataMasterPacking: any[] = [];
   dataPrePreventive: any[] = [];
   dataPreCorective: any[] = [];
   dataPreOverHaul: any[] = [];
   dataInjecPreventive: any[] = [];
   dataInjecCorective: any[] = [];
   dataInjecOverHaul: any[] = [];
   dataBlowPreventive: any[] = [];
   dataBlowCorective: any[] = [];
   dataBlowOverHaul: any[] = [];
   dataFilPreventive: any[] = [];
   dataFilCorective: any[] = [];
   dataFilOverHaul: any[] = [];
   dataPacPreventive: any[] = [];
   dataPacCorective: any[] = [];
   dataPacOverHaul: any[] = [];

   //pic
   dataPicMaster: any[] = [];
   dataPicMasterByLine: any[] = [];
   dataPicAutonomous: any[] = [];
   dataPicAutonomousByLine: any[] = [];
   dataPicScheduleWeekly: any[] = [];
   dataPicScheduleWeeklyByLine: any[] = [];
   dataPicFindingWeekly: any[] = [];
   dataPicFindingWeeklyByLine: any[] = [];
   dataPicScheduleMaintenance: any[] = [];
   dataPicScheduleMaintenanceByLine: any[] = [];
   dataAllPic: any[] = [];
   dataAllPicByLine: any[] = [];
   userData:any;

   month: number
   monthBefore!: number
   year: number
   yearBefore!: number

   fromDate: string;
   toDate: string;
   datePlaceholder: string
   dateRange = {from: new Date(), to: new Date()}

  @ViewChild('scrollRef') scrollRef: any;

  constructor(private RestApiService: RestApiService, private route:ActivatedRoute, private authService: AuthenticationService, private common: CommonService,
    private modalService:NgbModal) {
    this.month = new Date().getMonth() + 1
    this.year = new Date().getFullYear()
    this.fromDate = this.getDefaultFromDate();
    this.toDate = this.getDefaultToDate();
    this.datePlaceholder = `${common.getMonthName(this.month)} ${this.year}`
  }

  async ngOnInit() {
    /**
     * BreadCrumb
     */
    this.route.queryParams.subscribe({next: (params) => {
      this.idLine = params['lineId']
    }})
    await this.getAllWeeklyCare(this.fromDate, this.toDate);
    await this.getAllMasterPlan(this.fromDate, this.toDate);
    this.getTask();
    this.getNewData();
    await this.getPicMasterPlan(this.fromDate, this.toDate);
    await this.getAllPic(this.fromDate, this.toDate);
    await this.getPicAutonomous(this.fromDate, this.toDate);
    await this.getPicScheduleWeekly(this.fromDate, this.toDate);
    await this.getPicFindingWeekly(this.fromDate, this.toDate);
    await this.getPicScheduleMaintenance(this.fromDate, this.toDate);
    await this.getProgressAutonomous(this.fromDate, this.toDate);
    await this.getAllScheduleMaintenance(this.fromDate, this.toDate);
    await this.getAllScheduleWeekly(this.fromDate, this.toDate);
    this.userData = this.authService.getUserData();
    
    this.breadCrumbItems = [
      { label: 'Dashboards' },
      { label: 'Projects', active: true }
    ];

    /**
     * Fetches the data
     */

    // Chart Color Data Get Function
    this._StackedColumnChart('["--vz-danger", "#FFF798", "#9AF89E", "#8EB9E8", "#A7BBC7"]');
    this._distributedColumnChart('["#5A92AF", "#86C1D4", "--vz-warning", "--vz-danger", "--vz-dark", "--vz-info"]')
    this._multipleRadialbarChart('["--vz-danger", "#FFF798", "#9AF89E", "#8EB9E8", "#A7BBC7"]');
    this._weeklyCareFindings('["--vz-danger", "#FFF798", "#9AF89E", "#8EB9E8", "#A7BBC7"]');
    this._status7('["--vz-danger", "#FFF798", "--vz-warning", "--vz-secondary"]');
    this._preparationMasterPlan('["--vz-danger", "#FFF798", "#9CD9DE", "--vz-danger"]');
    this._injectionMasterPlan('["--vz-danger", "#FFF798", "#9CD9DE", "--vz-danger"]');
    this._blowMasterPlan('["--vz-danger", "#FFF798", "#9CD9DE", "--vz-danger"]');
    this._fillingMasterPlan('["--vz-danger", "#FFF798", "#9CD9DE", "--vz-info", "--vz-danger"]');
    this._packingMasterPlan('["--vz-danger", "#FFF798", "#9CD9DE", "--vz-info", "--vz-danger"]');
  }

  getDefaultFromDate(): string {
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const fromDate = `${year}-${monthFilter}-01`
    return fromDate
  }

  getDefaultToDate(): string {
    const getLastDayOfMonth = (year: number, month: number) => {
      month -= 1
      return new Date(year, month + 1, 0).getDate();
    }
    const [month, year] = [new Date().getMonth() + 1, new Date().getFullYear()]
    const lastDayOfMonth = getLastDayOfMonth(year, month)
    const monthFilter = month < 10 ? `0${month}` : `${month}`
    const toDate = `${year}-${monthFilter}-${lastDayOfMonth}`
    return toDate
  }

 onChangeDateRange(event: any) {
  const value = event.target.value as string;
  const datesArray = value.split(' to ');

  let newFromDate = '';
  let newToDate = '';

  if (datesArray.length >= 1) {
    newFromDate = datesArray[0];
    newToDate = datesArray[datesArray.length - 1];
  }

  if (newFromDate !== this.fromDate || newToDate !== this.toDate) {
    this.fromDate = newFromDate;
    this.toDate = newToDate;
    this.ngOnInit();
  }
}

  async getAllWeeklyCare(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getFindingsFailureMachine(from, to).subscribe(
        (res:any) => {
          this.dataWeeklyCare = res.data;
          this.dataWeeklyCarePet = this.dataWeeklyCare.filter(item => item.id_line == this.idLine)
          this.countDataWeeklyCare = this.dataWeeklyCarePet.length;
          this.countFindingsFailureMachine = this.dataWeeklyCarePet.length
          this.countWeeklyCarePreparation = this.dataWeeklyCarePet.filter(item => item.id_area == 4).length;
          this.countWeeklyCareInjection = this.dataWeeklyCarePet.filter(item => item.id_area == 5).length;
          this.countWeeklyCareBlow = this.dataWeeklyCarePet.filter(item => item.id_area == 6).length;
          this.countWeeklyCareFilling = this.dataWeeklyCarePet.filter(item => item.id_area == 7).length;
          this.countWeeklyCarePacking = this.dataWeeklyCarePet.filter(item => item.id_area == 8).length;
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        }
      );
    })
  }

  async getAllScheduleWeekly(from: string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getProgressScheduleWeekly(from, to).subscribe(
      (res:any) => {
        this.dataScheduleWeekly = res.data;
        this.dataScheduleWeeklyByLine = this.dataScheduleWeekly.filter(item => item.id_line == this.idLine)
        this.countDataScheduleWeekly = this.dataScheduleWeeklyByLine.length;
        this.dataSchedulePre = this.dataScheduleWeeklyByLine.filter(item => item.id_area == 4)
        this.dataScheduleInjec = this.dataScheduleWeeklyByLine.filter(item => item.id_area == 5)
        this.dataScheduleBlow = this.dataScheduleWeeklyByLine.filter(item => item.id_area == 6)
        this.dataScheduleFill = this.dataScheduleWeeklyByLine.filter(item => item.id_area == 7)
        this.dataSchedulePac = this.dataScheduleWeeklyByLine.filter(item => item.id_area == 8)
        resolve(true)
      },
      (error:any) => {
        console.error(error)
        reject(error)
      });
    })
  }

  progressScheduleWeekly(data: any[]){
    const totalProgres = data.length;
    const completeProgress = data.filter(item => item.follow_up === 1).length;

    if(totalProgres === 0){
      return 0;
    }

    const precentageProgress = (completeProgress / totalProgres) * 100;
    return precentageProgress;
  }

  async getProgressAutonomous(from: string, to: string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getProgressAutonomous(from, to).subscribe(
      (res:any) => {
        this.dataProgressAutonomous = res.data;
        this.dataProgressAutonomousPet = this.dataProgressAutonomous.filter(item => item.id_line == this.idLine)
        this.countDataAllAutonomous = this.dataProgressAutonomousPet.length
        this.dataPreAutonomous = this.dataProgressAutonomousPet.filter(item => item.id_area == 4);
        this.dataInjecAutonomous = this.dataProgressAutonomousPet.filter(item => item.id_area == 5);
        this.dataBlowAutonomous = this.dataProgressAutonomousPet.filter(item => item.id_area == 6);
        this.dataFilAutonomous = this.dataProgressAutonomousPet.filter(item => item.id_area == 7);
        this.dataPacAutonomous = this.dataProgressAutonomousPet.filter(item => item.id_area == 8);

        this.progressPreAuto = Math.round(this.progressAutonomous(this.dataPreAutonomous));
        this.progressInjecAuto = Math.round(this.progressAutonomous(this.dataInjecAutonomous));
        this.progressBlowAuto = Math.round(this.progressAutonomous(this.dataBlowAutonomous));
        this.progressFilAuto = Math.round(this.progressAutonomous(this.dataFilAutonomous));
        this.progressPacAuto = Math.round(this.progressAutonomous(this.dataPacAutonomous));  

        resolve(true)
      },
      (error:any) => {
        console.error(error)
        reject(error)
      });
    })
  }

  progressAutonomous(data: any[]){
    const totalProgres = data.length;
    const completeProgress = data.filter(item => item.id_status === 3).length;

    if(totalProgres === 0){
      return 0;
    }

    const precentageProgress = (completeProgress / totalProgres) * 100;
    return precentageProgress;
  }

  async getAllMasterPlan(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getProgressMasterPlan(from, to).subscribe(
        (res:any) => {
          this.dataAllMasterPlan = res.data;
          this.dataAllMasterPlanByLine = this.dataAllMasterPlan.filter(item => item.id_line == this.idLine)

          this.countDataMasterPlan = this.dataAllMasterPlanByLine.length;
          this.dataMasterPreparation = this.dataAllMasterPlanByLine.filter(item => item.id_type == 4);
          this.dataMasterInjection = this.dataAllMasterPlanByLine.filter(item => item.id_type == 5);
          this.dataMasterBlow = this.dataAllMasterPlanByLine.filter(item => item.id_type == 6);
          this.dataMasterFilling = this.dataAllMasterPlanByLine.filter(item => item.id_type == 7);
          this.dataMasterPacking = this.dataAllMasterPlanByLine.filter(item => item.id_type == 8);

          this.dataPrePreventive = this.dataMasterPreparation.filter(item => item.id_subcategory == 1);
          this.dataPreCorective = this.dataMasterPreparation.filter(item => item.id_subcategory == 2);
          this.dataPreOverHaul = this.dataMasterPreparation.filter(item => item.id_subcategory == 3);

          this.dataInjecPreventive = this.dataMasterInjection.filter(item => item.id_subcategory == 1);
          this.dataInjecCorective = this.dataMasterInjection.filter(item => item.id_subcategory == 2);
          this.dataInjecOverHaul = this.dataMasterInjection.filter(item => item.id_subcategory == 3);

          this.dataBlowPreventive = this.dataMasterBlow.filter(item => item.id_subcategory == 1);
          this.dataBlowCorective = this.dataMasterBlow.filter(item => item.id_subcategory == 2);
          this.dataBlowOverHaul = this.dataMasterBlow.filter(item => item.id_subcategory == 3);

          this.dataFilPreventive = this.dataMasterFilling.filter(item => item.id_subcategory == 1);
          this.dataFilCorective = this.dataMasterFilling.filter(item => item.id_subcategory == 2);
          this.dataFilOverHaul = this.dataMasterFilling.filter(item => item.id_subcategory == 3);

          this.dataPacPreventive = this.dataMasterPacking.filter(item => item.id_subcategory == 1);
          this.dataPacCorective = this.dataMasterPacking.filter(item => item.id_subcategory == 2);
          this.dataPacOverHaul = this.dataMasterPacking.filter(item => item.id_subcategory == 3);
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
      })
    }

  progressMasterPlan(data: any[]){
    const totalProgres = data.length;
    const completeProgress = data.filter(item => item.id_status === 3).length;

    if(totalProgres === 0){
      return 0;
    }

    const precentageProgress = (completeProgress / totalProgres) * 100;
    return precentageProgress;
  }

  async getAllPic(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getAllPic(from, to).subscribe(
        (res:any) => {
          this.dataAllPic = res.data;
          this.dataAllPicByLine = this.dataAllPic.filter(item => item.line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getPicMasterPlan(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getPicMasterPlan(from, to).subscribe(
        (res:any) => {
          this.dataPicMaster = res.data;
          this.dataPicMasterByLine = this.dataPicMaster.filter(item => item.id_line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getPicAutonomous(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getPicAutonomous(from, to).subscribe(
        (res:any) => {
          this.dataPicAutonomous = res.data;
          this.dataPicAutonomousByLine = this.dataPicAutonomous.filter(item => item.id_line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getPicScheduleWeekly(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getPicScheduleWeekly(from, to).subscribe(
        (res:any) => {
          this.dataPicScheduleWeekly = res.data;
          this.dataPicScheduleWeeklyByLine = this.dataPicScheduleWeekly.filter(item => item.id_line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getPicFindingWeekly(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getPicFindingWeekly(from, to).subscribe(
        (res:any) => {
          this.dataPicFindingWeekly = res.data;
          this.dataPicFindingWeeklyByLine = this.dataPicFindingWeekly.filter(item => item.id_line == this.idLine)
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getPicScheduleMaintenance(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getPicScheduleMaintenance(from, to).subscribe(
        (res:any) => {
          this.dataPicScheduleMaintenance = res.data;
          console.log(this.dataPicScheduleMaintenance)
          this.dataPicScheduleMaintenanceByLine = this.dataPicScheduleMaintenance.filter(item => item.line === "pet")
          console.log(this.dataPicScheduleMaintenanceByLine);
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

  async getAllScheduleMaintenance(from:string, to:string){
    return new Promise((resolve, reject) => {
      this.RestApiService.getProgressMaintenance(from, to).subscribe(
        (res:any) => {
          this.dataAllScheduleMaintenance = res.data;
          this.dataAllScheduleMaintenancePet = this.dataAllScheduleMaintenance.filter(item => item.id_status !== 5 && item.line === 'pet')
          this.dataMechaYetToStart = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 1 && item.category === 'Mechanical')
          this.dataMechaOnProgress = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 2 && item.category === 'Mechanical')
          this.dataMechaDone = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 3 && item.category === 'Mechanical')
          this.dataMechaPending = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 4 && item.category === 'Mechanical')
          this.dataElecYetToStart = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 1 && item.category === 'Electrical')
          this.dataElecOnProgress = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 2 && item.category === 'Electrical')
          this.dataElecDone = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 3 && item.category === 'Electrical')
          this.dataElecPending = this.dataAllScheduleMaintenancePet.filter(item => item.id_status == 4 && item.category === 'Electrical')
          this.countDataAllScheduleMaintenance = this.dataAllScheduleMaintenancePet.length;
          this.updateScheduleCounts();      
          resolve(true)
        },
        (error:any) => {
          console.error(error)
          reject(error)
        });
    });
  }

        /**
   * Open modal
   * @param list_mecha_new modal content
   */
        openModalMechaNew(list_mecha_new: any) {
          const modalRef = this.modalService.open(list_mecha_new, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataMechaYetToStart;
        }
        
           /**
     * Open modal
     * @param list_mecha_onProgress modal content
     */
        openModalMechaOnProgress(list_mecha_onProgress: any) {
          const modalRef = this.modalService.open(list_mecha_onProgress, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataMechaOnProgress;
        }
  
       /**
     * Open modal
     * @param list_mecha_done modal content
     */
        openModalMechaDone(list_mecha_done: any) {
          const modalRef = this.modalService.open(list_mecha_done, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataMechaDone;
        }
  
      /**
     * Open modal
     * @param list_mecha_pending modal content
     */
        openModalMechaPending(list_mecha_pending: any) {
          const modalRef = this.modalService.open(list_mecha_pending, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataMechaPending;
        }
  
              /**
     * Open modal
     * @param list_elec_new modal content
     */
        openModalElecNew(list_elec_new: any) {
          const modalRef = this.modalService.open(list_elec_new, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataElecYetToStart;
        }
        
            /**
      * Open modal
      * @param list_elec_onProgress modal content
      */
        openModalElecOnProgress(list_elec_onProgress: any) {
          const modalRef = this.modalService.open(list_elec_onProgress, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataElecOnProgress;
        }
  
        /**
      * Open modal
      * @param list_elec_done modal content
      */
        openModalElecDone(list_elec_done: any) {
          const modalRef = this.modalService.open(list_elec_done, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataElecDone;
        }
  
      /**
      * Open modal
      * @param list_elec_pending modal content
      */
        openModalElecPending(list_elec_pending: any) {
          const modalRef = this.modalService.open(list_elec_pending, { size: 'lg', centered: true });
          (modalRef.componentInstance as any).data = this.dataElecPending;
        }

        getNewData(){
          this.RestApiService.getNewData().subscribe(
            (res:any) => {
              this.newDataScheduleMaintenance = res.data[0];
              this.newDataScheduleMaintenanceByLine = this.newDataScheduleMaintenance.filter(item => item.line === "pet")
              this.countDataNewScheduleMaintenance = this.newDataScheduleMaintenanceByLine.length;
            },
            (error:any) => {
              console.error(error)
            }
          );
        }

  updateScheduleCounts(){
    const mechanicalData = this.dataAllScheduleMaintenancePet.filter((item:any) => {
      return item.category === 'Mechanical';
    });
    const electricalData = this.dataAllScheduleMaintenancePet.filter((item: any) => {
      return item.category === 'Electrical';
    });

    const statusIds = [1, 2, 3, 4];

    const mechanicalCounts: any = {};
    statusIds.forEach((statusId: number) => {
      const count = mechanicalData.filter((item: any) => item.id_status === statusId).length;
      mechanicalCounts[statusId] = count;
    });

    const electricalCounts: any = {};
    statusIds.forEach((statusId: number) => {
      const count = electricalData.filter((item: any) => item.id_status === statusId).length;
      electricalCounts[statusId] = count;
    });

    this.countYetToStartMechanical = mechanicalCounts[1] || 0;
    this.countOnProgressMechanical = mechanicalCounts[2] || 0;
    this.countCompletedMechanical = mechanicalCounts[3] || 0;
    this.countPendingMechanical = mechanicalCounts[4] || 0;

    this.countYetToStartElectrical = electricalCounts[1] || 0;
    this.countOnProgressElectrical = electricalCounts[2] || 0;
    this.countCompletedElectrical = electricalCounts[3] || 0;
    this.countPendingElectrical = electricalCounts[4] || 0;
    }

  // Chart Colors Set
  private getChartColorsArray(colors: any) {
    colors = JSON.parse(colors);
    return colors.map(function (value: any) {
      var newValue = value.replace(" ", "");
      if (newValue.indexOf(",") === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
        if (color) {
          color = color.replace(" ", "");
          return color;
        }
        else return newValue;;
      } else {
        var val = value.split(',');
        if (val.length == 2) {
          var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
          rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
          return rgbaColor;
        } else {
          return newValue;
        }
      }
    });
  }

  /**
 *  Status7
 */
  setstatusvalue(value: any) {
    if (value == 'all') {
      this.status7.series = [125, 42, 58, 89]
    }
    if (value == '7') {
      this.status7.series = [25, 52, 158, 99]
    }
    if (value == '30') {
      this.status7.series = [35, 22, 98, 99]
    }
    if (value == '90') {
      this.status7.series = [105, 32, 68, 79]
    }
  }

  _status7(colors: any) {
    colors = this.getChartColorsArray(colors);
    const mechanicalData = this.dataAllScheduleMaintenancePet.filter(item => item.category === 'Mechanical');
    const electricalData = this.dataAllScheduleMaintenancePet.filter(item => item.category === 'Electrical');

    const mechanicalCount = mechanicalData.length;
    const electricalCount = electricalData.length;
    this.status7= {
      series: [mechanicalCount, electricalCount],
      labels: ["Mechanical", "Electrical"],
      chart: {
        type: "donut",
        height: 230,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        pie: {
          offsetX: 0,
          offsetY: 0,
          donut: {
            size: "90%",
            labels: {
              show: false,
            }
          },
        },
      },
      dataLabels: {
        enabled: false,
      },
      legend: {
        show: false,
      },
      stroke: {
        lineCap: "round",
        width: 0
      },
      colors: colors
    };
  }

   /**
 * Simple Pie Chart
 */
    _weeklyCareFindings(colors:any) {
    colors = this.getChartColorsArray(colors);
    const countPreparation = this.countWeeklyCarePreparation
    const countInjection = this.countWeeklyCareInjection
    const countBlow = this.countWeeklyCareBlow
    const countFilling = this.countWeeklyCareFilling
    const countPacking = this.countWeeklyCarePacking
    this.simplePieChart = {
      series: [countPreparation, countInjection, countBlow, countFilling, countPacking],
      chart: {
        height: 340,
        type: "pie",
      },
      labels: ["Preparation", "Injection", "Blow", "Filling", "Packing"],
      legend: {
        position: "bottom",
      },
      dataLabels: {
        dropShadow: {
          enabled: false,
        },
      },
      colors: colors,
    };
  }

  /**
 * Stacked Column Charts
 */
  _StackedColumnChart(colors:any) {
    colors = this.getChartColorsArray(colors);
    const allPic: { [key: string]: { [key: string]: number } } = {};
    this.dataAllPicByLine.forEach((item: any) => {
      console.log(this.dataAllPicByLine);
      
    if (!allPic[item.pic]) {
        allPic[item.pic] = {};
    }
    if (!allPic[item.pic][item.table_name]) {
        allPic[item.pic][item.table_name] = 0;
    }
    allPic[item.pic][item.table_name] += item.total;
    });

    const allDataPic = Object.keys(allPic);
    const dataPicMaster = allDataPic.map((pic: any) => allPic[pic]['Master Plan'] || 0);
    const dataPicAuto = allDataPic.map((pic: any) => allPic[pic]['Autonomous Maintenance'] || 0);
    const dataPicSchedule = allDataPic.map((pic: any) => allPic[pic]['Schedule Weekly Care'] || 0);
    const dataPicFinding = allDataPic.map((pic: any) => allPic[pic]['Finding Weekly Care'] || 0);
    const dataPicMaintenance = allDataPic.map((pic: any) => allPic[pic]['Schedule Maintenance'] || 0);
    this.StackedColumnChart = {
      series: [{ name: "Master Plan", data: dataPicMaster },
              { name: "Autonomous Minatenance", data: dataPicAuto },
              { name: "Schedule Weekly Care", data: dataPicSchedule },
              { name: "Finding Weekly Care", data: dataPicFinding },
              { name: "Schedule Maintenance", data: dataPicMaintenance }],
      chart: {
        type: "bar",
        height: 350,
        stacked: true,
        toolbar: {
            show: false,
        },
        zoom: {
            enabled: false,
        },
      },
      responsive: [{
          breakpoint: 480,
          options: {
              legend: {
                  position: "bottom",
                  offsetX: 10,
                  offsetY: 0,
              },
          },
      }, ],
      plotOptions: {
          bar: {
              horizontal: false,
              // borderRadius: 10,
          },
      },
      xaxis: {
          type: "text",
          categories: allDataPic
      },
      legend: {
          // position: "bottom",
          // offsetY: 40,
      },
      fill: {
          opacity: 1,
      },
      colors: colors,
    };
   }

   setPicValue(value: any) {
    //all pic
    const allPic: { [key: string]: { [key: string]: number } } = {};
    this.dataAllPicByLine.forEach((item: any) => {
    if (!allPic[item.pic]) {
        allPic[item.pic] = {};
    }
    if (!allPic[item.pic][item.table_name]) {
        allPic[item.pic][item.table_name] = 0;
    }
    allPic[item.pic][item.table_name] += item.total;
    });

    const allDataPic = Object.keys(allPic);
    const dataPicMaster = allDataPic.map((pic: any) => allPic[pic]['Master Plan'] || 0);
    const dataPicAuto = allDataPic.map((pic: any) => allPic[pic]['Autonomous Maintenance'] || 0);
    const dataPicSchedule = allDataPic.map((pic: any) => allPic[pic]['Schedule Weekly Care'] || 0);
    const dataPicFinding = allDataPic.map((pic: any) => allPic[pic]['Finding Weekly Care'] || 0);
    const dataPicMaintenance = allDataPic.map((pic: any) => allPic[pic]['Schedule Maintenance'] || 0);

    //master
    const picMaster: { [key: string]: { [key: string]: number } } = {};
    this.dataPicMasterByLine.forEach((item: any) => {
    if (!picMaster[item.pic]) {
        picMaster[item.pic] = {};
    }
    if (!picMaster[item.pic][item.id_type]) {
        picMaster[item.pic][item.id_type] = 0;
    }
    picMaster[item.pic][item.id_type] += item.total;
    });

    const masterPic = Object.keys(picMaster);
    const dataPreMaster = masterPic.map((pic: any) => picMaster[pic][4] || 0);
    const dataInjecMaster = masterPic.map((pic: any) => picMaster[pic][5] || 0);
    const dataBlowMaster = masterPic.map((pic: any) => picMaster[pic][6] || 0);
    const dataFilMaster = masterPic.map((pic: any) => picMaster[pic][7] || 0);
    const dataPacMaster = masterPic.map((pic: any) => picMaster[pic][8] || 0);

    //Autonomous
    const picAutonomous: { [key: string]: { [key: string]: number } } = {};
    this.dataPicAutonomousByLine.forEach((item: any) => {
    if (!picAutonomous[item.pic]) {
        picAutonomous[item.pic] = {};
    }
    if (!picAutonomous[item.pic][item.id_area]) {
        picAutonomous[item.pic][item.id_area] = 0;
    }
    picAutonomous[item.pic][item.id_area] += item.total;
    });

    const AutonomousPic = Object.keys(picAutonomous);
    const dataPreAutonomous = AutonomousPic.map((pic: any) => picAutonomous[pic][4] || 0);
    const dataInjecAutonomous = AutonomousPic.map((pic: any) => picAutonomous[pic][5] || 0);
    const dataBlowAutonomous = AutonomousPic.map((pic: any) => picAutonomous[pic][6] || 0);
    const dataFilAutonomous = AutonomousPic.map((pic: any) => picAutonomous[pic][7] || 0);
    const dataPacAutonomous = AutonomousPic.map((pic: any) => picAutonomous[pic][8] || 0);

    //Schedule weekly
    const picSchedule: { [key: string]: { [key: string]: number } } = {};
    this.dataPicScheduleWeeklyByLine.forEach((item: any) => {
    if (!picSchedule[item.pic]) {
        picSchedule[item.pic] = {};
    }
    if (!picSchedule[item.pic][item.id_area]) {
        picSchedule[item.pic][item.id_area] = 0;
    }
    picSchedule[item.pic][item.id_area] += item.total;
    });

    const schedulePic = Object.keys(picSchedule);
    const dataPreSchedule = schedulePic.map((pic: any) => picSchedule[pic][4] || 0);
    const dataInjecSchedule = schedulePic.map((pic: any) => picSchedule[pic][5] || 0);
    const dataBlowSchedule = schedulePic.map((pic: any) => picSchedule[pic][6] || 0);
    const dataFilSchedule = schedulePic.map((pic: any) => picSchedule[pic][7] || 0);
    const dataPacSchedule = schedulePic.map((pic: any) => picSchedule[pic][8] || 0);
    
    //Finding weekly
    const picFinding: { [key: string]: { [key: string]: number } } = {};
    this.dataPicFindingWeeklyByLine.forEach((item: any) => {
    if (!picFinding[item.genba]) {
        picFinding[item.genba] = {};
    }
    if (!picFinding[item.genba][item.id_area]) {
        picFinding[item.genba][item.id_area] = 0;
    }
    picFinding[item.genba][item.id_area] += item.total;
    });

    const findingPic = Object.keys(picFinding);
    const dataPreFinding = findingPic.map((genba: any) => picFinding[genba][4] || 0);
    const dataInjecFinding = findingPic.map((genba: any) => picFinding[genba][5] || 0);
    const dataBlowFinding = findingPic.map((genba: any) => picFinding[genba][6] || 0);
    const dataFilFinding = findingPic.map((genba: any) => picFinding[genba][7] || 0);
    const dataPacFinding = findingPic.map((genba: any) => picFinding[genba][8] || 0);

    //Schedule maintenance
    const picMaintenance: { [key: string]: { [key: string]: number } } = {};
    this.dataPicScheduleMaintenanceByLine.forEach((item: any) => {
    if (!picMaintenance[item.pic]) {
        picMaintenance[item.pic] = {};
    }
    if (!picMaintenance[item.pic][item.category]) {
        picMaintenance[item.pic][item.category] = 0;
    }
    picMaintenance[item.pic][item.category] += item.total;
    });

    const maintenancePic = Object.keys(picMaintenance);
    const dataMechaMaintenance = maintenancePic.map((pic: any) => picMaintenance[pic]['Mechanical'] || 0);
    const dataElecMaintenance = maintenancePic.map((pic: any) => picMaintenance[pic]['Electrical'] || 0);

    if (value == 'all-pic') {
      this.StackedColumnChart.series = [{ name: "Master Plan", data: dataPicMaster },
                                        { name: "Autonomous Minatenance", data: dataPicAuto },
                                        { name: "Schedule Weekly Care", data: dataPicSchedule },
                                        { name: "Finding Weekly Care", data: dataPicFinding },
                                        { name: "Schedule Maintenance", data: dataPicMaintenance }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: allDataPic
                                      }
    }

    if (value == 'master-plan') {
      this.StackedColumnChart.series = [{ name: "Preparation", data: dataPreMaster },
                                        { name: "Injection", data: dataInjecMaster },
                                        { name: "Blow", data: dataBlowMaster },
                                        { name: "Filling", data: dataFilMaster },
                                        { name: "Packing", data: dataPacMaster }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: masterPic
                                      }
    }
    if (value == 'autonomous') {
      this.StackedColumnChart.series = [{ name: "Preparation", data: dataPreAutonomous},
                                        { name: "Injection", data: dataInjecAutonomous },
                                        { name: "Blow", data: dataBlowAutonomous },
                                        { name: "Filling", data: dataFilAutonomous },
                                        { name: "Packing", data: dataPacAutonomous }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: AutonomousPic
                                      }
    }
    if (value == 'schedule-weekly') {
      this.StackedColumnChart.series = [{ name: "Preparation", data: dataPreSchedule },
                                        { name: "Injection", data: dataInjecSchedule },
                                        { name: "Blow", data: dataBlowSchedule },
                                        { name: "Filling", data: dataFilSchedule },
                                        { name: "Packing", data: dataPacSchedule }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: schedulePic
                                      }
    }
    if (value == 'finding-weekly') {
      this.StackedColumnChart.series = [{ name: "Preparation", data: dataPreFinding },
                                        { name: "Injection", data: dataInjecFinding },
                                        { name: "Blow", data: dataBlowFinding },
                                        { name: "Filling", data: dataFilFinding },
                                        { name: "Packing", data: dataPacFinding }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: findingPic
                                      }
    }
    if (value == 'schedule-maintenance') {
      this.StackedColumnChart.series = [{ name: "Mechanical", data: dataMechaMaintenance},
                                        { name: "Electrical", data: dataElecMaintenance }]
      this.StackedColumnChart.xaxis = {
                                          type: "text",
                                          categories: maintenancePic
                                      }
    }
  }

 _multipleRadialbarChart(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressScheduleWeekly(this.dataScheduleWeeklyByLine));
    const preparationProgress = Math.round(this.progressScheduleWeekly(this.dataSchedulePre));
    const injectionProgress = Math.round(this.progressScheduleWeekly(this.dataScheduleInjec));
    const blowProgress = Math.round(this.progressScheduleWeekly(this.dataScheduleBlow));
    const fillingProgress = Math.round(this.progressScheduleWeekly(this.dataScheduleFill));
    const packingProgress = Math.round(this.progressScheduleWeekly(this.dataSchedulePac));  
    this.multipleRadialbarChart = {
      series: [preparationProgress, injectionProgress, blowProgress, fillingProgress, packingProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preparation", "Injection", "Blow", "Filling", "Packing"],
      colors: colors,
    };
  }

  _preparationMasterPlan(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressMasterPlan(this.dataMasterPreparation));
    const preventiveProgress = Math.round(this.progressMasterPlan(this.dataPrePreventive));
    const corectiveProgress = Math.round(this.progressMasterPlan(this.dataPreCorective));
    const overHaulProgress = Math.round(this.progressMasterPlan(this.dataPreOverHaul));  
    this.preparationMasterChart = {
      series: [preventiveProgress, corectiveProgress, overHaulProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preventive", "Corective", "Over Haul"],
      colors: colors,
    };
  }

  _injectionMasterPlan(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressMasterPlan(this.dataMasterInjection));
    const preventiveProgress = Math.round(this.progressMasterPlan(this.dataInjecPreventive));
    const corectiveProgress = Math.round(this.progressMasterPlan(this.dataInjecCorective));
    const overHaulProgress = Math.round(this.progressMasterPlan(this.dataInjecOverHaul));  
    this.injectionMasterChart = {
      series: [preventiveProgress, corectiveProgress, overHaulProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preventive", "Corective", "Over Haul"],
      colors: colors,
    };
  }

  _blowMasterPlan(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressMasterPlan(this.dataMasterBlow));
    const preventiveProgress = Math.round(this.progressMasterPlan(this.dataBlowPreventive));
    const corectiveProgress = Math.round(this.progressMasterPlan(this.dataBlowCorective));
    const overHaulProgress = Math.round(this.progressMasterPlan(this.dataBlowOverHaul));  
    this.blowMasterChart = {
      series: [preventiveProgress, corectiveProgress, overHaulProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preventive", "Corective", "Over Haul"],
      colors: colors,
    };
  }

  _fillingMasterPlan(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressMasterPlan(this.dataMasterFilling));
    const preventiveProgress = Math.round(this.progressMasterPlan(this.dataFilPreventive));
    const corectiveProgress = Math.round(this.progressMasterPlan(this.dataFilCorective));
    const overHaulProgress = Math.round(this.progressMasterPlan(this.dataFilOverHaul));  
    this.fillingMasterChart = {
      series: [preventiveProgress, corectiveProgress, overHaulProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preventive", "Corective", "Over Haul"],
      colors: colors,
    };
  }

  _packingMasterPlan(colors:any) {
    colors = this.getChartColorsArray(colors);
    const totalProgres = Math.round(this.progressMasterPlan(this.dataMasterPacking));
    const preventiveProgress = Math.round(this.progressMasterPlan(this.dataPacPreventive));
    const corectiveProgress = Math.round(this.progressMasterPlan(this.dataPacCorective));
    const overHaulProgress = Math.round(this.progressMasterPlan(this.dataPacOverHaul));   
    this.packingMasterChart = {
      series: [preventiveProgress, corectiveProgress, overHaulProgress],
      chart: {
          height: 290,
          type: "radialBar",
      },
      plotOptions: {
          radialBar: {
              dataLabels: {
                  name: {
                      fontSize: "22px",
                  },
                  value: {
                      fontSize: "16px",
                  },
                  total: {
                      show: true,
                      label: "Total",
                      formatter: function () {
                        return totalProgres.toString() + "%";
                      }
                  },
              },
          },
      },
      labels: ["Preparation", "Filling", "Packing"],
      colors: colors,
    };
  }

  /**
 * Distributed Columns Charts
   */
  private _distributedColumnChart(colors:any) {
    colors = this.getChartColorsArray(colors);
    this.distributedColumnChart = {
      series: [{
        data: [21, 22, 10, 28, 16, 21, 13, 30]
      }],
      chart: {
        height: 350,
        type: 'bar',
        events: {
          click: function (chart:any, w:any, e:any) {
          }
        }
      },
      colors: colors,
      plotOptions: {
        bar: {
          columnWidth: '45%',
          distributed: true,
        }
      },
      dataLabels: {
        enabled: false
      },
      legend: {
        show: false
      },
      xaxis: {
        categories: [
          ['John', 'Doe'],
          ['Joe', 'Smith'],
          ['Jake', 'Williams'],
          'Amber',
          ['Peter', 'Brown'],
          ['Mary', 'Evans'],
          ['David', 'Wilson'],
          ['Lily', 'Roberts'],
        ],
        labels: {
          style: {
            colors: colors,
            fontSize: '12px'
          }
        }
      }
    };
  }

  getTask(){
    this.RestApiService.getAllTaskImprovement().subscribe(
      (res:any) => {
        this.dataTask = res.data;
        this.dataTaskByLine = this.dataTask.filter(item => item.id_line == this.idLine)
        this.dataDatePriority = [...this.dataTask]
        this.totalImprovement = this.dataTaskByLine.length
        this.checkDatePriority();
        this.paginateData();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  checkDate(due_date: string): string {
    const currentDate = new Date();
    const taskDueDate = new Date(due_date);
    const differenceInTime = taskDueDate.getTime() - currentDate.getTime();
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);
  
    if (differenceInDays <= -1) {
      return 'Over Date';
    } else if (differenceInDays <= 0) {
      return 'Urgent';
    } else if (differenceInDays <= 6) {
      return 'High';
    } else if (differenceInDays <= 29) {
      return 'Medium';
    } else {
      return 'Low';
    }
  }

  checkDatePriority() {
    this.dataDatePriority.forEach(task => {
      const priority = this.checkDate(task.due_date);
      const newPriorityId = this.getPriorityId(priority);
      
      if (task.id_priority !== newPriorityId) {
        const newDueDate = new Date(task.due_date);
        newDueDate.setDate(newDueDate.getDate() + 1);
        task.due_date = newDueDate.toISOString().split('T')[0];
        task.id_priority = newPriorityId;
        this.updateTask(task, task.id);
      }
    });
  }

  updateTask(dataTaskByLine: any, id:any) {
    this.RestApiService.updateTask(id, dataTaskByLine).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.newTaskImprovement= dataTaskByLine;
          this.getTask()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  } 

  paginateData() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    this.dataForCurrentPage = this.dataTaskByLine.slice(startIndex, endIndex);
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.paginateData();
  }

  exportImprovementToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('improvement') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'improvement.png');
        this.exporting = false;
      });
    });
  }

  exportScheduleMaintenanceToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('schedule_maintenance') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'schedule_maintenance.png');
        this.exporting = false;
      });
    });
  }

  exportAutonomousMaintenanceToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('autonomous_maintenance') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'autonomous_maintenance.png');
        this.exporting = false;
      });
    });
  }

  exportWeeklyCareToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('weekly_care') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'weekly_care.png');
        this.exporting = false;
      });
    });
  }

  exportPicToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('most_diligent') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'most_diligent.png');
        this.exporting = false;
      });
    });
  }

  exportMasterPlanToPNG(): void {
    this.exporting = true;
    const element = document.getElementById('master_plan') as HTMLElement;
    html2canvas(element).then((canvas: any) => {
      canvas.toBlob((blob: any) => {
        saveAs(blob, 'master_plan.png');
        this.exporting = false;
      });
    });
  }

  getStatusLabelClass(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'success'; // New
        case 2:
            return 'info'; // On Progress
        case 3:
            return 'secondary'; // Done
        case 4:
            return 'danger'; // Late
        default:
            return 'secondary';
    }
}

getStatusLabel(id_status: number): string {
    switch (id_status) {
        case 1:
            return 'New';
        case 2:
            return 'On Progress';
        case 3:
            return 'Done';
        case 4:
            return 'Late';
        default:
            return 'Unknown';
    }
}

getPriorityLabelClass(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'primary'; 
      case 2:
          return 'warning'; 
      case 3:
          return 'orange';
      case 4:
          return 'danger'; 
      case 5:
          return 'danger';
      default:
          return 'secondary';
  }
}

getPriorityLabel(id_status: number): string {
  switch (id_status) {
      case 1:
          return 'Low';
      case 2:
          return 'Medium';
      case 3:
          return 'High';
      case 4:
          return 'Urgent';
      case 5:
          return 'Over Date';
      default:
          return 'Unknown';
  }
}

getPriorityId(priority: string): number {
  switch (priority) {
    case 'Over Date':
      return 5; 
    case 'Urgent':
      return 4; 
    case 'High':
      return 3; 
    case 'Medium':
      return 2; 
    case 'Low':
      return 1; 
    default:
      return 1; 
  }
}

getStatusId(priority: string): number {
  switch (priority) {
    case 'Late':
      return 4; 
    case 'Done':
      return 3; 
    case 'On Progress':
      return 2; 
    case 'New':
      return 1; 
    default:
      return 1; 
  }
}

}
