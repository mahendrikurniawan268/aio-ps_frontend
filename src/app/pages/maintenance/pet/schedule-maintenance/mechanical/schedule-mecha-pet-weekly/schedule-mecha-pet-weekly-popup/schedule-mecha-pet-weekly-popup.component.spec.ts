import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetWeeklyPopupComponent } from './schedule-mecha-pet-weekly-popup.component';

describe('ScheduleMechaPetWeeklyPopupComponent', () => {
  let component: ScheduleMechaPetWeeklyPopupComponent;
  let fixture: ComponentFixture<ScheduleMechaPetWeeklyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetWeeklyPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetWeeklyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
