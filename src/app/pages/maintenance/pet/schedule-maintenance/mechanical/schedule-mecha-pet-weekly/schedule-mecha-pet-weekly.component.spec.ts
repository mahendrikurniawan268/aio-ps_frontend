import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetWeeklyComponent } from './schedule-mecha-pet-weekly.component';

describe('ScheduleMechaPetWeeklyComponent', () => {
  let component: ScheduleMechaPetWeeklyComponent;
  let fixture: ComponentFixture<ScheduleMechaPetWeeklyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetWeeklyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetWeeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
