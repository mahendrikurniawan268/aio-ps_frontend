import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedule-mecha-pet-weekly-popup',
  templateUrl: './schedule-mecha-pet-weekly-popup.component.html',
  styleUrls: ['./schedule-mecha-pet-weekly-popup.component.scss']
})
export class ScheduleMechaPetWeeklyPopupComponent implements OnInit {
  @Input() dataWeekly: any = {}

  constructor(private RestApiService: RestApiService,  
  private activeModal: NgbActiveModal,
  private router: Router) {}
  schedule_maintenance: any;
  public activity_group: any = {};

  ngOnInit(): void {
  }

  getWeekly(){
    this.RestApiService.getMechaWeekly().subscribe(
      (res:any) => {
        this.dataWeekly = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onClose(){
    this.activeModal.close();
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}