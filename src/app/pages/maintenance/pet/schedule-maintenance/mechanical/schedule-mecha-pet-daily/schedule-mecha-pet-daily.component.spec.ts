import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetDailyComponent } from './schedule-mecha-pet-daily.component';

describe('ScheduleMechaPetDailyComponent', () => {
  let component: ScheduleMechaPetDailyComponent;
  let fixture: ComponentFixture<ScheduleMechaPetDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetDailyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
