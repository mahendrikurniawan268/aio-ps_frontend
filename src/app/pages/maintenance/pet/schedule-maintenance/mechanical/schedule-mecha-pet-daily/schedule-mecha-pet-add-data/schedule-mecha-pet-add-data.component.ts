import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ScheduleMaintenancePet } from '../../../electrical/schedule-elec-pet-daily/schedule-maintenance-pet.model';

@Component({
  selector: 'app-schedule-mecha-pet-add-data',
  templateUrl: './schedule-mecha-pet-add-data.component.html',
  styleUrls: ['./schedule-mecha-pet-add-data.component.scss']
})
export class ScheduleMechaPetAddDataComponent {

  public schedule:any = [];
  public idParam:any;
  public scheduleSaveData: boolean[] = [];
  dataDaily:any;
  dataFunction:any [] = [];
  functionLocationOptions: string[] = [];
  functionLocationSearchKeyword: string = '';
  workCenterOptions = ['Production', 'Maintenance'];
  workCenterSearchKeyword: string = '';

  newScheduleMaintenance : ScheduleMaintenancePet = {
  function_location: null,
  equipment: '',
  operation_task: '',
  description_material: '',
  date: '',
  pic: [],
  email: [],
  qty: null,
  status_part: '',
  work_center: '',
  duration: '',
  notes: '',
  category: 'Mechanical',
  unplanned: 1,
  line: 'pet'
}

  // bread crumb items
  breadCrumbItems!: Array < {} > ;
  idSchedule: any;
  schedule_maintenance: any;
  scheduleById: any;
  statusPartOptions = ['Ready Stock', 'Stock Not Available'];
  statusPartrSearchKeyword: string = '';
  namePic: any [] = [];
  picEmployee: any;
  functionLocation:any [] = [];
  functionLocationPet: any = [];

  constructor(
    private router: Router,
    private RestApiService: RestApiService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {}
  
  ngOnInit(): void {
    this.getDaily();
    this.getNamePic();
    this.getFunctionLocation();
    this.idParam = this.route.snapshot.paramMap.get('id');
    if (this.idParam) {
      this.getScheduleMaintenanceById(this.idParam);
    }
  }

  onPic(event: any, type?: string) {
    const selectedPics = event.map((selectedItem: any) => {
      const selectedPic = this.picEmployee.find((item: any) => item.name === selectedItem.name);
      if (selectedPic) {
        return { name: selectedPic.name, email: selectedPic.email };
      }
      return null;
    }).filter((pic: any) => pic !== null);
    const picNames = selectedPics.map((pic: any) => pic.name);
    const emails = selectedPics.map((pic: any) => pic.email);
    if (type !== 'update') {
      this.newScheduleMaintenance.pic = picNames.join(', ');
      this.newScheduleMaintenance.email = emails.join(', ');
    } else {
      this.newScheduleMaintenance.pic = picNames
      this.newScheduleMaintenance.email = emails
    }
  }

  getFunctionLocation(){
    this.RestApiService.getAllFunctionLocation().subscribe(
      (res:any) => {
        this.functionLocation = res.data;
        this.functionLocationPet = this.functionLocation.filter(item => item.line === 'pet')
      }
    )
  }

  getNamePic(){
    this.RestApiService.getNamePic().subscribe(
      (res: any) => {
        this.namePic = res.data;
        this.picEmployee = this.namePic.filter(item => item.role_id === 4)
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

   getDaily(){
    this.RestApiService.getMechaDaily().subscribe(
      (res:any) => {
        this.dataDaily = res.data[0]; 
        this.dataFunction = this.dataDaily.function_location
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  onWorkCenterSelect(selectedItem: string) {
    this.newScheduleMaintenance.work_center = selectedItem;
  }

  onStatusPartSelect(selectedItem: string) {
    this.newScheduleMaintenance.status_part = selectedItem;
  }

  forBack() {
    this.router.navigate(['maintenance/pet/schedule/mecha/pet/daily']);
  }

  onClick() {
    if (this.isDataValid(this.newScheduleMaintenance)) {
      this.insertSchedule(this.newScheduleMaintenance);
      this.router.navigate(['maintenance/pet/schedule/mecha/pet/daily']);
      Swal.fire({
        title: 'Notification',
        text: 'Data Added Successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      }).then(() => {
        setTimeout(() => {
          window.location.reload();
        },);
      });
    } else {
      Swal.fire({
        title: 'Error',
        text: 'Please complete all fields',
        icon: 'error',
        timer: 1500,
        showConfirmButton: false,
        customClass: {
          popup: 'custom-swal-text', 
        },
      });
    }
  }
  
  isDataValid(data: any): boolean {
    return (
      data.function_location &&
      data.equipment &&
      data.operation_task &&
      data.description_material &&
      data.date &&
      data.pic &&
      data.email &&
      data.qty &&
      data.status_part &&
      data.work_center &&
      data.duration
    );
  }

  getScheduleMaintenanceById(id:any){
    this.RestApiService.getScheduleMaintenanceById(id).subscribe(
      (res:any) => {
        this.newScheduleMaintenance = res.data[0];
        this.scheduleById = this.newScheduleMaintenance
        this.scheduleById.date = this.convertDate(this.scheduleById.date);
        if (this.scheduleById.pic && this.scheduleById.email) {
          this.scheduleById.pic = (this.scheduleById.pic as string).split(',').map(pic => pic.trim());
          this.newScheduleMaintenance.email = (this.scheduleById.email as string).split(',').map(pic => pic.trim());
        }
      })
    }

  insertSchedule(dataSchedule: any) {
    this.RestApiService.insertScheduleMaintenance(dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.idSchedule = res.data[0]
          this.schedule_maintenance = dataSchedule;
          this.schedule_maintenance.id = this.idSchedule
          this.getDaily()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

updateSchedule(dataSchedule: any) {
  dataSchedule.pic = dataSchedule.pic.join(', ');
    this.RestApiService.updateScheduleMaintenance(this.idParam, dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.schedule_maintenance = dataSchedule;
          this.getDaily()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onUpdateSchedule() {
    (this.newScheduleMaintenance.pic as any) = this.newScheduleMaintenance.pic.join(', '),
    (this.newScheduleMaintenance.email as any) = this.newScheduleMaintenance.email.join(', ')
    console.log(this.newScheduleMaintenance)
    this.updateSchedule(this.newScheduleMaintenance);
    Swal.fire({
      title: 'Notification',
      text: 'Data Updated Successfully!',
      icon: 'success',
      timer: 1500,
      showConfirmButton: false,
      customClass: {
        popup: 'custom-swal-text', 
      },
    });
    this.getScheduleMaintenanceById(this.idParam)
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}
