import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetPopupComponent } from './schedule-mecha-pet-popup.component';

describe('ScheduleMechaPetPopupComponent', () => {
  let component: ScheduleMechaPetPopupComponent;
  let fixture: ComponentFixture<ScheduleMechaPetPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
