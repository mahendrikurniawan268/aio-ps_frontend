import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetAddDataComponent } from './schedule-mecha-pet-add-data.component';

describe('ScheduleMechaPetAddDataComponent', () => {
  let component: ScheduleMechaPetAddDataComponent;
  let fixture: ComponentFixture<ScheduleMechaPetAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
