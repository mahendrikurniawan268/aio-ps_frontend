import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMechaPetUnplannedComponent } from './schedule-mecha-pet-unplanned.component';

describe('ScheduleMechaPetUnplannedComponent', () => {
  let component: ScheduleMechaPetUnplannedComponent;
  let fixture: ComponentFixture<ScheduleMechaPetUnplannedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleMechaPetUnplannedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleMechaPetUnplannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
