import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

// Calendar option
import { CalendarOptions, EventClickArg, EventApi } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

// BootStrap
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';

// Calendar Services
import { RestApiService } from "../../../../../core/services/rest-api.service";
import { calendarEvents } from './data';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-calendar-activity',
  templateUrl: './calendar-activity.component.html',
  styleUrls: ['./calendar-activity.component.scss']
})
export class CalendarActivityComponent implements OnInit {

  // bread crumb items
  breadCrumbItems!: Array<{}>;

  // calendar
  calendarEvents: any[] = []
  editEvent: any;
  formEditData!: UntypedFormGroup;
  newEventDate: any;
  category!: any[];
  submitted = false;

  // Calendar click Event
  formData!: UntypedFormGroup;
  @ViewChild('editmodalShow') editmodalShow!: TemplateRef<any>;
  @ViewChild('modalShow') modalShow !: TemplateRef<any>;

  constructor(private router:Router, private formBuilder: UntypedFormBuilder,
    private datePipe: DatePipe,private RestApiService: RestApiService) { }

    activityData:any;
    scheduleData:any [] = [];
    scheduleDataPet:any [] = [];
    masterData:any [] = [];
    masterDataPet:any [] = [];

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: 'Apps' },
      { label: 'Calendar', active: true }
    ];

    // Validation
    this.formData = this.formBuilder.group({
      title: ['', [Validators.required]],
      category: ['', [Validators.required]],
      location: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required]
    });

    this._fetchData();
  }

  private _fetchData() {
    this.getAllSchedule();
    this.getAllMasterPlan();
  }

  calendarOptions: CalendarOptions = {
    plugins: [
      interactionPlugin,
      dayGridPlugin,
      timeGridPlugin,
      listPlugin,
    ],
    headerToolbar: {
      left: 'dayGridMonth,dayGridWeek,dayGridDay',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: this.calendarEvents || calendarEvents,
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    eventDidMount: this.addIconToEvent.bind(this),
    eventClick: (info) => {
      const eventData = info.event;
      console.log(eventData);
      
      const eventCategory = eventData.extendedProps['category'];
      if (eventCategory === 'Mechanical') {
        this.router.navigate(['maintenance/pet/schedule/mecha/pet/daily']);
      } else if (eventCategory === 'Electrical') {
        this.router.navigate(['maintenance/pet/schedule/elec/pet/daily']);
      }
      const eventMaster = eventData.extendedProps;
        //preparation
      if (eventMaster['type'] === 4 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 1}});
      } else if(eventMaster['type'] === 4 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 2}})
      } else if(eventMaster['type'] === 4 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 3}})
      } else if(eventMaster['type'] === 4 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 1}})
      } else if(eventMaster['type'] === 4 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 2}})
      } else if(eventMaster['type'] === 4 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 3}})
      }
      //Injection
      else if(eventMaster['type'] === 5 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 1}});
      } else if(eventMaster['type'] === 5 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 2}})
      } else if(eventMaster['type'] === 5 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 3}})
      } else if(eventMaster['type'] === 5 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 1}})
      } else if(eventMaster['type'] === 5 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 2}})
      } else if(eventMaster['type'] === 5 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 3}})
      }
      //blow
      else if(eventMaster['type'] === 6 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 1}});
      } else if(eventMaster['type'] === 6 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 2}})
      } else if(eventMaster['type'] === 6 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 3}})
      } else if(eventMaster['type'] === 6 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 1}})
      } else if(eventMaster['type'] === 6 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 2}})
      } else if(eventMaster['type'] === 6 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 3}})
      }
        //filling
        else if(eventMaster['type'] === 7 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 1}});
      } else if(eventMaster['type'] === 7 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 2}})
      } else if(eventMaster['type'] === 7 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 3}})
      } else if(eventMaster['type'] === 7 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 1}})
      } else if(eventMaster['type'] === 7 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 2}})
      } else if(eventMaster['type'] === 7 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 3}})
      }
      //packing
        else if(eventMaster['type'] === 8 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 1}});
      } else if(eventMaster['type'] === 8 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 2}})
      } else if(eventMaster['type'] === 8 && eventMaster['category'] === 1 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 3}})
      } else if(eventMaster['type'] === 8 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 1){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 1}})
      } else if(eventMaster['type'] === 8 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 2){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 2}})
      } else if(eventMaster['type'] === 8 && eventMaster['category'] === 2 && eventMaster['subCategory'] === 3){
        this.router.navigate(['maintenance/master-plan/view'], {queryParams:{lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 3}})
      }
    }
  };

  addIconToEvent(info:any): void {
    const event = info.event;
    if (event.extendedProps && event.extendedProps.id_status === 3) {
      const iconElement = document.createElement('i');
      iconElement.className = 'bi bi-check-circle text-white';
      iconElement.style.fontSize = '15px';
      iconElement.style.marginRight = '10px';

      const eventElement: HTMLElement = info.el as HTMLElement;
      eventElement.appendChild(iconElement);
    }else if (event.extendedProps && event.extendedProps.id_status === 4) {
      const iconElement = document.createElement('i');
      iconElement.className = 'bi bi-x-circle text-white';
      iconElement.style.fontSize = '15px';
      iconElement.style.marginRight = '10px';
  
      const eventElement: HTMLElement = info.el as HTMLElement;
      eventElement.appendChild(iconElement);
    }
  }

  eventRender(info: any) {
    const idStatus = info.event.extendedProps['id_status'];
    const eventTitle = info.event.title;
  
    let eventColor = '';
    if (idStatus === 1) {
      eventColor = 'green'; // Jika id_status = 1, warna hijau
    } else if (idStatus === 2) {
      eventColor = 'blue'; // Jika id_status = 2, warna biru
    } else {
      eventColor = 'grey'; // Jika tidak ada id_status yang cocok, gunakan warna abu-abu (opsional)
    }
  
    const eventContent = document.createElement('div');
    eventContent.innerHTML = `
      <div style="background-color: ${eventColor}; color: white; padding: 5px; border-radius: 5px;">
        ${eventTitle}
      </div>
    `;
  
    info.el.innerHTML = ''; // Kosongkan elemen asli event
    info.el.appendChild(eventContent); // Tambahkan tampilan baru
  }
  

  getAllSchedule() {
    this.RestApiService.getAllScheduleMaintenance().subscribe(
      (res: any) => {
        this.scheduleData = res.data;
        this.scheduleDataPet = this.scheduleData.filter(item => item.line === 'pet')
        this.combineAndSetCalendarEvents();
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getAllMasterPlan() {
    this.RestApiService.getTabelViewMasterPlan().subscribe(
      (res: any) => {
        this.masterData = res.data;
        this.masterDataPet = this.masterData.filter(item => item.id_line === 2)
        this.combineAndSetCalendarEvents();
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  combineAndSetCalendarEvents() {
    const scheduleEvents = this.transformDataSchedule(this.scheduleDataPet);
    const masterPlanEvents = this.transformDataMaster(this.masterDataPet);
    this.calendarEvents = [...scheduleEvents, ...masterPlanEvents];
    this.calendarOptions.events = this.calendarEvents;
  }

  transformDataSchedule(data: any[]): any[] {
    const calendarEvents: any[] = [];

    for (const item of data) {
      const event = {
        title: item.function_location + ' - ' +item.operation_task,
        start: item.date,
        category: item.category,
        id_status: item.id_status,
        color: 'yellow',
      };
      calendarEvents.push(event);
    }

    return calendarEvents;
  }

  transformDataMaster(data: any[]): any[] {
    const calendarEvents: any[] = [];

    for (const item of data) {
      const event = {
        title: item.function_location + ' - ' +item.operation_task,
        start: item.date,
        type: item.id_type,
        category: item.id_category,
        subCategory: item.id_subcategory,
        id_status: item.id_status,
        color: 'yellow',
      };
      calendarEvents.push(event);
    }

    return calendarEvents;
  }
}
