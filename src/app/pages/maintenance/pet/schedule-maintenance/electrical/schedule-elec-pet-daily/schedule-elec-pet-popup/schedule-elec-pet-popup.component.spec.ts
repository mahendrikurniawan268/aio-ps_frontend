import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetPopupComponent } from './schedule-elec-pet-popup.component';

describe('ScheduleElecPetPopupComponent', () => {
  let component: ScheduleElecPetPopupComponent;
  let fixture: ComponentFixture<ScheduleElecPetPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
