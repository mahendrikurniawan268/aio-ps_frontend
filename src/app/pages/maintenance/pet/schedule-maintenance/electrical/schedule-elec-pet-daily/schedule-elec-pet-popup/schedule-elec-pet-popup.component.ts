import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'; 
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { ScheduleMaintenancePet } from '../schedule-maintenance-pet.model';

@Component({
  selector: 'app-schedule-elec-pet-popup',
  templateUrl: './schedule-elec-pet-popup.component.html',
  styleUrls: ['./schedule-elec-pet-popup.component.scss']
})
export class ScheduleElecPetPopupComponent implements OnInit {
  @Input() dataDaily: any = {}
  @Input() dataUnplanned: any = {}

  constructor(private RestApiService: RestApiService,  
  private activeModal: NgbActiveModal, private authService:AuthenticationService,
  private router: Router, private modalService: NgbModal) {}
  schedule_maintenance: any;
  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isPlanner: boolean = false;
  isEmployee: boolean = false;
  idTask:any;

  public activity_group: any = {};

  newScheduleMaintenance : ScheduleMaintenancePet = {
    function_location: '',
    equipment: '',
    operation_task: '',
    description_material: '',
    date: '',
    pic: [],
    email: [],
    qty: null,
    status_part: '',
    work_center: '',
    duration: '',
    notes: '',
    category: '',
    unplanned: null,
    line: ''
  }

  ngOnInit(): void {
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isPlanner();
    this.isEmployee = this.authService.isEmployee();
  }

  clearFormData(){
    this. newScheduleMaintenance = {
      function_location: '',
      equipment: '',
      operation_task: '',
      description_material: '',
      date: '',
      pic: [],
      email: [],
      qty: null,
      status_part: '',
      work_center: '',
      duration: '',
      notes: '',
      category: '',
      unplanned: null,
      line: ''
   }
  }

  getDaily(){
    this.RestApiService.getElecDaily().subscribe(
      (res:any) => {
        this.dataDaily = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getUnplanned(){
    this.RestApiService.getElecUnplanned().subscribe(
      (res:any) => {
        this.dataUnplanned = res.data[0];
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  /**
 * Open modal
 * @param revisi modal content
 */
  revisiTask(revisi: any, task:any, id: any) {
    this.clearFormData();

    this.newScheduleMaintenance = { ...task };
    this.idTask = id;
    this.modalService.open(revisi, { size: 'md', centered: true }).result.then(
      (result) => {
        if (result === 'Close click') {
          this.clearFormData();
        }
      },
      (reason) => {
        this.clearFormData();
        console.log(`Dismissed with reason: ${reason}`);
      }
    );
  }

  onRevision(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to revisi this task?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, Revisi it!',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.isConfirmed) {
        const reviseData = {
          id_status: 5,
          notes: this.newScheduleMaintenance.notes
        };
        this.updateSchedule(reviseData, id);
        this.activeModal.close();
      }
      this.router.navigate(['maintenance/pet/schedule/elec/daily']);
      window.location.reload();
    });
  }

  onApprove(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to approve this schedule?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: 'danger',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.isConfirmed) {
        const approveData = { id_status: 2 };
        this.updateSchedule(approveData, id);
        this.activeModal.close();
        this.router.navigate(['maintenance/pet/schedule/elec/daily']);
        window.location.reload();
      }
    });
  }
  
  onDone(id: any) {
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to mark this schedule as done?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: 'danger',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, mark it as done!'
    }).then((result) => {
      if (result.isConfirmed) {
        const approveData = { id_status: 3 };
        this.updateSchedule(approveData, id);
        this.activeModal.close();
        this.router.navigate(['maintenance/pet/schedule/elec/daily']);
        window.location.reload();
      }
    });
  }

  updateSchedule(dataSchedule: any, id:any) {
    this.RestApiService.updateScheduleMaintenance(id, dataSchedule).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.schedule_maintenance = dataSchedule;
          this.getDaily()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onClose(){
    this.activeModal.close();
  }

  convertDate(date: string): string {
    if (date) {
      const inputDate = new Date(date);
      inputDate.setDate(inputDate.getDate() + 1);
      const formattedDate = inputDate.toISOString().split('T')[0];
      return formattedDate;
    }
    return '';
  }
}
