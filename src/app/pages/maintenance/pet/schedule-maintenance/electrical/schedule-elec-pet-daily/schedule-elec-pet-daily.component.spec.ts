import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetDailyComponent } from './schedule-elec-pet-daily.component';

describe('ScheduleElecPetDailyComponent', () => {
  let component: ScheduleElecPetDailyComponent;
  let fixture: ComponentFixture<ScheduleElecPetDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetDailyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
