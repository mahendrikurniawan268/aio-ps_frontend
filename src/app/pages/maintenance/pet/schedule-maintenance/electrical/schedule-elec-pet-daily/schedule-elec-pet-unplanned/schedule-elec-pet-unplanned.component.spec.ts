import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetUnplannedComponent } from './schedule-elec-pet-unplanned.component';

describe('ScheduleElecPetUnplannedComponent', () => {
  let component: ScheduleElecPetUnplannedComponent;
  let fixture: ComponentFixture<ScheduleElecPetUnplannedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetUnplannedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetUnplannedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
