import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetAddDataComponent } from './schedule-elec-pet-add-data.component';

describe('ScheduleElecPetAddDataComponent', () => {
  let component: ScheduleElecPetAddDataComponent;
  let fixture: ComponentFixture<ScheduleElecPetAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
