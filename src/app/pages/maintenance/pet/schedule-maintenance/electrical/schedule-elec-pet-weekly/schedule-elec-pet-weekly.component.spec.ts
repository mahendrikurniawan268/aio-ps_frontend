import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetWeeklyComponent } from './schedule-elec-pet-weekly.component';

describe('ScheduleElecPetWeeklyComponent', () => {
  let component: ScheduleElecPetWeeklyComponent;
  let fixture: ComponentFixture<ScheduleElecPetWeeklyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetWeeklyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetWeeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
