import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleElecPetWeeklyPopupComponent } from './schedule-elec-pet-weekly-popup.component';

describe('ScheduleElecPetWeeklyPopupComponent', () => {
  let component: ScheduleElecPetWeeklyPopupComponent;
  let fixture: ComponentFixture<ScheduleElecPetWeeklyPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleElecPetWeeklyPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleElecPetWeeklyPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
