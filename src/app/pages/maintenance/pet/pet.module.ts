import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { PetRoutingModule } from './pet-routing.module';
import lottie from 'lottie-web';
import { NgbDropdownModule, NgbHighlight, NgbModal, NgbNavModule, NgbPaginationModule, NgbRatingModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { SimplebarAngularModule } from 'simplebar-angular';
import { defineElement } from 'lord-icon-element';


// Ng Search 
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { NgSelectModule } from '@ng-select/ng-select';
import { ArchwizardModule } from 'angular-archwizard';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask';
import { UiSwitchModule } from 'ngx-ui-switch';
import { FormRoutingModule } from '../../form/form-routing.module';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { InViewportModule } from '@thisissoon/angular-inviewport';

import { ScheduleMechaPetDailyComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-daily.component';
import { ScheduleMechaPetWeeklyComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-weekly/schedule-mecha-pet-weekly.component';
import { ScheduleMechaPetAddDataComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-add-data/schedule-mecha-pet-add-data.component';
import { ScheduleMechaPetPopupComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-popup/schedule-mecha-pet-popup.component';
import { ScheduleMechaPetUnplannedComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-unplanned/schedule-mecha-pet-unplanned.component';
import { ScheduleMechaPetWeeklyPopupComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-weekly/schedule-mecha-pet-weekly-popup/schedule-mecha-pet-weekly-popup.component';
import { ScheduleElecPetDailyComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-daily.component';
import { ScheduleElecPetWeeklyComponent } from './schedule-maintenance/electrical/schedule-elec-pet-weekly/schedule-elec-pet-weekly.component';
import { ScheduleElecPetAddDataComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-add-data/schedule-elec-pet-add-data.component';
import { ScheduleElecPetPopupComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-popup/schedule-elec-pet-popup.component';
import { ScheduleElecPetUnplannedComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-unplanned/schedule-elec-pet-unplanned.component';
import { ScheduleElecPetWeeklyPopupComponent } from './schedule-maintenance/electrical/schedule-elec-pet-weekly/schedule-elec-pet-weekly-popup/schedule-elec-pet-weekly-popup.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { CalendarActivityComponent } from './schedule-maintenance/calendar-activity/calendar-activity.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { BudgetingMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-maintenance-pet.component';
import { BudgetingRepairMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-maintenance-pet.component';
import { BudgetingMaintenanceMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-maintenance-pet.component';
import { BudgetingOverhaulMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-maintenance-pet.component';
import { BudgetingRepairPetAddDataComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-pet-add-data/budgeting-repair-pet-add-data.component';
import { BudgetingRepairPetEditDataComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-pet-edit-data/budgeting-repair-pet-edit-data.component';
import { BudgetingMaintenancePetAddDataComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-pet-add-data/budgeting-maintenance-pet-add-data.component';
import { BudgetingMaintenancePetEditDataComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-pet-edit-data/budgeting-maintenance-pet-edit-data.component';
import { BudgetingOverhaulPetAddDataComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-pet-add-data/budgeting-overhaul-pet-add-data.component';
import { BudgetingOverhaulPetEditDataComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-pet-edit-data/budgeting-overhaul-pet-edit-data.component';
import { DashboardPetMaintenanceComponent } from './dashboard-pet-maintenance/dashboard-pet-maintenance.component';
import { CountToModule } from 'angular-count-to';


@NgModule({
  declarations: [
    DashboardPetMaintenanceComponent,
    ScheduleMechaPetDailyComponent,
    ScheduleMechaPetWeeklyComponent,
    ScheduleMechaPetAddDataComponent,
    ScheduleMechaPetPopupComponent,
    ScheduleMechaPetUnplannedComponent,
    ScheduleMechaPetWeeklyPopupComponent,
    ScheduleElecPetDailyComponent,
    ScheduleElecPetWeeklyComponent,
    ScheduleElecPetAddDataComponent,
    ScheduleElecPetPopupComponent,
    ScheduleElecPetUnplannedComponent,
    ScheduleElecPetWeeklyPopupComponent,
    CalendarActivityComponent,
    BudgetingMaintenancePetComponent,
    BudgetingRepairMaintenancePetComponent,
    BudgetingMaintenanceMaintenancePetComponent,
    BudgetingOverhaulMaintenancePetComponent,
    BudgetingRepairPetAddDataComponent,
    BudgetingRepairPetEditDataComponent,
    BudgetingMaintenancePetAddDataComponent,
    BudgetingMaintenancePetEditDataComponent,
    BudgetingOverhaulPetAddDataComponent,
    BudgetingOverhaulPetEditDataComponent,
  ],
  imports: [
    CommonModule,
    FullCalendarModule,
    FormsModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    HttpClientModule,
    NgApexchartsModule,
    FlatpickrModule,
    SharedModule,
    SimplebarAngularModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
    DropzoneModule,
    NgbHighlight,
    NgbNavModule,
    NgSelectModule,
    UiSwitchModule,
    ColorPickerModule,
    NgxMaskDirective, 
    NgxMaskPipe,
    NgxSliderModule,
    ArchwizardModule,
    AutocompleteLibModule,
    FormRoutingModule,
    NgbRatingModule,
    NgxUsefulSwiperModule,
    InViewportModule,
    PetRoutingModule,
    CountToModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PetModule {
  constructor() {
    defineElement(lottie.loadAnimation);
    
  }
}
