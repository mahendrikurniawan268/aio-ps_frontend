import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulMaintenancePetComponent } from './budgeting-overhaul-maintenance-pet.component';

describe('BudgetingOverhaulMaintenancePetComponent', () => {
  let component: BudgetingOverhaulMaintenancePetComponent;
  let fixture: ComponentFixture<BudgetingOverhaulMaintenancePetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulMaintenancePetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulMaintenancePetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
