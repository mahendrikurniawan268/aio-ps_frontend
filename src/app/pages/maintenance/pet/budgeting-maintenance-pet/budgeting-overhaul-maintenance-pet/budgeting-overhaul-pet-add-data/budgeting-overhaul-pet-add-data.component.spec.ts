import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulPetAddDataComponent } from './budgeting-overhaul-pet-add-data.component';

describe('BudgetingOverhaulPetAddDataComponent', () => {
  let component: BudgetingOverhaulPetAddDataComponent;
  let fixture: ComponentFixture<BudgetingOverhaulPetAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulPetAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulPetAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
