import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingOverhaulPetEditDataComponent } from './budgeting-overhaul-pet-edit-data.component';

describe('BudgetingOverhaulPetEditDataComponent', () => {
  let component: BudgetingOverhaulPetEditDataComponent;
  let fixture: ComponentFixture<BudgetingOverhaulPetEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingOverhaulPetEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingOverhaulPetEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
