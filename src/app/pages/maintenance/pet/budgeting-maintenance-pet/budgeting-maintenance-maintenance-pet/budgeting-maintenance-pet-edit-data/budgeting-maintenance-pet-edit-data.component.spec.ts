import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenancePetEditDataComponent } from './budgeting-maintenance-pet-edit-data.component';

describe('BudgetingMaintenancePetEditDataComponent', () => {
  let component: BudgetingMaintenancePetEditDataComponent;
  let fixture: ComponentFixture<BudgetingMaintenancePetEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenancePetEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenancePetEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
