import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenanceMaintenancePetComponent } from './budgeting-maintenance-maintenance-pet.component';

describe('BudgetingMaintenanceMaintenancePetComponent', () => {
  let component: BudgetingMaintenanceMaintenancePetComponent;
  let fixture: ComponentFixture<BudgetingMaintenanceMaintenancePetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenanceMaintenancePetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenanceMaintenancePetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
