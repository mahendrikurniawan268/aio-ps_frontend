import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { RestApiService } from 'src/app/core/services/rest-api.service';
import Swal from 'sweetalert2';
import { SubBudgeting } from '../subbudgeting.model';

@Component({
  selector: 'app-budgeting-maintenance-maintenance-pet',
  templateUrl: './budgeting-maintenance-maintenance-pet.component.html',
  styleUrls: ['./budgeting-maintenance-maintenance-pet.component.scss']
})
export class BudgetingMaintenanceMaintenancePetComponent implements OnInit {
  tableColumns = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  titlePage = 'Activity List';
  
  userData:any;
  budgeting_maintenance:any;
  budgeting_detail:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  dataBudgeting: any [] = [];
  dataBudgetingPet: any [] = [];
  dataDetails:any [] = [];
  groupedData: any = {};
  uniqueMonths: string[] = [];
  preData: any;
  searchText: string = '';
  activeMonths: { [year: string]: string } = {};
  dataPlus = 0
  budgetingByParamId:any;

  newSubBudgeting : SubBudgeting = {
    month: '',
    id_budgeting: null,
   }

  constructor(private router : Router, private RestApiService:RestApiService, 
    private modalService:NgbModal, private authService: AuthenticationService){}

  ngOnInit():void {
    this.getMaintenanceBudgeting()
    this.getDetails();
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
  }

  setActiveMonth(year: string, month: string) {
    this.activeMonths[year] = month;
  }
  
  isMonthActive(year: string, month: string): boolean {
    return this.activeMonths[year] === month;
  }

  addData(){
    this.router.navigate(['maintenance/pet/budgeting/maintenance-pet/add-data'])
  }

  editData(id:any){
    this.router.navigate([`maintenance/pet/budgeting/maintenance-pet/add-data/${id}`])
  }

  editDetails(id:any){
    this.router.navigate([`maintenance/pet/budgeting/maintenance-pet/edit-data/${id}`])
  }

  groupDataByMonth() {
    this.groupedData = {};
    for (const detail of this.dataDetails) {
      const month = detail.month;
      if (!this.groupedData[month]) {
        this.groupedData[month] = [];
      }
      this.groupedData[month].push(detail);
    }
    this.uniqueMonths = Object.keys(this.groupedData);
  }

  getDetailsForMonth(id_budgeting: number, month: string): any[] {
  return (this.groupedData[month] || []).filter((detail: any) => detail.id_budgeting === id_budgeting);
  }


  getMaintenanceBudgeting(){
    this.RestApiService.getMaintenanceBudgeting().subscribe(
      (res:any) => {
        this.dataBudgeting = res.data[0];
        this.dataBudgetingPet = this.dataBudgeting.filter(item => item.line === 'pet')
        this.preData = [...this.dataBudgetingPet]
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  getDetails(){
    this.RestApiService.getDetails().subscribe(
      (res:any) => {
        this.dataDetails = res.data[0];
        this.groupDataByMonth();
      },
      (error:any) => {
        console.error(error)
      }
    );
  }

  updateBudgeting(dataBudgetingPet: any, id:any) {
    this.RestApiService.updateBudgeting(id, dataBudgetingPet).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.budgeting_maintenance = dataBudgetingPet;
          this.getMaintenanceBudgeting()
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  onDelete(id:any){
    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const deleteData = { status: 0 };
        this.updateBudgeting(deleteData, id);
      }
    });
  }

  updateDetail(dataDetail: any, id:any) {
    this.RestApiService.updateBudgetingDetails(id, dataDetail).subscribe(
      (res: any) => {
        if (res.status == true) {
          this.budgeting_detail = dataDetail;
          this.groupDataByMonth();
          this.getDetails();
          this.getMaintenanceBudgeting();
        }
      },
      (error) => {
        console.error(error)
      }
    )
  }

  updateBudgetingWithData(detail: any) {
    const parseOrDefault = (value: any) => {
      const parsedValue = parseInt(value);
      return isNaN(parsedValue) ? 0 : parsedValue;
    };
  
    const w1 = parseOrDefault(detail.w1);
    const w2 = parseOrDefault(detail.w2);
    const w3 = parseOrDefault(detail.w3);
    const w4 = parseOrDefault(detail.w4);
  
    const sumW = w1 + w2 + w3 + w4;
    const monthToUpdate = this.newSubBudgeting.month;
    const budgetingToUpdate = this.budgetingByParamId.find((budgeting: any) => budgeting[monthToUpdate.toLowerCase() + '_actual'] !== null);
    this.dataPlus += sumW;
    if (budgetingToUpdate) {
      budgetingToUpdate[monthToUpdate.toLowerCase() + '_actual'] = this.dataPlus;
      this.updateBudgeting(budgetingToUpdate, budgetingToUpdate.id);
    }
  }

  onDeleteDetail(id: any) {
    const detailToDelete = this.dataDetails.find(detail => detail.details_id === id);
    
    if (!detailToDelete) {
      console.error('Detail not found');
      return;
    }

    Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oke',
      cancelButtonText: 'Cancel',
      cancelButtonColor: '#d33',
      customClass: {
        popup: 'custom-swal-text',
      },
    }).then((result) => {
      if (result.isConfirmed) {
        const sumW = (parseInt(detailToDelete.w1) || 0) + 
                     (parseInt(detailToDelete.w2) || 0) + 
                     (parseInt(detailToDelete.w3) || 0) + 
                     (parseInt(detailToDelete.w4) || 0);

        const month = detailToDelete.month.toLowerCase() + '_actual';
        const budgetingRecord = this.dataBudgeting.find(budgeting => budgeting.id === detailToDelete.id_budgeting);

        if (budgetingRecord && month in budgetingRecord) {
          budgetingRecord[month] = (parseInt(budgetingRecord[month]) || 0) - sumW;

          this.updateBudgeting(budgetingRecord, budgetingRecord.id);
        }

        const deleteData = { status: 0 };
        this.updateDetail(deleteData, id);
      }
    });
  }

  search() {
    this.dataBudgetingPet = this.preData.filter((item: { year: string;}) => {
      return item.year.toLowerCase().includes(this.searchText.toLowerCase());
    });
  }
}
