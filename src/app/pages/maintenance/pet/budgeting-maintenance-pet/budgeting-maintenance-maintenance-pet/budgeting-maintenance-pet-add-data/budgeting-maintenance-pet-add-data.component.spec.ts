import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenancePetAddDataComponent } from './budgeting-maintenance-pet-add-data.component';

describe('BudgetingMaintenancePetAddDataComponent', () => {
  let component: BudgetingMaintenancePetAddDataComponent;
  let fixture: ComponentFixture<BudgetingMaintenancePetAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenancePetAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenancePetAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
