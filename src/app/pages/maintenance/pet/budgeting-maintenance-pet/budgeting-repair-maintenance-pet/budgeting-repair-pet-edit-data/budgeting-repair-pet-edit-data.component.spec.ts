import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairPetEditDataComponent } from './budgeting-repair-pet-edit-data.component';

describe('BudgetingRepairPetEditDataComponent', () => {
  let component: BudgetingRepairPetEditDataComponent;
  let fixture: ComponentFixture<BudgetingRepairPetEditDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairPetEditDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairPetEditDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
