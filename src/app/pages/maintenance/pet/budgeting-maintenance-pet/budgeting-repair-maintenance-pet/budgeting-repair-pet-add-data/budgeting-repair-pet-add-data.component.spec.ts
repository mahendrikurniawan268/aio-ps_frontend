import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairPetAddDataComponent } from './budgeting-repair-pet-add-data.component';

describe('BudgetingRepairPetAddDataComponent', () => {
  let component: BudgetingRepairPetAddDataComponent;
  let fixture: ComponentFixture<BudgetingRepairPetAddDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairPetAddDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairPetAddDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
