import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingRepairMaintenancePetComponent } from './budgeting-repair-maintenance-pet.component';

describe('BudgetingRepairMaintenancePetComponent', () => {
  let component: BudgetingRepairMaintenancePetComponent;
  let fixture: ComponentFixture<BudgetingRepairMaintenancePetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingRepairMaintenancePetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingRepairMaintenancePetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
