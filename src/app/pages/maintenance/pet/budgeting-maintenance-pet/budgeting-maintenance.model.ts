export interface BudgetingMaintenance {
    year: string;
    januari_plan: number | null;
    januari_actual: number | null;
    februari_plan: number | null;
    februari_actual: number | null;
    maret_plan: number | null;
    maret_actual: number | null;
    april_plan: number | null;
    april_actual: number | null;
    mei_plan: number | null;
    mei_actual: number | null;
    juni_plan: number | null;
    juni_actual: number | null;
    juli_plan: number | null;
    juli_actual: number | null;
    agustus_plan: number | null;
    agustus_actual: number | null;
    september_plan: number | null;
    september_actual: number | null;
    oktober_plan: number | null;
    oktober_actual: number | null;
    november_plan: number | null;
    november_actual: number | null;
    desember_plan: number | null;
    desember_actual: number | null;
    category: string;
    line: string;
}