import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetingMaintenancePetComponent } from './budgeting-maintenance-pet.component';

describe('BudgetingMaintenancePetComponent', () => {
  let component: BudgetingMaintenancePetComponent;
  let fixture: ComponentFixture<BudgetingMaintenancePetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetingMaintenancePetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BudgetingMaintenancePetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
