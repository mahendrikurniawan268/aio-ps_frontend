import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPetMaintenanceComponent } from './dashboard-pet-maintenance/dashboard-pet-maintenance.component';
import { ScheduleMechaPetWeeklyComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-weekly/schedule-mecha-pet-weekly.component';
import { ScheduleMechaPetDailyComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-daily.component';
import { ScheduleMechaPetAddDataComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-add-data/schedule-mecha-pet-add-data.component';
import { ScheduleMechaPetUnplannedComponent } from './schedule-maintenance/mechanical/schedule-mecha-pet-daily/schedule-mecha-pet-unplanned/schedule-mecha-pet-unplanned.component';
import { ScheduleElecPetWeeklyComponent } from './schedule-maintenance/electrical/schedule-elec-pet-weekly/schedule-elec-pet-weekly.component';
import { ScheduleElecPetDailyComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-daily.component';
import { ScheduleElecPetAddDataComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-add-data/schedule-elec-pet-add-data.component';
import { ScheduleElecPetUnplannedComponent } from './schedule-maintenance/electrical/schedule-elec-pet-daily/schedule-elec-pet-unplanned/schedule-elec-pet-unplanned.component';
import { CalendarActivityComponent } from './schedule-maintenance/calendar-activity/calendar-activity.component';
import { BudgetingMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-maintenance-pet.component';
import { BudgetingRepairMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-maintenance-pet.component';
import { BudgetingMaintenanceMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-maintenance-pet.component';
import { BudgetingOverhaulMaintenancePetComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-maintenance-pet.component';
import { BudgetingRepairPetAddDataComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-pet-add-data/budgeting-repair-pet-add-data.component';
import { BudgetingRepairPetEditDataComponent } from './budgeting-maintenance-pet/budgeting-repair-maintenance-pet/budgeting-repair-pet-edit-data/budgeting-repair-pet-edit-data.component';
import { BudgetingMaintenancePetAddDataComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-pet-add-data/budgeting-maintenance-pet-add-data.component';
import { BudgetingMaintenancePetEditDataComponent } from './budgeting-maintenance-pet/budgeting-maintenance-maintenance-pet/budgeting-maintenance-pet-edit-data/budgeting-maintenance-pet-edit-data.component';
import { BudgetingOverhaulPetAddDataComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-pet-add-data/budgeting-overhaul-pet-add-data.component';
import { BudgetingOverhaulPetEditDataComponent } from './budgeting-maintenance-pet/budgeting-overhaul-maintenance-pet/budgeting-overhaul-pet-edit-data/budgeting-overhaul-pet-edit-data.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardPetMaintenanceComponent
  },
  {
    path: "budgeting-pet",
    component: BudgetingMaintenancePetComponent
  },
  {
    path: "repair-pet",
    component: BudgetingRepairMaintenancePetComponent
  },
  {
    path: "budgeting/repair-pet/add-data",
    component: BudgetingRepairPetAddDataComponent
  },
  {
    path: "budgeting/repair-pet/add-data/:id",
    component: BudgetingRepairPetAddDataComponent
  },
  {
    path: "budgeting/repair-pet/edit-data",
    component: BudgetingRepairPetEditDataComponent
  },
  {
    path: "budgeting/repair-pet/edit-data/:id",
    component: BudgetingRepairPetEditDataComponent
  },
  {
    path: "maintenance-pet",
    component: BudgetingMaintenanceMaintenancePetComponent
  },
  {
    path: "budgeting/maintenance-pet/add-data",
    component: BudgetingMaintenancePetAddDataComponent
  },
  {
    path: "budgeting/maintenance-pet/add-data/:id",
    component: BudgetingMaintenancePetAddDataComponent
  },
  {
    path: "budgeting/maintenance-pet/edit-data",
    component: BudgetingMaintenancePetEditDataComponent
  },
  {
    path: "budgeting/maintenance-pet/edit-data/:id",
    component: BudgetingMaintenancePetEditDataComponent
  },
  {
    path: "overhaul-pet",
    component: BudgetingOverhaulMaintenancePetComponent
  },
  {
    path: "budgeting/overhaul-pet/add-data",
    component: BudgetingOverhaulPetAddDataComponent
  },
  {
    path: "budgeting/overhaul-pet/add-data/:id",
    component: BudgetingOverhaulPetAddDataComponent
  },
  {
    path: "budgeting/overhaul-pet/edit-data",
    component: BudgetingOverhaulPetEditDataComponent
  },
  {
    path: "budgeting/overhaul-pet/edit-data/:id",
    component: BudgetingOverhaulPetEditDataComponent
  },
  {
    path: "calendar-activity",
    component: CalendarActivityComponent
  },
  {
    path: "schedule/mecha/pet/weekly",
    component: ScheduleMechaPetWeeklyComponent
  },
  {
    path: "schedule/mecha/pet/daily",
    component: ScheduleMechaPetDailyComponent
  },
  {
    path: "schedule/mecha/pet/daily/add-data",
    component: ScheduleMechaPetAddDataComponent
  },
  {
    path: "schedule/mecha/pet/daily/add-data/:id",
    component: ScheduleMechaPetAddDataComponent
  },
  {
    path: "schedule/mecha/pet/daily/unplanned",
    component: ScheduleMechaPetUnplannedComponent
  },
  {
    path: "schedule/mecha/pet/daily/unplanned/:id",
    component: ScheduleMechaPetUnplannedComponent
  },
  {
    path: "schedule/elec/pet/weekly",
    component: ScheduleElecPetWeeklyComponent
  },
  {
    path: "schedule/elec/pet/daily",
    component: ScheduleElecPetDailyComponent
  },
  {
    path: "schedule/elec/pet/daily/add-data",
    component: ScheduleElecPetAddDataComponent
  },
  {
    path: "schedule/elec/pet/daily/add-data/:id",
    component: ScheduleElecPetAddDataComponent
  },
  {
    path: "schedule/elec/pet/daily/unplanned",
    component: ScheduleElecPetUnplannedComponent
  },
  {
    path: "schedule/elec/pet/daily/unplanned/:id",
    component: ScheduleElecPetUnplannedComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PetRoutingModule { }
