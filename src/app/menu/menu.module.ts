import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuRoutingModule } from './menu-routing.module';
import { TopMenuComponent } from './top-menu/top-menu/top-menu.component';
import { SecondMenuComponent } from './second-menu/second-menu/second-menu.component';

@NgModule({
  declarations: [
    TopMenuComponent,
    SecondMenuComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
  ]
})
export class MenuModule { }
