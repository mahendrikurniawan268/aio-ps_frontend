import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  userData:any;
  isAdmin: boolean = false;
  isSpv: boolean = false;
  isPlanner: boolean = false;
  isEmployee: boolean = false;
  isOther: boolean = false;

  constructor(private authService: AuthenticationService, private router:Router){}

  ngOnInit(): void {
    this.userData = this.authService.getUserData();
    this.isAdmin = this.authService.isAdmin();
    this.isSpv = this.authService.isSpv();
    this.isPlanner = this.authService.isSpv();
    this.isEmployee = this.authService.isEmployee();
    this.isOther = this.authService.isOther();  
  }
  onCanClick(){
    sessionStorage.setItem("category", "can");
  }
  onPetClick(){
    sessionStorage.setItem("category", "pet");
  }
  onAdminClick(){
    sessionStorage.setItem("category", "admin");
    sessionStorage.setItem("subCategory", "admin")
    this.router.navigate(['/gateway'])
  }
}