import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SecondMenuComponent } from './second-menu/second-menu/second-menu.component';
import { TopMenuComponent } from './top-menu/top-menu/top-menu.component';

const routes: Routes = [
  {
    path: "top-menu",
    component: TopMenuComponent
  },
  {
    path: "second-menu",
    component: SecondMenuComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
