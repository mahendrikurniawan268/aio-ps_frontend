import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/core/services/event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-second-menu',
  templateUrl: './second-menu.component.html',
  styleUrls: ['./second-menu.component.scss']
})
export class SecondMenuComponent implements OnInit {
  attribute: any;

  constructor(private eventService: EventService, private router: Router) { }

  ngOnInit(): void {

    
  }
  onCanMaintClick() {
    sessionStorage.setItem("subCategory", "maintenance")
    this.router.navigate(['/dashboard/can'], { queryParams: {lineId : 1}})
  }

  onCanProdClick() {
    sessionStorage.setItem("subCategory", "production")
    this.router.navigate(['/dashboard-prod'], { queryParams: {lineId : 1}})
  }

  onPetMaintClick() {
    sessionStorage.setItem("subCategory", "maintenance")
    this.router.navigate(['/maintenance/pet/dashboard'], { queryParams: {lineId : 2}})
  }

  onPetProdClick() {
    sessionStorage.setItem("subCategory", "production")
    this.router.navigate(['/dashboard-prod'], { queryParams: {lineId : 2}})
  }

  isCanCategory(): boolean {
    return sessionStorage.getItem("category") === "can";
  }

  isPetCategory(): boolean {
    return sessionStorage.getItem("category") === "pet";
  }

  goBack(){
    this.router.navigate(['/menu/top-menu'])
  }
}

