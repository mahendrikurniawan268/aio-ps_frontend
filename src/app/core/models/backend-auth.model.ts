export class UserData {
    id!: number;
    nik!: string;
    name!: string;
    email!: string;
    role_id!: number;
    role_name!: string;
    role_detail!: string;
    photo!: string
}