import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalComponent } from "../../global-component";
import { environment } from 'src/environments/environment.development';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem('token')}` })
};


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  public httpOptions(): any {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
  }
  constructor(private httpClint: HttpClient) { }

  getCalendarData(): Observable<any> {
    var headerToken = {'Authorization': `Bearer `+ localStorage.getItem('token')};    
    return this.httpClint.get(GlobalComponent.API_URL + 'apps/calendar/', {  headers: headerToken, responseType: 'text' });
  }

  getCustomApps(userId: number) {
    return this.httpClint.get<any>(
      environment.API_URL + environment.userAppId + userId,
      this.httpOptions()
    );
  }

  insertCustomApp(data: any) {
    return this.httpClint.post<any>(
      environment.API_URL + environment.userApp,
      { form_data: data },
      this.httpOptions()
    );
  }

  deleteCustomApp(customId: number) {
    return this.httpClint.delete<any>(
      environment.API_URL + environment.userAppId + customId,
      this.httpOptions()
    );
  }

  getAllApps() {
    return this.httpClint.get<any>(
      environment.API_URL + environment.applications,
      this.httpOptions()
    );
  }

  insertApp(data: any) {
    return this.httpClint.post<any>(
      environment.API_URL + environment.applications,
      { form_data: data },
      this.httpOptions()
    );
  }

  updateApp(id: number, data: any) {
    return this.httpClint.put<any>(
      environment.API_URL + environment.applicationId + id,
      { form_data: data },
      this.httpOptions()
    );
  }

  deleteApp(id: number) {
    return this.httpClint.delete<any>(
      environment.API_URL + environment.applicationId + id,
      this.httpOptions()
    );
  }

  getImage(filename: string) {
    return this.httpClint.get<any>(
      environment.API_URL + environment.getImage + filename,
      this.httpOptions()
    );
  }

  insertImage(file: any) {
    return this.httpClint.post<any>(
      environment.API_URL + environment.image,
      { file: file },
      this.httpOptions()
    );
  }

  deleteImage(filename: string) {
    return this.httpClint.delete<any>(
      environment.API_URL + environment.image + filename,
      this.httpOptions()
    );
  }

  //Master Plan

  getAllMasterPlan(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllMasterPlan,
      this.httpOptions());
  }

  getAllPic(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllPic + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getProgressMasterPlan(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProgressMasterPlan + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getPicMasterPlan(from:string, to:string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPicMasterPlan + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getPicAutonomous(from:string, to:string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPicAutonomous + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getPicScheduleWeekly(from:string, to:string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPicScheduleWeekly + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getPicFindingWeekly(from:string, to:string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPicFindingWekklyCare + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getPicScheduleMaintenance(from:string, to:string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPicScheduleMaintenance + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getActivityGroupById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getActivityGroupById + id, 
      this.httpOptions());
  }

  getActivityMaterialByActivityId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getActivityMaterialByActivityId + id, 
      this.httpOptions());
  }

  getActivityByGroupId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getActivityByGroupId + id, 
      this.httpOptions());
  }

  getActivityById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getActivityById + id, 
      this.httpOptions());
  }

  getAllFillingMechanical(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllFillingMechanical,
      this.httpOptions());
  }

  insertActivityGroup(dataActivityGroup:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertActivityGroup, {form_data: dataActivityGroup},
      this.httpOptions());
  }

  updateActivityGroup(id:any, dataActivityGroup:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateActivityGroup + id, {form_data: dataActivityGroup},
      this.httpOptions());
  }

  insertActivity(dataActivity:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertActivity, {form_data: dataActivity},
      this.httpOptions());
  }

  updateActivity(dataActivity:any, id:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateActivity + id, {form_data: dataActivity},
      this.httpOptions());
  }

  insertMaterial(dataMaterial:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertMaterial, {form_data: dataMaterial},
      this.httpOptions());
  }

  updateMaterial(dataMaterial:any, id:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateMaterial + id, {form_data: dataMaterial},
      this.httpOptions());
  }

  deleteActivityGroup(id:any){
    return this.httpClint.delete<any>(
      environment.API_URL + environment.deleteActivityGroup + id,
      this.httpOptions());
  }

  deleteActivity(id:any){
    return this.httpClint.delete<any>(
      environment.API_URL + environment.deleteActivity + id,
      this.httpOptions());
  }

  deleteMaterial(id:any){
    return this.httpClint.delete<any>(
      environment.API_URL + environment.deleteMaterial + id,
      this.httpOptions());
  }

  //get data master plan
  getTabelViewMasterPlan(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTabelViewMasterPlan,
      this.httpOptions());
  }

  getFillMechaCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFillMechaCorective,
      this.httpOptions());
  }

  getFillMechaOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFillMechaOverHaul,
      this.httpOptions());
  }

  getFillElecPrev(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFillElecPreventive,
      this.httpOptions());
  }

  getFillElecCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFillElecCorective,
      this.httpOptions());
  }

  getFillElecOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFillElecOverHaul,
      this.httpOptions());
  }

  getPreMechaPrev(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreMechaPreventive,
      this.httpOptions());
  }

  getPreMechaCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreMechaCorective,
      this.httpOptions());
  }

  getPreMechaOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreMechaOverHaul,
      this.httpOptions());
  }

  getPreElecPrev(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreElecPreventive,
      this.httpOptions());
  }

  getPreElecCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreElecCorective,
      this.httpOptions());
  }

  getPreElecOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPreElecOverHaul,
      this.httpOptions());
  }

  getPacMechaPrev(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacMechaPreventive,
      this.httpOptions());
  }

  getPacMechaCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacMechaCorective,
      this.httpOptions());
  }

  getPacMechaOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacMechaOverHaul,
      this.httpOptions());
  }

  getPacElecPrev(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacElecPreventive,
      this.httpOptions());
  }

  getPacElecCorec(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacElecCorective,
      this.httpOptions());
  }

  getPacElecOver(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPacElecOverHaul,
      this.httpOptions());
  }

  //report master
  getAllReportMaster(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllReportMaster,
      this.httpOptions());
  }

  getPlanByMasterId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPlanByMasterId + id, 
      this.httpOptions());
  }

  getReportByAreaLine(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getReportByAreaLine,
      this.httpOptions());
  }

  getReportById(id:any){
  return this.httpClint.get<any>(
    environment.API_URL + environment.getReportMasterById + id,
    this.httpOptions());
  }

  getReportByLineId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getReportByLineId + id,
      this.httpOptions());
    }

  insertReport(dataReportMaster:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertReportMaster, {form_data: dataReportMaster},
      this.httpOptions());
  }

  updateReportMaster(id:any, dataReportMaster:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateReportMaster + id, {form_data: dataReportMaster},
      this.httpOptions());
  }

  //report master plan
  getReportMasterPlanById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getReportMasterPlanById + id,
      this.httpOptions());
    }

  getReportMasterPlanByReportMasterId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getReportMasterPlanByReportMasterId + id, 
      this.httpOptions());
  }

    getAllReportPlan(){
      return this.httpClint.get<any>(
        environment.API_URL + environment.getAllReportMasterPlan,
        this.httpOptions());
    }

  insertReportMaster(dataReportMasterPlan:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertReportMasterPlan, {form_data: dataReportMasterPlan},
      this.httpOptions());
  }

  updateReportMasterPlan(id:any, dataReportMasterPlan:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateReportMasterPlan + id, {form_data: dataReportMasterPlan},
      this.httpOptions());
  }

  updatePlanByReportId(id:any, dataPlanByReport:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updatePlanByReportId + id, {form_data: dataPlanByReport},
       this.httpOptions())
  }

  uploadMultipleImage(files: FormData) {
    return this.httpClint.post(environment.API_URL + environment.multiImage, files)
  }

  uploadMultiplePdf(files: FormData) {
    return this.httpClint.post(environment.API_URL + environment.pdfMulti, files)
  }

  getPdf(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPdf,
      this.httpOptions());
  }

  //master
  getAllMachine(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllMachine,
      this.httpOptions());
  }

  getMachineById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMachineById + id,
      this.httpOptions());
    }

  insertMachine(dataMachine:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertMachine, {form_data: dataMachine},
      this.httpOptions());
  }

  updateMachine(id:any, dataMachine:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateMachine + id, {form_data: dataMachine},
       this.httpOptions())
  }

  //funloc
  getAllFunloc(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFunloc,
      this.httpOptions());
  }

  getFunlocById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFunlocById + id,
      this.httpOptions());
    }

  insertFunloc(dataFunloc:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertFunloc, {form_data: dataFunloc},
      this.httpOptions());
  }

  updateFunloc(id:any, dataFunloc:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateFunloc + id, {form_data: dataFunloc},
       this.httpOptions())
  }

  getAllArea(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllArea,
      this.httpOptions());
  }

  //Weekly care
  getAllWeekly(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllWeeklyCare,
      this.httpOptions());
  }

  getAllWeeklyCare(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllWeekly,
      this.httpOptions());
  }

  getFindingsFailureMachine(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getFindingsFailureMachine + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getWeeklyById(id:any){
  return this.httpClint.get<any>(
    environment.API_URL + environment.getWeeklyCareById + id,
    this.httpOptions());
  }

  insertWeekly(dataWeeklyCare:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertWeeklyCare, {form_data: dataWeeklyCare},
      this.httpOptions());
  }
  
  updateWeekly(id:any, dataWeeklyCare:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateWeeklyCare + id, {form_data: dataWeeklyCare},
      this.httpOptions());
  }

  //schedule weekly

  getScheduleWeeklyById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getSheduleWeeklyById + id,
      this.httpOptions());
    }

  getAllScheduleWeekly(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllSheduleWeekly,
      this.httpOptions());
  }

  getTabelViewScheduleWeekly(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTabelViewSheduleWeekly,
      this.httpOptions());
  }

  getProgressScheduleWeekly(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProgressScheduleWeekly + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getAllType(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllType,
      this.httpOptions());
  }

  getNotificationWeeklyCare(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getNotificationWeekly,
      this.httpOptions());
  }

  insertScheduleWeekly(dataScheduleWeekly:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertSheduleWeekly, {form_data: dataScheduleWeekly},
      this.httpOptions());
  }

  updateScheduleWeekly(id:any, dataScheduleWeekly:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateSheduleWeekly + id, {form_data: dataScheduleWeekly},
      this.httpOptions());
  }

  //Autonomous Maintenance
  insertAutonomous(dataAutonomous:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertAutonomous, {form_data: dataAutonomous},
      this.httpOptions());
  }

  getAllAutonomous(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllAutonomous,
      this.httpOptions());
  }

  getAutonomousById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAutonomousById + id,
      this.httpOptions());
    }

  getProgressAutonomous(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProgressAutonomous + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  // getNotifAutonomous(){
  //   return this.httpClint.get<any>(
  //     environment.API_URL + environment.getNotifAutonomous,
  //     this.httpOptions());
  // }

  getAllAutonomousMaintenance(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllAutonomousMaintenance,
      this.httpOptions());
  }

  updateAutonomous(dataAutonomous:any, id:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateAutonomous + id, {form_data: dataAutonomous},
       this.httpOptions())
  }

  //Picture
  getPictureById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPictureById + id,
      this.httpOptions());
    }

  getAutonomousByPictureId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAutonomousByPictureId + id, 
      this.httpOptions());
  }

  getAllPicture(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllPicture,
      this.httpOptions());
  }

  insertPicture(dataPicture:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertPicture, {form_data: dataPicture},
      this.httpOptions());
  }

  updatePicture(id:any, dataPicture:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updatePicture + id, {form_data: dataPicture},
       this.httpOptions())
  }

  //task improvement
  getAllTaskImprovement(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllTaskImprovement,
      this.httpOptions());
  }

  insertTaskImprovement(dataTask:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertTaskImprovement, {form_data: dataTask},
      this.httpOptions());
  }

  getTaskImprovementById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskImprovementById + id,
      this.httpOptions());
  }

  getStatus(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getStatus,
      this.httpOptions());
  }

  getPriority(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPriority,
      this.httpOptions());
  }

  updateTask(id:any, dataTask:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateTaskImprovement + id, {form_data:dataTask},
       this.httpOptions())
  }

  //subtask imrovement
  getAllSubTaskImprovement(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllSubTaskImprovement,
      this.httpOptions());
  }

  getStatusSub(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getStatusSub,
      this.httpOptions());
  }

  getAllData(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllData,
      this.httpOptions());
  }

  insertSubTaskImprovement(dataSubTask:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertSubTaskImprovement, {form_data: dataSubTask},
      this.httpOptions());
  }

  updateSubTask(id:any, dataSubTask:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateSubTaskImprovement + id, {form_data:dataSubTask},
       this.httpOptions())
  }

  getSubTaskByTaskId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getSubTaskByTaskId + id, 
      this.httpOptions());
  }

  updateSubTaskByTaskId(id:any, dataSubTaskByTask:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateSubTaskByTaskId + id, {form_data:dataSubTaskByTask},
       this.httpOptions())
  }

  //Schedule maintenance
  getAllScheduleMaintenance(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllScheduleMaintenance,
      this.httpOptions());
  }

  getProgressMaintenance(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProgressMaintenance + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  cobaNotif(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.cobaNotif,
      this.httpOptions());
  }

  getNewData(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getNewData,
      this.httpOptions());
  }

  getScheduleMaintenanceById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleMaintenanceById + id,
      this.httpOptions());
  }

  getMechaDaily(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMechaDaily,
      this.httpOptions());
  }

  getMechaUnplanned(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMechaUnplanned,
      this.httpOptions());
  }

  getMechaWeekly(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMechaWeekly,
      this.httpOptions());
  }

  getMechaPending(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMechaPending,
      this.httpOptions());
  }

  getElecDaily(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getElecDaily,
      this.httpOptions());
  }

  getElecUnplanned(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getElecUnplanned,
      this.httpOptions());
  }

  getElecWeekly(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getElecWeekly,
      this.httpOptions());
  }

  getElecPending(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getElecPending,
      this.httpOptions());
  }

  insertScheduleMaintenance(dataScheduleMaintenance:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertScheduleMaintenance, {form_data: dataScheduleMaintenance},
      this.httpOptions());
  }

  updateScheduleMaintenance(id:any, dataSchedule:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateScheduleMaintenance + id, {form_data: dataSchedule},
       this.httpOptions())
  }

  //master user
  getMasterUserById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMasterUserById + id,
      this.httpOptions());
    }

  getNamePic(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getNamePic,
      this.httpOptions());
  }

  getAllMasterUser(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllMasterUser,
      this.httpOptions());
  }

  getTabelViewUser(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllTabelViewUser,
      this.httpOptions());
  }

  getRole(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getRole,
      this.httpOptions());
  }

  insertMasterUser(dataMasterUser:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertMasterUser, {form_data: dataMasterUser},
      this.httpOptions());
  }

  updateMasterUser(id:any, dataMasterUser:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateMasterUser + id, {form_data: dataMasterUser},
      this.httpOptions());
  }

  //quote
  getAllQuote(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getQuote,
      this.httpOptions());
  }

  getAllMonth(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllMonth,
      this.httpOptions());
  }

  getAllFunctionLocation(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllFunctionLocation,
      this.httpOptions());
  }

  getQuoteById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getQuoteById + id,
      this.httpOptions());
    }
  
  insertQuote(dataQuote:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertQuote, {form_data: dataQuote},
      this.httpOptions());
  }

  updateQuote(id:any, quoteData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateQuote + id, {form_data: quoteData},
      this.httpOptions());
  }

  //image landing page
  getAllImageLanding(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getImageLanding,
      this.httpOptions());
  }

  getImageLandingById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getImageLandingById + id,
      this.httpOptions());
    }
  
  insertImageLanding(dataImage:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertImageLanding, {form_data: dataImage},
      this.httpOptions());
  }

  updateImageLanding(id:any, imageData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateImageLanding + id, {form_data: imageData},
      this.httpOptions());
  }

  //budgeting
  getAllBudgeting(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllBudgeting,
      this.httpOptions());
  }

  getRepairBudgeting(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getRepairBudgeting,
      this.httpOptions());
  }

  getMaintenanceBudgeting(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getMaintenanceBudgeting,
      this.httpOptions());
  }

  getOverHaulBudgeting(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getOverHaulBudgeting,
      this.httpOptions());
  }

  insertBudgeting(dataBudgeting:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertBudgeting, {form_data: dataBudgeting},
      this.httpOptions());
  }

  getBudgetingById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getBudgetingById + id,
      this.httpOptions());
  }

  updateBudgeting(id:any, dataBudgeting:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateBudgeting + id, {form_data:dataBudgeting},
       this.httpOptions())
  }

  //subbudgeting
  getAllSubBudgeting(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllSubBudgeting,
      this.httpOptions());
  }

  insertSubBudgeting(dataSubBudgeting:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertSubBudgeting, {form_data: dataSubBudgeting},
      this.httpOptions());
  }

  getSubBudgetingById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getSubBudgetingById + id,
      this.httpOptions());
  }

  updateSubBudgeting(id:any, dataSubBudgeting:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateSubBudgeting + id, {form_data:dataSubBudgeting},
       this.httpOptions())
  }

  getSubBudgetingByBudgetingId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getSubBudgetingByBudgetingId + id, 
      this.httpOptions());
  }

  //budgeting details
  getAllBudgetingDetails(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllBudgetingDetails,
      this.httpOptions());
  }

  getDetails(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getDetails,
      this.httpOptions());
  }

  insertBudgetingDetails(dataBudgetingDetails:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertBudgetingDetails, {form_data: dataBudgetingDetails},
      this.httpOptions());
  }

  getBudgetingDetailsById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getBudgetingDetailsById + id,
      this.httpOptions());
  }

  updateBudgetingDetails(id:any, dataBudgetingDetails:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateBudgetingDetails + id, {form_data:dataBudgetingDetails},
       this.httpOptions())
  }

  getBudgetingDetailsBySubBudgetingId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getBudgetingDetailsBySubBudgeting + id, 
      this.httpOptions());
  }

  //approval
  getAllApproval(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getApproval,
      this.httpOptions());
  }

  getApprovalById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getApprovalById + id,
      this.httpOptions());
    }
  
  insertApproval(dataApproval:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertApproval, {form_data: dataApproval},
      this.httpOptions());
  }

  updateApproval(id:any, ApprovalData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateApproval + id, {form_data: ApprovalData},
      this.httpOptions());
  }

  //approval
  getAllApprovalSigned(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAllApprovalSigned,
      this.httpOptions());
  }

  getApprovalSigned(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getApprovalSigned,
      this.httpOptions());
  }

  getApprovalSignedById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getApprovalSignedById + id,
      this.httpOptions());
    }
  
  insertApprovalSigned(dataApprovalSigned:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertApprovalSigned, {form_data: dataApprovalSigned},
      this.httpOptions());
  }

  updateApprovalSigned(id:any, ApprovalSignedData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateApprovalSigned + id, {form_data: ApprovalSignedData},
      this.httpOptions());
  }

  //task spv
  getAllTaskSpv(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskSpv,
      this.httpOptions());
  }

  getTaskSpvByFilter(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskSpvByFilter + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getDeadlineTaskSpv(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getDeadlineTaskSpv,
      this.httpOptions());
  }

  getAchievementsSpv(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAchievementsSpv + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getTaskSpvById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskSpvById + id,
      this.httpOptions());
    }
  
  insertTaskSpv(dataTaskSpv:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertTaskSpv, {form_data: dataTaskSpv},
      this.httpOptions());
  }

  updateTaskSpv(id:any, TaskSpvData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateTaskSpv + id, {form_data: TaskSpvData},
      this.httpOptions());
  }

  //task spv detail
  getAllTaskSpvDetail(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskSpvDetail,
      this.httpOptions());
  }

  getTaskEmployee(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskEmployee,
      this.httpOptions());
  }

  getTaskSpvDetailById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getTaskSpvDetailById + id,
      this.httpOptions());
    }
  
  insertTaskSpvDetail(dataTaskSpvDetail:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertTaskSpvDetail, {form_data: dataTaskSpvDetail},
      this.httpOptions());
  }

  updateTaskSpvDetail(id:any, TaskSpvDetailData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateTaskSpvDetail + id, {form_data: TaskSpvDetailData},
      this.httpOptions());
  }

   //ppic
   getAllPpic(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPpic,
      this.httpOptions());
  }

  getPpicById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPpicById + id,
      this.httpOptions());
    }
  
  insertPpic(dataPpic:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertPpic, {form_data: dataPpic},
      this.httpOptions());
  }

  updatePpic(id:any, PpicData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updatePpic + id, {form_data: PpicData},
      this.httpOptions());
  }

  //pro
  getAllPro(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getPro,
      this.httpOptions());
  }

  getProByFilter(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProByFilter + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getProById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getProById + id,
      this.httpOptions());
    }
  
  insertPro(dataPro:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertPro, {form_data: dataPro},
      this.httpOptions());
  }

  updatePro(id:any, ProData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updatePro + id, {form_data: ProData},
      this.httpOptions());
  }

  //Schedule Production
  getAllScheduleProduction(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProduction,
      this.httpOptions());
  }

  getScheduleProductionByFilter(from: string, to: string){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProductionByFilter + `?from=${from}&to=${to}`,
      this.httpOptions());
  }

  getScheduleProductionById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProductionById + id,
      this.httpOptions());
    }
  
  insertScheduleProduction(dataScheduleProduction:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertScheduleProduction, {form_data: dataScheduleProduction},
      this.httpOptions());
  }

  updateScheduleProduction(id:any, ScheduleProductionData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateScheduleProduction + id, {form_data: ScheduleProductionData},
      this.httpOptions());
  }

  //Schedule Production Detail
  getAllScheduleProductionDetail(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProductionDetail,
      this.httpOptions());
  }

  getScheduleProductionDetailByScheduleId(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProductionDetailByScheduleId + id, 
      this.httpOptions());
  }

  getScheduleProductionDetailById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getScheduleProductionDetailById + id,
      this.httpOptions());
    }
  
  insertScheduleProductionDetail(dataScheduleProductionDetail:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertScheduleProductionDetail, {form_data: dataScheduleProductionDetail},
      this.httpOptions());
  }

  updateScheduleProductionDetail(id:any, dataDetail:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateScheduleProductionDetail + id, {form_data: dataDetail},
      this.httpOptions());
  }

  //acces line
  getAllAccesLine(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAccesLine,
      this.httpOptions());
  }

  getHistoryAcces(){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getHistoryAcces,
      this.httpOptions());
  }

  getAccesLineById(id:any){
    return this.httpClint.get<any>(
      environment.API_URL + environment.getAccesLineById + id,
      this.httpOptions());
    }
  
  insertAccesLine(dataAccesLine:any){
    return this.httpClint.post<any>(
      environment.API_URL + environment.insertAccesLine, {form_data: dataAccesLine},
      this.httpOptions());
  }

  updateAccesLine(id:any, AccesLineData:any){
    return this.httpClint.put<any>(
      environment.API_URL + environment.updateAccesLine + id, {form_data: AccesLineData},
      this.httpOptions());
  }
}
