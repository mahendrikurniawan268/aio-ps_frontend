import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment} from 'src/environments/environment';


@Injectable({
  providedIn: 'root',
})
export class ApiService {

  apiUrl: any;
  public httpOptions(): any {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
  }

  constructor(private httpClient: HttpClient) {}
  getCustomApps(userId: number) {
    return this.httpClient.get<any>(
      environment.API_URL + environment.userAppId + userId,
      this.httpOptions()
    );
  }

  insertCustomApp(data: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.userApp,
      { form_data: data },
      this.httpOptions()
    );
  }

  deleteCustomApp(customId: number) {
    return this.httpClient.delete<any>(
      environment.API_URL + environment.userAppId + customId,
      this.httpOptions()
    );
  }

  getAllApps() {
    return this.httpClient.get<any>(
      environment.API_URL + environment.applications,
      this.httpOptions()
    );
  }

  insertApp(data: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.applications,
      { form_data: data },
      this.httpOptions()
    );
  }

  updateApp(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + environment.applicationId + id,
      { form_data: data },
      this.httpOptions()
    );
  }

  deleteApp(id: number) {
    return this.httpClient.delete<any>(
      environment.API_URL + environment.applicationId + id,
      this.httpOptions()
    );
  }

  getImage(filename: string) {
    return this.httpClient.get<any>(
      environment.API_URL + environment.getImage + filename,
      this.httpOptions()
    );
  }

  insertImageAbnormal(file: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.image,
      { file: file },
      this.httpOptions()
    );
  }

  insertImageRakor(file: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.Image_rakormom,
      { file: file },
      this.httpOptions()
    );
  }

  deleteImage(filename: string) {
    return this.httpClient.delete<any>(
      environment.API_URL + environment.image + filename,
      this.httpOptions()
    );
  }

  deleteImageAbnormal(filename: string) {
    return this.httpClient.delete<any>(
      environment.API_URL + environment.Image_abnormal + filename,
      this.httpOptions()
    );
  }

  getJoinReportDaily() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/daily_report_history', 
    this.httpOptions())
  }

  getAlldaily() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/daily_report', 
    this.httpOptions())
  }

  getTabelViewProcess() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/daily_report/tabel_view', 
    this.httpOptions())
  }

  insertdaily(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/daily_report`, {form_data : data}, 
    this.httpOptions())
  }
  resetdaily(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/reset_daily_report`, {form_data : data}, 
    this.httpOptions())
  }
  getByIdDaily(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/daily_report/${id}`, 
      this.httpOptions())
  }

  updatedaily(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + '/master/daily_report/' + id,
      { form_data: data },
      this.httpOptions()
    );
  }

  //dail_activity
  getAlldailyActivity() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/daily_activity', 
    this.httpOptions())
  }

  insertDataActivity(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/daily_activity`, {form_data : data}, 
    this.httpOptions())
  }

  getAllHistory() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/history', 
    this.httpOptions())
  }

  getByIdHistory(id: number){
    return this.httpClient.get<any>
    (environment.API_URL + `/master/history/${id}`, 
    this.httpOptions())
  }

  insertData(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `/master/history`, {form_data : data}, 
    this.httpOptions())
  }

  deleteReportHistory(id: number) {
    return this.httpClient.delete<any>(
      environment.API_URL + '/master/history/'+id,
      this.httpOptions()
    );
  }

  // schedule Endpoints
  getAllschedule() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/schedule', 
    this.httpOptions())
  }

  notifSchedule() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/schedule/notif', 
    this.httpOptions())
  }

  insertschedule(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/schedule`, {form_data : data}, 
    this.httpOptions())
  }
  getByIdSchedule(id: number){
    return this.httpClient.get<any>
    (environment.API_URL + `/master/schedule/${id}`, 
    this.httpOptions())
  }

  updateschedule(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + '/master/schedule/'+id,
      { form_data: data },
      this.httpOptions()
    );
  }

  deleteschedule(id: number) {
    return this.httpClient.delete<any>(
      environment.API_URL + '/master/schedule/'+id,
      this.httpOptions()
    );
  }

  // abnormal Endpoints
  getAllabnormal() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/abnormal', 
    this.httpOptions())
  }

  getTabelViewAbnormal() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/abnormal/tabel_view', 
    this.httpOptions())
  }

  notifAbnormal() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/abnormal/notif', 
    this.httpOptions())
  }

  getByIdabnormal(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/abnormal/${id}`, 
      this.httpOptions())
  }

  insertAbnormal(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `/master/abnormal`, {form_data : data}, 
    this.httpOptions())
  }

  updateAbnormal(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + 'master/abnormal/' + id,
      { form_data: data },
      this.httpOptions()
    );
  }
  
  getJoinAbnormal() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/abnormaljoin', 
    this.httpOptions())
  }

  updatereset(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + environment.historyId + id,
      { form_data: data },
      this.httpOptions()
    );
  }
  
  getAllMachine() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/machine', 
    this.httpOptions())
  }

  getByIdMachine(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/machine/${id}`, 
      this.httpOptions())
  }

  deleteMachine(id: number) {
    return this.httpClient.delete<any>(
      environment.API_URL + '/master/machine/'+id,
      this.httpOptions()
    );
  }

  insertMachine(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/machine`, {form_data : data}, 
    this.httpOptions())
  }

  updatemachine(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + '/master/machine/'+id,
      { form_data: data },
      this.httpOptions()
    );
  }

  insertProfile(data: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.getImageUser,
      { form_data: data },
      this.httpOptions()
    );
  }

  //Weekly Report
  getAllWeekly() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/weekly', 
    this.httpOptions())
  }

  insertWeekly(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/weekly`, {form_data : data}, 
    this.httpOptions())
  }

  getByIdWeekly(id: number){
    return this.httpClient.get<any>
    (environment.API_URL + `master/activity_weekly/by_id/${id}`, 
    this.httpOptions())
  }

  getByIdViewWeekly(id: number){
    return this.httpClient.get<any>
    (environment.API_URL + `master/activity_weekly/tabel_view/${id}`, 
    this.httpOptions())
  }

  //Users
  getAllUsers() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/mst_user', 
    this.httpOptions())
  }

  getAllUserRole() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/userrole', 
    this.httpOptions())
  }

  insertUser(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `master/user`, {form_data : data}, 
    this.httpOptions())
  }

  getByIdUser(id: number){
    return this.httpClient.get<any>
    (environment.API_URL + `master/user/${id}`, 
    this.httpOptions())
  }

  updateUser(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + '/master/user/'+id,
      { form_data: data },
      this.httpOptions()
    );
  }

  getImageUser(filename: string) {
    return this.httpClient.get<any>(
      environment.API_URL + environment.getImageUser + filename,
      this.httpOptions()
    );
  }

  insertImageUser(file: any) {
    return this.httpClient.post<any>(
      environment.API_URL + environment.Image_user, file
    );
  }

      //Pending Job Montly Report
    getAllResume() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity_monthly', 
      this.httpOptions())
    }

    getTabelViewMonthly() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity_monthly/tabel_view', 
      this.httpOptions())
    }

    insertActivityMonthly(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/activity_monthly`, {form_data : data}, 
      this.httpOptions())
    }

    getByIdResume(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/activity_monthly/${id}`, 
      this.httpOptions())
    }

    updateActivityMonthly(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/activity_monthly/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }
    
    //Pending Job Montly Report
    getAllKartuStock() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/kartustock', 
      this.httpOptions())
    }

    insertHeader(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/kartustock`, {form_data : data}, 
      this.httpOptions())
    }

    //stock
    getAllMasterStock() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/mst_stock', 
      this.httpOptions())
    }

    getMasterStockById(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/mst_stock/${id}`, 
        this.httpOptions())
    }

    insertMasterStock(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/mst_stock`, {form_data : data}, 
      this.httpOptions())
    }

    updateMasterStock(id: number, dataMasterStock: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/mst_stock/' + id, 
        { form_data: dataMasterStock },
        this.httpOptions()
      );
    }

    insertDataStock(data: any[]) {
      return this.httpClient.post<any>(
        environment.API_URL + `master/detail`,
        { form_data: data },
        this.httpOptions()
      );
    }

    getTransactionByMasterId(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_stock_detail/by_master_id/${id}`, 
        this.httpOptions())
    }

    getTabelViewTransaction(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_stock_detail/tabel_view/${id}`, 
        this.httpOptions())
    }

    insertTransactionStock(data: any[]) {
      return this.httpClient.post<any>(
        environment.API_URL + `master/tr_stock_detail`,
        { form_data: data },
        this.httpOptions()
      );
    }

    updateTransactionStock(id: number, dataTransactionStock: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/tr_stock_detail/' + id, 
        { form_data: dataTransactionStock },
        this.httpOptions()
      );
    }

    //Investigasi
    getAllInvestigasi() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/tr_investigasi', 
      this.httpOptions())
    }

    getInvestigasiById(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_investigasi/${id}`, 
        this.httpOptions())
    }

    insertInvestigasi(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/tr_investigasi`, {form_data : data}, 
      this.httpOptions())
    }

    updateInvestigasi(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/tr_investigasi/' + id, 
        { form_data: data },
        this.httpOptions()
      );
    }

    //SubInvstigasi
    getAllSubInvestigasi() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/tr_subinvestigasi', 
      this.httpOptions())
    }

    getSubInvestigasiById(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_subinvestigasi/${id}`, 
        this.httpOptions())
    }

    getSubInvestigasiByInvestigasiId(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_subinvestigasi/by_investigasi_id/${id}`, 
        this.httpOptions())
    }

    insertSubInvestigasi(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/tr_subinvestigasi`, {form_data : data}, 
      this.httpOptions())
    }

    updateSubInvestigasi(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/tr_subinvestigasi/' + id, 
        { form_data: data },
        this.httpOptions()
      );
    }

    //SubInvestigasiDetail
    getAllSubInvestigasiDetail() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/tr_subinvestigasi_detail', 
      this.httpOptions())
    }

    getSubInvestigasiDetailById(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_subinvestigasi_detail/${id}`, 
        this.httpOptions())
    }

    getSubInvestigasiDetailOnUpdate(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_subinvestigasi_detail/on_update/${id}`, 
        this.httpOptions())
    }

    getSubInvestigasiDetailBySubInvestigasiId(id: number) {
      return this.httpClient.get<any>(
        environment.API_URL + `master/tr_subinvestigasi_detail/by_subinvestigasi_id/${id}`, 
        this.httpOptions())
    }

    insertSubInvestigasiDetail(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/tr_subinvestigasi_detail`, {form_data : data}, 
      this.httpOptions())
    }

    updateSubInvestigasiDetail(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/tr_subinvestigasi_detail/' + id, 
        { form_data: data },
        this.httpOptions()
      );
    }

    ///ACTIVITY
    getAllActivity() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity', 
      this.httpOptions())
    }

    insertActivity(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/activity`, {form_data : data}, 
      this.httpOptions())
    }

    getByIdActivity(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/activity/${id}`, 
      this.httpOptions())
    }

    updateActivity(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/activity/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

    getTabelViewDaily() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity/daily', 
      this.httpOptions())
    }

    getAllActivityWeekly() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity_weekly', 
      this.httpOptions())
    }

    insertActivityWeekly(dataWeekly: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/activity_weekly`, {form_data : dataWeekly}, 
      this.httpOptions())
    }

    getByIdActivityWeekly(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/activity_weekly/${id}`, 
      this.httpOptions())
    }

    updateActivityWeekly(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/activity_Weekly/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

    getTabelViewWeekly() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/activity_weekly/tabel_view', 
      this.httpOptions())
    }

    insertDataFollow(data: any[]) {
      return this.httpClient.post<any>(
        environment.API_URL + `master/followup`,
        { form_data: data },
        this.httpOptions()
      );
    }

    updateActivityFollow(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/followup/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

    getByIdFollowup(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/followup/${id}`, 
      this.httpOptions())
    }

    //Rakor MOM
    getAllRakorMOM() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/rakormom', 
      this.httpOptions())
    }

    insertRakorMOM(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/rakormom`, {form_data : data}, 
      this.httpOptions())
    }

    getByIdRakorMOM(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/rakormom/${id}`, 
      this.httpOptions())
    }

    updateRakorMOM(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/rakormom/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

    //Action plan Rakor MOM
    getAllActionPlan() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/action_plan', 
      this.httpOptions())
    }

    insertActionPlan(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/action_plan`, {form_data : data}, 
      this.httpOptions())
    }

    getByIdActionPlan(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/action_plan/${id}`, 
      this.httpOptions())
    }

    getActionPlanByRakorId(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/action_plan/by_rakorId/${id}`, 
      this.httpOptions())
    }

    getActionPlanViewPic(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/action_plan/view_pic/${id}`, 
      this.httpOptions())
    }

    updateActionPlan(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/action_plan/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

    //Next plan
    getAllNextPlan() {
      return this.httpClient.get<any>
      (environment.API_URL + 'master/next_plan', 
      this.httpOptions())
    }

    insertNextPlan(data: any) {
      return this.httpClient.post<any>
      (environment.API_URL + `master/next_plan`, {form_data : data}, 
      this.httpOptions())
    }

    getByIdNextPlan(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/next_plan/${id}`, 
      this.httpOptions())
    }

    getNextPlanByRakorId(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/next_plan/by_rakorId/${id}`, 
      this.httpOptions())
    }

    getNextPlanViewPic(id: number){
      return this.httpClient.get<any>
      (environment.API_URL + `master/next_plan/view_pic/${id}`, 
      this.httpOptions())
    }

    updateNextPlan(id: number, data: any) {
      return this.httpClient.put<any>(
        environment.API_URL + 'master/next_plan/' + id,
        { form_data: data },
        this.httpOptions()
      );
    }

  // Gmp
  getAllGmp() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/tr_gmp', 
    this.httpOptions())
  }

  getTabelViewGmp() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/tr_gmp/tabel_view', 
    this.httpOptions())
  }

  getByIdGmp(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/tr_gmp/${id}`, 
      this.httpOptions())
  }

  insertGmp(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `/master/tr_gmp`, {form_data : data}, 
    this.httpOptions())
  }

  updateGmp(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + 'master/tr_gmp/' + id,
      { form_data: data },
      this.httpOptions()
    );
  }

  //Mapping
  getAllMapping() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/tr_mapping', 
    this.httpOptions())
  }

  getByIdMapping(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/tr_mapping/${id}`, 
      this.httpOptions())
  }

  insertMapping(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `/master/tr_mapping`, {form_data : data}, 
    this.httpOptions())
  }

  updateMapping(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + 'master/tr_mapping/' + id,
      { form_data: data },
      this.httpOptions()
    );
  }

  //Structure
  getAllStructureCan() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/tr_structure/can', 
    this.httpOptions())
  }

  getAllStructurePet() {
    return this.httpClient.get<any>
    (environment.API_URL + 'master/tr_structure/pet', 
    this.httpOptions())
  }

  getByIdStructure(id: number) {
    return this.httpClient.get<any>(
      environment.API_URL + `master/tr_structure/${id}`, 
      this.httpOptions())
  }

  insertStructure(data: any) {
    return this.httpClient.post<any>
    (environment.API_URL + `/master/tr_structure`, {form_data : data}, 
    this.httpOptions())
  }

  updateStructure(id: number, data: any) {
    return this.httpClient.put<any>(
      environment.API_URL + 'master/tr_structure/' + id,
      { form_data: data },
      this.httpOptions()
    );
  }
} 