export interface MenuItem {
  id?: number;
  label?: any;
  icon?: string;
  link?: string;
  subItems?: any;
  isTitle?: boolean;
  badge?: any;
  parentId?: number;
  isLayout?: boolean;
  category?: string;
  subCategory?: string;
  eventCLick?:any;
  role_id?:number | number[];
  queryParams?: any;
}

// function customizeMenuAccess(menuItems: MenuItem[]): MenuItem[] {
//   return menuItems.filter(item => {
//     if (item.label === 'Maintenance Part') {
//       return true; // Tampilkan menu 'Maintenance Part' untuk semua id_line selain 1
//     } else if (item.label === 'Supplies' || item.label === 'Standard Operating Procedure') {
//       // Tampilkan 'Supplies' dan 'Standard Operating Procedure' hanya jika id_line-nya sesuai
//       return item.id_line === userLineId;
//     }
//     return false;
//   });
// }

// const accessibleMenuItems = customizeMenuAccess(menuItems);
// console.log(accessibleMenuItems);