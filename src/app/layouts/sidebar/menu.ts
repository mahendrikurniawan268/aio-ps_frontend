import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    //profil
    {
      id: 1,
      label: 'Menu',
      category: 'profile',
      subCategory: 'profile',
      isTitle: true
    },
    {
      id: 2,
      label: 'Top Menu',
      icon: 'home',
      category: 'profile',
      subCategory: 'profile',
      link: '/menu/top-menu'
    },
  
  //Admin
    {
      id: 1,
      label: 'Menu',
      category: 'admin',
      subCategory: 'admin',
      isTitle: true
    },
    {
      id: 2,
      label: 'Top Menu',
      icon: 'home',
      category: 'admin',
      subCategory: 'admin',
      link: '/menu/top-menu'
    },
    {
      id: 3,
      label: 'Master',
      icon: 'home',
      category: 'admin',
      subCategory: 'admin',
      role_id: 1 || 2,
      subItems: [
        {
          id: 19,
          label: 'User',
          link: 'auth/add-user',
          parentId: 16,
        },
        {
          id: 19,
          label: 'Quote',
          link: 'quote',
          parentId: 16,
        },
        {
          id: 19,
          label: 'Landing Page Image',
          link: 'image/landing',
          parentId: 16,
        },
      ]
    },
    {
      id: 4,
      label: 'Admin',
      isTitle: true,
      category: 'admin',
      subCategory: 'admin',
    },
    {
      id: 3,
      label: 'Dashboard',
      icon: 'home',
      category: 'admin',
      subCategory: 'admin',
      link: 'menu/dashboard-admin',
      role_id: 1 || 2
    },
    {
      id: 3,
      label: 'Apps Gateway',
      icon: 'home',
      category: 'admin',
      subCategory: 'admin',
      link: 'menu/apps-gateway/admin',
    },
    {
      id: 3,
      label: 'Acces Line',
      icon: 'home',
      category: 'admin',
      subCategory: 'admin',
      link: 'menu/acces-line',
      role_id: 2
    },
    {
      id: 5,
      label: 'Approval',
      category: 'admin',
      subCategory: 'admin',
      subItems: [
        {
          id: 19,
          label: 'Upload',
          link: 'menu/upload-approval',
          parentId: 16,
          role_id: !1,
        },
        {
          id: 19,
          label: 'List Approval',
          link: 'menu/list-approval',
          parentId: 16,
          role_id: 1 || 2,
        },
        {
          id: 19,
          label: 'Signed',
          link: 'menu/signed-approval',
          parentId: 16,
          role_id: !1,
        },
      ]
    },
    {
      id: 5,
      label: 'Task SPV',
      link: 'menu/task-head-to-spv',
      category: 'admin',
      subCategory: 'admin',
    },
    {
      id: 6,
      label: 'Harmony Schedule System',
      category: 'admin',
      subCategory: 'admin',
      subItems: [
        {
          id: 19,
          label: 'PPIC',
          link: 'menu/hss/ppic',
          parentId: 16,
        },
        {
          id: 19,
          label: 'PRO',
          link: 'menu/hss/pro',
          parentId: 16,
        },
        {
          id: 19,
          label: 'Schedule',
          link: 'menu/hss/schedule',
          parentId: 16,
        },
      ]
    },

  //CAN Mainetenance
  {
    id: 1,
    label: 'Menu',
    category: 'can',
    subCategory: 'maintenance',
    isTitle: true
  },
  {
    id: 2,
    label: 'Top Menu',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    link: '/menu/top-menu'
  },
  {
    id: 3,
    label: 'Second Menu',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    link: '/menu/second-menu'
  },
  {
    id: 3,
    label: 'Master',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'User',
        link: 'auth/add-user',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Function Location',
        link: 'function-location',
        queryParams: {lineId : 1},
        parentId: 16,
      },
      {
        id: 19,
        label: 'Machine',
        link: 'machine',
        queryParams: {lineId : 1},
        parentId: 16,
      },
      {
        id: 19,
        label: 'Quote',
        link: 'quote',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Landing Page Image',
        link: 'image/landing',
        parentId: 16,
      },
    ]
  },
  {
    id: 3,
    label: 'My Task',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    link: 'menu/spv-to-employee',
    role_id: 3 || 4
  },
  {
    id: 4,
    label: 'CAN Maintenance',
    isTitle: true,
    category: 'can',
    subCategory: 'maintenance',
  },
  {
    id: 3,
    label: 'Dashboard',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    link: '/dashboard/can',
    queryParams: {lineId: 1},
  },
  {
    id: 5,
    label: 'Apps Gateway',
    link: '/gateway',
    category: 'can',
    subCategory: 'maintenance',
  },
  {
    id: 5,
    label: 'Calendar Activity',
    link: '/schedule/master-plan',
    category: 'can',
    subCategory: 'maintenance',
  },
  {
    id: 5,
    label: 'Budgeting',
    category: 'can',
    subCategory: 'maintenance',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'Total',
        link: 'budgeting/total/maintenance',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Repair',
        link: 'budgeting/repair/maintenance',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Maintenance',
        link: 'budgeting/maintenance/maintenance',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Over Haul',
        link: 'budgeting/overhaul/maintenance',
        parentId: 16,
      },
    ]
  },
  {
    id: 6,
    label: 'Master Plan',
    category: 'can',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 7,
        label: 'Preparation',
        parentId: 6,
        subItems: [
          {
            id: 8,
            label: 'Mechanical',
            parentId: 7,
            subItems: [
              {
                id: 9,
                label: 'Preventive',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 1, subCategoryId: 1},
              },
              {
                id: 10,
                label: 'Corective',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 1, subCategoryId: 2},
              },
              {
                id: 11,
                label: 'Over haul',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 1, subCategoryId: 3},
              }
            ]
          },
          {
            id: 12,
            label: 'Electrical',
            parentId: 7,
            subItems: [
              {
                id: 13,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 2, subCategoryId: 1},
                parentId: 12,
              },
              {
                id: 14,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 2, subCategoryId: 2},
                parentId: 12,
              },
              {
                id: 15,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 1, categoryId: 2, subCategoryId: 3},
                parentId: 12,
              }
            ]
          }
        ]
      },
      {
        id: 16,
        label: 'Filling',
        parentId: 6,
        subItems: [
          {
            id: 17,
            label: 'Mechanical',
            parentId: 16,
            subItems: [
              {
                id: 18,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 1, subCategoryId: 1},
                parentId: 16,
              },
              {
                id: 19,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 1, subCategoryId: 2},
                parentId: 16,
              },
              {
                id: 20,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 1, subCategoryId: 3},
                parentId: 16,
              }
            ]
          },
          {
            id: 21,
            label: 'Electrical',
            parentId: 16,
            subItems: [
              {
                id: 22,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 2, subCategoryId: 1},
                parentId: 21,
              },
              {
                id: 23,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 2, subCategoryId: 2},
                parentId: 21,
              },
              {
                id: 24,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 2, categoryId: 2, subCategoryId: 3},
                parentId: 21,
              }
            ]
          }
        ]
      },
      {
        id: 25,
        label: 'Packing',
        parentId: 6,
        subItems: [
          {
            id: 26,
            label: 'Mechanical',
            parentId: 21,
            subItems: [
              {
                id: 27,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 1, subCategoryId: 1},
                parentId: 26,
              },
              {
                id: 28,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 1, subCategoryId: 2},
                parentId: 26,
              },
              {
                id: 29,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 1, subCategoryId: 3},
                parentId: 26,
              }
            ]
          },
          {
            id: 30,
            label: 'Electrical',
            parentId: 21,
            subItems: [
              {
                id: 31,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 2, subCategoryId: 1},
                parentId: 30,
              },
              {
                id: 32,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 2, subCategoryId: 2},
                parentId: 30,
              },
              {
                id: 33,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 1, typeId: 3, categoryId: 2, subCategoryId: 3},
                parentId: 30,
              }
            ]
          }
        ]
      },
    ]
  },
  {
    id: 30,
    label: 'Continous Improvement',
    category: 'can',
    subCategory: 'maintenance',
    parentId:10,
    subItems: [
      {
        id: 31,
        label: 'Autonomous Maintenance',
        link: 'continous/auto-maintenance',
        parentId: 30,
        subItems: [
          {
            id: 33,
            label: 'Preparation',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 1, areaId: 1},
          },
          {
            id: 33,
            label: 'Filling',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 1, areaId: 2},
          },
          {
            id: 33,
            label: 'Packing',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 1, areaId: 3},
          }
        ]
      },
      {
        id: 32,
        label: 'Weekly Care',
        link: 'continous/weekly-care',
        parentId: 30,
        subItems: [
          {
            id: 33,
            label: 'Schedule',
            parentId: 30,
            link: 'continous/weekly-care/schedule',
            queryParams: {lineId: 1},
          },
          {
            id: 33,
            label: 'Preparation',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 1, areaId: 1},
          },
          {
            id: 33,
            label: 'Filling',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 1, areaId: 2},
          },
          {
            id: 33,
            label: 'Packing',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 1, areaId: 3},
          },
        ]
      },
      {
        id: 33,
        label: 'Improvement',
        link: 'continous/improvement',
        queryParams: {lineId: 1},
        parentId: 30,
      },
    ]
  },
  {
    id: 34,
    label: 'Schedule Maintenance',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 35,
        label: 'Mechanical',
        parentId: 34,
        subItems: [
          // {
          //   id: 36,
          //   label: 'Monthly',
          //   link: 'schedule/mecha/monthly',
          //   parentId: 35,
          // },
          {
            id: 37,
            label: 'Weekly',
            link: 'schedule/mecha/weekly',
            parentId: 35,
          },
          {
            id: 38,
            label: 'Daily',
            link: 'schedule/mecha/daily',
            parentId: 35,
          }
        ]
      },
      {

        id: 39,
        label: 'Electrical',
        parentId: 34,
        subItems: [
          {
            id: 41,
            label: 'Weekly',
            link: 'schedule/elec/weekly',
            parentId: 39,
          },
          {
            id: 42,
            label: 'Daily',
            link: 'schedule/elec/daily',
            parentId: 39,
          }
        ]
      },
    ]
  },

  {
    id: 34,
    label: 'Report',
    icon: 'home',
    category: 'can',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 35,
        label: 'Activity Master Plan',
        link: 'report/list-report',
        queryParams: {lineId: 1},
        parentId: 34,
      },
      {

        id: 39,
        label: 'Activity Maintenance',
        link: 'list-report/schedule-maintenance',
        queryParams: {lineId: 1},
        parentId: 34,
      },
    ]
  },

  //PET Maintenance
  {
    id: 1,
    label: 'Menu',
    category: 'pet',
    subCategory: 'maintenance',
    isTitle: true
  },
  {
    id: 2,
    label: 'Top Menu',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    link: '/menu/top-menu'
  },
  {
    id: 3,
    label: 'Second Menu',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    link: '/menu/second-menu'
  },
  {
    id: 3,
    label: 'Master',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'User',
        link: 'auth/add-user',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Function Location',
        link: 'function-location',
        queryParams: {lineId : 2},
        parentId: 16,
      },
      {
        id: 19,
        label: 'Machine',
        link: 'machine',
        queryParams: {lineId : 2},
        parentId: 16,
      },
      {
        id: 19,
        label: 'Quote',
        link: 'quote',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Landing Page Image',
        link: 'image/landing',
        parentId: 16,
      },
    ]
  },
  {
    id: 3,
    label: 'My Task',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    link: 'menu/spv-to-employee',
    role_id: 3 || 4
  },
  {
    id: 4,
    label: 'PET Maintenance',
    isTitle: true,
    category: 'pet',
    subCategory: 'maintenance',
  },
  {
    id: 3,
    label: 'Dashboard',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    link: 'maintenance/pet/dashboard',
    queryParams: {lineId: 2},
  },
  {
    id: 5,
    label: 'Apps Gateway',
    link: '/gateway',
    category: 'pet',
    subCategory: 'maintenance',
  },
  {
    id: 5,
    label: 'Calendar Activity',
    link: 'maintenance/pet/calendar-activity',
    category: 'pet',
    subCategory: 'maintenance',
  },
  {
    id: 5,
    label: 'Budgeting',
    category: 'pet',
    subCategory: 'maintenance',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'Total',
        link: 'maintenance/pet/budgeting-pet',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Repair',
        link: 'maintenance/pet/repair-pet',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Maintenance',
        link: 'maintenance/pet/maintenance-pet',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Over Haul',
        link: 'maintenance/pet/overhaul-pet',
        parentId: 16,
      },
    ]
  },
  {
    id: 6,
    label: 'Master Plan',
    category: 'pet',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 7,
        label: 'Preparation',
        parentId: 6,
        subItems: [
          {
            id: 8,
            label: 'Mechanical',
            parentId: 7,
            subItems: [
              {
                id: 9,
                label: 'Preventive',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 1},
              },
              {
                id: 10,
                label: 'Corective',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 2},
              },
              {
                id: 11,
                label: 'Over haul',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 1, subCategoryId: 3},
              }
            ]
          },
          {
            id: 12,
            label: 'Electrical',
            parentId: 7,
            subItems: [
              {
                id: 13,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 1},
                parentId: 12,
              },
              {
                id: 14,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 2},
                parentId: 12,
              },
              {
                id: 15,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 4, categoryId: 2, subCategoryId: 3},
                parentId: 12,
              }
            ]
          }
        ]
      },
      {
        id: 7,
        label: 'Injection',
        parentId: 6,
        subItems: [
          {
            id: 8,
            label: 'Mechanical',
            parentId: 7,
            subItems: [
              {
                id: 9,
                label: 'Preventive',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 1},
              },
              {
                id: 10,
                label: 'Corective',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 2},
              },
              {
                id: 11,
                label: 'Over haul',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 1, subCategoryId: 3},
              }
            ]
          },
          {
            id: 12,
            label: 'Electrical',
            parentId: 7,
            subItems: [
              {
                id: 13,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 1},
                parentId: 12,
              },
              {
                id: 14,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 2},
                parentId: 12,
              },
              {
                id: 15,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 5, categoryId: 2, subCategoryId: 3},
                parentId: 12,
              }
            ]
          }
        ]
      },
      {
        id: 7,
        label: 'Blow',
        parentId: 6,
        subItems: [
          {
            id: 8,
            label: 'Mechanical',
            parentId: 7,
            subItems: [
              {
                id: 9,
                label: 'Preventive',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 1},
              },
              {
                id: 10,
                label: 'Corective',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 2},
              },
              {
                id: 11,
                label: 'Over haul',
                parentId: 8,
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 1, subCategoryId: 3},
              }
            ]
          },
          {
            id: 12,
            label: 'Electrical',
            parentId: 7,
            subItems: [
              {
                id: 13,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 1},
                parentId: 12,
              },
              {
                id: 14,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 2},
                parentId: 12,
              },
              {
                id: 15,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 6, categoryId: 2, subCategoryId: 3},
                parentId: 12,
              }
            ]
          }
        ]
      },
      {
        id: 16,
        label: 'Filling',
        parentId: 6,
        subItems: [
          {
            id: 17,
            label: 'Mechanical',
            parentId: 16,
            subItems: [
              {
                id: 18,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 1},
                parentId: 16,
              },
              {
                id: 19,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 2},
                parentId: 16,
              },
              {
                id: 20,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 1, subCategoryId: 3},
                parentId: 16,
              }
            ]
          },
          {
            id: 21,
            label: 'Electrical',
            parentId: 16,
            subItems: [
              {
                id: 22,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 1},
                parentId: 21,
              },
              {
                id: 23,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 2},
                parentId: 21,
              },
              {
                id: 24,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 7, categoryId: 2, subCategoryId: 3},
                parentId: 21,
              }
            ]
          }
        ]
      },
      {
        id: 25,
        label: 'Packing',
        parentId: 6,
        subItems: [
          {
            id: 26,
            label: 'Mechanical',
            parentId: 21,
            subItems: [
              {
                id: 27,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 1},
                parentId: 26,
              },
              {
                id: 28,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 2},
                parentId: 26,
              },
              {
                id: 29,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 1, subCategoryId: 3},
                parentId: 26,
              }
            ]
          },
          {
            id: 30,
            label: 'Electrical',
            parentId: 21,
            subItems: [
              {
                id: 31,
                label: 'Preventive',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 1},
                parentId: 30,
              },
              {
                id: 32,
                label: 'Corective',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 2},
                parentId: 30,
              },
              {
                id: 33,
                label: 'Over haul',
                link: 'maintenance/master-plan/view',
                queryParams: {lineId: 2, typeId: 8, categoryId: 2, subCategoryId: 3},
                parentId: 30,
              }
            ]
          }
        ]
      },
    ]
  },
  {
    id: 30,
    label: 'Continous Improvement',
    category: 'pet',
    subCategory: 'maintenance',
    parentId:10,
    subItems: [
      {
        id: 31,
        label: 'Autonomous Maintenance',
        parentId: 30,
        subItems: [
          {
            id: 33,
            label: 'Preparation',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 2, areaId: 4},
          },
          {
            id: 33,
            label: 'Injection',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 2, areaId: 5},
          },
          {
            id: 33,
            label: 'Blow',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 2, areaId: 6},
          },
          {
            id: 33,
            label: 'Filling',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 2, areaId: 7},
          },
          {
            id: 33,
            label: 'Packing',
            parentId: 30,
            link: 'maintenance/autonom-maintenance',
            queryParams: {lineId: 2, areaId: 8},
          }
        ]
      },
      {
        id: 32,
        label: 'Weekly Care',
        link: '',
        parentId: 30,
        subItems: [
          {
            id: 33,
            label: 'Schedule',
            parentId: 30,
            link: 'continous/weekly-care/schedule',
            queryParams: {lineId: 2},
          },
          {
            id: 33,
            label: 'Preparation',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 2, areaId: 4},
          },
          {
            id: 33,
            label: 'Injection',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 2, areaId: 5},
          },
          {
            id: 33,
            label: 'Blow',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 2, areaId: 6},
          },
          {
            id: 33,
            label: 'Filling',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 2, areaId: 7},
          },
          {
            id: 33,
            label: 'Packing',
            parentId: 30,
            link: 'maintenance/weekly-care',
            queryParams: {lineId: 2, areaId: 8},
          },
        ]
      },
      {
        id: 33,
        label: 'Improvement',
        link: 'continous/improvement',
        queryParams: {lineId: 2},
        parentId: 30,
      },
    ]
  },
  {
    id: 34,
    label: 'Schedule Maintenance',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 35,
        label: 'Mechanical',
        parentId: 34,
        subItems: [
          // {
          //   id: 36,
          //   label: 'Monthly',
          //   link: 'schedule/mecha/monthly',
          //   parentId: 35,
          // },
          {
            id: 37,
            label: 'Weekly',
            link: 'maintenance/pet/schedule/mecha/pet/weekly',
            parentId: 35,
          },
          {
            id: 38,
            label: 'Daily',
            link: 'maintenance/pet/schedule/mecha/pet/daily',
            parentId: 35,
          }
        ]
      },
      {

        id: 39,
        label: 'Electrical',
        parentId: 34,
        subItems: [
          {
            id: 41,
            label: 'Weekly',
            link: 'maintenance/pet/schedule/elec/pet/weekly',
            parentId: 39,
          },
          {
            id: 42,
            label: 'Daily',
            link: 'maintenance/pet/schedule/elec/pet/daily',
            parentId: 39,
          }
        ]
      },
    ]
  },
  {
    id: 34,
    label: 'Report',
    icon: 'home',
    category: 'pet',
    subCategory: 'maintenance',
    subItems: [
      {
        id: 35,
        label: 'Activity Master Plan',
        link: 'report/list-report',
        queryParams: {lineId: 2},
        parentId: 34,
      },
      {

        id: 39,
        label: 'Activity Maintenance',
        link: 'list-report/schedule-maintenance',
        queryParams: {lineId: 2},
        parentId: 34,
      },
    ]
  },

  // Can production
  {
    id: 2,
    label: 'MENUITEMS.MENU.TEXT',
    isTitle: true,
    category : 'can',
    subCategory : 'production'
  },
  {
    id: 3,
    label: 'Top Menu',
    icon: 'ri-refresh-line',
    link: '/menu/top-menu',
    category : 'can',
    subCategory : 'production'
  },
  {
    id: 3,
    label: 'Second Menu',
    icon: 'home',
    category : 'can',
    subCategory : 'production',
    link: '/menu/second-menu'
  },
  {
    id: 3,
    label: 'Master',
    icon: 'home',
    category: 'can',
    subCategory: 'production',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'User',
        link: 'auth/add-user',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Quote',
        link: 'quote',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Landing Page Image',
        link: 'image/landing',
        parentId: 16,
      },
    ]
  },
  {
    id: 4,
    label: 'CAN Production',
    isTitle: true,
    category: 'can',
    subCategory: 'production'
  },
  {
    id: 3,
    label: 'Dashboard',
    icon: 'ri-home-8-line',
    link: '/dashboard-prod',
    queryParams: {lineId: 1},
    category: 'can',
    subCategory: 'production',
  },
  {
    id: 3,
    label: 'Apps Gateway',
    icon: 'ri-apps-2-line',
    link: 'production/myapps',
    category: 'can',
    subCategory: 'production',
  },
  {
    id: 82,
    label: 'Abnormalitas',
    icon: ' ri-file-excel-line',
    category: 'can',
    subCategory: 'production',
    subItems: [
      {
        id: 56,
        label: 'List Abnormalitas',
        parentId: 49,
        link: 'production/abnormal',
        queryParams: {lineId: 1},
      },
      {
        id: 56,
        label: 'Hasil Investigasi',
        parentId: 49,
        link: 'production/abnormal/hasil-investigasi',
        queryParams: {lineId: 1},
      },
    ]
  },
  {
    id: 55,
    label: 'Schedule',
    icon: ' ri-calendar-event-line',
    category: 'can',
    subCategory: 'production',
    link : 'production/schedule/calendar',
    queryParams: {lineId: 1},
  },
  {
    id: 55,
    label: 'Report',
    icon: ' ri-mail-send-line',
    category: 'can',
    subCategory: 'production',
    subItems: [
      {
        id: 56,
        label: 'Daily',
        parentId: 49,
        subItems: [
          {
            id: 56,
            label: 'Proses',
            parentId: 49,
            link : 'production/report/daily/history',
            queryParams: {lineId: 1},
          },
          {
            id: 56,
            label: 'Activity',
            parentId: 49,
            link : 'production/report/daily/activity',
            queryParams: {lineId: 1},
          },
        ]
      },
      {
        id: 59,
        label: 'Weekly',
        parentId: 49,
        subItems: [
          {
            id: 56,
            label: 'Activity',
            parentId: 49,
            link : 'production/report/weekly/activity',
            queryParams: {lineId: 1},
          },
          {
            id: 56,
            label: 'Rakor MOM',
            parentId: 49,
            link : 'production/report/weekly/rakor/resume',
            queryParams: {lineId: 1},
          },
          {
            id: 56,
            label: 'Mapping',
            parentId: 49,
            link : 'production/report/weekly/mapping',
            queryParams: {lineId: 1},
          },
        ]
      },
      {
        id: 59,
        label: 'Monthly',
        parentId: 49,
        subItems : [
          {
            id: 59,
            label: 'List Report',
            parentId: 49,
            link: 'production/report/monthly/resume',
            queryParams: {lineId: 1},
          },
          {
            id: 59,
            label: 'GMP',
            parentId: 49,
            link: 'production/report/monthly/gmp',
            queryParams: {lineId: 1},
          },
        ]
      },
    ]
  },

  {
    id: 55,
    label: 'Kartu Stock',
    icon: ' ri-calendar-event-line',
    category: 'can',
    subCategory: 'production',
    link : 'production/kartustock/master',
    queryParams: {lineId: 1},
  },

  // Pet production
  {
    id: 2,
    label: 'MENUITEMS.MENU.TEXT',
    isTitle: true,
    category : 'pet',
    subCategory : 'production'
  },
  {
    id: 3,
    label: 'Top Menu',
    icon: 'ri-refresh-line',
    link: '/menu/top-menu',
    category : 'pet',
    subCategory : 'production'
  },
  {
    id: 3,
    label: 'Second Menu',
    icon: 'home',
    category : 'pet',
    subCategory : 'production',
    link: '/menu/second-menu'
  },
  {
    id: 3,
    label: 'Master',
    icon: 'home',
    category: 'pet',
    subCategory: 'production',
    role_id: 1 || 2,
    subItems: [
      {
        id: 19,
        label: 'User',
        link: 'auth/add-user',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Quote',
        link: 'quote',
        parentId: 16,
      },
      {
        id: 19,
        label: 'Landing Page Image',
        link: 'image/landing',
        parentId: 16,
      },
    ]
  },
  {
    id: 4,
    label: 'PET Production',
    isTitle: true,
    category: 'pet',
    subCategory: 'production'
  },
  {
    id: 3,
    label: 'Dashboard',
    icon: 'ri-home-8-line',
    link: '/dashboard-prod',
    queryParams: {lineId: 2},
    category: 'pet',
    subCategory: 'production',
  },
  {
    id: 3,
    label: 'Apps Gateway',
    icon: 'ri-apps-2-line',
    link: 'production/myapps',
    category: 'pet',
    subCategory: 'production',
  },
  {
    id: 82,
    label: 'Abnormalitas',
    icon: ' ri-file-excel-line',
    category: 'pet',
    subCategory: 'production',
    subItems: [
      {
        id: 56,
        label: 'List Abnormalitas',
        parentId: 49,
        link: 'production/abnormal',
        queryParams: {lineId: 2},
      },
      {
        id: 56,
        label: 'Hasil Investigasi',
        parentId: 49,
        link: 'production/abnormal/hasil-investigasi',
        queryParams: {lineId: 2},
      },
    ]
  },
  {
    id: 55,
    label: 'Schedule',
    icon: ' ri-calendar-event-line',
    category: 'pet',
    subCategory: 'production',
    link : 'production/schedule/calendar',
    queryParams: {lineId: 2},
  },
  {
    id: 55,
    label: 'Report',
    icon: ' ri-mail-send-line',
    category: 'pet',
    subCategory: 'production',

    subItems: [
      {
        id: 56,
        label: 'Daily',
        parentId: 49,
        subItems: [
          {
            id: 56,
            label: 'Proses',
            parentId: 49,
            link : 'production/report/daily/history',
            queryParams: {lineId: 2},
          },
          {
            id: 56,
            label: 'Activity',
            parentId: 49,
            link : 'production/report/daily/activity',
            queryParams: {lineId: 2},
          },
        ]
      },

      {
        id: 59,
        label: 'Weekly',
        parentId: 49,
        subItems: [
          {
            id: 56,
            label: 'Activity',
            parentId: 49,
            link : 'production/report/weekly/activity',
            queryParams: {lineId: 2},
          },
          {
            id: 56,
            label: 'Rakor MOM',
            parentId: 49,
            link : 'production/report/weekly/rakor/resume',
            queryParams: {lineId: 2},
          },
          {
            id: 56,
            label: 'Mapping',
            parentId: 49,
            link : 'production/report/weekly/mapping',
            queryParams: {lineId: 2},
          },
        ]
      },

      {
        id: 59,
        label: 'Monthly',
        parentId: 49,
        subItems : [
          {
            id: 59,
            label: 'List Report',
            parentId: 49,
            link: 'production/report/monthly/resume',
            queryParams: {lineId: 2},
          },
          {
            id: 59,
            label: 'GMP',
            parentId: 49,
            link: 'production/report/monthly/gmp',
            queryParams: {lineId: 2},
          },
        ]
      },

    ]
  },

  {
    id: 55,
    label: 'Kartu Stock',
    icon: ' ri-calendar-event-line',
    category: 'pet',
    subCategory: 'production',
    link : 'production/kartustock/master',
    queryParams: {lineId: 2},
  },
];
